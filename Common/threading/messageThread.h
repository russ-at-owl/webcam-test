#ifndef messageThread_H_
#define messageThread_H_

#include "thread.h"
#include <deque>
#ifdef WIN32
#include <crtdbg.h>
#else
#include <assert.h>
#endif

/////////////////////////////////////////////////////////////////
//* templates that allow you to build a message driven thread
//*
//*
//* define a class that is your handler. It must have a type
//* message_t which is the message type, and a method
//* bool operator()(const message_t &m) which is the message handler.
//* this method should return false when all processing is done and
//* the thread should shutdown
//*
//* example
//*
//*	class mh
//*	{
//*		string m_string;
//*	
//*	public:
//*		struct message
//*			{
//*			int m_v;
//*			message(int v):m_v(v){}
//*			};
//*		typedef message message_t;
//*		
//*	protected:
//*		
//*		bool operator()(const message_t &mm)
//*		{
//*			if (!mm.m_v) return false; // if value is zero, then we are done
//*			::MessageBox(0,tos(mm.m_v).c_str(),m_string.c_str(),MB_OK);
//*			return true;
//*			}
//*		
//*		mh(const string &s):m_string(s){}
//*	};
//*		
//*	main()
//*	{
//*		messageThread<mh> m("this is a test");
//*		m.start();
//*		m.postMessage(m::message(1));
//*		m.postMessage(m::message(2));
//*		m.postMessage(m::message(3));
//*		m.postMessage(m::message(4));
//*		m.postMessage(m::message(0));
//*		m.join();
//*	}
//*
//*

#ifdef WIN32
template <class T>
class messageQueue: private std::deque<T>
{
    nocopy(messageQueue)

private:
    mutex	m_mutex;
    event	m_event;
public:
    typedef T message_t;

    messageQueue():m_event(FALSE) {}

    size_t currentSize()
    {
        stackLock sl(m_mutex);
        return size();
    }
    bool	isEmpty()
    {
        stackLock sl(m_mutex);
        return empty();
    }

    T get()
    {
        ::WaitForSingleObject(m_mutex,INFINITE);
        while (empty())
        {
            ::SignalObjectAndWait(m_mutex,m_event,INFINITE,FALSE);
            ::WaitForSingleObject(m_mutex,INFINITE);
        }
        T t = front();
        pop_front();

        ::ReleaseMutex(m_mutex);

        return t;
    }

    std::deque<T> getAll()
    {
        ::WaitForSingleObject(m_mutex,INFINITE);
        std::deque<T> result = (*this);
        clear();
        ::ReleaseMutex(m_mutex);
        return result;
    }

    void put(const T &t)
    {
        stackLock sl(m_mutex);
        push_back(t);
        ::SetEvent(m_event);
    }
};

#else

template <class T>
class messageQueue: private std::deque<T>
{
    nocopy(messageQueue)

private:
    AOL_namespace::mutex m_mutex;
    conditionVariable	m_conditionVariable;
public:
    typedef T message_t;

    messageQueue() {}

    size_t currentSize()
    {
        stackLock sl(m_mutex);
        return std::deque<T>::size();
    }
    bool	isEmpty()
    {
        stackLock sl(m_mutex);
        return std::deque<T>::empty();
    }

    T get()
    {
        stackLock sl(m_mutex);

        while (std::deque<T>::empty())
            m_conditionVariable.wait(m_mutex);

        T t = std::deque<T>::front();
        std::deque<T>::pop_front();

        return t;
    }

    std::deque<T> getAll()
    {
        stackLock sl(m_mutex);

        std::deque<T> result = (*this);
        std::deque<T>::clear();
        return result;
    }

    void put(const T &t)
    {
        stackLock sl(m_mutex);
        std::deque<T>::push_back(t);
        m_conditionVariable.signal();
    }
};

#endif



template<class Handler,class Queue>
class messageHandler: public Handler
{
private:
    Queue	m_queue;

protected:
    unsigned operator()() throw()
    {
        unsigned result = -1;
        try {
            while (true)
            {
                typename Queue::message_t m = m_queue.get();
                if (!Handler::operator()(m))
                    break;
            }
            result = 0;
        }
        catch(...) {}

        return result;
    }
public:
    inline void postMessage(const typename Queue::message_t &m)
    {
        m_queue.put(m);
    }

    template <class P1> messageHandler(const P1 &p1):Handler(p1) {}
    template <class P1,class P2> messageHandler(const P1 &p1,const P2 &p2):Handler(p1,p2) {}
    template <class P1,class P2,class P3> messageHandler(const P1 &p1,const P2 &p2,const P3 &p3):Handler(p1,p2,p3) {}
    template <class P1,class P2,class P3,class P4> messageHandler(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4):Handler(p1,p2,p3,p4) {}
    template <class P1,class P2,class P3,class P4,class P5> messageHandler(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5):Handler(p1,p2,p3,p4,p5) {}
    template <class P1,class P2,class P3,class P4,class P5,class P6> messageHandler(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,const P6 &p6):Handler(p1,p2,p3,p4,p5,p6) {}
    template <class P1,class P2,class P3,class P4,class P5,class P6,class P7> messageHandler(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,const P6 &p6,const P7 &p7):Handler(p1,p2,p3,p4,p5,p6,p7) {}
    template <class P1> messageHandler(P1 &p1):Handler(p1) {}
};

template<class Handler,class Message = typename Handler::message_t>
class messageThread: public Tthread<messageHandler<Handler,messageQueue<Message> > >
{
public:
    template <class P1> messageThread(const P1 &p1):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1) {}
    template <class P1,class P2> messageThread(const P1 &p1,const P2 &p2):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1,p2) {}
    template <class P1,class P2,class P3> messageThread(const P1 &p1,const P2 &p2,const P3 &p3):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1,p2,p3) {}
    template <class P1,class P2,class P3,class P4> messageThread(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1,p2,p3,p4) {}
    template <class P1,class P2,class P3,class P4,class P5> messageThread(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1,p2,p3,p4,p5) {}
    template <class P1,class P2,class P3,class P4,class P5,class P6> messageThread(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,const P6 &p6):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1,p2,p3,p4,p5,p6) {}
    template <class P1,class P2,class P3,class P4,class P5,class P6,class P7> messageThread(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,const P6 &p6,const P7 &p7):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1,p2,p3,p4,p5,p6,p7) {}
    template <class P1> messageThread(P1 &p1):Tthread<messageHandler<Handler,messageQueue<Message> > >(p1) {}
};


//////////////// Finite state machine
template <class Handler>
class fsm: public Handler {


protected:
    typedef bool (Handler::*state)(const typename Handler::message_t &m);// throw(); // should not throw an exception

    bool operator()(const typename Handler::message_t &m) throw() {
        _ASSERTE(m_State);
        return (this->*m_State)(m);
    }

    // this is broken in VC 7, so trans must be made public
    //friend class Handler;
public:
    void trans(state target) throw()
    {
        if (target == m_State) return;
        _ASSERTE(m_State);
#ifndef NDEBUG
        state tmp = m_State;
#endif
        (this->*m_State)(Handler::message_t::exitStateMessage());
#ifndef NDEBUG
        // sanity check - don't change states in exit_sig
        _ASSERTE(tmp == m_State);
#endif
        m_State = target;
        (this->*m_State)(Handler::message_t::enterStateMessage());
        _ASSERTE(m_State == target); // don't change states in enter_sig
    }
    //#define TRAN(targ) trans(static_cast<state>(targ))
protected:

    template <class P1> fsm(const P1 &p1):m_State(Handler::initialState),Handler(p1) {}
    template <class P1,class P2> fsm(const P1 &p1,const P2 &p2):m_State(Handler::initialState),Handler(p1,p2) {}
    template <class P1,class P2,class P3> fsm(const P1 &p1,const P2 &p2,const P3 &p3):m_State(Handler::initialState),Handler(p1,p2,p3) {}
    template <class P1,class P2,class P3,class P4> fsm(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4):m_State(Handler::initialState),Handler(p1,p2,p3,p4) {}
    template <class P1,class P2,class P3,class P4,class P5> fsm(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5):m_State(Handler::initialState),Handler(p1,p2,p3,p4,p5) {}
    template <class P1,class P2,class P3,class P4,class P5,class P6> fsm(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,const P6 &p6):m_State(Handler::initialState),Handler(p1,p2,p3,p4,p5,p6) {}
    template <class P1,class P2,class P3,class P4,class P5,class P6,class P7> fsm(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,const P6 &p6,const P7 &p7):m_State(Handler::initialState),Handler(p1,p2,p3,p4,p5,p6,p7) {}
    template <class P1> fsm(P1 &p1):m_State(Handler::initialState),Handler(p1) {}

private:
    state	m_State;
};

#endif

