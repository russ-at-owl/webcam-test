

#include <XPThread.h>
#include <XPMutex.h>
#include <XPSemaphore.h>




#define THREAD_DELAY(x)			XpSleep(x)

#define THREAD_SLEEP_DUR		50

#define THREAD_KILL_MAX			100



typedef void (*VoidCbProc_def)(void *pData);


class CThreadBase;

class CThreadMgr;


class CThreadInstData
{
	friend class CThreadMgr;

	XpMutex				m_mTdLock;

	XpMutex				m_mCbLock;

	bool				m_bLoop;
	bool				m_bThreadActive;
	bool				m_bTerminate;
	bool				m_bCbActive;

	VoidCbProc_def		m_pCbProc;

	void				*m_pCbData;

	long				m_lLoopDelay;

	std::string			m_sName;

public:

	Semaphore			m_sDone;

	CThreadInstData() :
		m_bTerminate(false),
		m_bThreadActive(false),
		m_bLoop(false),
		m_lLoopDelay(1000),
		m_bCbActive(false),
		m_pCbProc(NULL),
		m_pCbData(NULL)
	{

	}

	inline VoidCbProc_def getCbProc()
	{
		m_mTdLock.Lock();

		VoidCbProc_def pProc = m_pCbProc;

		m_mTdLock.Unlock();

		return pProc;
	}

	inline void setCbProc(VoidCbProc_def pProc)
	{
		m_mTdLock.Lock();

		m_pCbProc = pProc;

		m_mTdLock.Unlock();
	}

	inline void * getCbData()
	{
		m_mTdLock.Lock();

		void * pData = m_pCbData;

		m_mTdLock.Unlock();

		return pData;
	}

	inline void setCbData(void * pData)
	{
		m_mTdLock.Lock();

		m_pCbData = pData;

		m_mTdLock.Unlock();
	}

	inline bool	isCbActive()
	{
		//m_mTdLock.Lock();

		bool bVal = m_bCbActive;

		//m_mTdLock.Unlock();

		return bVal;
	}

	inline void	setCbActive(bool bVal)
	{
		m_mTdLock.Lock();

		//if (m_bCbActive == true)
		//{
		//	m_mCbLock.Lock();
		//}
		//else
		//{
		//	m_mCbLock.Unlock();
		//}

		m_bCbActive = bVal;

		m_mTdLock.Unlock();
	}

	inline bool isLooping()
	{
		//m_mTdLock.Lock();

		bool bVal = m_bLoop;

		//m_mTdLock.Unlock();

		return bVal;
	}

	inline void setLooping(bool bVal)
	{
		m_mTdLock.Lock();

		m_bLoop = bVal;

		m_mTdLock.Unlock();
	}

	inline void setLoopDelay(long lVal)
	{
		m_mTdLock.Lock();

		m_lLoopDelay = lVal;

		m_mTdLock.Unlock();
	}

	inline bool	isThreadActive()
	{
		m_mTdLock.Lock();

		bool bVal = m_bThreadActive;

		m_mTdLock.Unlock();

		return bVal;
	}

	inline void	setThreadActive(bool bVal)
	{
		m_mTdLock.Lock();

		m_bThreadActive = bVal;

		m_mTdLock.Unlock();
	}

	inline bool	isTerminated()
	{
		//m_mTdLock.Lock();

		bool bVal = m_bTerminate;

		//m_mTdLock.Unlock();

		return bVal;
	}

	inline void	setTerminate(bool bVal)
	{
		m_mTdLock.Lock();

		m_bTerminate = bVal;

		m_mTdLock.Unlock();
	}

	inline void	setName(std::string sName)
	{
		m_sName = sName;
	}

	inline void lock()
	{
		m_mTdLock.Lock();
	}

	inline void unlock()
	{
		m_mTdLock.Unlock();
	}

	inline bool doCallback()
	{
		if (m_pCbProc == NULL)
		{
			return false;
		}

		m_mCbLock.Lock();

		m_pCbProc(m_pCbData);

		m_mCbLock.Unlock();

		return true;
	}

	inline void cleanupForExit()
	{
		m_mTdLock.Unlock();
		m_mCbLock.Unlock();

		m_bTerminate = false;
		m_bThreadActive = false;
		m_bLoop = false;
		m_bCbActive = false;

		m_sDone.Post();
	}

protected:

};

typedef CThreadInstData	* PThreadData_def;

class CThreadMgr : protected XpThreadDW
{
	friend class CThreadBase;

	friend class CThreadInstData;

	PThreadData_def		m_pTD;

public:
	CThreadMgr() :
		m_pTD(NULL)
	{
	};

	virtual void Init(PThreadData_def pTD)
	{
		m_pTD = pTD;

		XpThreadDW::SetInstData((DWORD) pTD);
	}

	virtual Handle GetHandle()
	{
		return Self();
	}

protected:

	virtual inline bool SleepForInterval(DWORD dwInt)
	{
		DWORD dwAccum = 0;

		//* if continuous looping was configured...

		while (dwAccum < dwInt)
		{
			if (m_pTD == NULL)
			{
				return false;
			}

			if (m_pTD->isTerminated() == true)
			{
				//* verify that thread termination was not requested

				return false;
			}

			//* "sleep" this thread until the callback interval is reached

			THREAD_DELAY(25);

			dwAccum += THREAD_SLEEP_DUR;
		}

		return true;
	}

	virtual void ThreadMainDW(DWORD dwData)
	{
		if (m_pTD == NULL)
		{
			return;
		}

		m_pTD->setThreadActive(true);

		bool bLooping = m_pTD->isLooping();

		while (true)
		{
			if (m_pTD == NULL)
			{
				return;
			}

			if (m_pTD->isTerminated() == true)
			{
				//* verify that thread termination was not requested

				m_pTD->cleanupForExit();

				return;
			}

			//* execute the callback proc

			if (m_pTD->doCallback() == false)
			{
				m_pTD->cleanupForExit();

				return;
			}

			if (bLooping == true)
			{
				if (SleepForInterval(m_pTD->m_lLoopDelay) == false)
				{
					m_pTD->cleanupForExit();

					return;
				}

				continue;
			}
		}

		//* exit the thread
			
		m_pTD->cleanupForExit();

		return;
	};
};


class CThreadBase
{
//public:

protected:

	//* CThreadBase instance data

	CThreadInstData	m_ThreadData;

	bool			m_bInitialized;

	CThreadMgr		m_Thread;

	//*
	//* Thead worker function
	//*
	//* this function should be over-ridden with 
	//* the one that does the actual work
	//*

public:

	CThreadBase() :
		m_bInitialized(false)
	{

	}

	CThreadBase(bool bLoop, long lDelay = 1000) :
		m_bInitialized(false)
	{
		m_ThreadData.setLooping(bLoop);
		m_ThreadData.setLoopDelay(lDelay);

		m_Thread.Init(&m_ThreadData);

		m_bInitialized = true;
	}

	//* Start the thread

	bool Start()
	{
		try
		{
			int nRet = 
				m_Thread.Create(0, true, 2048, true, false);
			if (nRet != 0)
			{
				return false;
			}

			return true;
		}
		catch(...)
		{

		}

		return false;
	}

	//* Stop a looping thread

	bool Stop()
	{
		try
		{
			if (m_ThreadData.isThreadActive() == true)
			{
				if (m_ThreadData.isLooping() == true)
				{
					m_ThreadData.setTerminate(true);
				}
				else
				{
					long lAccum = 0;

					while (m_ThreadData.isCbActive() == true)
					{
						if (lAccum > THREAD_KILL_MAX)
						{
							break;
						}

						THREAD_DELAY(10);

						lAccum++;
					}

					m_Thread.Kill();
				}

				return true;
			}
		}
		catch(...)
		{

		}

		return false;
	}

	//* Kill (abort) the thread

	bool Kill()
	{
		try
		{
			if (m_ThreadData.isThreadActive() == true)
			{
				long lAccum = 0;

				while (m_ThreadData.isCbActive() == true)
				{
					if (lAccum > THREAD_KILL_MAX)
					{
						break;
					}

					THREAD_DELAY(10);

					lAccum++;
				}

				m_Thread.Kill();

				return true;
			}
		}
		catch(...)
		{

		}

		return false;
	}

	//* Wait for the thread to complete processing (exit)

	void Wait()
	{
		m_ThreadData.m_sDone.Wait();
	}

	bool IsAactive()
	{
		return m_ThreadData.isThreadActive();
	}

	void SetCbInt(long lInt)
	{
		m_ThreadData.setLoopDelay(lInt);
	}

	void SetCbProc(VoidCbProc_def pProc)
	{
		m_ThreadData.setCbProc(pProc);
	}

	void SetCbProc(VoidCbProc_def pProc, void * pData)
	{
		m_ThreadData.setCbProc(pProc);
		m_ThreadData.setCbData(pData);
	}

	void SetCbData(void * pData)
	{
		m_ThreadData.setCbData(pData);
	}

	void SetLoopDelay(long lDelay)
	{
		m_ThreadData.setLoopDelay(lDelay);
	}
};