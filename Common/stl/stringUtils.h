//**************************************************************************************************
//* FILE:		string_utils.h
//*
//* DESCRIP:
//*
//*



#ifndef _stringUtils_H_
#define _stringUtils_H_


#include <sstream>
#include <string>
#include <functional>
#include <vector>
#include <locale>
#include <ctype.h>
//#include <stdlib.h>
//#include <stdio.h>
//#include <direct.h>
#include <stdarg.h>

//#include <stlx.h>

#ifdef WIN32
//#define string  basic_string
#ifndef _WINDOWS_
#include <windows.h>
#endif
#endif


#include "..\other\VaPass.h"



namespace stringUtil
{
#ifdef WIN32
inline std::string eol()
{
    return "\r\n";
}
#elif MACINTOSH
inline std::string eol()
{
    return "\r";
}
#else
inline std::string eol()
{
    return "\n";
}
#endif

inline std::string tab()
{
    return "\t";
}

inline bool removeTrailingChar(std::string &sPrm, const char chVal)
{
	bool bFound = false;

	int nLastChar = (sPrm.length() - 1);

	if (nLastChar > 0)
	{
		try
		{
			if (sPrm[nLastChar] == chVal)
			{
				sPrm.erase(nLastChar);

				bFound = true;
			}
		}
		catch (...)
		{
			bFound = false;
		}
	}

	return bFound;
}

inline int removeTrailingSpaces(std::string &sStr)
{
	int nFound = 0;

	try
	{
		int nLastChar = (sStr.length() - 1);

		while (nLastChar > 0)
		{
			if (sStr[nLastChar] != ' ')
			{
				break;
			}

			sStr.erase(nLastChar);

			nFound++;

			nLastChar--;
		}
	}
	catch (...)
	{
	}

	return nFound;
}


inline std::string replaceStrChars(const std::string &sIn, const std::string &sFind, const std::string &sReplace)
{
	std::string sOut = "";

	int nInLen = (sIn.length() - 1);
	int nFindLen = (sFind.length() - 1);

	int nIdxIn = 0;
	//int nIdxOut = 0;

	while (nIdxIn < nIdxIn)
	{
		try
		{
			if (sIn.substr(nIdxIn, nFindLen) == sFind)
			{
				sOut += sReplace;

				nIdxIn += nFindLen;
				//nIdxOut += sReplace.length();
			}
			else
			{
				sOut += sIn[nIdxIn];

				nIdxIn += 1;
				//nIdxOut += 1;
			}
		}
		catch (...)
		{

		}
	}

	return sOut;
}


//std::string get_current_path()
//{
//	char *pTmp = _getcwd(NULL, 0);
//
//	std::string sRet = "";
//
//	if (pTmp != NULL)
//	{
//		sRet = pTmp;
//	}
//
//	return sRet;
//}

inline bool isNumeric(const std::string &sIn)
{
	std::string::size_type nLen = sIn.length();

    for(std::string::size_type x = 0; x < nLen; ++x)
    {
        if (isdigit((int) (sIn[x])) == 0)
		{
			return false;
		}
    }

    return true;
}


//***************************************************************
//* String Conversion Routines
//***************************************************************

inline void str2wstr(std::wstring &wsTarget, const std::string &sSource)
{
	int nLen = (sSource.size() + 2);

    wchar_t* pTmp = (wchar_t *) calloc(nLen + 1, sizeof(wchar_t));

	if (pTmp == NULL)
	{
		return;
	}

	memset(pTmp, 0, (nLen * sizeof(wchar_t)));

	mbstowcs(pTmp, sSource.c_str(), nLen);

	wsTarget = pTmp;

	free(pTmp);
}

inline std::wstring str2wstr(const std::string &sStr)
{
	std::wstring wsTmp = L"";

	str2wstr(wsTmp, sStr);

	return wsTmp;
}

inline void wstr2str(std::string &sTarget, const std::wstring &wsSource)
{
	int nLen = (wsSource.size() + 2);

    char* pTmp = new char[nLen];

	memset(pTmp, 0, nLen);

	wcstombs(pTmp, wsSource.c_str(), nLen);

	sTarget = pTmp;
}

inline std::string wstr2str(const std::wstring &wsStr)
{
	std::string sTmp = "";

	wstr2str(sTmp, wsStr);

	return sTmp;
}

#define s2ws(x) str2wstr(x)
#define ws2s(x) wstr2str(x)

inline std::string toLower(const std::string &sIn)
{
    std::string sOut = "";

	std::string::size_type nLen = sIn.length();

    for(std::string::size_type x = 0; x < nLen; ++x)
    {
        sOut += ((isalpha((int) (sIn[x])) != 0) ? tolower(sIn[x]) : sIn[x]);
    }

    return sOut;
}

inline std::string toUpper(const std::string &sIn)
{
    std::string sOut = "";

	std::string::size_type nLen = sIn.length();

    for(std::string::size_type x = 0; x < nLen; ++x)
    {
        sOut += ((isalpha((int) (sIn[x])) != 0) ? toupper(sIn[x]) : sIn[x]);
    }

    return sOut;
}

//#define toString(x)	tos(x)

//***************************************************************
//* tos, tows
//*
//* These templates and overloaded functions allow you to convert
//* any streamable value to a string or wide-string. It can be a
//* great convenience to be able to do
//*
//* string("test five ") + tos(5);
//*
//* instead of
//*
//* ostringstream o;
//* o << "test five " << 5;
//* o.str()
//*
//* tows is just like tos, but for wide strings. You can also use these
//* to convert standard strings to wide strings and vice-versa
//*
//* for example:
//*
//* wstring ws = tows(string("hello"));
//* string s = tos(wstring(L"goodbye"));
//*
//*

template<typename t>
std::string tos(t v)
{
    std::ostringstream o;

    o << v;

    return o.str();
}

template<typename t>
std::string toStr(t v)
{
	std::ostringstream o;

	o << v;

	return o.str();
}

#ifdef WIN32
template<typename t>
std::wstring tows(t v)
{
    std::wostringstream o;

    o << v;

    return o.str();
}

template<typename t>
std::wstring toWStr(t v)
{
	std::wostringstream o;

	o << v;

	return o.str();
}

#ifndef TCHAR
inline std::wstring tows(const char *pValue) 
{
    if (pValue == NULL) 
	{
		return L"";
	}

    size_t nLen = ::strlen(pValue);

    wchar_t *pBuf = (wchar_t *) calloc((nLen + 2), (sizeof(wchar_t)));

	if (pBuf == NULL)
	{
		return L"";
	}

	memset(pBuf, 0, ((nLen + 1) * sizeof(wchar_t)));

    ::mbstowcs(pBuf, pValue, nLen);

    std::wstring sOut(pBuf);

    free(pBuf);

    return sOut;
}
#else
inline std::wstring tows(const TCHAR *pValue) 
{
    if (pValue == NULL) 
	{
		return L"";
	}

    size_t nLen = ::strlen(pValue);

    wchar_t *pBuf = (wchar_t *) calloc((nLen + 2), (sizeof(wchar_t)));

	if (pBuf == NULL)
	{
		return L"";
	}

	memset(pBuf, 0, ((nLen + 1) * sizeof(wchar_t)));

    ::mbstowcs(pBuf, pValue, nLen);

    std::wstring sOut(pBuf);

    free(pBuf);

    return sOut;
}
#endif

inline std::wstring tows(const std::string &sValue)
{
    return tows(sValue.c_str());
}
#endif


inline std::string tos(const int nValue)
{
    if (nValue == 0)
	{
        return "0";
	}

    std::string sOut = "";
	
#if 1
	sOut = tos<int>(nValue);
#else
	std::stringstream ss;

	ss << nValue;

	sOut = ss.str();
#endif

    return sOut;
}


inline std::string tos(const unsigned int nValue)
{
	if (nValue == 0)
	{
		return "0";
	}

	std::string sOut = "";

#if 1
	sOut = tos<unsigned int>(nValue);
#else
	std::stringstream ss;

	ss << nValue;

	sOut = ss.str();
#endif

	return sOut;
}


inline std::string tos(const long lValue)
{
	if (lValue == 0)
	{
		return "0";
	}

	std::string sOut = "";

#if 1
	sOut = tos<long>(lValue);
#else
	std::stringstream ss;

	ss << lValue;

	sOut = ss.str();
#endif

	return sOut;
}


#ifndef WIN32
inline std::string tos(const unsigned long lValue)
{
	if (lValue == 0)
	{
		return "0";
	}

	std::string sOut = "";

#if 1
	sOut = tos<unsigned long>(lValue);
#else
	std::stringstream ss;

	ss << lValue;

	sOut = ss.str();
#endif

	return sOut;
}
#endif


#ifdef WIN32
inline std::string tos(const DWORD dwValue)
{
	if (dwValue == 0)
	{
		return "0";
	}

	std::string sOut = "";

#if 1
	sOut = tos<unsigned int>(dwValue);
#else
	std::stringstream ss;

	ss << dwValue;
	
	sOut = ss.str();
#endif

	return sOut;
}
#endif


#ifdef WIN32
#ifndef TCHAR
inline std::string tos(const char *pValue)
{
    if (pValue == NULL)
	{
        return "";
	}

    std::string sOut = "";

#if 0
	sOut = tos<char *>(nValue);
#else	
	int nLen = strlen(pValue);

	if (nLen > 0)
	{
		sOut.append(pValue, nLen);
	}
#endif

    return sOut;
}
#else
inline std::string tos(const TCHAR *value)
{
    if (pValue == NULL)
	{
        return "";
	}

#if 0
	sOut = tos<TCHAR *>(nValue);
#else	
	int nLen = strlen(pValue);

	if (nLen > 0)
	{
		sOut.append(pValue, nLen);
	}
#endif

    return sOut;
}
#endif
#else
inline std::string tos(const char *pValue)
{
	if (pValue == NULL)
	{
		return "";
	}

#if 0
	sOut = tos<char *>(nValue);
#else	
	int nLen = strlen(pValue);

	if (nLen > 0)
	{
		sOut.append(pValue, nLen);
	}
#endif

	return sOut;
}
#endif


#ifdef WIN32
inline std::string tos(const wchar_t *pValue)
{
    if (pValue == NULL)
	{
        return "";
	}

    size_t nLen = ::wcslen(pValue);

    char *pBuf = (char *) calloc((nLen + 2), sizeof(char));

	if (pBuf == NULL)
	{
        return "";
	}

	memset(pBuf, 0, (nLen + 1));

    ::wcstombs(pBuf, pValue, nLen);

    std::string sOut(pBuf);

    free(pBuf);

    return sOut;
}


inline std::string tos(const std::wstring &sIn)
{
    return tos(sIn.c_str());
}

#endif


inline std::string tos(bool bVal, bool bCaps = false)
{
    std::string sOut = "";

	if (bVal == true)
	{
		if (bCaps == true)
		{
			sOut = "TRUE";
		}
		else
		{
			sOut = "true";
		}
	}
	else
	{
		if (bCaps == true)
		{
			sOut = "FALSE";
		}
		else
		{
			sOut = "false";
		}
	}

    return sOut;
}

#ifdef WIN32
inline std::string BOOL_tos(BOOL bVal, bool bCaps = false)
{
	std::string sOut = "";

	if (bVal == TRUE)
	{
		if (bCaps == true)
		{
			sOut = "TRUE";
		}
		else
		{
			sOut = "true";
		}
	}
	else
	{
		if (bCaps == true)
		{
			sOut = "FALSE";
		}
		else
		{
			sOut = "false";
		}
	}

	return sOut;
}
#endif

inline bool toBool(const std::string &sVal) //throw(std::runtime_error)
{
	//* if string is blank/empty, return false
	if (sVal == "")
	{
		return false;
	}

	std::string sCmpVal = toLower(sVal);

	//* check for assorted string values for "true"
	if 
		(
			(sCmpVal == "true") ||
			(sCmpVal == "yes") ||
			(sCmpVal == "y") ||
			(sVal == "1") 
		)
	{
		return true;
	}

	//* check for assorted string values for "false"
	if 
		(
			(sCmpVal == "false") ||
			(sCmpVal == "no") ||
			(sCmpVal == "n") ||
			(sVal == "0") 
		)
	{
		return false;
	}

	//* bad value... throw an error
	throw std::runtime_error("toBool - ERROR: Invalid input string value");
}

#ifdef WIN32
inline BOOL toBOOL(const std::string &sVal) //throw(std::runtime_error)
{
	//* if string is blank/empty, return false
	if (sVal == "")
	{
		return FALSE;
	}

	std::string sCmpVal = toLower(sVal);

	//* check for assorted string values for "true"
	if
		(
			(sCmpVal == "true") ||
			(sCmpVal == "yes") ||
			(sCmpVal == "y") ||
			(sVal == "1")
		)
	{
		return TRUE;
	}

	//* check for assorted string values for "false"
	if
		(
			(sCmpVal == "false") ||
			(sCmpVal == "no") ||
			(sCmpVal == "n") ||
			(sVal == "0")
		)
	{
		return FALSE;
	}

	//* bad value... throw an error
	throw std::runtime_error("toBOOL - ERROR: Invalid input string value");
}
#endif

#ifndef _TOHEXSTR_
#define _TOHEXSTR_

const char g_HexChars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

inline std::string toHexStr(const char chValue)
{
	try
	{
		std::string sOut = "";

		unsigned char ucVal = (unsigned char) chValue;

		sOut += g_HexChars[ ( ucVal & 0xF0 ) >> 4 ];
		sOut += g_HexChars[ ( ucVal & 0x0F ) >> 0 ];

		return sOut;
	}
	catch(...)
	{
		
	}

	return "";
}

inline std::string toHexStr(const unsigned char *pBuf, const int nLen)
{
	try
	{
		if (pBuf == NULL || nLen < 1)
		{
			return "";
		}

		std::string sOut = "";

		unsigned char ucVal;

		for (int x = 0; x < nLen; x++)
		{
			ucVal = (unsigned char) *(pBuf + x);

			sOut += g_HexChars[ ( ucVal & 0xF0 ) >> 4 ];
			sOut += g_HexChars[ ( ucVal & 0x0F ) ];
		}

		return sOut;
	}
	catch(...)
	{
		
	}

	return "";
}

inline bool toByteArray(unsigned char *pTarget, int nLen, const std::string sSrc)
{
	try
	{
		if (pTarget == NULL || nLen < 1)
		{
			//* invalid target param info
			return false;
		}

		int nSrcLen = sSrc.length();

		if ((nSrcLen % 2) > 0 || (nSrcLen / 2) > nLen)
		{
			//* sSrc contains an odd number of hex chars
			//* or target buffer is too small
			return false;
		}

		char cSrc;

		unsigned char cTmp, cOut;

		for (int x = 0; x < nSrcLen; x++)
		{
			//* convert 1st 4 bits (hex char) to binary, then left shift

			cSrc = sSrc[x]; 

			if (cSrc >= '0' && cSrc <= '9')
			{
				cTmp = (unsigned char) (((int) cSrc) - ((int) '0')) << 4; 
			}
			else if (cSrc >= 'A' && cSrc <= 'F')
			{
				cTmp = (unsigned char) (((int) cSrc) - ((int) 'A')) << 4; 
			}
			else if (cSrc >= 'a' && cSrc <= 'f')
			{
				cTmp = (unsigned char) (((int) cSrc) - ((int) 'a')) << 4; 
			}
			else
			{
				return false;
			}

			x++;

			//* convert 2nd 4 bits (hex char) to binary, then OR with 1st 4bit value

			cSrc = sSrc[x]; 

			if (cSrc >= '0' && cSrc <= '9')
			{
				cOut = (unsigned char) (((int) cSrc) - ((int) '0')) | cTmp; 
			}
			else if (cSrc >= 'A' && cSrc <= 'F')
			{
				cOut = (unsigned char) (((int) cSrc) - ((int) 'A')) | cTmp; 
			}
			else if (cSrc >= 'a' && cSrc <= 'f')
			{
				cOut = (unsigned char) (((int) cSrc) - ((int) 'a')) | cTmp; 
			}
			else
			{
				return false;
			}

			*(pTarget + (x / 2)) = cOut;
		}
	}
	catch(...)
	{
		return false;
	}

	return true;
}

#else

inline std::string toHexStr(const char chValue);
inline std::string toHexStr(const unsigned char *pBuf, const int nLen);

inline bool toByteArray(unsigned char *pBuf, int nLen, const std::string sSrc);

#endif // _TOHEXSTR_


//******************************************************************
//* String printf routines
//******************************************************************

//******************************************************************
//* A collection of templates and functions to allow performing of
//* "printf" type operations to a string
//*
//* --- strPrintf
//* --- wstrPrintf
//*
//* Perform "printf" formatting operation and return value to string
//*
//*

//template <typename F>
inline std::string strPrintf(int nLen, const char *szFmt, ...)
{
	std::string sRet = "";

	if (szFmt == NULL)
	{
		return sRet;
	}

	if (nLen > 1 || szFmt != NULL)
	{
		char *pTmp = (char *) calloc((nLen + 1), sizeof(char));

		if (pTmp == NULL)
		{
			return sRet;
		}

		memset(pTmp, 0, nLen);

		va_list ArgList;

		va_start(ArgList, szFmt);

		sprintf(pTmp, szFmt, va_pass(ArgList));

		va_end(ArgList);

		sRet = pTmp;
	}

    return sRet;
}

inline std::string strPrintf(const char *szFmt, ...)
{
	std::string sRet = "";

	if (szFmt == NULL)
	{
		return sRet;
	}

	int nLen = 0;

	while (*(szFmt + nLen) != 0)
	{
		nLen++;
	}

	if (nLen < 1)
	{
		return sRet;
	}

	nLen++;

	va_list ArgList;

	va_start(ArgList, szFmt);

	sRet = strPrintf(nLen, szFmt, va_pass(ArgList));

	va_end(ArgList);

	return sRet;
}


inline std::wstring wstrPrintf(int nLen, const char *szFmt, ...)
{
	std::wstring wsOut = L"";

	if (nLen > 1 || szFmt != NULL)
	{
		char *pTmp = (char *) calloc(nLen + 1, sizeof(char));

		if (pTmp == NULL)
		{
			return wsOut;
		}

		va_list ArgList;

		va_start(ArgList, szFmt);

		sprintf(pTmp, szFmt, va_pass(ArgList));

		va_end(ArgList);

		wsOut = tows(pTmp);

		free(pTmp);
	}

    return wsOut;
}

inline std::wstring wstrPrintf(const char *szFmt, ...)
{
	std::wstring wsRet = L"";

	if (szFmt == NULL)
	{
		return wsRet;
	}

	int nLen = 0;

	while (*(szFmt + nLen) != 0)
	{
		nLen++;
	}

	if (nLen < 1)
	{
		return wsRet;
	}

	nLen++;

	va_list ArgList;

	va_start(ArgList, szFmt);

	wsRet = wstrPrintf(nLen, szFmt, va_pass(ArgList));

	va_end(ArgList);

	return wsRet;
}

//******************************************************************
//* String stripping routines
//******************************************************************

//******************************************************************
//* A collection of templates and functions to allow stripping of
//* leading and trailing items from a string
//*
//* --- stripItem
//*
//* Strip leading and trailing items from a container
//* template parameters are the container type, and a function which returns
//* true if we have the value to strip
//*
//*
//* ---- stripChar
//* strips characters off a string by either a function (which returns
//* true if the character is found), or by an actual character mathc.
//* There are also forms for wide strings
//*
//* ---- stripWhitespace
//* removes leading and trailing spaces from a string using isspace()
//*

template <typename F>
inline std::string stripItem(const std::string &s, F func, bool stripLeading, bool stripTrailing)
{
    auto	leftit  = s.begin();
    auto	rightit = s.rbegin();

    if (s.length() < 1 )
	{
        return s;
	}

    if (stripLeading)
    {
        //while((leftit != s.end()) && F func (*leftit))
		while((leftit != s.end()) && func(*leftit))
		{
            leftit++;
		}
    }
    
	if (leftit == s.end()) 
	{
		//return S();
		return s;
	}

    if (stripTrailing)
    {
        while(rightit != s.rend() && func(*rightit))
		{
            rightit++;
		}
    }

    if (rightit == s.rend()) 
	{
		//return S();
		return s;
	}

    auto endpnt = (++rightit).base();

    if (leftit > endpnt)
	{
        return std::string();
	}

    return s.substr( (leftit - s.begin()) , (endpnt-leftit) +1);
}

template <typename S,typename F>
S stripItem(const S &s, F func, bool stripLeading = true, bool stripTrailing = true)
{
    auto leftit  = s.begin();
    auto rightit = s.rbegin();

    if (s.length() < 1 )
	{
        return s;
	}

    if (stripLeading)
    {
        while(leftit != s.end() && func(*leftit))
		{
            leftit++;
		}
    }

    if (leftit == s.end()) 
	{
		//return S();
        return s;
	}

    if (stripTrailing)
    {
        while(rightit != s.rend() && func(*rightit))
		{
            rightit++;
		}
    }

    if (rightit == s.rend()) 
	{
		//return S();
        return s;
	}

    typename S::const_iterator endpnt = (++rightit).base();

    if (leftit > endpnt)
	{
		//return S();
        return s;
	}

    return s.substr((leftit - s.begin()), ((endpnt - leftit) + 1));
}

//template<typename FUNC>
//std::string stripChar(const std::string &s, FUNC f, bool stripLeading = true, bool stripTrailing = true)
//{
//    return stripItem(s, f, stripLeading, stripTrailing);
//}

inline std::string stripChar(const std::string &s, char c, bool stripLeading = true, bool stripTrailing = true)
	{ return stripItem(s, bind1st(std::equal_to<char>(), c), stripLeading, stripTrailing); }

#ifdef WIN32
//template<typename FUNC>
//std::wstring stripChar(const std::wstring &s, FUNC f, bool stripLeading = true, bool stripTrailing = true)
//{
//    return stripItem(s, f, stripLeading, stripTrailing);
//}

inline std::wstring stripChar(const std::wstring &s, wchar_t c, bool stripLeading = true, bool stripTrailing = true)
	{ return stripItem(s, bind1st(std::equal_to<wchar_t>(), c), stripLeading, stripTrailing); }
#endif

//inline std::string stripWhitespace(const std::string &s) { return stripChar(s, isspace, true, true); }
inline std::string stripWhitespace(const std::string &s)
{
    std::string result = "";

	if (s.length() < 1)
	{
        return result;
	}

    for (int i = 0; i < ((int) (s.length())); i++)
	{
		if (s[i] != ' ')
		{
			result += s[i];
		}
	}

    return result;
}



//*****************************************************************
//* tokenizer
//*
//* break up a string into substrings based on a delimiter item.
//*

template<typename S,typename F>
std::vector<S> tokenizer(const S &ins,F isdelimiter) throw()
{
    std::vector<S> result;
    S accum;

    for(typename S::const_iterator i = ins.begin(); i != ins.end(); ++i)
    {
        if (!isdelimiter(*i))
        {
            accum += (*i);
        }
        else
        {
            if (!accum.empty())
            {
                result.push_back(accum);
                accum = S();
            }
        }
    }

    if (!accum.empty())
	{
        result.push_back(accum);
	}

    return result;
}

inline std::vector<std::string> tokenizer(const std::string &ins, char delim) throw()
{
    return tokenizer(ins, bind1st(std::equal_to<char>(), delim));
}

#ifdef WIN32
inline std::vector<std::wstring> tokenizer(const std::wstring &ins, wchar_t delim) throw()
{
    return tokenizer(ins, bind1st(std::equal_to<wchar_t>(), delim));
}

#endif

};

#endif

