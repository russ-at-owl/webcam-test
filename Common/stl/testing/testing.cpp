#include <iostream>
#include "stringUtils.h"

using namespace std;
using namespace stringUtil;


int main(int argc, char* argv[])
{
	{
	cout << "Stripping tests" << endl;
	cout << "----------------------------" << endl;
	string s1("    this is a test    ");
	string s2("2");
	string s3(" ");
	string s4("4 ");
	string s5(" 5");
	wstring w1(L"   this is a test    ");
	wstring w2(L"2");
	wstring w3(L" ");
	wstring w4(L"4 ");
	wstring w5(L" 5");
	
	string sss("  \t\tfoobar\t\t\n");
	
	cout << "[" << stripChar(s1,' ') << "]" << endl;
	cout << "[" << stripChar(s2,' ') << "]" << endl;
	cout << "[" << stripChar(s3,' ') << "]" << endl;
	cout << "[" << stripChar(s4,' ') << "]" << endl;
	cout << "[" << stripChar(s5,' ') << "]" << endl;
	cout << "[" << tos(stripChar(w1,L' ')) << "]" << endl;
	cout << "[" << tos(stripChar(w2,L' ')) << "]" << endl;
	cout << "[" << tos(stripChar(w3,L' ')) << "]" << endl;
	cout << "[" << tos(stripChar(w4,L' ')) << "]" << endl;
	cout << "[" << tos(stripChar(w5,L' ')) << "]" << endl;

	cout << "[" << stripChar(sss,' ') << "]" << endl;
	cout << "[" << stripWhitespace(sss) << "]" << endl;
	cout << "-------------------------------" << endl;
	
	cout << "any char to continue" << endl;
	char c;
	cin >> c;
	}
	
	{
	cout << "Tokenizer tests" << endl;
	cout << "----------------" << endl;
	
	string s1("This.is.a.test.of.the");
	vector<string> sv = tokenizer(s1,'.');
	for(vector<string>::const_iterator i = sv.begin(); i != sv.end(); ++i)
		cout << "[" << (*i) << "] ";
	cout << endl;
	
	s1 = "This is a\ttest\tof\nthe";
	sv = tokenizer(s1,' ');
	for(vector<string>::const_iterator i = sv.begin(); i != sv.end(); ++i)
		cout << "[" << (*i) << "] ";
	cout << endl;

	sv = tokenizer(s1,isspace);
	for(vector<string>::const_iterator i = sv.begin(); i != sv.end(); ++i)
		cout << "[" << (*i) << "] ";
	cout << endl;
			
	wstring s2(L"This is a test of the");
	vector<wstring> swv = tokenizer(s2,L' ');
	for(vector<wstring>::const_iterator i = swv.begin(); i != swv.end(); ++i)
		cout << "[" << tos((*i)) << "] ";
	cout << endl;
	
	cout << "---------------" << endl;
	cout << "any char to continue" << endl;
	char c;
	cin >> c;
	}

	return 0;
}

