//**************************************************************************************************
//* FILE:		stlx.h
//*
//* DESCRIP:	
//*
//*		templates which allow STL iterations using a class and it's method
//*		(as opposed to mem_fun which requires the container class to hold the
//*		class and it's method
//*		
//*		ie
//*			
//*		vector<CLASSA> a
//*		B b;
//*		for_each(a.begin(),a.end(),class_method_ref(b,B::foo))
//*		
//*		this will invoke b.foo(a) for each element.
//*



#ifndef _stlx_H_
#define _stlx_H_

#include <functional>

namespace stlx 
{

	
template<typename Result, typename T, typename P>
	class class_method_functor: public std::unary_function<P,Result>
		{
			typedef Result (T::*methodType)(P s);
			T			*m_class;
			methodType	m_method;
		public:
			class_method_functor(T *c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s) { return (m_class->*m_method)(s); }
		};

template<typename Result,typename T,typename P>
	class class_method_functor_const: public std::unary_function<P,Result>
		{
			typedef Result (T::*methodType)(P s) const;
			const T		*m_class;
			methodType	m_method;
		public:
			class_method_functor_const(const T *c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s) const { return (m_class->*m_method)(s); }
		};

template<typename Result,typename T,typename P>
	class class_method_ref_functor: public std::unary_function<P,Result>
		{
			typedef Result (T::*methodType)(P s);
			T			&m_class;
			methodType	m_method;
		public:
			class_method_ref_functor(T &c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s) { return (m_class.*m_method)(s); }
		};
		
template<typename Result,typename T,typename P>
	class class_method_ref_functor_const: public std::unary_function<P,Result>
		{
			typedef Result (T::*methodType)(P s) const;
			const T		&m_class;
			methodType	m_method;
		public:
			class_method_ref_functor_const(const T &c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s) const { return (m_class.*m_method)(s); }
		};
		
template<typename Result,typename T,typename P,typename PP>
	class class_method_functor1: public std::binary_function<P,PP,Result>
		{
			typedef Result (T::*methodType)(P s,PP ss);
			T			*m_class;
			methodType	m_method;
		public:
			class_method_functor1(T *c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s,PP ss) { return (m_class->*m_method)(s,ss); }
		};

template<typename Result,typename T,typename P,typename PP>
	class class_method_functor1_const: public std::binary_function<P,PP,Result>
		{
			typedef Result (T::*methodType)(P s,PP ss) const;
			const T		*m_class;
			methodType	m_method;
		public:
			class_method_functor1_const(const T *c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s,PP ss) const { return (m_class->*m_method)(s,ss); }
		};

template<typename Result,typename T,typename P,typename PP>
	class class_method_ref_functor1: public std::binary_function<P,PP,Result>
		{
			typedef Result (T::*methodType)(P s,PP ss);
			T			&m_class;
			methodType	m_method;
		public:
			class_method_ref_functor1(T &c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s,PP ss) { return (m_class.*m_method)(s,ss); }
		};

template<typename Result,typename T,typename P,typename PP>
	class class_method_ref_functor1_const: public std::binary_function<P,PP,Result>
		{
			typedef Result (T::*methodType)(P s,PP ss) const;
			const T		&m_class;
			methodType	m_method;
		public:
			class_method_ref_functor1_const(const T &c,methodType m):m_class(c),m_method(m){}
			Result operator()(P s,PP ss) const { return (m_class.*m_method)(s,ss); }
		};

template<typename Result,typename T,typename P>
	class_method_functor<Result,T,P>
		class_method(T *c,Result (T::*m)(P s))
			{ return class_method_functor<Result,T,P>(c,m); }

template<typename Result,typename T,typename P>
	class_method_functor_const<Result,T,P>
		class_method(const T *c,Result (T::*m)(P s) const)
			{ return class_method_functor_const<Result,T,P>(c,m); }

template<typename Result,typename T,typename P>
	class_method_ref_functor<Result,T,P>
		class_method_ref(T &c,Result (T::*m)(P s))
			{ return class_method_ref_functor<Result,T,P>(c,m); }

template<typename Result,typename T,typename P>
	class_method_ref_functor_const<Result,T,P>
		class_method_ref(const T &c,Result (T::*m)(P s) const)
			{ return class_method_ref_functor_const<Result,T,P>(c,m); }

template<typename Result,typename T,typename P,typename PP>
	class_method_functor1<Result,T,P,PP>
		class_method(T *c,Result (T::*m)(P s,PP ss))
			{ return class_method_functor1<Result,T,P,PP>(c,m); }

template<typename Result,typename T,typename P,typename PP>
	class_method_functor1_const<Result,T,P,PP>
		class_method(const T *c,Result (T::*m)(P s,PP ss) const)
			{ return class_method_functor1_const<Result,T,P,PP>(c,m); }

template<typename Result,typename T,typename P,typename PP>
	class_method_ref_functor1<Result,T,P,PP>
		class_method_ref(T &c,Result (T::*m)(P s,PP ss))
			{ return class_method_ref_functor1<Result,T,P,PP>(c,m); }

template<typename Result,typename T,typename P,typename PP>
	class_method_ref_functor1_const<Result,T,P,PP>
		class_method_ref(const T &c,Result (T::*m)(P s,PP ss) const)
			{ return class_method_ref_functor1_const<Result,T,P,PP>(c,m); }


//**********************************************************
//* streamOutFunctor
//*
//* functor class which will output an element to a stream
//* with a prefix and suffix string. Useful for outputing
//* elements of a container
//*********************************************************
template<class T> class streamOutFunctor
	{
	std::ostream &m_o;
	const std::string m_prefix;
	const std::string m_suffix;
	public:
		inline streamOutFunctor(std::ostream &o) : m_o(o){}
		inline streamOutFunctor(std::ostream &o,const std::string &prefix,const std::string &suffix)
			: m_o(o),m_prefix(prefix),m_suffix(suffix){}
		inline void operator()(const T &t) { m_o << m_prefix << t << m_suffix; }
	};

};

#endif
