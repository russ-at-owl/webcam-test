//**************************************************************************************************
//* FILE:		stlx.h
//*
//*



#ifndef _stlx_H_
#define _stlx_H_

#include <vector>
#include <map>

//#ifndef _WINBASE_
//#include <WinBase.h>
//#endif

#include "stringUtils.h"

#include "..\other\VaPass.h"


namespace stlx 
{

//* enhanced template classes


#ifndef CHAR
typedef char  CHAR;	
#endif




#ifndef _xstring_
#define _xstring_

#include <string>

class xstring : public std::string
{
public:
	xstring(void) 
	{
		assign("");
	}

	xstring(const char *pVal) 
	{
		assign(pVal);
	}

	//xstring(std::string sVal) 
	//{
	//	assign(sVal);
	//}

	xstring(std::string &sVal) 
	{
		assign(sVal);
	}
	
	void clear()
	{
		assign("");
	}

	std::string& operator=(const char *pVal) 
	{
		assign(pVal);

		return (std::string&) *this;
	}

	//std::string& operator=(std::string sVal) 
	//{
	//	assign(sVal);
	//
	//	return (std::string&) *this;
	//}

	std::string& operator=(std::string &sVal) 
	{
		assign(sVal);

		return (std::string&) *this;
	}

	bool operator==(const char *pVal) 
	{
		return (compare(pVal) == 0 ? true : false);
	}

	//bool operator==(std::string sVal) 
	//{
	//	return (compare(sVal) == 0 ? true : false);
	//}

	bool operator==(std::string &sVal) 
	{
		return (compare(sVal) == 0 ? true : false);
	}

	bool operator!=(const char *pVal) 
	{
		return (compare(pVal) == 0 ? false : true);
	}

	//bool operator!=(std::string sVal) 
	//{
	//	return (compare(sVal) == 0 ? false : true);
	//}

	bool operator!=(std::string &sVal) 
	{
		return (compare(sVal) == 0 ? false : true);
	}

	std::string tolower()
	{
		return (stringUtil::toLower(this->c_str()));
	}

	void format(const char *pFmtStr, ...)
	{
		if (pFmtStr == NULL)
		{
			return;
		}

		va_list ArgList;

		CHAR szTextString[1024];

		memset(szTextString, 0, sizeof(szTextString));

		va_start(ArgList, pFmtStr);

		//sprintf(szTextString, pFmtStr, va_pass(ArgList));
		sprintf_s(szTextString, pFmtStr, va_pass(ArgList));

		va_end(ArgList);

		this->assign(szTextString);
	}
};

#endif // _xstring_


#ifndef _xvector_
#define _xvector_
template <typename V>
class xvector : public std::vector<V>
{
public:
    //Constructor
    xvector() : std::vector<V>() {};

	//* Destructor
	~xvector()
	{
		xvector::clear();
	};

	//* delete all elements in this map
	inline void clear()
	{
		if (size() > 0)
		{
			typename std::vector<V>::iterator iter = begin();
			while (size() > 0) 
			{
				if (iter == end())
				{
					break;
				}

				erase(iter);

				::Sleep(5);

				iter = begin();
			}
		}
	};
};
#endif 

#ifndef _ptr_xvector_
#define _ptr_xvector_
template <typename V>
class ptr_xvector : public std::vector<V*>
{
public:
    //* Constructor
    ptr_xvector() : std::vector<V*>() {};

	//* Destructor
	~ptr_xvector()
	{
		ptr_xvector::clear();
	};

	//* delete all elements in this vector
	inline void clear()
	{
		if (size() > 0)
		{
			//v* pItem = NULL;

			while (size() > 0) 
			{
				//pItem = back();

				//if (pItem != NULL)
				//{
				//	delete pItem;
				//}
				
				pop_back();
			}
		}
	};
};
#endif 

#ifndef _xmap_
#define _xmap_
template <typename K, typename V>
class xmap : public std::map<K, V>
{
public:
    //Constructor
    xmap() : std::map<K, V>() {};

	//* Destructor
	~xmap()
	{
		xmap::clear();
	};

	//V & operator [] (K index) 
	//{
	//	this->[index] = V;
	//};
	
	//* delete all elements in this map
	inline void clear()
	{
		if (size() > 0)
		{
			typename std::map<K, V>::iterator iter = begin();
			while (size() > 0) 
			{
				if (iter == end())
				{
					break;
				}

				erase(iter);

				::Sleep(5);

				iter = begin();
			}
		}
	};

	//* get the map iterator at index 'idx'
    inline typename std::map<K, V>::iterator get(unsigned int idx)
    {
		if (idx >= size())
		{
			return end();
		}
        typename std::map<K, V>::iterator iter = begin();
		std::advance(iter, idx);
        return iter;
    };

	//inline void set(K key, K val)
	//{
	//	std::map<K, V>::at(key) = val;
	//};
};
#endif 

#ifndef _ptr_xmap_
#define _ptr_xmap_
template <typename K, typename V>
class ptr_xmap : public std::map<K, V*>
{
public:
    //* Constructor
    ptr_xmap() : std::map<K, V*>() {};

	//* Destructor
	~ptr_xmap()
	{
		ptr_xmap::clear();
	};

	//* delete all elements in this map
	inline void clear()
	{
		if (size() > 0)
		{
			typename std::map<K, V*>::iterator iter = begin();
			while ((size() > 0) && (iter != end()))
			{
				if ((*iter).second != NULL)
				{
					delete ((*iter).second);
				}

				erase(iter);

				::Sleep(5);
				
				iter = begin();
			}
		}
	};

	//* get the map iterator at index 'idx'
    inline typename std::map<K, V*>::iterator get(unsigned int idx)
    {
		if (idx >= size())
		{
			return end();
		}
        typename std::map<K, V*>::iterator iter = begin();
		std::advance(iter, idx);
        return iter;
    };

	//inline void set(K key, K val)
	//{
	//	std::map<K, V>::at(key) = val;
	//};
};
#endif 



//* class macros

#ifndef FOR_EACH
#define FOR_EACH(t, v, e)			for (t::iterator e = v.begin(); e != v.end(); e++)
#endif

#ifndef SET_LIST
#define SET_LIST(LT, LV, VE, VV)	for (LT::iterator idx = LV.begin(); idx != LV.end(); idx++) {(*idx).VE = VV;}
#endif


};

#endif
