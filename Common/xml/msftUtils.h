#ifndef _msftUtils_H_
#define _msftUtils_H_

#ifdef WIN32
#include <comdef.h>
#include <msxml2.h>
#include <string>
#include <stdexcept>
#include "http/simpleGet.h"

_COM_SMARTPTR_TYPEDEF(IXMLDOMDocument,		__uuidof(IXMLDOMDocument));
_COM_SMARTPTR_TYPEDEF(IXMLDOMParseError,	__uuidof(IXMLDOMParseError));
_COM_SMARTPTR_TYPEDEF(IXMLDOMNodeList,		__uuidof(IXMLDOMNodeList));
_COM_SMARTPTR_TYPEDEF(IXMLDOMNode,			__uuidof(IXMLDOMNode));

namespace msftXML
{

// given a parser object, url, and executing function (for diagnostics),
// send out the URL request. Throw an exception if anything goes wrong
inline void loadXML(IXMLDOMDocumentPtr domP,const std::string &url,const std::string &from) throw(std::runtime_error)
	{
	// all this really does is call load(), but there's lots of
	// things that you have to check afterward to make sure
	// it succeeded.
	VARIANT_BOOL status;
	if (url.find("http://") == 0)
		{
		std::string xml = simpleGet(url);
		_bstr_t b(xml.c_str());
		if (FAILED(domP->loadXML(b,&status)))
			throw std::runtime_error(from + " IXMLDOMDocument::loadXML failed for url [" + url + "]");
		}
	else
		{
		_variant_t Vurl(url.c_str());
		if (FAILED(domP->load(Vurl,&status)))
			throw std::runtime_error(from + " IXMLDOMDocument::load failed for url [" + url + "]");
		}
	if (status != VARIANT_TRUE)
		{
		IXMLDOMParseErrorPtr errP;
		if (FAILED(domP->get_parseError(&errP)))
			throw std::runtime_error(from + " parse failed but was unable to get error for url [" + url + "]");
		BSTR reason = NULL;
		if (FAILED(errP->get_reason(&reason)))
			throw std::runtime_error(from + " parse failed but was unable to get reason for url [" + url + "]");
		_bstr_t rsn(reason,false);
		throw std::runtime_error(from + " parse failure " + std::string((const char *)rsn) + " for url [" + url + "]");
		}
	}

// templates to convert the text in a DOM node to
// a convenient format
template<class R> // convert to anything that a _variant_t can be cast to
inline R nodeText(IXMLDOMNodePtr nodeP)
	{
	BSTR t = NULL;
	nodeP->get_text(&t);
	_bstr_t safeT(t,false);
	return _variant_t(safeT);
	}
template<> // convert specifically to a std::string
inline std::string nodeText(IXMLDOMNodePtr nodeP)
	{
	BSTR t = NULL;
	nodeP->get_text(&t);
	_bstr_t safeT(t,false);
	return std::string((const char *)safeT);
	}

template<>
inline bool nodeText(IXMLDOMNodePtr nodeP)
	{
	std::string s = nodeText<std::string>(nodeP);
	if (s == "") return false;
	return (s[0] == 't' || s[0] == 'T' || s[0] == '1' || s[0] == 'y' || s[0] == 'Y');
	}
	
// get a subnode value. Returns true if value existed
template<typename T>
inline bool subNodeText(IXMLDOMNodePtr node,const char *tag,T& value,const T defaultValue) throw()
	{
	value = defaultValue;
	if (node == NULL)
		return false;
		
	IXMLDOMNodePtr subnode;
	node->selectSingleNode(_bstr_t(tag),&subnode);
	if (subnode == NULL)
		return false;
		
	value = nodeText<T>(subnode);
	
	return true;
	}

// same as above, but you cannot detect missing value
template<typename T>
inline T subNodeText(IXMLDOMNodePtr node,const char *tag,const T defaultValue) throw()
	{
	if (node == NULL)
		return defaultValue;
		
	IXMLDOMNodePtr subnode;
	node->selectSingleNode(_bstr_t(tag),&subnode);
	if (subnode == NULL)
		return defaultValue;
		
	return nodeText<T>(subnode);
	}

template<typename T>
inline T subNodeTextTHROW(IXMLDOMNodePtr node,const char *tag) throw(std::runtime_error)
	{
	if (node == NULL)
		throw std::runtime_error("node NULL");
		
	IXMLDOMNodePtr subnode;
	node->selectSingleNode(_bstr_t(tag),&subnode);
	if (subnode == NULL)
		throw std::runtime_error(std::string(tag) + " missing");
		
	return nodeText<T>(subnode);
	}

template<typename T>
inline void subNodeTextTHROW(IXMLDOMNodePtr node,const char *tag,T &result) throw(std::runtime_error)
	{
	if (node == NULL)
		throw std::runtime_error("node NULL");
		
	IXMLDOMNodePtr subnode;
	node->selectSingleNode(_bstr_t(tag),&subnode);
	if (subnode == NULL)
		throw std::runtime_error(std::string(tag) + " missing");
		
	result =  nodeText<T>(subnode);
	}

/// calls function 'f' with each node in the list and the value 'l'
// The purpose is to allow you to write a function which will insert
// data from each node into container 'l'
template<typename L,typename Func>
inline void XMLList(L &l,IXMLDOMNodeListPtr lnode,Func f) throw(std::runtime_error)
		{
		lnode->reset();
		while (true)
			{
			IXMLDOMNodePtr n;
			lnode->nextNode(&n);
			if (n == NULL)
				break;
			f(l,n);
			}
		}

};


#endif
#endif
