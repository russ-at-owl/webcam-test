//*****************************************************************************
//* Module Name:	util.C 
//*
//* Description:
//*
//*
//*
//* Version:        1.20a
//* Last Updated:   06/16/97
//*
//*****************************************************************************
//*
//* Created By:     Russ Barker
//* Copyright (c):	Galileo Designs,Inc. 1996,1997
//*
//****************************************************************************

#ifdef WIN32

#include "windows.h"

//#else

#endif


#include "Utils.h"


//********************************************************************
//* Function: Greater
//*
//* Purpose : 
//*         
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

int Greater (int nVal1, int nVal2)
{
	if (nVal1 > nVal2)
	{
		return nVal1;
	}

	return nVal2;
}


//********************************************************************
//* Function: Lesser
//*
//* Purpose : 
//*         
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

int Lesser (int nVal1, int nVal2)
{
	if (nVal1 < nVal2)
	{
		return nVal1;
	}

	return nVal2;
}


//********************************************************************
//* Function: WCopy
//*
//* Purpose : 
//*         
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

VOID WCopy (LPWORD pTarget, LPWORD pSource, DWORD dwCnt)
{
	DWORD	x;

	for (x = 0; x < dwCnt; x++)
	{
		*(pTarget + x) = *(pSource + x);
	}
}


//********************************************************************
//* Function: WSet
//*
//* Purpose : 
//*         
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

VOID WSet (LPWORD pTarget, WORD wValue, DWORD dwCnt)
{
	DWORD	x;

	for (x = 0; x < dwCnt; x++)
	{
		*(pTarget + x) = wValue;
	}
}


//********************************************************************
//* Function: DWCopy
//*
//* Purpose : 
//*         
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

VOID DWCopy (LPDWORD pTarget, LPDWORD pSource, WORD dwCnt)
{
	DWORD	x;

	for (x = 0; x < dwCnt; x++)
	{
		*(pTarget + x) = *(pSource + x);
	}
}


//********************************************************************
//* Function: DWSet
//*
//* Purpose : 
//*         
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

VOID DWSet (LPDWORD pTarget, DWORD dwValue, WORD dwCnt)
{
	DWORD	x;

	for (x = 0; x < dwCnt; x++)
	{
		*(pTarget + x) = dwValue;
	}
}


//********************************************************************
//* Function: SwapShort
//*
//* Purpose : 
//*         Swaps the BYTEs in a WORD
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

VOID SwapShort (LPWORD pWord)
{
	LPBYTE	pByte;
	BYTE	byHold;

	pByte = (LPBYTE) pWord;

	byHold = *(pByte);
	*(pByte) = *(pByte + 1);
	*(pByte + 1) = byHold;
}


//********************************************************************
//* Function: SwapLong
//*
//* Purpose : 
//*         Swaps the BYTEs in a DWORD
//*
//* Params: 
//*
//* Return:  nothing
//********************************************************************

VOID SwapLong (LPDWORD pLong)
{
	LPBYTE	pByte;
	BYTE	byHold;

	pByte = (LPBYTE) pLong;

	byHold = *(pByte);
	*(pByte) = *(pByte + 3);
	*(pByte + 3) = byHold;

	byHold = *(pByte + 2);
	*(pByte + 2) = *(pByte + 1);
	*(pByte + 1) = byHold;
}


//********************************************************************
//* Function: FlipShort
//*
//* Purpose : 
//*         Flips the BYTEs in a WORD
//*
//* Params: 
//*
//* Return:  
//********************************************************************

WORD FlipShort (WORD wValue)
{
	LPBYTE	pByte;
	BYTE	byHold;

	pByte = (LPBYTE) &(wValue);

	byHold = *(pByte);
	*(pByte) = *(pByte + 1);
	*(pByte + 1) = byHold;

	return wValue;
}


//********************************************************************
//* Function: FlipLong
//*
//* Purpose : 
//*         Flips the BYTEs in a DWORD
//*
//* Params: 
//*
//* Return:  
//********************************************************************

DWORD FlipLong (DWORD dwValue)
{
	LPBYTE	pByte;
	BYTE	byHold;

	pByte = (LPBYTE) &(dwValue);

	byHold = *(pByte);
	*(pByte) = *(pByte + 3);
	*(pByte + 3) = byHold;

	byHold = *(pByte + 2);
	*(pByte + 2) = *(pByte + 1);
	*(pByte + 1) = byHold;

	return dwValue;
}


//********************************************************************
//* Function: Byte2Ascii
//*
//* Purpose : 
//*         Gets the numeric string for an BYTE value 0 - 255
//*
//* Params: 
//*
//* Return:  
//********************************************************************

int Byte2Ascii (int nValue, PSTR pStr, int nFormat)
{
    static BYTE2ASCIIDEF StrTable[256] = 
                        {
                            {"00", "000", "00000000"},
                            {"01", "001", "00000001"},
                            {"02", "002", "00000010"},
                            {"03", "003", "00000011"},
                            {"04", "004", "00000100"},
                            {"05", "005", "00000101"},
                            {"06", "006", "00000110"},
                            {"07", "007", "00000111"},
                            {"08", "008", "00001000"},
                            {"09", "009", "00001001"},
                            {"0A", "010", "00001010"},
                            {"0B", "011", "00001011"},
                            {"0C", "012", "00001100"},
                            {"0D", "013", "00001101"},
                            {"0E", "014", "00001110"},
                            {"0F", "015", "00001111"},
                            {"10", "016", "00010000"},
                            {"11", "017", "00010001"},
                            {"12", "018", "00010010"},
                            {"13", "019", "00010011"},
                            {"14", "020", "00010100"},
                            {"15", "021", "00010101"},
                            {"16", "022", "00010110"},
                            {"17", "023", "00010111"},
                            {"18", "024", "00011000"},
                            {"19", "025", "00011001"},
                            {"1A", "026", "00011010"},
                            {"1B", "027", "00011011"},
                            {"1C", "028", "00011100"},
                            {"1D", "029", "00011101"},
                            {"1E", "030", "00011110"},
                            {"1F", "031", "00011111"},
                            {"20", "032", "00100000"},
                            {"21", "033", "00100001"},
                            {"22", "034", "00100010"},
                            {"23", "035", "00100011"},
                            {"24", "036", "00100100"},
                            {"25", "037", "00100101"},
                            {"26", "038", "00100110"},
                            {"27", "039", "00100111"},
                            {"28", "040", "00101000"},
                            {"29", "041", "00101001"},
                            {"2A", "042", "00101010"},
                            {"2B", "043", "00101011"},
                            {"2C", "044", "00101100"},
                            {"2D", "045", "00101101"},
                            {"2E", "046", "00101110"},
                            {"2F", "047", "00101111"},
                            {"30", "048", "00110000"},
                            {"31", "049", "00110001"},
                            {"32", "050", "00110010"},
                            {"33", "051", "00110011"},
                            {"34", "052", "00110100"},
                            {"35", "053", "00110101"},
                            {"36", "054", "00110110"},
                            {"37", "055", "00110111"},
                            {"38", "056", "00111000"},
                            {"39", "057", "00111001"},
                            {"3A", "058", "00111010"},
                            {"3B", "059", "00111011"},
                            {"3C", "060", "00111100"},
                            {"3D", "061", "00111101"},
                            {"3E", "062", "00111110"},
                            {"3F", "063", "00111111"},
                            {"40", "064", "01000000"},
                            {"41", "065", "01000001"},
                            {"42", "066", "01000010"},
                            {"43", "067", "01000011"},
                            {"44", "068", "01000100"},
                            {"45", "069", "01000101"},
                            {"46", "070", "01000110"},
                            {"47", "071", "01000111"},
                            {"48", "072", "01001000"},
                            {"49", "073", "01001001"},
                            {"4A", "074", "01001010"},
                            {"4B", "075", "01001011"},
                            {"4C", "076", "01001100"},
                            {"4D", "077", "01001101"},
                            {"4E", "078", "01001110"},
                            {"4F", "079", "01001111"},
                            {"50", "080", "01010000"},
                            {"51", "081", "01010001"},
                            {"52", "082", "01010010"},
                            {"53", "083", "01010011"},
                            {"54", "084", "01010100"},
                            {"55", "085", "01010101"},
                            {"56", "086", "01010110"},
                            {"57", "087", "01010111"},
                            {"58", "088", "01011000"},
                            {"59", "089", "01011001"},
                            {"5A", "090", "01011010"},
                            {"5B", "091", "01011011"},
                            {"5C", "092", "01011100"},
                            {"5D", "093", "01011101"},
                            {"5E", "094", "01011110"},
                            {"5F", "095", "01011111"},
                            {"60", "096", "01100000"},
                            {"61", "097", "01100001"},
                            {"62", "098", "01100010"},
                            {"63", "099", "01100011"},
                            {"64", "100", "01100100"},
                            {"65", "101", "01100101"},
                            {"66", "102", "01100110"},
                            {"67", "103", "01100111"},
                            {"68", "104", "01101000"},
                            {"69", "105", "01101001"},
                            {"6A", "106", "01101010"},
                            {"6B", "107", "01101011"},
                            {"6C", "108", "01101100"},
                            {"6D", "109", "01101101"},
                            {"6E", "110", "01101110"},
                            {"6F", "111", "01101111"},
                            {"70", "112", "01110000"},
                            {"71", "113", "01110001"},
                            {"72", "114", "01110010"},
                            {"73", "115", "01110011"},
                            {"74", "116", "01110100"},
                            {"75", "117", "01110101"},
                            {"76", "118", "01110110"},
                            {"77", "119", "01110111"},
                            {"78", "120", "01111000"},
                            {"79", "121", "01111001"},
                            {"7A", "122", "01111010"},
                            {"7B", "123", "01111011"},
                            {"7C", "124", "01111100"},
                            {"7D", "125", "01111101"},
                            {"7E", "126", "01111110"},
                            {"7F", "127", "01111111"},
                            {"80", "128", "10000000"},
                            {"81", "129", "10000001"},
                            {"82", "130", "10000010"},
                            {"83", "131", "10000011"},
                            {"84", "132", "10000100"},
                            {"85", "133", "10000101"},
                            {"86", "134", "10000110"},
                            {"87", "135", "10000111"},
                            {"88", "136", "10001000"},
                            {"89", "137", "10001001"},
                            {"8A", "138", "10001010"},
                            {"8B", "139", "10001011"},
                            {"8C", "140", "10001100"},
                            {"8D", "141", "10001101"},
                            {"8E", "142", "10001110"},
                            {"8F", "143", "10001111"},
                            {"90", "144", "10010000"},
                            {"91", "145", "10010001"},
                            {"92", "146", "10010010"},
                            {"93", "147", "10010011"},
                            {"94", "148", "10010100"},
                            {"95", "149", "10010101"},
                            {"96", "150", "10010110"},
                            {"97", "151", "10010111"},
                            {"98", "152", "10011000"},
                            {"99", "153", "10011001"},
                            {"9A", "154", "10011010"},
                            {"9B", "155", "10011011"},
                            {"9C", "156", "10011100"},
                            {"9D", "157", "10011101"},
                            {"9E", "158", "10011110"},
                            {"9F", "159", "10011111"},
                            {"A0", "160", "10100000"},
                            {"A1", "161", "10100001"},
                            {"A2", "162", "10100010"},
                            {"A3", "163", "10100011"},
                            {"A4", "164", "10100100"},
                            {"A5", "165", "10100101"},
                            {"A6", "166", "10100110"},
                            {"A7", "167", "10100111"},
                            {"A8", "168", "10101000"},
                            {"A9", "169", "10101001"},
                            {"AA", "170", "10101010"},
                            {"AB", "171", "10101011"},
                            {"AC", "172", "10101100"},
                            {"AD", "173", "10101101"},
                            {"AE", "174", "10101110"},
                            {"AF", "175", "10101111"},
                            {"B0", "176", "10110000"},
                            {"B1", "177", "10110001"},
                            {"B2", "178", "10110010"},
                            {"B3", "179", "10110011"},
                            {"B4", "180", "10110100"},
                            {"B5", "181", "10110101"},
                            {"B6", "182", "10110110"},
                            {"B7", "183", "10110111"},
                            {"B8", "184", "10111000"},
                            {"B9", "185", "10111001"},
                            {"BA", "186", "10111010"},
                            {"BB", "187", "10111011"},
                            {"BC", "188", "10111100"},
                            {"BD", "189", "10111101"},
                            {"BE", "190", "10111110"},
                            {"BF", "191", "10111111"},
                            {"C0", "192", "11000000"},
                            {"C1", "193", "11000001"},
                            {"C2", "194", "11000010"},
                            {"C3", "195", "11000011"},
                            {"C4", "196", "11000100"},
                            {"C5", "197", "11000101"},
                            {"C6", "198", "11000110"},
                            {"C7", "199", "11000111"},
                            {"C8", "200", "11001000"},
                            {"C9", "201", "11001001"},
                            {"CA", "202", "11001010"},
                            {"CB", "203", "11001011"},
                            {"CC", "204", "11001100"},
                            {"CD", "205", "11001101"},
                            {"CE", "206", "11001110"},
                            {"CF", "207", "11001111"},
                            {"D0", "208", "11010000"},
                            {"D1", "209", "11010001"},
                            {"D2", "210", "11010010"},
                            {"D3", "211", "11010011"},
                            {"D4", "212", "11010100"},
                            {"D5", "213", "11010101"},
                            {"D6", "214", "11010110"},
                            {"D7", "215", "11010111"},
                            {"D8", "216", "11011000"},
                            {"D9", "217", "11011001"},
                            {"DA", "218", "11011010"},
                            {"DB", "219", "11011011"},
                            {"DC", "220", "11011100"},
                            {"DD", "221", "11011101"},
                            {"DE", "222", "11011110"},
                            {"DF", "223", "11011111"},
                            {"E0", "224", "11100000"},
                            {"E1", "225", "11100001"},
                            {"E2", "226", "11100010"},
                            {"E3", "227", "11100011"},
                            {"E4", "228", "11100100"},
                            {"E5", "229", "11100101"},
                            {"E6", "230", "11100110"},
                            {"E7", "231", "11100111"},
                            {"E8", "232", "11101000"},
                            {"E9", "233", "11101001"},
                            {"EA", "234", "11101010"},
                            {"EB", "235", "11101011"},
                            {"EC", "236", "11101100"},
                            {"ED", "237", "11101101"},
                            {"EE", "238", "11101110"},
                            {"EF", "239", "11101111"},
                            {"F0", "240", "11110000"},
                            {"F1", "241", "11110001"},
                            {"F2", "242", "11110010"},
                            {"F3", "243", "11110011"},
                            {"F4", "244", "11110100"},
                            {"F5", "245", "11110101"},
                            {"F6", "246", "11110110"},
                            {"F7", "247", "11110111"},
                            {"F8", "248", "11111000"},
                            {"F9", "249", "11111001"},
                            {"FA", "250", "11111010"},
                            {"FB", "251", "11111011"},
                            {"FC", "252", "11111100"},
                            {"FD", "253", "11111101"},
                            {"FE", "254", "11111110"},
                            {"FF", "255", "11111111"}
                        };

    switch ( nFormat )
    {
        case 0:
            memmove ( (PVOID) pStr, (PVOID) StrTable[nValue].szHex, 3);
            break;

        case 1:
            memmove ( (PVOID) pStr, (PVOID) StrTable[nValue].szDec, 4);
            break;

        case 2:
            memmove ( (PVOID) pStr, (PVOID) StrTable[nValue].szBin, 9);
            break;
                  
        default:
            return -1;
    }


    return 0;
}



//****************************************************************************
//* Link-list management functions
//*    


//*****************************************************************************
//* Function: LinkListAlloc
//*
//* Purpose : 
//*      Get a pointer to the data block in the Linked List struct
//*
//* Params:  pLlStruct		Linked List structure
//*
//* Return:  A pointer to the allocated data buffer
//*****************************************************************************
PVOIDLISTDEF CALLBACK LinkListAlloc ()
{
	PVOIDLISTDEF	pVoidList = NULL;

	MYHANDLE		hMem = 0;
    
    /* Allocate and lock global memory.     */
#ifdef WIN32
	hMem = GlobalAlloc(GMEM_FIXED, sizeof(PVOIDLISTDEF));
	if (hMem == NULL)
    {
        return NULL;
    }

    pVoidList = (PVOIDLISTDEF) GlobalLock (hMem);
    if(pVoidList == NULL)
    {
        GlobalFree (hMem);
        return NULL;
    }
#else
	hMem =  malloc(wSize);
    if (hMem == NULL)
    {
        return NULL;
    }
    
#endif

    memset ((PVOID) pVoidList, 0, sizeof(PVOIDLISTDEF));

    /* Save the handle.         */
    pVoidList->hSelf = hMem; 
    
    pVoidList->pNext = NULL;    
    pVoidList->pPrev = NULL; 

	pVoidList->pData = NULL;

	pVoidList->nDataSize = LINKEDLIST_DATASIZE_UNKNOWN;

    return pVoidList;
}



//*****************************************************************************
//* Function: LinkListFree 
//*
//* Purpose : 
//*    Frees the given link-list structure.
//*
//* Params:  lpBuf - Points to the link-list structure to be freed.
//*
//* Return:  void
//*****************************************************************************
void CALLBACK LinkListFree (PVOIDLISTDEF pVoidList)
{
	if (pVoidList == NULL)
	{
		return;
	}

    MYHANDLE      hMem;

    /* Save the handle until we're through here.     */
    hMem = pVoidList->hSelf;

    /* Free the structure.                           */
#ifdef WIN32
    GlobalUnlock (hMem);
    GlobalFree (hMem);
#else
    free(hMem); 
#endif
}



//****************************************************************************
//*
//* LinkListAppend - This function is called to append a struct to the list.
//*
//****************************************************************************
PVOIDLISTDEF CALLBACK	LinkListAppend 
						(
							PLNKLSTBASEDEF pLnkLstBase
						)
{
    PVOIDLISTDEF     pPrevStruct;
    PVOIDLISTDEF     pCurrStruct;

    //******************************************************
    //* Add a new element to the end of the 
    //* link-list 
    //* 

    pPrevStruct = pLnkLstBase->pEndOfList;

    pCurrStruct = (PVOIDLISTDEF) LinkListAlloc();
    if (pCurrStruct == NULL)
    {
        return NULL;        
    }    
        
    pCurrStruct->pNext = NULL;
    pCurrStruct->pPrev = (LPVOID) pPrevStruct;

    if (pPrevStruct != NULL)
    {
        pPrevStruct->pNext = (LPVOID) pCurrStruct;
    }
    else
    {
        pLnkLstBase->pStartOfList = pCurrStruct;
    }
    
    pLnkLstBase->pEndOfList = pCurrStruct;

    return pCurrStruct;
}



//****************************************************************************
//*
//* LinkListInsert - This function is called to insert a struct in the list.
//*
//****************************************************************************
PVOIDLISTDEF CALLBACK	LinkListInsert
				(
					PLNKLSTBASEDEF pLnkLstBase,
					PVOIDLISTDEF pCurrStruct
				)
{
    PVOIDLISTDEF     pPrevStruct;
    PVOIDLISTDEF     pNextStruct;
    PVOIDLISTDEF     pNewStruct;

    //******************************************************
    //* Add a new element to the link-list 
    //* It will be inserted after the "CurrStruct"
    //* 

    pPrevStruct = (PVOIDLISTDEF) pCurrStruct->pPrev;
    pNextStruct = (PVOIDLISTDEF) pCurrStruct->pNext;

    pNewStruct = (PVOIDLISTDEF) LinkListAlloc ();
    if (pNewStruct == NULL)
    {
        return NULL;        
    }    
        
    pCurrStruct->pNext = NULL;
    pCurrStruct->pPrev = (LPVOID) pPrevStruct;

    if (pPrevStruct != NULL)
    {
        pPrevStruct->pNext = (LPVOID) pCurrStruct;
    }
    else
    {
        pLnkLstBase->pStartOfList = pCurrStruct;
    }
    
    pLnkLstBase->pEndOfList = pCurrStruct;

    return pCurrStruct;
}



//****************************************************************************
//*
//* LinkListDel - This function is called to delete a struct from the list.
//*
//****************************************************************************
bool CALLBACK	LinkListDel 
				(
					PLNKLSTBASEDEF pLnkLstBase,
					PVOIDLISTDEF pCurrStruct
				)
{
    PVOIDLISTDEF     pPrevStruct;
    PVOIDLISTDEF     pNextStruct;

    //******************************************************
    //* Delete an element from the  
    //* link-list 
    //* 

    if (pCurrStruct == NULL)
    {
        return false;
    }

    pPrevStruct = (PVOIDLISTDEF) pCurrStruct->pPrev;
    pNextStruct = (PVOIDLISTDEF) pCurrStruct->pNext;  
    
    LinkListFree ((PVOIDLISTDEF) pCurrStruct);
    
    if (pPrevStruct == NULL)
    {
        //******************************************************
        //* No previous struct
        //* 
    
        if (pNextStruct == NULL)
        {
            //******************************************************
            //* No next struct
            //* 
            //* Set start & end pointers for the list to NULL.
            //* 
            
            pLnkLstBase->pStartOfList = NULL;
            pLnkLstBase->pEndOfList = NULL;
        }
        else
        {
            //******************************************************
            //* Set start pointer for the list to the next struct
            //* Set the next struct's "prev" pointer to NULL.
            //* 

            pLnkLstBase->pStartOfList = pNextStruct;
            pNextStruct->pPrev = NULL;
        }
    }
    else
    {
        if (pNextStruct == NULL)
        {
            //******************************************************
            //* No next struct
            //* 
            //* Set previous struct's "next" pointer to NULL.
            //* Set end pointer for the list to previous struct.
            //* 
            
            pPrevStruct->pNext = NULL;
            pLnkLstBase->pEndOfList = pPrevStruct;
        }
        else
        {
            //******************************************************
            //* Set previous note's "next" pointer to next note.
            //* Set next note's "prev" pointer to previous note.
            //* 

            pPrevStruct->pNext = (LPVOID) pNextStruct;
            pNextStruct->pPrev = (LPVOID) pPrevStruct;
        }
    }

    return true;
}


//****************************************************************************
//*
//* LinkListClear - This function is called to delete ALL structs from the list.
//*
//****************************************************************************
bool CALLBACK LinkListClear (PLNKLSTBASEDEF pLnkLstBase)
{
	if (pLnkLstBase == NULL)
	{
		return false;
	}

    PVOIDLISTDEF     pNextStruct;

	while (pLnkLstBase->pStartOfList != NULL)
	{
		pNextStruct = pLnkLstBase->pStartOfList;

		LinkListDel (pLnkLstBase, pNextStruct);
	}

    return true;
}




