//*****************************************************************************
//* file:			utils.h
//*
//* Description:
//*
//*
//* Version:        1.20a
//*
//* Created By:     Russ Barker
//*
//*
//*


#ifndef _UTILS_H
#define _UTILS_H


/* Structure used for BYTE2ASCII function */
typedef struct 
{
    char    szHex[3];
    char    szDec[4];
    char    szBin[9];

} BYTE2ASCIIDEF;


#ifndef PVOID
typedef	void	*PVOID;
#endif

#ifdef WIN32
#include <windows.h>
#define MYHANDLE HGLOBAL
#else
#define MYHANDLE int
#endif


#define LINKEDLIST_DATASIZE_UNKNOWN		-1;


//****************************************************************************
//* Function definition
//*

typedef struct 
{
	MYHANDLE	hSelf;

	PVOID		pPrev;
	PVOID		pNext;

	PVOID		pData;

	int			nDataSize;

} VOIDLISTDEF;

#define PVOIDLISTDEF	VOIDLISTDEF *

typedef struct 
{
	PVOIDLISTDEF  	pStartOfList;

	PVOIDLISTDEF  	pEndOfList;

} LNKLSTBASEDEF;

#define PLNKLSTBASEDEF LNKLSTBASEDEF *



//****************************************************************************
//* Function defs
//*

int Greater (int nVal1, int nVal2);
int Lesser (int nVal1, int nVal2);

VOID WCopy (LPWORD pTarget, LPWORD pSource, DWORD dwCnt);
VOID WSet (LPWORD pTarget, WORD wValue, DWORD dwCnt);

VOID DWCopy (LPDWORD pTarget, LPDWORD pSource, WORD dwCnt);
VOID DWSet (LPDWORD pTarget, DWORD dwValue, WORD dwCnt);

VOID SwapShort (LPWORD pWord);
VOID SwapLong (LPDWORD pLong);

WORD FlipShort (WORD wValue);
DWORD FlipLong (DWORD dwValue);

int Byte2Ascii (int nValue, PSTR pStr, int nFormat);

PVOIDLISTDEF CALLBACK LinkListAlloc ();
VOID CALLBACK LinkListFree (PVOIDLISTDEF pVOIDLIST);

PVOIDLISTDEF CALLBACK	LinkListAppend
						(
							PLNKLSTBASEDEF pLnkLstBase
						);

PVOIDLISTDEF CALLBACK	LinkListInsert
						(
							PLNKLSTBASEDEF pLnkLstBase,
							PVOIDLISTDEF pCurrStruct
						);

bool CALLBACK	LinkListDel 
				(
					PLNKLSTBASEDEF pLnkLstBase,
					PVOIDLISTDEF pCurrStruct
				);

bool CALLBACK LinkListClear (PLNKLSTBASEDEF pLnkLstBase);


#endif  // _UTILS_H
