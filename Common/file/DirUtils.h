//*****************************************************************************
//* DirUtils.cpp - Directory utility functions header file
//*


#ifndef _DIRUTILS_H_
#define _DIRUTILS_H_


#include <string>


int GetDirPath(LPSTR pTargetPath, LPCSTR pSourcePath, int MaxSourceLen);
std::string GetDirPath(const std::string &sSource);

int GetDirPathLen(LPCSTR pSourcePath);
int GetDirPathLen(const std::string &sSource);

int GetPathFileName(LPSTR pFilename, LPCSTR pSourcePath);
std::string GetPathFileName(const std::string &sSource);

int GetPathExt(LPSTR pExt, LPCSTR pSourcePath);
std::string GetPathExt(const std::string &sSource);

int ComparePathExt(LPCSTR pSourcePath, LPCSTR pExt);
int ComparePathExt(const std::string &sSource, const std::string &sExt);

#endif
