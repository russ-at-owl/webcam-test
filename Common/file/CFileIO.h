//*****************************************************************************
//* FILE: 		CFileIO.h
//*
//* DESCRIP:	Header file for a class to manage file IO
//*
//*



#ifndef _FILEIO_H_
#define _FILEIO_H_


#ifdef WIN32

#define FOLDER_DELIMITOR	'\\'

#else

#define FOLDER_DELIMITOR	'/'

#endif


#ifndef MAX_LINE_LEN
#define MAX_LINE_LEN		10240
#endif


enum FIO_STATE
{
	FIOS_None = 0,
	FIOS_Open
};



enum FIO_TYPE
{
	FIOT_None = 0,
	FIOT_StdOut,
	FIOT_StdErr,
	FIOT_Filename
};


enum FIO_FLAG
{
	FIOF_None = 0,
	FIOF_Text,
	FIOF_Binary
};


enum FIO_MODE
{
	FIOM_None = 0,
	FIOM_Read,
	FIOM_ReadUpdate,
	FIOM_Write,
	FIOM_WriteUpdate,
	FIOM_Append,
	FIOM_AppendUpdate,
};


enum FIO_ERROR
{
	FIOE_None = 0,
	FIOE_FileNotOpen,
	FIOE_InvalidParam,
	FIOE_InvalidFileDir,
	FIOE_InvalidFileName,
	FIOE_InvalidFileMode,
	FIOE_InvalidFileHandle,
	FIOE_ProblemOpeningFile,
	FIOE_ProblemClosingFile,
	FIOE_FilePosError,
	FIOE_FilePosNotAllowed,
	FIOE_FileLenError,
	FIOE_ReadError,
	FIOE_WriteError,
	FIOE_EndOfFile = 9999
};



class CFileIO
{
private:

	bool		m_bFileDirSet;
	bool		m_bFileNameSet;
	bool		m_bFileModeSet;

	FIO_TYPE	m_FIO_Type;

	FIO_STATE	m_FIO_State;

	FIO_MODE	m_FIO_Mode;

	FIO_FLAG	m_FIO_CharMode;

	FIO_ERROR 	m_FIO_LastError;

public:
	CFileIO(void);
    ~CFileIO(void);
	
	// application settings variables

	TCHAR		m_szFileDir[MAX_PATH];
	TCHAR		m_szFileName[MAX_PATH];

	TCHAR		m_szFileMode[10];

	FILE		*m_FileHandle;

	// application settings functions

	bool SetFileDir(TCHAR *pDir);
	bool SetFileName(TCHAR *pName);
	bool SetFileMode(TCHAR *pMode);
	
	bool Open();
	bool Open(TCHAR *pName);
	bool OpenStdOut();
	bool OpenStdErr();

	bool Close();

	FIO_TYPE GetFileType()
	{
		return m_FIO_Type;
	};

	FIO_FLAG GetCharMode()
	{
		return m_FIO_CharMode;
	};

	FIO_STATE GetFileState()
	{
		return m_FIO_State;
	};

	LONG GetFileLen();

	bool SetFilePointer(LONG lPos);

	LONG GetFilePointer();

	bool ReadLine(TCHAR *pBuf, LONG nMax);

	bool ReadStr(std::string &sText);

	bool ReadBuffer(TCHAR *pBuf, LONG &nLen);

	bool ReadFile(TCHAR *pBuf);
	bool ReadFile(std::string &sText);

	bool WriteLine(TCHAR *pBuf);

	bool WriteStr(const std::string &sText);

	bool WriteBuffer(TCHAR *pBuf, LONG nLen);

	bool CheckEOF();

	bool CheckExists(TCHAR *pFName = NULL);

	void Flush()
	{
		if (m_FIO_Type == FIOT_Filename)
		{
			fflush(m_FileHandle);
		}

	};

	bool Delete(TCHAR *pFName = NULL);

	bool Rename(TCHAR *pNewName, bool bUseFileDir = false, bool bUpdateName = false);

	bool Copy(TCHAR *pTrgtName, TCHAR *pSrcName = NULL);

};



#endif  // _FILEIO_H_

