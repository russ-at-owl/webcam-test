




typedef struct 
{
    pthread_rwlock_t rwlock;
    TreeNode *root_node;
} Tree;

void Tree_init(Tree *tree) 
{
    pthread_rwlock_init(&tree->rwlock, NULL);
    tree->root_node = NULL;
}


int32_t Tree_insert(Tree *tree, int32_t data) 
{
    pthread_rwlock_wrlock(&tree->rwlock);
    int32_t ret = _insertNode(&tree->root_node, data);
    pthread_rwlock_unlock(&tree->rwlock);
    return ret;
}


int32_t Tree_locate(Tree *tree) 
{
    pthread_rwlock_rdlock(&tree->rwlock);
    int32_t ret = _locateNode(&tree->root_node);
    pthread_rwlock_unlock(&tree->rwlock);
    return ret;
}


void Tree_destroy(Tree *tree) 
{
    pthread_rwlock_destroy(&tree->rwlock);
    // yada yada
}

