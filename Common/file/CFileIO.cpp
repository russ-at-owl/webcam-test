//*****************************************************************************
//* FILE: 		CFileIO.cpp :
//*
//* DESCRIP:	Defines the class for managing file IO
//*
//*

#pragma warning( disable : 4996 ) 



#ifdef WIN32
#include <windows.h>
#endif


#include <string>

//#include "fileUtils.h"
//#include "utils.h"

#include "CFileIO.h"




CFileIO::CFileIO()
{
	// initialize all settings variables


	m_bFileDirSet = false;
	m_bFileNameSet = false;
	m_bFileModeSet = false;

	m_FIO_Type = FIOT_None;

	m_FIO_State = FIOS_None;

	m_FIO_Mode = FIOM_None;

	m_FIO_CharMode = FIOF_None;

	m_FIO_LastError = FIOE_None;

	m_FileHandle = NULL;

	ZeroMemory(m_szFileDir, sizeof(m_szFileDir));
	ZeroMemory(m_szFileName, sizeof(m_szFileName));
	ZeroMemory(m_szFileMode, sizeof(m_szFileMode));

}


CFileIO::~CFileIO()
{
	if (m_FIO_Mode != FIOM_None)
	{
		Close();
	}
}

bool CFileIO::SetFileDir(TCHAR *pDir)
{
	if (pDir == NULL)
	{
		m_FIO_LastError = FIOE_InvalidParam;
		return false;
	}

	ZeroMemory(m_szFileDir, sizeof(m_szFileDir));

	strcpy(m_szFileDir, pDir);

	m_bFileDirSet = true;

	return true;
}


bool CFileIO::SetFileName(TCHAR *pName)
{
	if (pName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidParam;
		return false;
	}

	ZeroMemory(m_szFileName, sizeof(m_szFileName));

	strcpy(m_szFileName, pName);

	m_bFileNameSet = true;

	return true;
}


bool CFileIO::SetFileMode(TCHAR *pMode)
{
	if (pMode == NULL)
	{
		m_FIO_LastError = FIOE_InvalidParam;
		return false;
	}

	ZeroMemory(m_szFileMode, sizeof(m_szFileMode));

	strcpy(m_szFileMode, pMode);

	m_bFileModeSet = true;

	return true;
}


bool CFileIO::Open()
{
	if (m_bFileNameSet == false)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	if (m_bFileModeSet == false)
	{
		m_FIO_LastError = FIOE_InvalidFileMode;
		return false;
	}

	if (m_FIO_Type != FIOT_None)
	{
		Close();
	}

	FILE *fHandle = NULL;

	INT nNameLen = strlen(m_szFileName);

	if (nNameLen < 1)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	INT nDirLen = 0;

	if (m_bFileDirSet == true)
	{
		nDirLen = strlen(m_szFileDir);

		if (nDirLen < 1)
		{
			m_FIO_LastError = FIOE_InvalidFileDir;
			return false;
		}
	}

	int nModeLen = strlen(m_szFileMode);

	if (strncmp(m_szFileMode, "r", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_Read;
		m_FIO_CharMode = FIOF_Text;
	}
	else if (strncmp(m_szFileMode, "rb", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_Read;
		m_FIO_CharMode = FIOF_Binary;
	}
	else if (strncmp(m_szFileMode, "r+", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_ReadUpdate;
		m_FIO_CharMode = FIOF_Text;
	}
	else if (strncmp(m_szFileMode, "rb+", nModeLen) == 0 || strncmp(m_szFileMode, "r+b", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_ReadUpdate;
		m_FIO_CharMode = FIOF_Binary;
	}
	else if (strncmp(m_szFileMode, "w", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_Write;
		m_FIO_CharMode = FIOF_Text;
	}
	else if (strncmp(m_szFileMode, "wb", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_Write;
		m_FIO_CharMode = FIOF_Binary;
	}
	else if (strncmp(m_szFileMode, "w+", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_WriteUpdate;
		m_FIO_CharMode = FIOF_Text;
	}
	else if (strncmp(m_szFileMode, "wb+", nModeLen) == 0 || strncmp(m_szFileMode, "w+b", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_WriteUpdate;
		m_FIO_CharMode = FIOF_Binary;
	}
	else if (strncmp(m_szFileMode, "a", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_Append;
		m_FIO_CharMode = FIOF_Text;
	}
	else if (strncmp(m_szFileMode, "ab", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_Append;
		m_FIO_CharMode = FIOF_Binary;
	}
	else if (strncmp(m_szFileMode, "a+", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_AppendUpdate;
		m_FIO_CharMode = FIOF_Text;
	}
	else if (strncmp(m_szFileMode, "ab+", nModeLen) == 0 || strncmp(m_szFileMode, "a+b", nModeLen) == 0)
	{
		m_FIO_Mode = FIOM_AppendUpdate;
		m_FIO_CharMode = FIOF_Binary;
	}
	else
	{
		m_FIO_LastError = FIOE_InvalidFileMode;
		return false;
	}

	if (nDirLen > 0)
	{
		CHAR szFilePath[MAX_PATH];

		ZeroMemory(szFilePath, sizeof(szFilePath));

		strcpy(szFilePath, m_szFileDir);

		if (szFilePath[nDirLen - 1] != FOLDER_DELIMITOR && szFilePath[nDirLen - 1] != ':')
		{
			szFilePath[nDirLen] = FOLDER_DELIMITOR;
			nDirLen++;
		}

		strcpy((szFilePath + nDirLen), m_szFileName);

		fHandle = fopen((const CHAR*) szFilePath, m_szFileMode);
	}
	else
	{
		fHandle = fopen((const CHAR*) m_szFileName, m_szFileMode);
	}

	if (fHandle == NULL)
	{
		m_FIO_LastError = FIOE_ProblemOpeningFile;
		return false;
	}

	m_FileHandle = fHandle;

	m_FIO_Type = FIOT_Filename;

	m_FIO_State = FIOS_Open;

	return true;
}


bool CFileIO::Open(TCHAR *pName)
{
	if (pName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidParam;
		return false;
	}

	bool bStatus = false;

	bStatus = SetFileName(pName);

	if (bStatus == false)
	{
		return false;
	}

	bStatus = Open();

	return bStatus;
}


bool CFileIO::OpenStdOut()
{
	if (m_bFileNameSet == true)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	if (m_bFileModeSet == true)
	{
		m_FIO_LastError = FIOE_InvalidFileMode;
		return false;
	}

	if (m_FIO_Type != FIOT_None)
	{
		Close();
	}

	m_FileHandle = stdout;

	m_FIO_Type = FIOT_StdOut;

	m_FIO_State = FIOS_Open;

	return true;
}


bool CFileIO::OpenStdErr()
{
	if (m_bFileNameSet == true)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	if (m_bFileModeSet == true)
	{
		m_FIO_LastError = FIOE_InvalidFileMode;
		return false;
	}

	if (m_FIO_Type != FIOT_None)
	{
		Close();
	}

	m_FileHandle = stderr;

	m_FIO_Type = FIOT_StdErr;

	m_FIO_State = FIOS_Open;

	return true;
}


bool CFileIO::Close()
{
	if (m_FIO_State == FIOS_None  || m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	if (m_FIO_Type == FIOT_Filename)
	{
		fflush(m_FileHandle);

		INT nRet = 0;

		nRet = fclose(m_FileHandle);

		if (nRet != 0)
		{
			m_FIO_LastError = FIOE_ProblemClosingFile;
			return false;
		}
	}

	m_FileHandle = NULL;

	m_FIO_Type = FIOT_None;

	m_FIO_State = FIOS_None;

	return true;
}


bool CFileIO::SetFilePointer(LONG lPos)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	if (m_FIO_Type == FIOT_StdOut || m_FIO_Type == FIOT_StdErr)
	{
		m_FIO_LastError = FIOE_FilePosError;
		return false;
	}

	if (m_FIO_Mode == FIOM_Append)
	{
		m_FIO_LastError = FIOE_FilePosNotAllowed;
		return false;
	}

	INT nRet = 0;

	if (lPos == -1)
	{
		nRet = fseek(m_FileHandle, 0, SEEK_END);
	}
	else
	{
		nRet = fseek(m_FileHandle, lPos, SEEK_SET);
	}

	if (nRet != 0)
	{
		m_FIO_LastError = FIOE_FilePosError;
		return false;
	}

	return true;
}


LONG CFileIO::GetFilePointer()
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return -1;
	}

	if (m_FIO_Type == FIOT_StdOut || m_FIO_Type == FIOT_StdErr)
	{
		m_FIO_LastError = FIOE_FilePosError;
		return -1;
	}

	LONG lFileLen = ftell(m_FileHandle);

	if (lFileLen < 0)
	{
		m_FIO_LastError = FIOE_FilePosError;
		return false;
	}

	return lFileLen;
}


LONG CFileIO::GetFileLen()
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return -1;
	}

	if (m_FIO_Type == FIOT_StdOut || m_FIO_Type == FIOT_StdErr)
	{
		m_FIO_LastError = FIOE_FileLenError;
		return -1;
	}

	INT nRet = 0;

	nRet = fseek(m_FileHandle, 0L, SEEK_END);

	if (nRet != 0)
	{
		m_FIO_LastError = FIOE_FilePosError;
		return -1;
	}

	LONG lFileLen = ftell(m_FileHandle);

	if (lFileLen < 0)
	{
		m_FIO_LastError = FIOE_FilePosError;
		return false;
	}

	return lFileLen;
}


bool CFileIO::ReadLine(TCHAR *pBuf, LONG nMax)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	CHAR *pRet = NULL;

	pRet = fgets(pBuf, nMax, m_FileHandle);

	if (pRet == NULL)
	{
		INT nRet = 0;

		nRet = feof(m_FileHandle);
		if (nRet != 0)
		{
			m_FIO_LastError = FIOE_EndOfFile;
		}
		else
		{
			m_FIO_LastError = FIOE_ReadError;
		}

		return false;
	}

	return true;
}


bool CFileIO::ReadStr(std::string &sText)
{
	long lMax = sText.max_size();

	if (lMax < 1)
	{
		lMax = MAX_LINE_LEN;
	}

	char *pBuf = (char *) calloc(lMax, sizeof(char));
	if (pBuf == NULL)
	{
		return false;
	}

	if (ReadLine(pBuf, lMax) == true)
	{
		sText.assign(pBuf);

		free(pBuf);

		return true;
	}

	free(pBuf);

	return false;
}


bool CFileIO::ReadBuffer(TCHAR *pBuf, LONG &lIoLen)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	LONG lStatus =		0;
	LONG lReadLen =		lIoLen;
	LONG lBytesRead =	0;

	char *pReadPtr = pBuf;

	while (1)
	{
		lStatus = fread((void *) pReadPtr, 1, lReadLen, m_FileHandle);

		if (lStatus > 0)
		{
			lBytesRead += lStatus;

			if (lBytesRead == lIoLen)
			{
				break;
			}
		}

		INT nRet = 0;

		nRet = feof(m_FileHandle);
		if (nRet != 0)
		{
			m_FIO_LastError = FIOE_EndOfFile;

			lIoLen = lBytesRead; 

			return true;
		}
		else if (lStatus < 1)
		{
			m_FIO_LastError = FIOE_ReadError;

			return false;
		}

		lReadLen = (lIoLen - lBytesRead); 

		pReadPtr = (((char *) pBuf) + lBytesRead);
	}

	return true;
}


bool CFileIO::ReadFile(TCHAR *pBuf)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	LONG lReadLen =		GetFileLen();

	if (ReadBuffer(pBuf, lReadLen) != true)
	{
		return false;
	}

	return true;
}


bool CFileIO::ReadFile(std::string &sText)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	int nMax = sText.max_size();
	
	LONG lReadLen =	GetFileLen();

	if (lReadLen > nMax)
	{
		return false;
	}

	char *pBuf = (char *) calloc(lReadLen, sizeof(char));
	if (pBuf == NULL)
	{
		return false;
	}

	if (ReadBuffer(pBuf, lReadLen) != true)
	{
		return false;
	}

	return true;
}


bool CFileIO::WriteLine(TCHAR *pBuf)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	INT nStatus = 0;

	nStatus = fputs((const char * ) pBuf, m_FileHandle);

	if (nStatus < 1)
	{
		m_FIO_LastError = FIOE_WriteError;
		return false;
	}

	return true;
}


bool CFileIO::WriteStr(const std::string &sText)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	INT nStatus = 0;

	INT nLen = sText.length();

	nStatus = fwrite((void *) (sText.c_str()), sizeof(TCHAR), nLen, m_FileHandle);

	if (nStatus != nLen)
	{
		m_FIO_LastError = FIOE_WriteError;
		return false;
	}

	return true;
}


bool CFileIO::WriteBuffer(TCHAR *pBuf, LONG nLen)
{
	if (m_FIO_Type == FIOT_None)
	{
		m_FIO_LastError = FIOE_FileNotOpen;
		return false;
	}

	INT nStatus = 0;

	nStatus = fwrite((void *) pBuf, sizeof(TCHAR), nLen, m_FileHandle);

	if (nStatus != nLen)
	{
		m_FIO_LastError = FIOE_WriteError;
		return false;
	}

	return true;
}


bool CFileIO::CheckEOF()
{
	int nStatus = feof(m_FileHandle);

	if (nStatus != 0)
	{
		m_FIO_LastError = FIOE_EndOfFile;

		return true;
	}

	return false;
}


//bool CFileIO::CheckExists(TCHAR *pFName = NULL)
bool CFileIO::CheckExists(TCHAR *pFName)
{
	if (m_bFileNameSet == false && pFName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	bool bRet = false;

	if (m_FIO_Type != FIOT_None)
	{
		Close();
	}

	INT nDirLen = 0;

	if (m_bFileDirSet == true)
	{
		nDirLen = strlen(m_szFileDir);

		if (nDirLen < 1)
		{
			m_FIO_LastError = FIOE_InvalidFileDir;
			return false;
		}
	}

	if (nDirLen > 0)
	{
		CHAR szFilePath[MAX_PATH];

		ZeroMemory(szFilePath, sizeof(szFilePath));

		strcpy(szFilePath, m_szFileDir);

		if (szFilePath[nDirLen - 1] != FOLDER_DELIMITOR)
		{
			szFilePath[nDirLen] = FOLDER_DELIMITOR;
			nDirLen++;
		}

		if (pFName == NULL)
		{
			strcpy((szFilePath + nDirLen), m_szFileName);
		}
		else
		{
			strcpy((szFilePath + nDirLen), pFName);
		}

#ifdef WIN32
		HANDLE hPF = ::CreateFile(szFilePath, 0, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
		if (hPF == INVALID_HANDLE_VALUE)
		{
			bRet = false;
		}
		else
		{
			bRet = true;
		}

		::CloseHandle(hPF);
#else
		FILE *fHandle = fopen((const CHAR*) szFilePath, "r");
		if (fHandle == NULL)
		{
			bRet = false;
		}
		else
		{
			bRet = true;
		}

		fclose(fHandle);
#endif
	}
	else
	{
		if (pFName == NULL)
		{
#ifdef WIN32
			HANDLE hPF = ::CreateFile(m_szFileName, 0, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
			if (hPF == INVALID_HANDLE_VALUE)
			{
				bRet = false;
			}
			else
			{
				bRet = true;
			}

			::CloseHandle(hPF);
#else
			FILE *fHandle = fopen((const CHAR*) m_szFileName, "r");
			if (fHandle == NULL)
			{
				bRet = false;
			}
			else
			{
				bRet = true;
			}

			fclose(fHandle);
#endif
		}
		else
		{
			#ifdef WIN32
			HANDLE hPF = ::CreateFile(m_szFileName, 0, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
			if (hPF == INVALID_HANDLE_VALUE)
			{
				bRet = false;
			}
			else
			{
				bRet = true;
			}

			::CloseHandle(hPF);
#else
			FILE *fHandle = fopen((const CHAR*) pFName, "r");
			if (fHandle == NULL)
			{
				bRet = false;
			}
			else
			{
				bRet = true;
			}

			fclose(fHandle);
#endif
		}
	}

	return bRet;
}


//bool CFileIO::Delete(TCHAR *pFName = NULL)
bool CFileIO::Delete(TCHAR *pFName)
{
	if (m_bFileNameSet == false && pFName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	int nDirLen = 0;
	int nStatus = 0;

	if (m_bFileDirSet == true)
	{
		nDirLen = strlen(m_szFileDir);

		if (nDirLen < 1)
		{
			m_FIO_LastError = FIOE_InvalidFileDir;
			return false;
		}
	}

	if (nDirLen > 0)
	{
		CHAR szFilePath[MAX_PATH];

		ZeroMemory(szFilePath, sizeof(szFilePath));

		strcpy(szFilePath, m_szFileDir);

		if (m_szFileDir[nDirLen - 1] != FOLDER_DELIMITOR)
		{
			szFilePath[nDirLen] = FOLDER_DELIMITOR;
			nDirLen++;
		}

		if (pFName != NULL)
		{
			strcpy((szFilePath + nDirLen), pFName);
		}
		else
		{
			strcpy((szFilePath + nDirLen), m_szFileName);
		}

#ifdef WIN32
		nStatus = ::DeleteFile(szFilePath);
#else
		nStatus = remove(szFilePath);
#endif
	}
	else
	{
		if (pFName != NULL)
		{
#ifdef WIN32
			nStatus = ::DeleteFile(pFName);
#else
			nStatus = remove(pFName);
#endif
		}
		else
		{
#ifdef WIN32
			nStatus = ::DeleteFile(m_szFileName);
#else
			nStatus = remove(m_szFileName);
#endif
		}
	}

#ifdef WIN32
	if (nStatus == 0)
#else
	if (nStatus != 0)
#endif
	{
		return false;
	}

	return true;
};


//bool CFileIO::Rename(TCHAR *pNewName, bool bUseFileDir = false, bool bUpdateName = false)
bool CFileIO::Rename(TCHAR *pNewName, bool bUseFileDir, bool bUpdateName)
{
	if (m_bFileNameSet == false || pNewName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	int nDirLen = 0;
	int nStatus = 0;

	if (m_bFileDirSet == true)
	{
		nDirLen = strlen(m_szFileDir);

		if (nDirLen < 1)
		{
			m_FIO_LastError = FIOE_InvalidFileDir;
			return false;
		}
	}

	if (nDirLen > 0)
	{
		CHAR szOldPath[MAX_PATH];
		CHAR szNewPath[MAX_PATH];

		ZeroMemory(szOldPath, sizeof(szOldPath));
		ZeroMemory(szNewPath, sizeof(szNewPath));

		strcpy(szOldPath, m_szFileDir);

		if (bUseFileDir == true)
		{
			strcpy(szNewPath, m_szFileDir);
		}

		if (m_szFileDir[nDirLen - 1] != FOLDER_DELIMITOR)
		{
			szOldPath[nDirLen] = FOLDER_DELIMITOR;
		
			if (bUseFileDir == true)
			{
				szNewPath[nDirLen] = FOLDER_DELIMITOR;
			}

			nDirLen++;
		}

		strcpy((szOldPath + nDirLen), m_szFileName);

		if (bUseFileDir == true)
		{
			strcpy((szNewPath + nDirLen), pNewName);

#ifdef WIN32
			nStatus = ::MoveFile(szOldPath, szNewPath);
#else
			nStatus = rename(szOldPath, szNewPath);
#endif
		}
		else
		{
#ifdef WIN32
			nStatus = ::MoveFile(szOldPath, pNewName);
#else
			nStatus = rename(szOldPath, pNewName);
#endif
		}
	}
	else
	{
#ifdef WIN32
		nStatus = ::MoveFile(m_szFileName, pNewName);
#else
		nStatus = rename(m_szFileName, pNewName);
#endif
	}

#ifdef WIN32
	if (nStatus == 0)
#else
	if (nStatus != 0)
#endif
	{
		return false;
	}

	if (bUpdateName == true)
	{
		strcpy(m_szFileName, pNewName);
	}

	return true;
};


//bool CFileIO::Copy(TCHAR *pTrgtName, TCHAR *pSrcName = NULL)
bool CFileIO::Copy(TCHAR *pTrgtName, TCHAR *pSrcName)
{
	if (m_bFileNameSet == false && pSrcName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidFileName;
		return false;
	}

	if (pTrgtName == NULL)
	{
		m_FIO_LastError = FIOE_InvalidParam;
		return false;
	}

	FILE *fTrgtHandle = NULL;
	FILE *fSrcHandle = NULL;

	int nDirLen = 0;

	if (m_bFileDirSet == true)
	{
		nDirLen = strlen(m_szFileDir);

		if (nDirLen < 1)
		{
			m_FIO_LastError = FIOE_InvalidFileDir;
			return false;
		}
	}

	if (nDirLen > 0)
	{
		CHAR szTrgtPath[MAX_PATH];
		CHAR szSrcPath[MAX_PATH];

		ZeroMemory(szTrgtPath, sizeof(szTrgtPath));
		ZeroMemory(szSrcPath, sizeof(szSrcPath));

		strcpy(szTrgtPath, m_szFileDir);
		strcpy(szSrcPath, m_szFileDir);

		if (m_szFileDir[nDirLen - 1] != FOLDER_DELIMITOR)
		{
			szTrgtPath[nDirLen] = FOLDER_DELIMITOR;
			szSrcPath[nDirLen] = FOLDER_DELIMITOR;
			nDirLen++;
		}

		strcpy((szTrgtPath + nDirLen), pTrgtName);

		if (pSrcName != NULL)
		{
			strcpy((szSrcPath + nDirLen), pSrcName);
		}
		else
		{
			strcpy((szSrcPath + nDirLen), m_szFileName);
		}

		fTrgtHandle = fopen((const CHAR*) szTrgtPath, "w");

		fSrcHandle = fopen((const CHAR*) szSrcPath, "r");
	}
	else
	{
		fTrgtHandle = fopen((const CHAR*) pTrgtName, "w");

		if (pSrcName != NULL)
		{
			fSrcHandle = fopen((const CHAR*) pSrcName, "r");
		}
		else
		{
			fSrcHandle = fopen((const CHAR*) m_szFileName, "r");
		}
	}

	if (fTrgtHandle == NULL || fSrcHandle == NULL)
	{
		m_FIO_LastError = FIOE_FileNotOpen;

		//fclose(fSrcHandle);
		//fclose(fTrgtHandle);

		return false;
	}

	int nReadSize = 0;

#ifndef BUFSIZ
#define BUFSIZ	8192 
#endif

	char szCpyBuf[BUFSIZ];

	while (nReadSize = fread(szCpyBuf, sizeof(char), sizeof(szCpyBuf), fSrcHandle)) 
	{
        fwrite(szCpyBuf, 1, nReadSize, fTrgtHandle);
    }

	fclose(fSrcHandle);
	fclose(fTrgtHandle);

	return true;
}

