//*********************************************************************
//*
//*  CIniIO.h
//*
//*		"INI" file management class
//*
//*
//*  Written by: RUSS BARKER
//*



#ifndef _CINIIO_H_
#define _CINIIO_H_

#include <string>

#define REG_MAX_KEY_LEN				256





class CIniParamIO
{
	std::string	m_sFileName;

	std::string	m_sSecName;

	std::string	m_sKeyName;

	long		m_lLastSecOffset;

public:

	CIniParamIO();
	CIniParamIO(TCHAR *pFName);
	CIniParamIO(const std::string &sFName);

	bool SetFileName (TCHAR *pFName)
	{
		m_sFileName.assign(pFName);

		return TRUE;
	}
	bool SetFileName (const std::string &sFName)
	{
		m_sFileName = sFName;

		return TRUE;
	}

	bool SetSecName (TCHAR *pSec)
	{
		m_sSecName.assign(pSec);

		return TRUE;
	}
	bool SetSecName (const std::string &sSec)
	{
		m_sSecName = sSec;

		return TRUE;
	}

	bool SetKeyName (TCHAR *pKey)
	{
		m_sKeyName.assign(pKey);

		return TRUE;
	}
	bool SetKeyName (const std::string &sKey)
	{
		m_sKeyName = sKey;

		return TRUE;
	}

	bool GetFirstSecName(char* pSec, int nMax);
	bool GetFirstSecName(std::string &sSec);

	bool GetNextSecName(char* pSec, int nMax);
	bool GetNextSecName(std::string &sSec);

	bool GetStr (TCHAR *pKey, char* pTgtStr, char* szDefault);
	bool GetStr (const std::string &sKey, std::string &sTgtStr, const std::string &sDefault)
	{
		char szTmp[MAX_PATH];
		memset(szTmp, 0, sizeof(szTmp));
		sTgtStr = "";
		bool bRet = GetStr((TCHAR *) sKey.c_str(), szTmp, (char *) sDefault.c_str());
		sTgtStr.assign(szTmp);
		return bRet;
	}
	bool GetStr (char* pTgtStr, char* pDefault);
	bool GetStr (std::string &sTgtStr, const std::string &sDefault)
	{
		char szTmp[MAX_PATH];
		memset(szTmp, 0, sizeof(szTmp));
		sTgtStr = "";
		bool bRet = GetStr(szTmp, (char *) sDefault.c_str());
		sTgtStr.assign(szTmp);
		return bRet;
	}

	bool SetStr (TCHAR *pKey, char* pStr);
	bool SetStr (const std::string &sKey, const std::string &sStr)
	{
		return SetStr((TCHAR *) sKey.c_str(), (char *) sStr.c_str());
	}
	bool SetStr (char* pStr);
	bool SetStr (const std::string &sStr)
	{
		return SetStr((char *) sStr.c_str());
	}

	bool GetInt (TCHAR *pKey, int *pVar, int iDefault);
	bool GetInt (const std::string &sKey, int *pVar, int iDefault)
	{
		return GetInt((TCHAR *) sKey.c_str(), pVar, iDefault);
	}
	bool GetInt (int *pVar, int iDefault);

	bool SetInt (TCHAR *pKey, int iValue);
	bool SetInt (const std::string &sKey, int iValue)
	{
		return SetInt((TCHAR *) sKey.c_str(), iValue);
	}
	bool SetInt (int iValue);

	bool GetBool (TCHAR *pKey, bool *pVar, bool bDefault);
	bool GetBool (const std::string &sKey, bool *pVar, bool bDefault)
	{
		return GetBool((TCHAR *) sKey.c_str(), pVar, bDefault);
	}
	bool GetBool (bool *pVar, bool bDefault);

	bool SetBool (TCHAR *pKey, bool bValue);
	bool SetBool (const std::string &sKey, bool bValue)
	{
		return SetBool((TCHAR *) sKey.c_str(), bValue);
	}
	bool SetBool (bool bValue);

};





#endif  // _CINIIO_H_
