//*********************************************************************
//*
//*  AppLog.h
//*
//*		Applicarion log file header
//*


#ifndef _APPLOG_H_
#define _APPLOG_H_

#include <string>


#ifndef EXPORT_DEF
#define EXPORT_DEF 
#endif

EXPORT_DEF void OutputDebugStr(std::string sText);




#define CONDITION_STRING_SIZE	10


extern int g_nLogLevel;

extern bool g_bConsoleLogging;

extern bool g_bTruncateLog;

#ifdef WIN32
extern HANDLE g_hLogFile;
#else
extern FILE * g_hLogFile;
#endif


//namespace AppLog 
//{

void InitAppLog();

void SetLogLevel(int nLogLevel);

void SetConsoleLogging(bool bVal);

void SetTruncateLog(bool bVal);

bool OpenLog(std::string sLogfileName);
bool OpenLog(LPSTR szLogfileName);

void CloseLog();

void LogWrite(int nLogLevel, LPSTR szLogString, ...);

void LogWrite(int nLogLevel, LPSTR szConditionString, LPSTR szLogString, ...);

void LogWrite(int nLogLevel, const std::string &sLogString);

void LogWrite(int nLogLevel, LPSTR szConditionString, const std::string &sLogString);

//};  // namespace AppLog

#endif  // _APPLOG_H_



