//*********************************************************************
//*
//*  CIniIO.cpp
//*
//*      "INI" file management class
//*
//*
//*  Written by: RUSS BARKER
//*



#pragma warning(disable : 4800)
#pragma warning(disable : 4996) 

#include "Windows.h"

#include <string>

#include "CIniIO.h"

#include "CFileIO.h"




//*********************************************************************
//*
//*  CIniParamIO:: ... constructor
//*

CIniParamIO::CIniParamIO ()
{
	m_sFileName = "";

	m_sSecName = "";

	m_sKeyName = "";

	m_lLastSecOffset = -1;
}

CIniParamIO::CIniParamIO (TCHAR *pFName)
{
	m_sFileName.assign(pFName);

	m_sSecName = "";

	m_sKeyName = "";

	m_lLastSecOffset = -1;
}

CIniParamIO::CIniParamIO (const std::string &sFName)
{
	m_sFileName = sFName;

	m_sSecName = "";

	m_sKeyName = "";

	m_lLastSecOffset = -1;
}


//*********************************************************************
//*
//*  CIniParamIO::GetFirstSecName
//*

bool CIniParamIO::GetFirstSecName (TCHAR *pSec, int nMax)
{
	char *pBuf = NULL;

	try
	{
		CFileIO	file;

		file.SetFileName((TCHAR *) (m_sFileName.c_str()));

		file.SetFileMode("r");

		bool bStatus;

		bStatus = file.Open();

		if (bStatus == false)
		{
			return false;
		}

		LONG lSize = file.GetFileLen();

		pBuf = (char *) calloc((lSize + 2), sizeof(char));

		if (pBuf == NULL)
		{
			file.Close();

			return false;
		}

		file.SetFilePointer(0);

		bStatus = file.ReadBuffer(pBuf, lSize);

		file.Close();

		if (bStatus == false)
		{
			free(pBuf);

			return false;
		}

		long lStartPos = -1;
		long lEndPos = -1;

		for (long x = 0; x < lSize; x++)
		{
			if (*(pBuf + x) == '[')
			{
				lStartPos = (x + 1);

				continue;
			}

			if (*(pBuf + x) == ']')
			{
				lEndPos = (x - 1);

				break;
			}
		}

		if (lStartPos != -1 && lEndPos > lStartPos)
		{
			long lLen = (lEndPos - lStartPos);

			if (nMax < lLen)
			{
				lLen = nMax;
			}

			strncpy(pSec, (pBuf + lStartPos), (int) lLen);

			free(pBuf);

			m_lLastSecOffset = (lEndPos + 2);

			SetSecName(pSec);

			return true;
		}
	}
	catch(...)
	{

	}

	if (pBuf != NULL)
	{
		free(pBuf);
	}

	return false;
}

bool CIniParamIO::GetFirstSecName (std::string &sSec)
{
	char *pBuf = NULL;

	try
	{
		CFileIO	file;

		file.SetFileName((TCHAR *) (m_sFileName.c_str()));

		file.SetFileMode("r");

		bool bStatus;

		bStatus = file.Open();

		if (bStatus == false)
		{
			return false;
		}

		LONG lSize = file.GetFileLen();

		pBuf = (char *) calloc((lSize + 2), sizeof(char));

		if (pBuf == NULL)
		{
			file.Close();

			return false;
		}

		file.SetFilePointer(0);

		bStatus = file.ReadBuffer(pBuf, lSize);

		file.Close();

		if (bStatus == false)
		{
			free(pBuf);

			return false;
		}

		long lStartPos = -1;
		long lEndPos = -1;

		for (long x = 0; x < lSize; x++)
		{
			if (*(pBuf + x) == '[')
			{
				lStartPos = (x + 1);

				continue;
			}

			if (*(pBuf + x) == ']')
			{
				lEndPos = (x - 1);

				break;
			}
		}

		if (lStartPos != -1 && lEndPos > lStartPos)
		{
			long lLen = (lEndPos - lStartPos);

			sSec.assign((pBuf + lStartPos), (int) lLen);

			free(pBuf);

			m_lLastSecOffset = (lEndPos + 2);

			SetSecName(sSec);

			return true;
		}
	}
	catch(...)
	{

	}

	if (pBuf != NULL)
	{
		free(pBuf);
	}

	return false;
}


//*********************************************************************
//*
//*  CIniParamIO::GetNextSecName
//*

bool CIniParamIO::GetNextSecName (TCHAR *pSec, int nMax)
{
	char *pBuf = NULL;

	try
	{
		CFileIO	file;

		file.SetFileName((TCHAR *) (m_sFileName.c_str()));

		file.SetFileMode("r");

		bool bStatus;

		bStatus = file.Open();

		if (bStatus == false)
		{
			return false;
		}

		LONG lSize = file.GetFileLen();

		pBuf = (char *) calloc((lSize + 2), sizeof(char));

		if (pBuf == NULL)
		{
			file.Close();

			return false;
		}

		bStatus = file.SetFilePointer(m_lLastSecOffset);

		if (bStatus == false)
		{
			file.Close();

			free(pBuf);

			return false;
		}

		bStatus = file.ReadBuffer(pBuf, lSize);

		file.Close();

		if (bStatus == false)
		{
			free(pBuf);

			return false;
		}

		long lStartPos = -1;
		long lEndPos = -1;

		for (long x = 0; x < lSize; x++)
		{
			if (*(pBuf + x) == '[')
			{
				lStartPos = (x + 1);

				continue;
			}

			if (*(pBuf + x) == ']')
			{
				lEndPos = (x - 1);

				break;
			}
		}

		if (lStartPos != -1 && lEndPos > lStartPos)
		{
			long lLen = (lEndPos - lStartPos);

			if (nMax < lLen)
			{
				lLen = nMax;
			}

			strncpy(pSec, (pBuf + lStartPos), (int) lLen);

			free(pBuf);

			m_lLastSecOffset = (lEndPos + 2);

			SetSecName(pSec);

			return true;
		}
	}
	catch(...)
	{

	}

	if (pBuf != NULL)
	{
		free(pBuf);
	}

	return false;
}

bool CIniParamIO::GetNextSecName (std::string &sSec)
{
	char *pBuf = NULL;

	try
	{
		CFileIO	file;

		file.SetFileName((TCHAR *) (m_sFileName.c_str()));

		file.SetFileMode("r");

		bool bStatus;

		bStatus = file.Open();

		if (bStatus == false)
		{
			return false;
		}

		LONG lSize = file.GetFileLen();

		pBuf = (char *) calloc((lSize + 2), sizeof(char));

		if (pBuf == NULL)
		{
			file.Close();

			return false;
		}

		bStatus = file.SetFilePointer(m_lLastSecOffset);

		if (bStatus == false)
		{
			file.Close();

			free(pBuf);

			return false;
		}

		bStatus = file.ReadBuffer(pBuf, lSize);

		file.Close();

		if (bStatus == false)
		{
			free(pBuf);

			return false;
		}

		long lStartPos = -1;
		long lEndPos = -1;

		for (long x = 0; x < lSize; x++)
		{
			if (*(pBuf + x) == '[')
			{
				lStartPos = (x + 1);

				continue;
			}

			if (*(pBuf + x) == ']')
			{
				lEndPos = (x - 1);

				break;
			}
		}

		if (lStartPos != -1 && lEndPos > lStartPos)
		{
			long lLen = (lEndPos - lStartPos);

			sSec.assign((pBuf + lStartPos), (int) lLen);

			free(pBuf);

			m_lLastSecOffset = (lEndPos + 2);

			SetSecName(sSec);

			return true;
		}
	}
	catch(...)
	{

	}

	if (pBuf != NULL)
	{
		free(pBuf);
	}

	return false;
}


//*********************************************************************
//*
//*  CIniParamIO::GetStr
//*

bool CIniParamIO::GetStr (TCHAR *pKey, char* pTgtStr, char* pDefault)
{
	if (pKey == NULL)
	{
		return false;
	}

	SetKeyName (pKey);

	return GetStr (pTgtStr, pDefault);
}


bool CIniParamIO::GetStr (char* pTgtStr, char* pDefault)
{
	if (pTgtStr == NULL || pDefault == NULL)
	{
		return false;
	}

	try
	{
		DWORD dwRet;
	
		char  szKeyValue[REG_MAX_KEY_LEN];

		memset (szKeyValue, 0, sizeof(szKeyValue));

		dwRet = GetPrivateProfileString
				(
					(LPCTSTR) (m_sSecName.c_str()),
					(LPCTSTR) (m_sKeyName.c_str()),
					(LPCTSTR) pDefault,
					(LPTSTR) szKeyValue,
					REG_MAX_KEY_LEN,
					(LPCTSTR) (m_sFileName.c_str())
				);

		if (dwRet < 1) 
		{
			strcpy (pTgtStr, pDefault);

			return false;
		}

		strcpy(pTgtStr, szKeyValue);

		return true;
	}
	catch(...)
	{

	}

	return false;
}





//*********************************************************************
//*
//*  CIniParamIO::SetStr
//*

bool CIniParamIO::SetStr (TCHAR *pKey, char* pStr)
{
	if (pKey == NULL)
	{
		return false;
	}

	SetKeyName (pKey);

	return SetStr (pStr);
}


bool CIniParamIO::SetStr (char* pStr)
{
	if (pStr == NULL)
	{
		return false;
	}

	bool bRet = false;
	
	try
	{
		bRet =	(bool) WritePrivateProfileString
				(
					(LPCTSTR) (m_sSecName.c_str()),
					(LPCTSTR) (m_sKeyName.c_str()),
					(LPCTSTR) pStr,
					(LPCTSTR) (m_sFileName.c_str())
				);
	}
	catch(...)
	{
		bRet = false;
	}

	return bRet;
}



//*********************************************************************
//*
//*  CIniParamIO::GetInt
//*

bool CIniParamIO::GetInt (TCHAR *pKey, int *pVar, int iDefault)
{
	if (pKey == NULL)
	{
		return false;
	}

	SetKeyName (pKey);

    return GetInt (pVar, iDefault);
}


bool CIniParamIO::GetInt (int *pVar, int iDefault)
{
	if (pVar == NULL)
	{
		return false;
	}

	try
	{
		UINT uiRet;
    
		uiRet = GetPrivateProfileInt
				(
					(LPCTSTR) (m_sSecName.c_str()),
					(LPCTSTR) (m_sKeyName.c_str()),
					(INT) iDefault,
					(LPCTSTR) (m_sFileName.c_str())
				);
    
		*pVar = uiRet;

		if (uiRet != iDefault)
		{
			return true;
		}
	}
	catch(...)
	{

	}

	return false;
}




//*********************************************************************
//*
//*  CIniParamIO::SetInt
//*

bool CIniParamIO::SetInt (TCHAR *pKey, int iValue)
{
	if (pKey == NULL)
	{
		return false;
	}

	SetKeyName (pKey);

    return SetInt (iValue);
}


bool CIniParamIO::SetInt (int iValue)
{
	bool bRet = false;

	try
	{
		char szTemp[128];

		wsprintf (szTemp, "%d", iValue);
	
		bRet =	(bool) WritePrivateProfileString
				(
					(LPCTSTR) (m_sSecName.c_str()),
					(LPCTSTR) (m_sKeyName.c_str()),
					(LPCTSTR) szTemp,
					(LPCTSTR) (m_sFileName.c_str())
				);
	}
	catch(...)
	{
		bRet = false;
	}

	return bRet;
}


//*********************************************************************
//*
//*  CIniParamIO::GetBool
//*

bool CIniParamIO::GetBool (TCHAR *pKey, bool *pVar, bool bDefault)
{
	if (pKey == NULL)
	{
		return false;
	}

	SetKeyName (pKey);

    return GetBool (pVar, bDefault);
}


bool CIniParamIO::GetBool (bool *pVar, bool bDefault)
{
	if (pVar == NULL)
	{
		return false;
	}

	try
	{
		DWORD dwRet;
	
		char  szKeyValue[REG_MAX_KEY_LEN];
		char  szDefValue[REG_MAX_KEY_LEN];

		memset (szKeyValue, 0, sizeof(szKeyValue));
		memset (szDefValue, 0, sizeof(szDefValue));

		//if (bDefault = true)
		//{
		//	strcpy(szDefValue, "True");
		//}
		//else
		//{
		//	strcpy(szDefValue, "False");
		//}

		dwRet = GetPrivateProfileString
				(
					(LPCTSTR) (m_sSecName.c_str()),
					(LPCTSTR) (m_sKeyName.c_str()),
					(LPCTSTR) szDefValue,
					(LPTSTR) szKeyValue,
					REG_MAX_KEY_LEN,
					(LPCTSTR) (m_sFileName.c_str())
				);

		if (dwRet < 1) 
		{
			*pVar = bDefault;

			return false;
		}

		if  (
				strncmp(szKeyValue, "TRUE", 5) == 0 || 
				strncmp(szKeyValue, "True", 5) == 0 || 
				strncmp(szKeyValue, "true", 5) == 0 || 
				strncmp(szKeyValue, "1", 2) == 0
			)
		{
			*pVar = true;
		}
		else if 
			(
				strncmp(szKeyValue, "FALSE", 5) == 0 || 
				strncmp(szKeyValue, "False", 5) == 0 || 
				strncmp(szKeyValue, "false", 5) == 0 ||
				strncmp(szKeyValue, "0", 2) == 0
			)
		{
			*pVar = false;
		}
		else
		{
			*pVar = bDefault;

			return false;
		}

		return true;
	}
	catch(...)
	{

	}

	return false;
}




//*********************************************************************
//*
//*  CIniParamIO::SetBool
//*

bool CIniParamIO::SetBool (TCHAR *pKey, bool bValue)
{
	if (pKey == NULL)
	{
		return false;
	}

	SetKeyName (pKey);

    return SetInt (bValue);
}


bool CIniParamIO::SetBool (bool bValue)
{
	bool bRet = false;

	try
	{
		char szTemp[128];

		memset (szTemp, 0, sizeof(szTemp));

		if (bValue == true)
		{
			wsprintf (szTemp, "True");
		}
		else
		{
			wsprintf (szTemp, "False");
		}
	
		bRet =	(bool) WritePrivateProfileString
				(
					(LPCTSTR) (m_sSecName.c_str()),
					(LPCTSTR) (m_sKeyName.c_str()),
					(LPCTSTR) szTemp,
					(LPCTSTR) (m_sFileName.c_str())
				);
	}
	catch(...)
	{
		bRet = false;
	}

	return bRet;
}

