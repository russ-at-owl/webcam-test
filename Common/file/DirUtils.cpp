//*****************************************************************************
//* DirUtils.cpp - Directory functions implimentation file
//*

#pragma warning( disable : 4996 ) 

#ifdef WIN32
#include <Windows.h>
#include "Shlwapi.h"
#endif

#include "DirUtils.h"

#include "..\other\StrUtils.h"



int GetDirPath(LPSTR pTargetPath, LPCSTR pSourcePath, int nMaxPathLen)
{
	if (pTargetPath == NULL || pSourcePath == NULL || nMaxPathLen < 1)
	{
		return -1;
	}

	int	nReturnLen = 0;
	int	nSourcePathLen = 0;
	int nIdx = 0;

	nSourcePathLen =
		strlen(pSourcePath);

	if (nSourcePathLen > nMaxPathLen || nSourcePathLen < 1)
	{
		return -1;
	}

	if (nMaxPathLen > MAX_PATH)
	{
		return -1;
	}

	TCHAR szPathTemp[MAX_PATH];

	ZeroMemory(szPathTemp, sizeof(szPathTemp));

	strncpy(szPathTemp, pSourcePath, nMaxPathLen);

#ifdef WIN32
	BOOL bStatus =
		PathRemoveFileSpec(szPathTemp);
	if (bStatus == FALSE)
	{
		return -1;
	}
#else
	// need to do a non-Windows version of this
	return -1;
#endif

	strncpy(pTargetPath, szPathTemp, nMaxPathLen);

	return strlen(szPathTemp);
}

std::string GetDirPath(const std::string &sSource)
{
	std::string sOut = "";

	char szOut[MAX_PATH];

	memset(szOut, 0, sizeof(szOut));

	int nLen = MAX_PATH - 1;;

	if (sSource.length() < MAX_PATH)
	{
		nLen = sSource.length();
	}

	if (GetDirPath((LPSTR) szOut, (LPCSTR) (sSource.c_str()), nLen) > 0)
	{
		sOut = szOut;
	}

	return sOut;
}


int GetDirPathLen(LPCSTR pSourcePath)
{
	if (pSourcePath == NULL)
	{
		return -1;
	}

	int	nReturnLen = 0;
	int	nSourcePathLen = 0;
	int nIdx = 0;

	nSourcePathLen =
		strlen(pSourcePath);

	nReturnLen = (nSourcePathLen - 1);

	while (1)
	{
		nReturnLen--;

		if (nReturnLen < 1)
		{
			return -1;
		}

#ifdef WIN32
		if (*(pSourcePath + nReturnLen) == '\\' || *(pSourcePath + nReturnLen) == ':')
#else
		if (*(pSourcePath + nReturnLen) == '//' || *(pSourcePath + nReturnLen) == ':')
#endif
		{
			nReturnLen++;

			return nReturnLen;
		}
	}

	return 0;
}

int GetDirPathLen(const std::string &sSource)
{
	return GetDirPathLen((LPCSTR) sSource.c_str());
}


int GetPathFileName(LPSTR pFilename, LPCSTR pSourcePath)
{
	if (pSourcePath == NULL || pFilename == NULL)
	{
		return -1;
	}

	TCHAR *pFName = NULL;

#ifdef WIN32
	pFName =
		PathFindFileName(pSourcePath);
	if (pFName == NULL)
	{
		return -1;
	}
#else
	// need to do a non-Windows version of this
	return -1;
#endif

	strcpy(pFilename, pFName);

	return strlen(pFName);
}

std::string GetPathFileName(const std::string &sSource)
{
	std::string sOut = "";

	char szOut[MAX_PATH];

	memset(szOut, 0, sizeof(szOut));

	//int nLen = MAX_PATH - 1;;

	//if (sSource.length() < MAX_PATH)
	//{
	//	nLen = sSource.length();
	//}

	if (GetPathFileName((LPSTR) szOut, (LPCSTR) (sSource.c_str())) > 0)
	{
		sOut = szOut;
	}

	return sOut;
}


int GetPathExt(LPSTR pExt, LPCSTR pSourcePath)
{
	if (pSourcePath == NULL || pExt == NULL)
	{
		return -1;
	}

	TCHAR *pPathExt = NULL;

#ifdef WIN32
	pPathExt =
		PathFindExtension(pSourcePath);
	if (pPathExt == NULL)
	{
		return -1;
	}
#else
	// need to do a non-Windows version of this
	return -1;
#endif

	strcpy(pExt, (pPathExt + 1));

	return strlen(pPathExt + 1);
}

std::string GetPathExt(const std::string &sSource)
{
	std::string sOut = "";

	char szOut[MAX_PATH];

	memset(szOut, 0, sizeof(szOut));

	//int nLen = MAX_PATH - 1;;

	//if (sSource.length() < MAX_PATH)
	//{
	//	nLen = sSource.length();
	//}

	if (GetPathExt((LPSTR) szOut, (LPCSTR) (sSource.c_str())) > 0)
	{
		sOut = szOut;
	}

	return sOut;
}


int ComparePathExt(LPCSTR pSourcePath, LPCSTR pExt)
{
	if (pSourcePath == NULL || pExt == NULL)
	{
		return _NLSCMPERROR;
	}

	TCHAR szPathExt[MAX_PATH];

	ZeroMemory(szPathExt, sizeof(szPathExt));

	int nStatus =
		GetPathExt(szPathExt, pSourcePath);
	if (nStatus < 1)
	{
		return _NLSCMPERROR;
	}

	int nCmpLen = strlen(pExt);

	if (nCmpLen < nStatus)
	{
		nCmpLen = nStatus;
	}

	return Compare((TCHAR *) pExt, szPathExt, nCmpLen, TRUE);
}

int ComparePathExt(const std::string &sSource, const std::string &sExt)
{
	return ComparePathExt((LPCSTR) (sSource.c_str()), (LPCSTR) (sExt.c_str()));
}




