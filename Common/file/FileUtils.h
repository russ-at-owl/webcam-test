//**************************************************************************************************
//* FILE:		fileUtils.h
//*
//* DESCRIP:
//*
//*





#ifndef _fileUtils_H_
#define _fileUtils_H_

#include <string>
//#include <vector>
#include <stlx.h>

#include <stdexcept>


#pragma warning( disable : 4290 )


#ifndef GetCurrentDirectory
#ifdef UNICODE
#define GetCurrentDirectory  GetCurrentDirectoryW
#else
#define GetCurrentDirectory  GetCurrentDirectoryA
#endif // !UNICODE
#endif



namespace fileUtil
{

//********************************************
//* stripPath
//*
//* remove the path from a filename
//********************************************
template<class S>
S stripPath(const S &s, const S &delim) throw()
{
	typename S::size_type pos = s.find_last_of(delim);
	if (pos == S::npos) 
	{
		return s;
	}
	if (pos == s.size() -1) 
	{
		return S();
	}
	return s.substr(pos+1);
}

inline std::string stripPath(const std::string &s) throw()
{
	return fileUtil::stripPath(s, std::string("\\"));
}

#ifdef WIN32
inline std::wstring stripPath(const std::wstring &ws) throw()
{
	return fileUtil::stripPath(ws, std::wstring(L"\\"));
}
#endif

//********************************************
//* stripName
//*
//* remove the filename from a path
//********************************************
template<class S>
S stripName(const S &s, const S &delim, const S &delim2) throw()
{
	typename S::size_type pos = s.find_last_of(delim);
	if (pos == S::npos) 
	{
		pos = s.find_last_of(delim2);
		if (pos == S::npos)
		{
			return S();
		}
		return s.substr(0, pos);
	}
	if (pos == s.size() - 1) 
	{
		return S();
	}
	return s.substr(0, pos);
}

inline std::string stripName(const std::string &s) throw()
{
	return fileUtil::stripName(s, std::string("\\"), std::string(":"));
}

#ifdef WIN32
inline std::wstring stripName(const std::wstring &ws) throw()
{
	return fileUtil::stripName(ws, std::wstring(L"\\"), std::wstring(L":"));
}
#endif

//*

template<class S>
S stripSuffix(const S &s,const S &delim) throw()
{
	return s.substr(0,s.rfind(delim));
}

inline std::string stripSuffix(const std::string &s) throw()
{
	return fileUtil::stripSuffix(s, std::string("."));
}

#ifdef WIN32
inline std::wstring stripSuffix(const std::wstring &ws) throw()
{
	return fileUtil::stripSuffix(ws, std::wstring(L"."));
}
#endif

template<class S>
S getSuffix(const S &s, const S &delim) throw()
{
	S empty;
	typename S::size_type pos = s.rfind(delim);
	if (pos == S::npos)
	{
		return empty;
	}
	return s.substr(pos+1);
}

inline std::string getSuffix(const std::string &s) throw()
{
	return fileUtil::getSuffix(s, std::string("."));
}

#ifdef WIN32
inline std::wstring getSuffix(const std::wstring &ws) throw()
{
	return fileUtil::getSuffix(ws, std::wstring(L"."));
}
#endif

////////////


// get path to current directory. Throw exception if OS calls fail
std::string getCurrentDirectory() throw(std::runtime_error);

// return true if a dir exists
bool dirExists(const std::string &fullPath) throw(std::runtime_error);

bool createDir(const std::string &fullPath) throw(std::runtime_error);

// return true if a file exists
bool fileExists(const std::string &fullPath) throw(std::runtime_error);

bool pathExists(const std::string &fullPath);

typedef stlx::xvector<std::string>	filenameList_def;

// get list of all files that match a pattern
filenameList_def directoryFileList
	(
		const std::string &pattern, 
		bool fullPaths,
		bool forceLowerCase = false
	) throw(std::runtime_error);

bool isFileInList
	(
		const filenameList_def &fileList, 
		const std::string &file
	) throw(std::runtime_error);

// return true if a file is deleted
bool deleteFile(const std::string &fullPath) throw(std::runtime_error);

// return true if a file is renamed
bool renameFile
	(
		const std::string &fullSrcPath, 
		const std::string &fullDstPath
	) throw(std::runtime_error);

// return true if a file is copied
bool copyFile
	(
		const std::string &fullSrcPath, 
		const std::string &fullDstPath
	) throw(std::runtime_error);

};

#endif
