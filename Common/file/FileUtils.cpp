//**************************************************************************************************
//* FILE:		fileUtils.cpp
//*
//* DESCRIP:
//*
//*



#include <sys/types.h>
#include <sys/stat.h>

#include <string>


#ifdef WIN32
#include <windows.h>
#include <Shlwapi.h>

#pragma warning( disable : 4800 ) 
#pragma warning( disable : 4996 ) 
#endif

#include "stringUtils.h"

#include "fileUtils.h"

#include "WinUtils.h"


//using namespace std;


#ifdef WIN32

std::string fileUtil::getCurrentDirectory() throw(std::runtime_error)
{
	char *pDirBuf = NULL;

	std::string sOut = "";

	try
	{
		//* get the character length of the directory name

		DWORD dwDirLen = ::GetCurrentDirectory(0, 0);
		if (dwDirLen == 0)
		{
			throw std::runtime_error("Could not get current working directory because " + winUtils::errMessage());
		}

		pDirBuf = (char *) calloc((dwDirLen + 2), sizeof(char));
		if (pDirBuf == NULL)
		{
			throw std::runtime_error("Could not allocate director name buffer ");
		}

		//* copy the directory name (plus NULL terminator) to the char buffer

		dwDirLen = ::GetCurrentDirectory((dwDirLen + 1), pDirBuf);
		if (dwDirLen == 0)
		{
			throw std::runtime_error("Could not get current working directory because " + winUtils::errMessage());
		}

		sOut = pDirBuf;

		// must be slash terminated
		if ((sOut.length() > 0)  && (sOut[sOut.length() - 1] != '\\'))
		{
			sOut += "\\";
		}

		free(pDirBuf);
	}
	catch(...)
	{
		if (pDirBuf != NULL)
		{
			free(pDirBuf);
		}
		throw("Unknown exception while getting directory name");
	}

	return sOut;
}


bool fileUtil::dirExists(const std::string &fullPath) throw(std::runtime_error)
{
	return (bool) ::PathFileExists((LPCTSTR ) fullPath.c_str());
}


bool fileUtil::createDir(const std::string &fullPath) throw(std::runtime_error)
{
	return (bool) ::CreateDirectory((LPCTSTR ) fullPath.c_str(), NULL);
}


bool fileUtil::fileExists(const std::string &fullPath) throw(std::runtime_error)
{
	HANDLE hPF = ::CreateFile(fullPath.c_str(), 0, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if (hPF == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	::CloseHandle(hPF);

	return true;
}


bool fileUtil::pathExists(const std::string &fullPath)
{
	int nRet = 0;

	struct _stat Buf;

	nRet = _stat((char *) (fullPath.c_str()), &Buf);
	
	if (nRet != 0) 
	{
		return false;
	}

	return true;
}


fileUtil::filenameList_def fileUtil::directoryFileList
	(
		const std::string &pattern, 
		bool fullPaths,
		bool forceLowerCase
	) throw(std::runtime_error)
{
	fileUtil::filenameList_def	result;

	WIN32_FIND_DATA fd;

	std::string path = pattern.substr(0, pattern.rfind("\\"));
	HANDLE	h = ::FindFirstFile(pattern.c_str(), &fd);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (fullPaths)
			{
				if (forceLowerCase)
				{
					result.push_back(stringUtil::toLower(path + "\\" + fd.cFileName));
				}
				else
				{
					result.push_back((path + "\\" + fd.cFileName));
				}
			}
			else
			{
				if (forceLowerCase)
				{
					result.push_back(stringUtil::toLower(fd.cFileName));
				}
				else
				{
					result.push_back(fd.cFileName);
				}
			}
		}
		while(::FindNextFile(h, &fd));
	}

	if (h != INVALID_HANDLE_VALUE)
	{
		::FindClose(h);
	}

	return result;
}


bool fileUtil::isFileInList
	(
		const fileUtil::filenameList_def &fileList, 
		const std::string &file
	) throw(std::runtime_error)
{
	try
	{
		for (auto x = fileList.begin(); x != fileList.end(); x++)
		{
			if ((*x) == file)
			{
				return true;
			}
		}
	}
	catch (...)
	{
	}

	return false;
}


bool fileUtil::deleteFile(const std::string &fullPath) throw(std::runtime_error)
{
	return ::DeleteFile(fullPath.c_str());
}


bool fileUtil::renameFile(const std::string &fullSrcPath, const std::string &fullDstPath) throw(std::runtime_error)
{
	return ::MoveFile(fullSrcPath.c_str(), fullDstPath.c_str());
}


bool fileUtil::copyFile(const std::string &fullSrcPath, const std::string &fullDstPath) throw(std::runtime_error)
{
	return ::CopyFile(fullSrcPath.c_str(), fullDstPath.c_str(), true);
}


#else

#include <filesystem>


std::string fileUtil::getCurrentDirectory() throw(std::runtime_error)
{
	std::string result;

	char buf[2048];
	if (!getcwd(buf, 2047))
	{
		throw runtime_error("Could not get current working directory");
	}

	result = buf;
	// must be slash terminated
	if (result.size() && result[result.size() - 1] != '/')
	{
		result += "/";
	}

	return result;
}

bool fileUtil::dirExists(const std::string &fullPath) throw()
{
	struct stat sbuf;

	if (::stat(fullPath.c_str(), &sbuf) == 0)
	{
		if(sbuf.st_mode & S_IFDIR != 0)
		{
			return true;
		}

	}

	return false;
}


bool fileUtil::createDir(const std::string &fullPath) throw(std::runtime_error)
{
	return (bool) makedir((LPCTSTR ) fullPath.c_str());
}



bool fileUtil::pathExists(const std::string &fullPath)
{
	int nRet = 0;

	struct _stat Buf;

	nRet = stat((char *) (fullPath.c_str()), &Buf);

	if (nRet != 0) 
	{
		return false;
	}

	return true;
}


std::vector<string> fileUtil::directoryFileList(const std::string &pattern, bool fullPaths) throw()
{
	std::vector<std::string>	result;
	WIN32_FIND_DATA fd;

	string path = pattern.substr(0,pattern.rfind("\\"));
	HANDLE	h = ::FindFirstFile(pattern.c_str(), &fd);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (fullPaths)
			{
				result.push_back(stringUtil::toLower(path + "\\" + fd.cFileName));
			}
			else
			{
				result.push_back(stringUtil::toLower(fd.cFileName));
			}
		}
		while(::FindNextFile(h, &fd));
	}

	if (h != INVALID_HANDLE_VALUE)
	{
		::FindClose(h);
	}

	return result;
}


bool fileUtil::deleteFile(const std::string &fullPath) throw()
{
	int nRet = remove(fullPath.c_str());

	if (bRet != 0)
	{
		return false;
	}

	return true;
}


bool fileUtil::renameFile(const std::string &fullSrcPath, const std::string &fullDstPath) throw(std::runtime_error)
{
	try
	{
		std::filesystem::rename(fullSrcPath.c_str(), fullDstPath.c_str());
	}
	catch(...)
	{
		return false;
	}
	
	return true;
}


bool fileUtil::copyFile(const std::string &fullSrcPath, const std::string &fullDstPath) throw(std::runtime_error)
{
	try
	{
		std::filesystem::copy(fullSrcPath.c_str(), fullDstPath.c_str());
	}
	catch(...)
	{
		return false;
	}
	
	return true;
}


#endif


