//*********************************************************************
//*
//*  AppLog.cpp
//*
//*		Applicarion log file functions
//*

#pragma warning( disable : 4627 ) 
#pragma warning( disable : 4996 ) 

#ifdef WIN32
#include <Windows.h>
#else
#define LPSTR	char*	
#define LPCSTR	const char*	
#endif

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include <string>
#include <iostream>

#include "..\other\VaPass.h"

#include "AppLog.h"


EXPORT_DEF void OutputDebugStr(std::string sText)
{
#ifdef WIN32
	OutputDebugStringA((LPCSTR) sText.c_str());
#endif
}



int g_nLogLevel;

bool g_bConsoleLogging;

bool g_bTruncateLog;

#ifdef WIN32
HANDLE g_hLogFile;
#else
FILE * g_hLogFile;
#endif



void InitAppLog()
{
#ifdef _DEBUG
	g_nLogLevel =		1;
#else
	g_nLogLevel =		0;
#endif
	g_bConsoleLogging = false;

	g_bTruncateLog = false;

	g_hLogFile =		0;
}


void SetLogLevel(int nLogLevel)
{
	g_nLogLevel = nLogLevel;
}

void SetConsoleLogging(bool bVal)
{
	g_bConsoleLogging = bVal;
}

void SetTruncateLog(bool bVal)
{
	g_bTruncateLog = bVal;
}

bool OpenLog(LPSTR szLogfileName)
{
	if (g_hLogFile != 0)
	{
		CloseLog();
	}

	if (szLogfileName == NULL)
	{
		return false;
	}

	g_hLogFile = 0; 

#ifdef WIN32	

	if (g_bTruncateLog == true)
	{
		g_hLogFile = 
			CreateFile
			(
				szLogfileName,
				GENERIC_WRITE,
				FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);

	}
	else
	{
		g_hLogFile = 
			CreateFile
			(
				szLogfileName,
				GENERIC_WRITE,
				FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL,
				OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);

		LARGE_INTEGER	liPos;

		memset(&liPos, 0, sizeof(liPos));

		SetFilePointerEx(g_hLogFile, liPos, NULL, FILE_END);
	}
	
#else

	if (g_bTruncateLog == true)
	{
		g_hLogFile = fopen((const CHAR*) szLogfileName.c_str(), "w");
	}
	else
	{
		g_hLogFile = fopen((const CHAR*) szLogfileName.c_str(), "w+");
	}

#endif

	if (g_hLogFile != 0)
	{
		return true;
	}

	return false;
}


bool OpenLog(std::string sLogfileName)
{
	return OpenLog((LPSTR) (sLogfileName.c_str()));
}

void CloseLog()
{
	if (g_hLogFile != 0)
	{
#ifdef WIN32		
		CloseHandle(g_hLogFile);
#else
		fclose(g_hLogFile);
#endif

		g_hLogFile = 0;
	}
}


void LogWrite(int nLogLevel, LPSTR szLogString, ...)
{
	if (nLogLevel > g_nLogLevel)
	{
		return;
	}

	if (szLogString == NULL)
	{
		return;
	}

	va_list ArgList;

	CHAR szOutputString[1024];

	memset(szOutputString, 0, sizeof(szOutputString));

	va_start(ArgList, szLogString);

	sprintf(szOutputString, szLogString, va_pass(ArgList));

	va_end(ArgList);

	OutputDebugStr(szOutputString);

	if (g_bConsoleLogging == true)
	{
		std::cout << szOutputString << std::endl;
	}

	if (g_hLogFile != 0)
	{
		DWORD dwRet = 0;

		DWORD dwLen = strlen(szOutputString);

		WriteFile(g_hLogFile, szOutputString, dwLen, &dwRet, NULL); 
		WriteFile(g_hLogFile, TEXT("\r\n"), 2, &dwRet, NULL); 
	}
}



void LogWrite(int nLogLevel, LPSTR szConditionString, LPSTR szLogString, ...)
{
	if (nLogLevel > g_nLogLevel)
	{
		return;
	}

	if (szConditionString == NULL || szLogString == NULL)
	{
		return;
	}

	va_list ArgList;

	CHAR szTempString[2048];

	CHAR szOutputCondition[CONDITION_STRING_SIZE + 1];

	CHAR szOutputString[2048];

	memset(szTempString, 0, sizeof(szTempString));

	va_start(ArgList, szLogString);

	sprintf(szTempString, szLogString, va_pass(ArgList));

	va_end(ArgList);

	memset(szOutputCondition, ' ', sizeof(szOutputCondition));

	*(szOutputCondition + (CONDITION_STRING_SIZE - 1)) = 0;

	for (int i = 0; i < CONDITION_STRING_SIZE; i++)
	{
		if (*(szConditionString + i) < ' ' || *(szConditionString + i) > 'Z')
		{
			break;
		}

		*(szOutputCondition + i) = *(szConditionString + i);
	}

	memset(szOutputString, 0, sizeof(szOutputString));

	sprintf(szOutputString, "%s %s", szOutputCondition, szTempString);

	OutputDebugStr(szOutputString);

	if (g_bConsoleLogging == true)
	{
		std::cout << szOutputString << std::endl;
	}

	if (g_hLogFile != 0)
	{
		DWORD dwRet = 0;

		DWORD dwLen = strlen(szOutputString);

#ifdef WIN32
		WriteFile(g_hLogFile, szOutputString, dwLen, &dwRet, NULL); 
		WriteFile(g_hLogFile, TEXT("\r\n"), 2, &dwRet, NULL); 
#else
		fwrite((void *) szOutputString, sizeof(char), dwLen, m_FileHandle);
		fwrite((void *) "\r\n", sizeof(char), 2, m_FileHandle);
#endif
	}
}


void LogWrite(int nLogLevel, const std::string &sLogString)
{
	LogWrite(nLogLevel, (LPSTR) sLogString.c_str());
}


void LogWrite(int nLogLevel, LPSTR szConditionString, const std::string &sLogString)
{
	LogWrite(nLogLevel, szConditionString, (LPSTR)sLogString.c_str());
}

