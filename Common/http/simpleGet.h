//**************************************************************************************************
//* FILE:		simpleGet.h
//*
//* DESCRIP:	
//*
//*



#ifndef simpleGet_H_
#define simpleGet_H_

#include <string>
#include <stdexcept>

std::string simpleGet(const std::string &url) throw(std::exception);

#endif
