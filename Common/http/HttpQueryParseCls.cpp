//*****************************************************************************
//* HttpQueryParseCls.cpp - HTTP Query String Parser implementation file
//*

#include <HttpQueryParseCls.h>




std::string::size_type CHttpQueryParser::findOneOf(const std::string &sList, const std::string &sString, std::string::size_type nFirst)
{
	if (sList.length() < 1 || sString.length() < 1 || nFirst >= sString.length())
		return std::string::npos;

	std::string::size_type nRet = std::string::npos;

	std::string sTmp;

	for (unsigned int x = nFirst; x < sString.length(); x++)
	{
		sTmp = sString.substr(x, 1);

		if (sList.find(sTmp) != std::string::npos)
		{
			nRet = x;

			break;
		}
	}

	return nRet;
}


bool CHttpQueryParser::Parse(const std::string &sQery, ParamList_def &list)
{
	m_sQuery = sQery;

	return Parse(list);
}

bool CHttpQueryParser::Parse(ParamList_def &list)
{
	if (m_sQuery.length() < 1)
	{
		return false;
	}

	std::string sDelims = "?#";

	//* verify that 1st char is a delim

	std::string sTmp = m_sQuery.substr(0, 1);

	if (sDelims.find(sTmp) == std::string::npos)
	{
		return false;
	}

	std::string::size_type nFound = 0;
	std::string::size_type nCurPos = 1;

	std::string sDelims2 = "?#=";

	std::string sTmp2;

	//* search for the first start of query

	bool bRet = false;

	while (1)
	{
		if (nCurPos >= m_sQuery.length())
		{
			break;
		}

		//nFound = m_sQuery.find_first_of(sDelims2, nCurPos);
		nFound = findOneOf(sDelims2, m_sQuery, nCurPos);

		if (nFound == std::string::npos)
		{
			//* no more queries or values found

			sTmp = m_sQuery.substr(nCurPos);
			if (sTmp.length() > 0)
			{
				list[sTmp] = "";
			}

			break;
		}

		//* save query name

		sTmp = m_sQuery.substr(nCurPos, (nFound - nCurPos));

		nCurPos = nFound + 1;

		if (sTmp.length() < 1)
		{
			continue;
		}

		bRet = true;

		//* check is query has value

		if (m_sQuery[nFound] == '=')
		{
			//* get query value

			//nFound = m_sQuery.find_first_of(sDelims, nCurPos);
			nFound = findOneOf(sDelims, m_sQuery, nCurPos);

			if (nFound == std::string::npos)
			{
				//* no more queries

				if ((m_sQuery.length() - 1) > nCurPos)
				{
					sTmp2 = m_sQuery.substr(nCurPos);
				}
				else
				{
					sTmp2 = "";
				}

				list[sTmp] = sTmp2;

				break;
			}

			sTmp2 = m_sQuery.substr(nCurPos, (nFound - nCurPos));

			//* add query and value to list

			list[sTmp] = sTmp2;

			nCurPos = nFound + 1;
		}
		else
		{
			//* add query with no value to list

			list[sTmp] = "";
		}
	}

	return bRet;
}
