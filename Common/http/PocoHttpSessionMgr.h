//**************************************************************************************************
//* FILE:		PocoHttpSessionMgr.cpp
//*
//* DESCRIP:	PocoHttpSessionMgr utility class
//*

#ifndef _POCO_HTTP_CLIENT_H_
#define _POCO_HTTP_CLIENT_H_


#pragma warning( disable: 4081 )
#pragma warning( disable: 4101 )
#pragma warning( disable: 4251 )
#pragma warning( disable: 4996 )


#ifdef POCO_SSL
#define SUPPORT_POCO_SSL
#endif


#include <Poco/Net/StreamSocket.h>
#include <Poco/Net/HTTPSession.h>
#include <Poco/Net/HTTPCredentials.h>
#include <Poco/Net/HTTPBasicCredentials.h>
#include <Poco/StreamCopier.h>
#include <Poco/NullStream.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>

#ifdef SUPPORT_POCO_SSL
#include "Poco/Net/SecureStreamSocket.h"
#endif

#include <string>
#include <iostream>



#ifndef CreateSemaphore
#ifdef UNICODE
#define CreateSemaphore  CreateSemaphoreW
#else
#define CreateSemaphore  CreateSemaphoreA
#endif // !UNICODE
#endif


#ifndef CreateProcess
#ifdef UNICODE
#define CreateProcess  CreateProcessW
#else
#define CreateProcess  CreateProcessA
#endif // !UNICODE
#endif



using Poco::Net::StreamSocket;
using Poco::Net::HTTPSession;
using Poco::StreamCopier;
using Poco::Path;
using Poco::URI;
using Poco::Exception;

#ifdef SUPPORT_POCO_SSL
using Poco::Net::SecureStreamSocket;
#endif



#define HEADER_SEPERATION_STR		"\r\n"
#define HEADER_SEPERATION_STR_LEN	2


#define DEFAULT_PORT	80
#define HTTPS_PORT		443




class PocoHttpSessionMgr
{
protected:

	int						m_nPort;

	std::string				m_sPath;

	std::string				m_sUID;
	std::string				m_sPW;

#ifdef SUPPORT_POCO_SSL
	bool					m_bEnableSSL;
#endif

	Poco::Net::HTTPSession	*m_pSession;

public:

	PocoHttpSessionMgr() :
		m_nPort(0),
		m_pSession(NULL)
	{
		m_sPath = "";
		m_sUID = "";
		m_sPW = "";
	}

	~PocoHttpSessionMgr()
	{
		if (m_pSession != NULL)
		{
			endSession();
		}
	}

#if 0
	bool createSession()
	{
		try
		{
			m_pSession = new HTTPSession;
		}
		//catch (Exception& exc)
		catch (...)
		{
			return false;
		}

		return true;
	};


	bool createSession(const StreamSocket& socket)
	{
		try
		{
			m_pSession = new HTTPSession(socket);
		}
		//catch (Exception& exc)
		catch (...)
		{
			return false;
		}

		return true;
	};
#endif

	bool createSession
		(
			const std::string &sHost, 
			const int nPort = -1,
			const std::string &sPath = "",
			const std::string &sUID = "", 
			const std::string &sPW = ""
		)
	{
		try
		{
			//* If nPort = -1 ... use default port

			if ((nPort < 1) && (nPort != -1))
			{
				return false;
			}

			m_sPath = sPath;

			if (sUID != "")
			{
				m_sUID = sUID;

				if (sPW != "")
				{
					m_sPW = sPW;
				}
			}

#ifdef SUPPORT_POCO_SSL
			if (m_bEnableSSL == true)
			{
				if (nPort == -1)
				{
					m_nPort = HTTPS_PORT;
				}
				else
				{
					m_nPort = nPort;
				}

				m_pSession = new HTTPSClientSession(sHost, m_nPort);
			}
			else
#endif
			{
				if (nPort == -1)
				{
					m_nPort = DEFAULT_PORT;
				}
				else
				{
					m_nPort = nPort;
				}

				m_pSession = new HTTPClientSession(sHost, (Poco::UInt16) m_nPort);
			}
		}
		//catch (Exception& exc)
		catch (...)
		{
			return false;
		}

		return true;
	};

	Poco::Net::HTTPSession* getSession()
	{
		return m_pSession;
	};

	bool endSession()
	{
		try
		{
			if (m_pSession != NULL)
			{
				delete m_pSession;

				m_pSession = NULL;

				return true;
			}
		}
		catch (...)
		{
			;
		}

		return false;;
	};

	bool setTimeout(int nDur)
	{
		if (m_pSession == NULL)
		{
			return false;
		}

		m_pSession->setKeepAlive(nDur);

		return true;
	};

	void setPath(const std::string& sPath)
	{
		m_sPath = sPath;
	};


};

#endif //  _POCO_HTTP_CLIENT_H_
