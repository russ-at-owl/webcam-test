//*****************************************************************************
//* HttpQueryParseCls.h - HTTP Query String Parser header file
//*


#ifndef _HTTPQUERYPARSERCLS_H_
#define _HTTPQUERYPARSERCLS_H_

#include <string>
#include <map>


typedef std::map<std::string, std::string>	ParamList_def;

class CHttpQueryParser
{
	std::string				m_sQuery;

	std::string::size_type findOneOf(const std::string &sList, const std::string &sString, std::string::size_type nFirst);

public:

	CHttpQueryParser() 
	{
		m_sQuery = "";
	}

	CHttpQueryParser(const std::string &sQuery) 
	{
		m_sQuery = sQuery;
	}

	bool Parse(const std::string &sQery, ParamList_def &list);
	bool Parse(ParamList_def &list);

};






#endif // _HTTPQUERYPARSERCLS_H_

