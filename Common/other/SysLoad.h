
//*******************************************************************************
//*
//* FILE:	SysLoad.h
//*
//* DESC:	CPU info defs
//*
//*


#ifndef _SYSLOAD_H_
#define _SYSLOAD_H_

#ifdef WIN32

#include "TCHAR.h"
#include "pdh.h"

#include <string>

#else

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
    
#endif

#include "stringUtils.h"


#ifdef WIN32

#define SYSLOAD_MAX_CPUS	16

struct sysLoadData_def
{

	unsigned int	cpuCnt;
	PDH_HQUERY		cpuQuery;
	PDH_HCOUNTER	cpuTotal;
	PDH_HCOUNTER	cpuProcessor[SYSLOAD_MAX_CPUS];
};

#else

struct sysLoadData_def
{
	unsigned longlong	lastTotalUser;
	unsigned longlong	lastTotalUserLow;
	unsigned longlong	lastTotalSys;
	unsigned longlong	lastTotalIdle;
};

#endif



class CSysLoad
{
	static sysLoadData_def		m_sysLoadData;

public:

	CSysLoad(bool bInit = false)
	{
		if (bInit == true)
		{
			initCpuLoad();
		}
	}

	int getCpuCount();

	void initCpuLoad(bool bPerProcessor = false);

	double getCpuLoad(int nCpu = -1);
};

#endif //  _SYSLOAD_H_
