//*****************************************************************************
//* StrUtils.cpp - String utility functions header file
//*



#ifndef _STRUTILS_H_
#define _STRUTILS_H_


#define STR_MAX		4096


#define ZERO_BUFFER(s)			ZeroMemory(s, sizeof(s))

#define SET_TEXT_STRING(s, v)	{ZeroMemory(s, sizeof(s)); strcpy(s, v);}


INT LeftShiftString(CHAR *pStr, INT nLen, INT nShift);
INT RightShiftString(CHAR *pStr, CHAR cInsChar, INT nLen, INT nShift);

INT Str2Lower(CHAR *pStr, INT nLen);
INT Str2Upper(CHAR *pStr, INT nLen);

INT Compare(TCHAR *pStr1, TCHAR *pStr2, INT nCmpLen, BOOL bIgnoreCase);

INT GetNextLine(CHAR *pTarget, CHAR *pSource, INT nMax);


#endif

