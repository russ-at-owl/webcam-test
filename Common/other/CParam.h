//*****************************************************************************
//* Module Name:    CParam.h  
//*
//* Description:
//*
//*
//*
//* Version:        1.00
//* Last Updated:   11/20/03
//*
//*****************************************************************************
//*
//* Created By:     Russ Barker
//*
//****************************************************************************


//****************************************************************************
//* Data defs
//****************************************************************************






//****************************************************************************
//* Class defs
//****************************************************************************


//*
//*  CParamsBase class 
//*

class CParamsBase
{

public:
	//***********************************

	//* constructor 

	CParamsBase ();

	//* destructor 

	//~CParamsBase ();



	BOOL Load ();
	BOOL Save ();

	BOOL SetParamPath (char *pParamPath);
	BOOL GetParamPath (char *pParamPath);

	BOOL SetParamKey (char *pParamKey);
	BOOL GetParamKey (char *pParamKey);

	int ReadInt (char *szParamID, int nDefault);

	BOOL ReadStr 
	(
		char *szParamID, 
		char *szDefault, 
		char *szStr,
		int nMax
	);

	BOOL WriteInt (char *szParamID, int nValue);

	BOOL WriteStr (char *szParamID, char *szStr);

protected:

	//***********************************

	//* class data 

	char		m_szParamPath[256];

	BOOL		m_bParamPathSet;

	char		m_szParamKey[80];

	BOOL		m_bParamKeySet;


private:


};







//class CWinParamsBase;


//*
//*  CWinParamsBase class 
//*

class CWinParamsBase: 
    public CParamsBase
    //public CWndBase
{

public:

	//***********************************

	//* constructor
	
	CWinParamsBase ();

	//* destructor
	
	//~CWinParamsBase ();


	BOOL Load ();
	BOOL Save ();

	BOOL GetWndData (PWNDDATADEF pWndData);
	BOOL SetWndData (PWNDDATADEF pWndData);


protected:

	//***********************************

	//* class data

	WNDDATADEF m_WndData;

	//***********************************

	BOOL		m_bLoadWndPos;
	BOOL		m_bSaveWndPos;

	BOOL		m_bLoadWndFont;
	BOOL		m_bSaveWndFont;

	BOOL		m_bLoadBgColor;
	BOOL		m_bSaveBgColor;

	BOOL		m_bLoadWndName;
	BOOL		m_bSaveWndName;


private:

	
};







                     
//****************************************************************************
//* Support Function defs
//****************************************************************************






                     
                     

