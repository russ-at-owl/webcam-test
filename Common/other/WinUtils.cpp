//**************************************************************************************************
//* FILE:		winUtils.cpp
//*
//* DESCRIP:	
//*
//*


#ifdef WIN32

#include <windows.h>

#ifdef SUPPORT_DLL_FUNC
#include <winbase.h>
#include <ntdef.h>
#include <winnt.h>
//#include <ntddk.h>
#include <Subauth.h>
#include <Winternl.h>
#endif

#endif 

//#include <fstream>
//#include <iostream>
#include <string>


#include "winUtils.h"

#include "stringUtils.h"

#ifdef _USE_LOGGING_DLL_

#include "loggerCls.h"

extern loggerCls				*g_Log;

#else

#include "AppLog.h"

#endif


#ifdef WIN32

#ifndef FormatMessage
#define FormatMessage	FormatMessageA
#endif


std::string winUtils::errMessage()
{
    std::string result = "";

    LPVOID lpMsgBuf = NULL;

	DWORD dwStatus =
		::FormatMessage
		(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
		);
    
	if (lpMsgBuf != NULL)
	{
		result.assign((const char*) lpMsgBuf);

		::LocalFree(lpMsgBuf);
	}

	return result;
}


void winUtils::msgBox(const std::string sTitle, const std::string sMsg)
{
	MessageBox
	(
		NULL,
		sMsg.c_str(),
		sTitle.c_str(),
		MB_OK
	);

	return;
}


winUtils::LPFN_ISWOW64PROCESS fnIsWow64Process;

BOOL winUtils::IsWow64()
{
    BOOL bIsWow64 = FALSE;

    //IsWow64Process is not available on all supported versions of Windows.
    //Use GetModuleHandle to get a handle to the DLL that contains the function
    //and GetProcAddress to get a pointer to the function if available.

    fnIsWow64Process = (LPFN_ISWOW64PROCESS) 
		GetProcAddress
		(
			GetModuleHandle(TEXT("kernel32")),
			"IsWow64Process"
		);

    if (NULL != fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64))
        {
            //handle error
        }
    }
	
    return bIsWow64;
}

#ifdef SUPPORT_DLL_FUNC

#ifdef UNICODE
inline PVOID winUtils::getDllHandle(_In_ PWSTR DllName)
#else
inline PVOID winUtils::getDllHandle(_In_ PSTR DllName)
#endif
{
	PVOID dllHandle = NULL;
	
#if 0
	UNICODE_STRING usDllName;

    RtlInitUnicodeString(&usDllName, DllName);

    if (NT_SUCCESS(LdrGetDllHandle(NULL, NULL, &dllName, &dllHandle)))
	{
        return dllHandle;
	}
    else
	{
        return NULL;
	}
#else

	dllHandle = GetModuleHandle(DllName);

	return dllHandle;
#endif
}

inline PVOID winUtils::getProcedureAddress
	(
		_In_ PVOID DllHandle,
		_In_opt_ PSTR ProcedureName,
		_In_opt_ ULONG ProcedureNumber
    )
{
    NTSTATUS status;
    ANSI_STRING procedureName;
    PVOID procedureAddress;

    if (ProcedureName)
    {
        RtlInitAnsiString(&procedureName, ProcedureName);
        status = 
			LdrGetProcedureAddress
			(
				DllHandle,
				&procedureName,
				0,
				&procedureAddress
            );
    }
    else
    {
        status = 
			LdrGetProcedureAddress
			(
				DllHandle,
				NULL,
				ProcedureNumber,
				&procedureAddress
            );
    }

    if (!NT_SUCCESS(status))
	{
        return NULL;
	}
	
    return procedureAddress;
}

#endif

BOOL CALLBACK MonitorEnumProc
	(
		HMONITOR hMonitor,
		HDC      hdcMonitor,
		LPRECT   lprcMonitor,
		LPARAM   dwData
	)
{
	if (dwData == NULL)
	{
		return FALSE;
	}

	CDisplayInfo *pCls = (CDisplayInfo *) dwData;

	pCls->m_nDisplayCntr++;

	CDisplayInfo::DisplayInfo_def dispInfo;

	dispInfo.m_hDevice = hMonitor;

	//dispInfo.m_rDimentions = *(lprcMonitor);

	MONITORINFOEX	monitorInfo;

	monitorInfo.cbSize = sizeof(MONITORINFOEX);

	if (GetMonitorInfo(hMonitor, &monitorInfo) == FALSE)
	{
#if 0
		std::string sMsg = 
			("Unable to info for selected Windows display: " + stringUtil::tos(pCls->m_nDisplayCntr));
		
		LogWrite(0, "ERROR:", sMsg);

#endif
		return FALSE;
		
	}

	dispInfo.m_sID = monitorInfo.szDevice;

	dispInfo.m_rDimentions = monitorInfo.rcWork;

	pCls->m_displayList.push_back(dispInfo);

	return TRUE;
};


#else

std::string winUtils::errMessage()
{
    std::string result;

	result = "";

	return result;
}


void winUtils::msgBox(const std::string sTitle, const std::string sMsg)
{
	std::cout << sTitle << ": " << sMsg << std::endl;

	return;
}

BOOL void winUtils::IsWow64()
{
	return false;
}



#endif


