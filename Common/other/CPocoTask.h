//*******************************************************************************
//* FILE:				CPocoTask.h
//*
//* DESCRIPTION:		The header file uses the POCO Activity class to 
//*						impliment an asynctonous task execution class.
//*






#include "Poco/Activity.h"
#include "Poco/Thread.h"

#include <string>
#include <iostream>


using Poco::Activity;
using Poco::Thread;


class CPocoTask
{
	Activity<CPocoTask> 	m_task;
	
	std::string				m_sStatus;
	
	bool					m_bAbort;

public:
	CPocoTask():
		m_task(this, &CPocoTask::execTask)
	{
		m_sStatus = "";
		
		m_bAbort = false;
	}
	
	std::string status()
	{
		m_sStatus = getTaskStatus();
		
		return m_sStatus;
	}
	
	void start()
	{
		m_task.start();
	}
	
	void stop(bAbort = false)
	{
		if (bAbort == true)
		{
			m_bAbort = true;
		}
		
		m_task.stop();
		m_task.wait();
	}
	
	bool isActive()
	{
		m_task.isRunning();
	}
	
	void wait(unsigned long lDur = 0)
	{
		if (lDur == 0)
		{
			m_task.wait();
		}
		else
		{
			m_task.wait(lDur);
		}
	}
	
protected:

	//* This 'taskProc' function should be over-ridden with the 
	//* function that should be executed for this "task';
	
	virtual void taskProc()
	{
		m_sStatus = "CPocoTask:  Executing taskProc ";
	}

	//* This 'gettaskStatus' function should be over-ridden with a 
	//* function that would return a string with 'taskProc" status;
	
	virtual void gettaskStatus()
	{
		m_sStatus = "CPocoTask:  Getting current taskProc status ";
	}
	
	//* Internal function to execute the 'taskProc'

	void execTask()
	{
		m_sStatus = "CPocoTask:  taskProc started ";
		
		taskProc();

		m_sStatus = "CPocoTask:  taskProc exited ";
	}
	
};

