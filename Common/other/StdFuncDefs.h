//******************************************************************
//* FILE: 	StdFuncDefs.h
//* 
//* DESC:	#define/macro definitions for STD functions
//* 
//* 
//* 
//* 


#ifndef _STDFUNCDEFS_H_
#define _STDFUNCDEFS_H_


#ifndef _VA_PASS_
#define _VA_PASS_

template<byte count>
struct SVaPassNext
{
    SVaPassNext<count-1> big;
    DWORD dw;
};

template<> struct SVaPassNext<0>{};
//SVaPassNext - is generator of structure of any size at compile time.

class CVaPassNext
{
public:
    SVaPassNext<50> svapassnext;
    CVaPassNext(va_list & args)
	{
		try
		{//to avoid access violation
			memcpy(&svapassnext, args, sizeof(svapassnext));
		} catch (...) {}
    }
};

#define va_pass(valist) CVaPassNext(valist).svapassnext

#endif // _VA_PASS_




#define LOG_FUNC(l, c, ...)	LogWrite(l, "%20.20s %10.10s %s", sStdFuncName.c_str(), c, __VA_ARGS__) 


	
	
#define StdFuncStart(n)					\\
	try									\\
	{									\\
	std::string sStdFuncName = n;		\\



#define StdFuncEnd				\\
	}									\\
	catch(std::exception const Err)		\\
	{									\\
		LOG_FUNC(0, "ERROR", Err);
	}									\\
	catch(...)							\\
	{									\\
		LOG_FUNC(0, "ERROR", "Unknown exception at file: %s, line: %d ", __FILE__, __LINE__);	
	}									\\

	
	
#define StdFuncWarning(s)	ELOG("WARNING   %s", s)
	
#define StdFuncError(s)		throw std::runtime_error(s)


	
#define 	



#endif  // _STDFUNCDEFS_H_