//*********************************************************************
//*
//*  CRegIO.h
//*
//*  assorted utility routines
//*
//*
//*  Written by: RUSS BARKER
//*



#define REG_MAX_KEY_LEN				256





class CRegParamIO
{
	HKEY	m_hRootKey;

	TCHAR	m_szKeyPath[REG_MAX_KEY_LEN];

	TCHAR	m_szKeyName[REG_MAX_KEY_LEN];

public:

	CRegParamIO();

	BOOL SetRootKey (HKEY hRoot)
	{
		m_hRootKey = hRoot;

		return TRUE;
	}

	BOOL SetKeyPath (TCHAR *pPath)
	{
		strcpy (m_szKeyPath, pPath);

		return TRUE;
	}

	BOOL SetKeyName (TCHAR *pKey)
	{
		strcpy (m_szKeyName, pKey);

		return TRUE;
	}

	BOOL GetStr (TCHAR *pKey, char* szTgtStr, char* szDefault);
	BOOL GetStr (char* szTgtStr, char* szDefault);

	BOOL SetStr (TCHAR *pKey, char* szStr);
	BOOL SetStr (char* szStr);

	BOOL GetInt (TCHAR *pKey, int *pVar, int iDefault);
	BOOL GetInt (int *pVar, int iDefault);

	BOOL SetInt (TCHAR *pKey, int iValue);
	BOOL SetInt (int iValue);

};





