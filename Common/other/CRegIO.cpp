//*********************************************************************
//*
//*  RegIO.cpp
//*
//*      Registry management class
//*
//*
//*  Written by: RUSS BARKER
//*


#include "stdafx.h"

#include "CRegIO.h"




//*********************************************************************
//*
//*  CRegParamIO:: ... constructor
//*

CRegParamIO::CRegParamIO ()
{
	m_hRootKey = HKEY_LOCAL_MACHINE;

	memset ((void *) m_szKeyPath, 0, sizeof(m_szKeyPath));

	memset ((void *) m_szKeyName, 0, sizeof(m_szKeyName));
}



//*********************************************************************
//*
//*  CRegParamIO::GetStr
//*

BOOL CRegParamIO::GetStr (TCHAR *pKey, char* szTgtStr, char* szDefault)
{
	SetKeyName (pKey);

	return GetStr (szTgtStr, szDefault);
}


BOOL CRegParamIO::GetStr (char* szTgtStr, char* szDefault)
{
	LONG lRet;
	
	HKEY hKeyPath;

	char  szKeyValue[REG_MAX_KEY_LEN];

	memset (szKeyValue, 0, sizeof(szKeyValue));

    lRet = RegOpenKey (m_hRootKey, m_szKeyPath, &hKeyPath);
    
	if (lRet == ERROR_SUCCESS) 
	{
        DWORD   dwType =  REG_SZ;
		DWORD   dwLen = sizeof(szKeyValue);
        
		lRet = RegQueryValueEx
				(
					hKeyPath, 
					m_szKeyName, 
					0L, 
					&dwType,
                    (LPBYTE) szKeyValue, 
					&dwLen
				);

        RegCloseKey (hKeyPath);

        if (lRet != ERROR_SUCCESS || strlen (szKeyValue) == 0) 
		{
			if (szDefault != NULL)
			{
				strcpy (szTgtStr, szDefault);
			}

			return FALSE;
		}
    }
	else
	{
		if (szDefault != NULL)
		{
			strcpy (szTgtStr, szDefault);
		}

		return FALSE;
	}

    strcpy(szTgtStr, szKeyValue);

	return TRUE;
}





//*********************************************************************
//*
//*  CRegParamIO::SetStr
//*

BOOL CRegParamIO::SetStr (TCHAR *pKey, char* szStr)
{
	SetKeyName (pKey);

	return SetStr (szStr);
}


BOOL CRegParamIO::SetStr (char* szStr)
{
	LONG lRet;
	
	DWORD dwDisposition;   

	HKEY hKeyPath;

    lRet = RegOpenKey (m_hRootKey, m_szKeyPath, &hKeyPath);
	
	if (lRet != ERROR_SUCCESS)
	{
		lRet = RegCreateKeyEx
				(
					m_hRootKey, 
					m_szKeyPath,
					0, 
					m_szKeyName,
					REG_OPTION_NON_VOLATILE, 
					KEY_ALL_ACCESS, 
					NULL, 
					&hKeyPath, 
					&dwDisposition
				);
	}
    
	if (lRet == ERROR_SUCCESS) 
	{
        DWORD   dwLen = (DWORD) (strlen(szStr) + 1);
        
		lRet = RegSetValueEx 
				(
					hKeyPath, 
					m_szKeyName, 
					0L, 
					REG_SZ, 
					(LPBYTE) szStr, 
					dwLen
				);
        
		RegCloseKey (hKeyPath);

		if (lRet == ERROR_SUCCESS) 
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}



//*********************************************************************
//*
//*  CRegParamIO::GetInt
//*

BOOL CRegParamIO::GetInt (TCHAR *pKey, int *pVar, int iDefault)
{
	SetKeyName (pKey);

    return GetInt (pVar, iDefault);
}


BOOL CRegParamIO::GetInt (int *pVar, int iDefault)
{
	LONG lRet;
    
	HKEY hKeyPath;
    
    DWORD   dwType, dwLen;
        
	static int  iX;

	iX = iDefault;
	
    lRet = RegOpenKey (m_hRootKey, m_szKeyPath, &hKeyPath);
    
	if (lRet == ERROR_SUCCESS) 
	{
		dwLen = sizeof(iX);

		lRet = RegQueryValueEx
				(
					hKeyPath, 
					m_szKeyName, 
					0L, 
					&dwType,
                    (LPBYTE) &iX, 
					&dwLen
				);
        
        RegCloseKey(hKeyPath);

		if (lRet != ERROR_SUCCESS) 
		{
            *pVar = iDefault;

			return FALSE;
        }
    }

	*pVar = iX;

    return TRUE;
}




//*********************************************************************
//*
//*  CRegParamIO::SetInt
//*

BOOL CRegParamIO::SetInt (TCHAR *pKey, int iValue)
{
	SetKeyName (pKey);

    return SetInt (iValue);
}


BOOL CRegParamIO::SetInt (int iValue)
{
	LONG lRet;
    
	DWORD dwDisposition;   

	HKEY hKeyPath;

    DWORD   dwLen;
        
	static int  iX;

	iX = iValue;
	
    lRet = RegOpenKey (m_hRootKey, m_szKeyPath, &hKeyPath);

	if (lRet != ERROR_SUCCESS)
	{
		lRet = RegCreateKeyEx 
				(
					m_hRootKey, 
					m_szKeyPath,
					0, 
					m_szKeyName,
					REG_OPTION_NON_VOLATILE, 
					KEY_ALL_ACCESS, 
					NULL, 
					&hKeyPath, 
					&dwDisposition
				);

		if (lRet != ERROR_SUCCESS)
		{
			return FALSE;
		}
	}

    if (lRet == ERROR_SUCCESS) 
	{
		dwLen = sizeof(iX);
        
		lRet = RegSetValueEx
				(
					hKeyPath, 
					m_szKeyName, 
					0L, 
					REG_DWORD, 
					(LPBYTE) &iX, 
					dwLen
				);

		RegCloseKey(hKeyPath);

		if (lRet != ERROR_SUCCESS)
		{
			return FALSE;
		}
	}

	return TRUE;
}

