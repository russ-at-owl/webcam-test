//**************************************************************************************************
//* FILE:		winUtils.h
//*
//* DESCRIP:	
//*
//*



#ifndef _WINUTILS_H_
#define _WINUTILS_H_

#ifdef WIN32

// Currently a dumping ground for things that defy
// easy categorization.

#include <string>
#include <vector>



namespace winUtils
{
	
	typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);

	
	// return system error string
	std::string errMessage() throw();

	void msgBox(const std::string sTitle, const std::string sMsg) throw();

	BOOL IsWow64();

#ifdef SUPPORT_DLL_FUNC

#ifdef UNICODE
	inline PVOID getDllHandle(_In_ PWSTR DllName);
#else
	inline PVOID getDllHandle(_In_ PSTR DllName);
#endif

	inline PVOID getProcedureAddress
		(
			_In_ PVOID DllHandle,
			_In_opt_ PSTR ProcedureName,
			_In_opt_ ULONG ProcedureNumber
		);

#endif
};



BOOL CALLBACK MonitorEnumProc
	(
		HMONITOR hMonitor,
		HDC      hdcMonitor,
		LPRECT   lprcMonitor,
		LPARAM   dwData
	);

class CDisplayInfo
{

public:

	//* Type defs

	typedef struct
	{
		std::string m_sID;

		HMONITOR	m_hDevice;

		RECT		m_rDimentions;

	} DisplayInfo_def;

	//* Member data defs

	typedef std::vector<DisplayInfo_def> DisplayInfoList_def;

	DisplayInfoList_def	m_displayList;

	int 				m_nDisplayCntr;

	//* Function defs

	CDisplayInfo(bool bEnumDisplays = false) :
		m_nDisplayCntr(0)
	{
		if (bEnumDisplays == true)
		{
			enumDisplays();
		}
	}
	
	bool enumDisplays()
	{
		EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, (LPARAM) this);

		return true;
	}

	int getDisplayNum(HMONITOR hDevID)
	{
		int nIdx = 0;

		for (auto x = m_displayList.begin(); x != m_displayList.end(); x++)
		{
			if ((*x).m_hDevice == hDevID)
			{
				return nIdx;
			}

			nIdx++;
		}

		return -1;
	}

};


#endif	// WIN32

#endif  // _WINUTILS_H_

