//*****************************************************************************
//* FILE: 		SysUtils.cpp 
//*
//* DESCRIP:	System utility functions definitions
//*
//*


#include <cstdio>
#include <cstring>

#include <string>

#include <stdexcept>


void RebootSystem(const std::string &sRebootWarning) throw(std::runtime_error);
