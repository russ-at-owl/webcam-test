//*****************************************************************************
//* FILE: 		SysUtils.cpp 
//*
//* DESCRIP:	System utility functions
//*
//*



#ifdef WIN32
#ifndef _WINDOWS_
#include <windows.h>
#endif
#else


#endif




#include "SysUtils.h"





void RebootSystem(const std::string &sRebootWarning) throw(std::runtime_error)
{
	try
	{
#ifdef WIN32
		HANDLE hToken; 
		TOKEN_PRIVILEGES tkp; 

		BOOL bRet = FALSE;

		//* Get a token for this process. 
 
		bRet = OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
		if (bRet != TRUE)
		{
			throw("ERROR: Problem getting process token for 'Reboot' ");
		}

		//* Get the LUID for the shutdown privilege. 
 
		LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid); 
 
		tkp.PrivilegeCount = 1;  // one privilege to set    
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
 
		//* Get the shutdown privilege for this process. 
 
		bRet = AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES) NULL, 0); 
 		if (GetLastError() != ERROR_SUCCESS) 
		{
			throw("ERROR: Problem getting system privileges for 'Reboot' ");
		}

		bRet = 
			//InitiateShutdown
			//(
			//	NULL, 
			//	"Server has received an HTTP 'reboot' command",  
			//	5, 
			//	(SHUTDOWN_FORCE_OTHERS | SHUTDOWN_RESTART),
			//	(SHTDN_REASON_MAJOR_APPLICATION | SHTDN_REASON_MINOR_MAINTENANCE)
			//);
			InitiateSystemShutdownEx
			(
				NULL, 
				(LPSTR) sRebootWarning.c_str(),  
				5, 
				true,
				true,
				(SHTDN_REASON_MAJOR_APPLICATION | SHTDN_REASON_MINOR_MAINTENANCE)
			);
		if (bRet == FALSE)
		{
			throw("ERROR: Problem calling system 'Reboot' function ");
		}

#else

#define LINUX_REBOOT_CMD_POWER_OFF 0x4321fedc   

		cout << sRebootWarning << std::eol;
		
		sleep(5);
		
		sync();
		
		reboot(LINUX_REBOOT_CMD_POWER_OFF);

#endif
	}
	catch(...)
	{
		throw("ERROR: Problem calling system 'Reboot' function ");
	}
}

