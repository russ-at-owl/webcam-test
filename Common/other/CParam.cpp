//*****************************************************************************
//* Module Name:    CParam.cpp  
//*
//* Description:
//*
//*
//*
//* Version:        1.00
//* Last Updated:   11/20/03
//*
//*****************************************************************************
//*
//* Created By:     Russ Barker
//*
//****************************************************************************
                     
#pragma warning(disable : 4244)
#pragma warning(disable : 4311)
#pragma warning(disable : 4312)
#pragma warning(disable : 4800)
#pragma warning(disable : 4996) 

#include "Windows.h"

#include "CParam.h"
                     












//********************************************************************
//* Function: CParamsBase ... constructor
//*
//* Purpose : 
//*         Sets the param values to the defaults .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
CParamsBase::CParamsBase ()
{
	memset (m_szParamPath, 0, sizeof (m_szParamPath));
	memset (m_szParamKey, 0, sizeof (m_szParamKey));

	m_bParamPathSet = FALSE;
	m_bParamKeySet = FALSE;
}



//********************************************************************
//* Function: SetParamPath
//*
//* Purpose : 
//*         Sets the param Path value
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::SetParamPath (char *pParamPath)
{
	strcpy (m_szParamPath, pParamPath);

	m_bParamPathSet = TRUE;

	return TRUE;
}



//********************************************************************
//* Function: GetParamPath
//*
//* Purpose : 
//*         Gets the param Path value
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::GetParamPath (char *pParamPath)
{
	strcpy (pParamPath, m_szParamPath);

	return TRUE;
}



//********************************************************************
//* Function: SetParamKey
//*
//* Purpose : 
//*         Sets the param Key value
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::SetParamKey (char *pParamKey)
{
	strcpy (m_szParamKey, pParamKey);

	m_bParamKeySet = TRUE;

	return TRUE;
}



//********************************************************************
//* Function: GetParamKey
//*
//* Purpose : 
//*         Gets the param Key value
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::GetParamKey (char *pParamKey)
{
	strcpy (pParamKey, m_szParamKey);

	return TRUE;
}



//********************************************************************
//* Function: Load
//*
//* Purpose : 
//*         Gets the param values from the ".ini" file .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::Load ()
{
	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

	return TRUE;
}



//********************************************************************
//* Function: Save
//*
//* Purpose : 
//*         Saves the param values to the ".ini" file
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::Save ()
{
	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

	return TRUE;
}



//********************************************************************
//* Function: ReadInt
//*
//* Purpose : 
//*         Gets the param values from the ".ini" file .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
int CParamsBase::ReadInt (char *szParamID, int nDefault)
{
    int     x;

	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

    x = GetPrivateProfileInt 
		(
			m_szParamKey, 
			szParamID, 
			nDefault, 
			m_szParamPath
		);

	return x;
}



//********************************************************************
//* Function: ReadStr
//*
//* Purpose : 
//*         Gets the param values from the ".ini" file .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::ReadStr 
	(
		char *szParamID, 
		char *szDefault, 
		char *szStr,
		int nMax
	)
{
	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

    GetPrivateProfileString 
	(
		m_szParamKey, 
		szParamID, 
		szDefault,
		szStr,
		nMax,
		m_szParamPath
	);

	return TRUE;
}




//********************************************************************
//* Function: WriteInt
//*
//* Purpose : 
//*         Writes the param values to the ".ini" file .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::WriteInt (char *szParamID, int nValue)
{
    char    szTempStr[80];

	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

    wsprintf (szTempStr, "%d", nValue);

    WritePrivateProfileString 
	(
		m_szParamKey, 
		szParamID, 
		szTempStr, 
		m_szParamPath
	);

	return TRUE;
}



//********************************************************************
//* Function: WriteStr
//*
//* Purpose : 
//*         Writes the param values to the ".ini" file .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CParamsBase::WriteStr (char *szParamID, char *szStr)
{
	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

    WritePrivateProfileString 
	(
		m_szParamKey, 
		szParamID, 
		szStr, 
		m_szParamPath
	);

	return TRUE;
}




                     
                     

//********************************************************************
//* Function: CWinParams ... constructor
//*
//* Purpose : 
//*         Sets the param values to the defaults .
//*
//* Params: 
//*
//* Return:  
//********************************************************************
CWinParamsBase::CWinParamsBase ():
	CParamsBase ()
{
	strcpy (m_szParamKey, "UndefinedWnd");

    m_bLoadWndPos = TRUE;
    m_bSaveWndPos = TRUE;

    m_bLoadBgColor = FALSE;
    m_bSaveBgColor = FALSE;

    m_bLoadWndFont = FALSE;
    m_bSaveWndFont = FALSE;

    m_bLoadWndName = FALSE;
    m_bSaveWndName = FALSE;
}


//********************************************************************
//* Function: Load
//*
//* Purpose : 
//*         Gets the param values
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CWinParamsBase::Load ()
{
	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

	if (m_bLoadWndPos)
	{
		m_WndData.nXPos =         
			ReadInt 
			(
				"XPos", 
				m_WndData.nXPos
			);

		m_WndData.nYPos =         
			ReadInt 
			(
				"YPos", 
				m_WndData.nYPos
			);
	}

	if (m_bLoadBgColor)
	{
		m_WndData.crBgColor = (COLORREF)      
			ReadInt 
			(
				"BgColor", 
				(int) m_WndData.crBgColor
			);
	}

	if (m_bLoadWndFont)
	{
		m_WndData.hFont = (HFONT)
			ReadInt 
			(
				"WndFont", 
				(int) (m_WndData.hFont)
			);
	}

	if (m_bLoadWndName)
	{
		ReadStr
		(
			"WndName", 
			m_WndData.szWndName,
			m_WndData.szWndName,
			sizeof (m_WndData.szWndName)
		);
	}

	return TRUE;
}


//********************************************************************
//* Function: Save
//*
//* Purpose : 
//*         Saves the param values
//*
//* Params: 
//*
//* Return:  
//********************************************************************
BOOL CWinParamsBase::Save ()
{
	if (m_bParamPathSet == FALSE)
	{
		return FALSE;
	}

	if (m_bParamKeySet == FALSE)
	{
		return FALSE;
	}

	if (m_bSaveWndPos)
	{
		WriteInt 
		(
			"XPos", 
			m_WndData.nXPos
		);
    
		WriteInt 
		(
			"YPos", 
			m_WndData.nYPos
		);
	}

	if (m_bSaveBgColor)
	{
		WriteInt 
		(
			"BgColor", 
			(int) m_WndData.crBgColor
		);
	}

	if (m_bSaveWndFont)
	{
		WriteInt 
		(
			"WndFont", 
			(int) (m_WndData.hFont)
		);
	}

	if (m_bSaveWndName)
	{
		WriteStr 
		(
			"WndName", 
			m_WndData.szWndName
		);
	}

    return TRUE;
}




//********************************************************************
//* Function: GetWndData
//*
//* Purpose : 
//*         Copies the Window data structure
//*
//* Params: 
//*
//* Return:  
//********************************************************************

BOOL CWinParamsBase::GetWndData (PWNDDATADEF pWndData)
{
	memcpy (pWndData, &m_WndData, sizeof (WNDDATADEF));

    return TRUE;
}




//********************************************************************
//* Function: SetWndData
//*
//* Purpose : 
//*         Copies the Window data structure
//*
//* Params: 
//*
//* Return:  
//********************************************************************

BOOL CWinParamsBase::SetWndData (PWNDDATADEF pWndData)
{
	memcpy (&m_WndData, pWndData, sizeof (WNDDATADEF));

    return TRUE;
}






#pragma warning(default : 4244)
#pragma warning(default : 4311)
#pragma warning(default : 4312)
#pragma warning(default : 4800)
