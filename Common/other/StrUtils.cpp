//*****************************************************************************
//* StrUtils.cpp - String utility functions implimentation file
//*

#pragma warning( disable : 4996 ) 

#include <Windows.h>
#include <stdio.h>

#include <string>

#include "..\file\AppLog.h"
#include "StrUtils.h"



INT LeftShiftString(CHAR *pStr, INT nLen, INT nShift)
{
	if (pStr == NULL || nShift < 1 || nShift > (nLen - 1))
	{
		return -1;
	}

#if 1

	INT x = 0;

	CHAR *pTrgt = pStr;
	CHAR *pSrc = (pStr + nShift);

	//* copy the relevant portion of the string (start + shift) 
	//* to the beginning of the string

	while ((x + nShift) < nLen)
	{
		*(pTrgt) = *(pSrc);
		pTrgt++;
		pSrc++;
		x++;
	}

	//* zero out the remaining portion of the string \

	memset(pTrgt, 0, nShift);

#else

	CHAR *pTmp = (CHAR *) calloc((nLen + 1), sizeof(CHAR));

	if (pTmp == NULL)
	{
		return -1;
	}

	//* clear the temp buffer

	memset(pTmp, 0, (sizeof(CHAR) * nLen));

	//* copy the relevant portion of the string to the temp buffer

	strncpy(pTmp, (pStr + nShift), (nLen - nShift));

	//* clear the input string

	memset(pStr, 0, (sizeof(CHAR) * nLen));

	//* copy the temp buffer back to the input string

	strncpy(pStr, pTmp, (nLen - nShift));

	free(pTmp);

#endif

	return nLen;
}


INT RightShiftString(CHAR *pStr, CHAR cInsChar, INT nLen, INT nShift)
{
	if (pStr == NULL || nShift < 1 || (nShift + nLen) > STR_MAX)
	{
		return -1;
	}

	CHAR *pTmp = (CHAR *) calloc((nLen + 1), sizeof(CHAR));

	if (pTmp == NULL)
	{
		return -1;
	}

	//* clear the temp buffer

	memset(pTmp, 0, (sizeof(CHAR) * nLen));

	//* copy the relevant portion of the string to the temp buffer

	for (INT x = 0; x < nShift; x++)
	{
		*(pTmp + x) = cInsChar;
	}

	strncpy((pTmp + nShift), pStr, nLen);

	//* clear the input string

	memset(pStr, 0, (sizeof(CHAR) * nLen));

	//* copy the temp buffer back to the input string

	strncpy(pStr, pTmp, (nLen + nShift));

	free(pTmp);

	return nLen;
}


INT Str2Lower(CHAR *pTarget, CHAR *pSource, INT nLen)
{
	if (pTarget == NULL || pSource == NULL || nLen < 1)
	{
		//LogWrite(2, TEXT("ERROR - Str2Lower called with invalid parameter(s) "));

		return -1;
	}

	INT x;

	for (x = 0; x < nLen; x++)
	{
		if (*pSource == NULL)
		{
			break;
		}

		*pTarget = tolower(*pSource);

		pSource++;
		pTarget++;
	}

	return x;
}


INT Str2Upper(CHAR *pTarget, CHAR *pSource, INT nLen)
{
	if (pTarget == NULL || pSource == NULL || nLen < 1)
	{
		//LogWrite(2, TEXT("ERROR - Str2Lower called with invalid parameter(s) "));

		return -1;
	}

	INT x;

	for (x = 0; x < nLen; x++)
	{
		if (*pSource == NULL)
		{
			break;
		}

		*pTarget = tolower(*pSource);

		pSource++;
		pTarget++;
	}

	return x;
}


INT Compare(TCHAR *pStr1, TCHAR *pStr2, INT nCmpLen, BOOL bIgnoreCase)
{
	if (pStr1 == NULL || pStr2 == NULL)
	{
		//LogWrite(2, TEXT("ERROR - Compare called with invalid parameter(s) "));

		return _NLSCMPERROR;
	}

	if (bIgnoreCase == FALSE)
	{
		return strncmp(pStr1, pStr2, nCmpLen);
	}

	if (nCmpLen >= STR_MAX)
	{
		return _NLSCMPERROR;
	}

	int x;

	CHAR szTemp1[STR_MAX];
	CHAR szTemp2[STR_MAX];

	ZERO_BUFFER(szTemp1);
	ZERO_BUFFER(szTemp2);

	x = Str2Lower(szTemp1, pStr1, nCmpLen);
	if (x < 1)
	{
		return _NLSCMPERROR;
	}

	x = Str2Lower(szTemp2, pStr2, nCmpLen);
	if (x < 1)
	{
		return _NLSCMPERROR;
	}

	return strncmp(szTemp1, szTemp2, nCmpLen);
}


INT GetNextLine(CHAR *pTarget, CHAR *pSource, INT nMax)
{
	INT x = 0;

	for (x = 0; x < nMax; x++)
	{
		if (*pSource == '\n' || *pSource == '\r' || *pSource == '\0')
		{
			return x;
		}

		*pTarget = *pSource;

		pSource++;
		pTarget++;
	}

	return 0;
}



