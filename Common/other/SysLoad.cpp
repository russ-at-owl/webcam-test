//*******************************************************************************
//*
//* FILE:	SysLoad.cpp
//*
//* DESC:	CPU info functions
//*
//*



#include "SysLoad.h"



sysLoadData_def		CSysLoad::m_sysLoadData;

    
#ifdef WIN32

int CSysLoad::getCpuCount()
{
	SYSTEM_INFO sysinfo;

	GetSystemInfo(&sysinfo);
	
	return ((int) (sysinfo.dwNumberOfProcessors));
}

void CSysLoad::initCpuLoad(bool bPerProcessor)
{
	m_sysLoadData.cpuCnt = 0;
	m_sysLoadData.cpuQuery = 0;
	m_sysLoadData.cpuTotal = 0;

	for (int x = 0; x < SYSLOAD_MAX_CPUS; x++)
	{
		m_sysLoadData.cpuProcessor[x] = 0;
	}

	PdhOpenQuery(NULL, NULL, &(m_sysLoadData.cpuQuery));
    
	PdhAddCounter((m_sysLoadData.cpuQuery), "\\Processor(_Total)\\% Processor Time", NULL, &(m_sysLoadData.cpuTotal));

	m_sysLoadData.cpuCnt = CSysLoad::getCpuCount();

	//* These functions currently only
	//* support 16 or less CPUs

	if ((m_sysLoadData.cpuCnt) > SYSLOAD_MAX_CPUS)
	{
		m_sysLoadData.cpuCnt = SYSLOAD_MAX_CPUS;
	}

	if (bPerProcessor == true && (m_sysLoadData.cpuCnt) > 0)
	{
		std::string sCpu;
		std::string sQuery;

		for (unsigned int x = 0; x < (m_sysLoadData.cpuCnt); x++)
		{
			sCpu = stringUtil::tos(x);

			sQuery = ("\\Processor(" + sCpu + ")\\% Processor Time");

			PdhAddCounter((m_sysLoadData.cpuQuery), (sQuery.c_str()), NULL, &(m_sysLoadData.cpuTotal));
		}
	}
    
	PdhCollectQueryData((m_sysLoadData.cpuQuery));
}

double CSysLoad::getCpuLoad(int nCpu)
{
    PDH_FMT_COUNTERVALUE counterVal;

	PdhCollectQueryData((m_sysLoadData.cpuQuery));
    
	if (nCpu < 0)
	{
		PdhGetFormattedCounterValue((m_sysLoadData.cpuTotal), PDH_FMT_DOUBLE, NULL, &counterVal);
	}
	else
	{
		if (nCpu >= SYSLOAD_MAX_CPUS)
		{
			return -1;
		}

		PdhGetFormattedCounterValue((m_sysLoadData.cpuProcessor[nCpu]), PDH_FMT_DOUBLE, NULL, &counterVal);
	}
    
	return counterVal.doubleValue;
}

#else

int CSysLoad::getCpuCount()
{
	int nRet = sysconf(_SC_NPROCESSORS_ONLN);

	return nRet;
}

void CSysLoad::initCpuLoad(bool bPerProcessor)
{
	m_sysLoadData.lastTotalUser = 0;
	m_sysLoadData.lastTotalUserLow = 0;
	m_sysLoadData.lastTotalSys = 0;
	m_sysLoadData.lastTotalIdle = 0;

	FILE* file = fopen("/proc/stat", "r");

    fscanf
	(
		file, 
		"cpu %llu %llu %llu %llu", 
		&(m_sysLoadData.lastTotalUser), 
		&(m_sysLoadData.lastTotalUserLow), 
		&(m_sysLoadData.lastTotalSys), 
		&(m_sysLoadData.lastTotalIdle)
	);

    fclose(file);
}

double CSysLoad::getCpuLoad(int nCpu)
{
	if (nCpu >= 0)
	{
		//* Per CPU not currently supported

		return -1;
	}

    double percent;
    FILE* file;

    unsigned longlong totalUser = 0;
	unsigned longlong totalUserLow = 0;
	unsigned longlong totalSys = 0;
	unsigned longlong totalIdle = 0;
	unsigned longlong total = 0;

    file = fopen("/proc/stat", "r");

    fscanf
	(
		file, 
		"cpu %llu %llu %llu %llu", 
		&totalUser, 
		&totalUserLow, 
		&totalSys, 
		&totalIdle
	);
    
	fclose(file);

	if (totalUser < m_sysLoadData.lastTotalUser || totalUserLow < m_sysLoadData.lastTotalUserLow ||
		totalSys < m_sysLoadData.lastTotalSys || totalIdle < m_sysLoadData.lastTotalIdle)
	{
        //Overflow detection. Just skip this value.
        percent = -1.0;
    }
    else
	{
        total = 
			(totalUser - m_sysLoadData.lastTotalUser) + 
			(totalUserLow - m_sysLoadData.lastTotalUserLow) +
			(totalSys - m_sysLoadData.lastTotalSys);

        percent = total;

		total += (totalIdle - m_sysLoadData.lastTotalIdle);
        
		percent /= total;
        percent *= 100;
    }

	m_sysLoadData.lastTotalUser = totalUser;
	m_sysLoadData.lastTotalUserLow = totalUserLow;
	m_sysLoadData.lastTotalSys = totalSys;
	m_sysLoadData.lastTotalIdle = totalIdle;

    return percent;
}

#endif

