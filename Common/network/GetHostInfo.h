
#ifndef GETHOSTINFO_H
#define GETHOSTINFO_H

#ifdef WIN32

#include <Windows.h>

#include <Winhttp.h>
//#include <PocoHttpClient.h>

#include <iphlpapi.h>

#define MALLOC(x)	HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x)		{HeapFree(GetProcessHeap(), 0, (x)); x = NULL;}
#else

#define MALLOC(x)	malloc(x)
#define FREE(x)		{free(x); x = 0;}
#endif


#pragma warning( disable : 4081 )
#pragma warning( disable: 4251 )
#pragma warning( disable: 4996 )

#include "stringUtils.h"

#include <vector>



#ifndef IPv4_def
typedef struct 
{
	unsigned char b1, b2, b3, b4;
	
} IPv4_def;
#endif


typedef std::vector<IPv4_def>		IPList_def;
typedef std::vector<std::string>	AddrList_def;



std::string getHostName();

bool getLocalIPList(IPList_def &ipList);

bool getLocalAddrList(AddrList_def &addrList);

bool getIPList(IPList_def &ipList, const std::string sHost = "");

bool getAddrList(AddrList_def &addrList, const std::string sHost = "");

bool checkIP(IPList_def &ipList, const IPv4_def &ip);

bool checkAddr(AddrList_def &addrList, const std::string sAddr);


#endif // GETHOSTINFO_H
