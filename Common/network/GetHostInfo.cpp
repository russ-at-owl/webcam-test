

#include "GetHostInfo.h"




std::string getHostName()
{
	char		szBuffer[MAX_PATH];

#ifdef WIN32
#ifdef PERFORM_WSA_OPS
	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(2, 0);
	if(::WSAStartup(wVersionRequested, &wsaData) != 0)
	{
		return false;
	}
#endif
#endif

	if(gethostname(szBuffer, sizeof(szBuffer)) == SOCKET_ERROR)
	{
#ifdef WIN32
#ifdef PERFORM_WSA_OPS
		WSACleanup();
#endif
#endif
		return false;
	}

#ifdef WIN32
#ifdef PERFORM_WSA_OPS
	WSACleanup();
#endif
#endif

	std::string sOut = szBuffer;

	return sOut;
}


bool getLocalIPList(IPList_def &ipList)
{
	bool bRet = false;

#ifdef WIN32

	//* These are some Windows specific routines to get local IPs

#ifdef PERFORM_WSA_OPS
	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(2, 0);
	if(::WSAStartup(wVersionRequested, &wsaData) != 0)
	{
		return false;
	}
#endif

    int i;
    
    //* Variables used by GetIpAddrTable 
    PMIB_IPADDRTABLE pIPAddrTable;

    DWORD dwSize = 0;
    DWORD dwRetVal = 0;

    //* Before calling AddIPAddress 
	//* we use GetIpAddrTable to get an adapter 
    //* which we can add the IP.
    pIPAddrTable = (MIB_IPADDRTABLE *) MALLOC (sizeof (MIB_IPADDRTABLE));

    if (pIPAddrTable) 
	{
        //* Make an initial call to GetIpAddrTable to get the
        //* necessary size into the dwSize variable

        if (GetIpAddrTable(pIPAddrTable, &dwSize, 0) == ERROR_INSUFFICIENT_BUFFER) 
		{
			FREE(pIPAddrTable);

            pIPAddrTable = (MIB_IPADDRTABLE *) MALLOC (dwSize);
        }
        
		if (pIPAddrTable == NULL) 
		{
#ifdef PERFORM_WSA_OPS
			WSACleanup();
#endif
			return false;
        }
    }
	else
	{
#ifdef PERFORM_WSA_OPS
		WSACleanup();
#endif
		return false;
	}

    //* Make a second call to GetIpAddrTable to get the
    //* actual data we want

    if ((dwRetVal = GetIpAddrTable( pIPAddrTable, &dwSize, 0 )) != NO_ERROR) 
	{ 
#ifdef PERFORM_WSA_OPS
		WSACleanup();
#endif
		return false;
    }

	if (pIPAddrTable == NULL)
	{ 
#ifdef PERFORM_WSA_OPS
		WSACleanup();
#endif
		return false;
    }

	ipList.clear();

	in_addr *addr_entry = NULL;

	//unsigned int o1, o2, o3, o4;

	IPv4_def	IPAddr;

    for (i=0; i < (int) pIPAddrTable->dwNumEntries; i++) 
	{
#ifdef WINXP
		switch (pIPAddrTable->table[i].wType)
		{
		case MIB_IPADDR_PRIMARY:
		case MIB_IPADDR_DYNAMIC:
		case MIB_IPADDR_TRANSIENT:
#endif
			if (pIPAddrTable->table[i].dwAddr == 16777343)
			{
				continue;
			}

			addr_entry = (struct in_addr *) &(pIPAddrTable->table[i].dwAddr);

#if 0
			o1 = (addr_entry->S_un.S_un_b.s_b1);
			o2 = (addr_entry->S_un.S_un_b.s_b2);
			o3 = (addr_entry->S_un.S_un_b.s_b3);
			o4 = (addr_entry->S_un.S_un_b.s_b4);

			//* if this is "127.0.0.1"... skip it

			if (o1 == 127 && o2 == 0 && o3 == 0 && o4 == 1)
			{
				continue;
			}

			IPAddr.b1 = o1;
			IPAddr.b2 = o2;
			IPAddr.b3 = o3;
			IPAddr.b4 = o3;
#else
			IPAddr.b1 = (addr_entry->S_un.S_un_b.s_b1);
			IPAddr.b2 = (addr_entry->S_un.S_un_b.s_b2);
			IPAddr.b3 = (addr_entry->S_un.S_un_b.s_b3);
			IPAddr.b4 = (addr_entry->S_un.S_un_b.s_b4);
#endif

			//* add it to the list (vector)

			ipList.push_back(IPAddr);

			bRet = true;
#ifdef WINXP
			break;
		}
#endif
    }

    if (pIPAddrTable) 
	{
        FREE(pIPAddrTable);
    }

#ifdef PERFORM_WSA_OPS
	WSACleanup();
#endif

#else

	bRet = getIPList(ipList);

#endif

	return bRet;
}


bool getIPList(IPList_def &ipList, const std::string sHost)
{
	std::string sName = "";

	//if (sHost == "")
	if (sHost.length() == 0)
	{
		sName = getHostName();
	}
	else
	{
		sName = sHost;
	}

#ifdef WIN32
#ifdef PERFORM_WSA_OPS
	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(2, 0);
	if(::WSAStartup(wVersionRequested, &wsaData) != 0)
	{
		return false;
	}
#endif
#endif

	if (ipList.size() > 0)
	{
		ipList.clear();
	}

	struct hostent *host = NULL;

	host = gethostbyname(sName.c_str());
	if(host == NULL)
	{
#ifdef WIN32
#ifdef PERFORM_WSA_OPS
		WSACleanup();
#endif
#endif
		return false;
	}

	//* verify that the address type is an IPv4 internet address

	if (host->h_addrtype == AF_INET)
	{
		//unsigned int o1, o2, o3, o4;

		IPv4_def	IPAddr;

		in_addr *addr_entry = NULL;

		int i = 0;

		//* step through the list of IPs for this system

		while (host->h_addr_list[i] != 0)
		{
			addr_entry = (struct in_addr *) host->h_addr_list[i];

			//* Obtain the computer's IP (as octet's)
#if 0
			o1 = (addr_entry->S_un.S_un_b.s_b1);
			o2 = (addr_entry->S_un.S_un_b.s_b2);
			o3 = (addr_entry->S_un.S_un_b.s_b3);
			o4 = (addr_entry->S_un.S_un_b.s_b4);

			//* if this is "127.0.0.1"... skip it

			if (o1 == 127 && o2 == 0 && o3 == 0 && o4 == 1)
			{
				continue;
			}

			IPAddr.b1 = o1;
			IPAddr.b2 = o2;
			IPAddr.b3 = o3;
			IPAddr.b4 = o3;
#else
			IPAddr.b1 = (addr_entry->S_un.S_un_b.s_b1);
			IPAddr.b2 = (addr_entry->S_un.S_un_b.s_b2);
			IPAddr.b3 = (addr_entry->S_un.S_un_b.s_b3);
			IPAddr.b4 = (addr_entry->S_un.S_un_b.s_b4);
#endif
			//* add it to the list (vector)

			ipList.push_back(IPAddr);

			i++;
		}
	}

#ifdef WIN32
#ifdef PERFORM_WSA_OPS
	WSACleanup();
#endif
#endif

	return true;
}


bool getLocalAddrList(AddrList_def &addrList)
{
	return getAddrList(addrList, "");
}

bool getAddrList(AddrList_def &addrList, const std::string sHost)
{
	try
	{
		IPList_def ipList;

		//if (sHost == "")
		if (sHost.length() == 0)
		{
			if (getLocalIPList(ipList) == false)
			{
				return false;
			}
		}
		else
		{
			if (getIPList(ipList, sHost) == false)
			{
				return false;
			}
		}

		if (addrList.size() > 0)
		{
			ipList.clear();
		}

		std::string sAddr;

		unsigned int o1, o2, o3, o4;

		for (auto x = ipList.begin(); x != ipList.end(); x++)
		{
			o1 = ((*x).b1);
			o2 = ((*x).b2);
			o3 = ((*x).b3);
			o4 = ((*x).b4);

			sAddr = (stringUtil::tos(o1) + "." + stringUtil::tos(o2) + "." + stringUtil::tos(o3) + "." + stringUtil::tos(o4));

			addrList.push_back(sAddr);
		}

		return true;
	}
	catch (...)
	{
	}

	return false;
}



bool checkIP(const IPList_def ipList, const IPv4_def &ip)
{
	try
	{
		for (auto x = ipList.begin(); x != ipList.end(); x++)
		{
			if (((*x).b1 == ip.b1) && ((*x).b2 == ip.b2) && ((*x).b3 == ip.b3) && ((*x).b4 == ip.b4))
			{
				return true;
			}
		}

		return true;
	}
	catch (...)
	{
	}

	return false;
}


bool checkAddr(AddrList_def &addrList, const std::string sAddr)
{
	try
	{
		for (auto x = addrList.begin(); x != addrList.end(); x++)
		{
			if ((*x) == sAddr)
			{
				return true;
			}
		}

		return true;
	}
	catch (...)
	{
	}

	return false;
}
