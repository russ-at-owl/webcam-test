//*********************************************************************
//*
//* FILE: 	LinuxDefs.h
//*
//* DESCRIP:	Linux global definitions file header
//*






#define MAX_PATH		2048

//* variable definitions

#define TRUE			true
#define FALSE			false

#define BOOL			bool

#define CHAR			char
#define TCHAR			char
//#define TCHAR			wchar_t
#define LPSTR			char*
#define BYTE			unsigned char
#define LPSTR			char*
#define LPTSTR			char*
#define LPCSTR			const char*

#define INT				int
#define UINT			unsigned int
#define WORD			unsigned int
#define LONG			long
#define DWORD			unsigned long
#define FLOAT			float
#define DOUBLE 			float

//#define HANDLE			int
#define HANDLE			FILE*
#define HRESULT			unsigned int

#define VOID			void
#define PVOID			void *
#define LPVOID			void *

#define TEXT			(CHAR *)
#define _T			(CHAR *)


//#ifndef size_t
//#define size_t			int
//#endif


//* flag definitions

#define CREATE_ALWAYS	1
#define OPEN_ALWAYS		1

#define S_OK			0
#define E_FAIL			-1
#define E_INVALIDARG	-2
#define _NLSCMPERROR	-9999



//* function definition

#define OpenHandle						fopen
#define CloseHandle						fclose
#define WriteFile(f, s, l, r, p)		fwrite(f, s l)
#define DeleteFile						remove

#define OutputDebugString(s)

#define ZeroMemory(p, l)				memset((void *)p, 0, l)

#define _tcslen strlen
#define _tcsicmp strcmp
#define _tstoi  atoi






