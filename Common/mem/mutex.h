//*****************************************************************************
//* mutex.cpp - Cross-platform mutex funtion header file
//*


#ifndef _XP_MUTEX_H_
#define _XP_MUTEX_H_

#if defined(LINUX)

	//#include <span class="code-keyword"><pthread.h></span>
	#include <pthread.h>

#elif defined(WIN32) //(WINDOWS)

	//#include <span class="code-keyword"><windows.h></span>
	#include <windows.h>

	//#include <span class="code-keyword"><process.h></span>
	#include <process.h>

#endif



//* Data type definitions

#if defined(LINUX)
	#define XP_MUTEX pthread_mutex_t
#elif defined(WIN32) //(WINDOWS)
	#define XP_MUTEX HANDLE
#endif



//* Function definitions

int XP_MUTEX_INIT(XP_MUTEX *pMutex);

int XP_MUTEX_LOCK(XP_MUTEX *pMutex);

int XP_MUTEX_UNLOCK(XP_MUTEX *pMutex);


#endif // _XP_MUTEX_H_

