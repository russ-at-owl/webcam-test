//*****************************************************************************
//* CLinkedList.h
//*



#ifndef __CLINKEDLIST_H__
#define __CLINKEDLIST_H__


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef PVOID
#define PVOID	void*
#endif


#define NEW_CNODE()		(CNode *) NewCNode();
//#define NEW_CNODE()	new static CNode

//#define DEL_CNODE(x)	DelCNode(x);
//#define DEL_CNODE(x)	if (x != NULL) {delete x;}
#define DEL_CNODE(x)	if (x != NULL) {x->Free(); free(x); x = NULL;}


//using namespace std;

//template<class T>


PVOID NewCNode();


class CNode
{

public:
	//constructor & destructor
	CNode()
	{
		Init();
	}

	void Init();

	CNode(void *pPr)
	{
		Init();

		SetPrev(pPr);
	}

	~CNode()
	{
		Free();
	}

	void Free();

	// members
	PVOID GetPrev()
	{
		return m_pPrev;
	}

	void SetPrev(void *pPr)
	{
		m_pPrev = pPr;
	}

	PVOID GetNext()
	{
		return m_pNext;
	}

	void SetNext(void *pNx)
	{
		m_pNext = pNx;
	}

	bool SetData(void *pData);
	bool SetData(void *pData, long lSize);

	PVOID GetData()
	{ 
		return m_pData; 
	}

	long GetDataSize()
	{ 
		return m_lDataSize; 
	}

private:
	void *m_pNext;
	void *m_pPrev;

	void *m_pData;

	long m_lDataSize;

	bool m_bNodeInit;
	bool m_bDataInit;
	bool m_bAllocData;

};

#define PCNODE	CNode*


class CLinkedList
{

public:
	//* constructor & destructor
	CLinkedList()
	{
		Init();
	}

	~CLinkedList()
	{
		Free();
	}

	void Init();
	void Init(long lSize)
	{
		Init();

		SetFixedDataSize(lSize);
	}

	void Free()
	{
		while (m_pHead != NULL)
		{
			DelFistNode();
		}

		m_pHead = NULL;
		m_pTail = NULL;

		m_lNodeCntr = 0;
	}

	//* members

	PCNODE GetNode(long lNum);

	void * GetNodeData(long lNum);

	PCNODE GetNextNode(PCNODE pNode)
	{
		if (pNode == NULL)
		{
			return NULL;
		}
	
		return ((PCNODE) pNode->GetNext());
	}

	//* Append a new node at the
	//* end of the linked list
	bool Append();
	bool Append(void *pData, long lSize);
	bool Append(void *pData);

	//* Insert a new node before
	//* the selected node
	bool Insert(CNode *pNode);
	bool Insert(CNode *pNode, void *pData, long lSize);
	bool Insert(CNode *pNode, void *pData);

	//* Delete the selected node
	bool Delete(CNode *pNode);
	bool Delete(long x);
	
	bool DelFistNode()
	{
		return Delete(m_pHead);
	}

	bool DelLastNode()
	{
		return Delete(m_pTail);
	}

	PCNODE GetFirstNode()
	{
		return m_pHead;
	}

	PCNODE GetLastNode()
	{
		return m_pTail;
	}

	void * GetFirstData()
	{
		if (m_pHead == NULL)
		{
			return NULL;
		}
	
		return m_pHead->GetData();
	}

	void * GetLastData()
	{
		if (m_pTail == NULL)
		{
			return NULL;
		}
	
		return m_pTail->GetData();
	}

	bool SetFixedDataSize (long lSize);

	//* Allocate a data buffer in the 
	//* selected node of "lSize" bytes
	//* and copy data to it
	bool SetData(CNode *pNode, void *pData, long lSize);

	bool SetData(CNode *pNode, void *pData);

	//* Get a pointer to the data buffer
	//* in the slected node
	void * GetData(CNode *pNode)
	{
		if (pNode == NULL)
		{
			return NULL;
		}

		return pNode->GetData();
	}

private:

	CNode *m_pHead;
	CNode *m_pTail;

	bool bListInit;
	bool bFixedDataSize;
	bool bLocalAlloc;

	long m_lDataSize;

	long m_lNodeCntr;

public:

	int GetNodeCnt ()
	{
		return ((int) m_lNodeCntr);
	}
};


#endif // __CLINKEDLIST_H__