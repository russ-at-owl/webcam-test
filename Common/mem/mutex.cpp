//*****************************************************************************
//* mutex.cpp - Cross-platform mutex funtion implimentation file
//*


#include "mutex.h"



int XP_MUTEX_INIT(XP_MUTEX *pMutex)
{
    #if defined(LINUX)
        return pthread_mutex_init (pMutex, NULL);;
    #elif defined(WIN32) //(WINDOWS)
        *pMutex = CreateMutex(0, FALSE, 0);;
        return (*pMutex == 0);
    #endif
    return -1;
}

int XP_MUTEX_LOCK(XP_MUTEX *pMutex)
{
    #if defined(LINUX)
        return pthread_mutex_lock( pMutex );
    #elif defined(WIN32) //(WINDOWS)
        return (WaitForSingleObject(*pMutex, INFINITE) == WAIT_FAILED ? 1 : 0);
    #endif
    return -1;
}

int XP_MUTEX_UNLOCK(XP_MUTEX *pMutex)
{
    #if defined(LINUX)
        return pthread_mutex_unlock( pMutex );
    #elif defined(WIN32) //(WINDOWS)
        return (ReleaseMutex(*pMutex) == 0);
    #endif
    return -1;
}

