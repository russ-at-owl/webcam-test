//*****************************************************************************
//* CLinkedList.cpp - CLinkedList class implimentation file
//*


#pragma warning( disable : 4627 ) 
#pragma warning( disable : 4996 ) 


#include "CLinkedList.h"


//* global methods
//----------------------

PVOID NewCNode()
{
	CNode *pNew = (CNode *) 
		malloc(sizeof(CNode) + 1); 

	if (pNew != NULL) 
	{
		pNew->Init(); 
	}

	return pNew;
}

//void DelCNode(CNode *pNode)
//{
//	if (pNode != NULL) 
//	{
//		pNode->Free();
//	}
//
//	free(pNode);
//}


//* CNode methods
//----------------------


void CNode::Init()
{
	m_pNext = NULL;
	m_pPrev = NULL;
	m_pData = NULL;

	m_lDataSize = 0;

	m_bNodeInit = true;
	m_bDataInit = false;
	m_bAllocData = true;
}



//* CNode Free 
//* frees a data buffer if allocated by this class
//* and resets class variables
void CNode::Free()
{
	if (m_pData != NULL)
	{
		if (m_bAllocData == true)
		{
			free(m_pData);
		}
	}

	m_lDataSize = 0;
			
	m_bDataInit = false;
	//m_bAllocData = true;

	m_pData = NULL;
}


//* CNode SetData
bool CNode::SetData(void *pData)
{ 
	//if (pData == NULL)
	//{
	//	return false;
	//}

	m_pData = pData;

	m_bAllocData = false;

	return true;
}


//* CNode SetData
bool CNode::SetData(void *pData, long lSize)
{ 
	if (pData == NULL || lSize == 0)
	{
		return false;
	}

	if (m_pData != NULL)
	{
		free(m_pData);
	}

	m_pData = 
		malloc(lSize + 1);

	if (m_pData == NULL)
	{
		return false;
	}

	//memset(m_pData, 0, (lSize + 1));

	memcpy(m_pData, pData, lSize);

	m_lDataSize = lSize;

	m_bDataInit = true;

	m_bAllocData = true;

	return true;
}




//* CLinkedList methods
//----------------------


//* CLinkedList Init
void CLinkedList::Init()
{
	m_pHead = NULL;
	m_pTail = NULL;

	m_lNodeCntr = 0;

	m_lDataSize = 0;

	bFixedDataSize = false;

	bListInit = true;
}


//* CLinkedList  GetNode
//* Step into the linked list
//* until we find the "lNum" node
CNode * CLinkedList::GetNode(long lNum)
{
	long lIdx = 0;
	
	CNode *pCur = m_pHead;
	
	while (1)
	{
		if (pCur == NULL)
		{
			return NULL;
		}

		if (lIdx == lNum)
		{
			return pCur;
		}

		pCur = GetNextNode(pCur);

		lIdx++;
	}
}


void * CLinkedList::GetNodeData(long lNum)
{
	CNode *pNode = GetNode(lNum);

	if (pNode == NULL)
	{
		return NULL;
	}

	void *pData = pNode->GetData();

	return pData;
}

//* CLinkedList  Append
//* Append a new node at the
//* end of the linked list
bool CLinkedList::Append()
{
	if (m_pTail == NULL)
	{
		m_pTail = NEW_CNODE();
			
		m_pHead = m_pTail;
	}
	else
	{
		CNode *pNew = NEW_CNODE();

		m_pTail->SetNext(pNew);
		
		pNew->SetPrev(m_pTail);

		m_pTail = pNew;
	}

	m_lNodeCntr++;

	return true;
}

bool CLinkedList::Append(void *pData, long lSize)
{
	if (pData == NULL || lSize == 0)
	{
		return false;
	}

	if (bFixedDataSize == true)
	{
		return false;
	}

	if (Append() == NULL)
	{
		return false;
	}

	return SetData(m_pTail, pData, lSize);
}

bool CLinkedList::Append(void *pData)
{
	if (pData == NULL)
	{
		return false;
	}

	if (Append() == NULL)
	{
		return false;
	}

	return SetData(m_pTail, pData);
}


//* Insert a new node before
//* the selected node
bool CLinkedList::Insert(CNode *pNode)
{
	if (pNode == NULL)
	{
		return false;
	}

	CNode *pNew = NEW_CNODE();
		
	if (pNode == m_pHead)
	{
		m_pHead = pNew;
	}
	else
	{
		CNode *pPrv = (CNode *)
			pNode->GetPrev();

		pPrv->SetNext(pNew);

		pNew->SetPrev(pPrv);
	}

	pNew->SetNext(pNode);

	pNode->SetPrev(pNew);

	m_lNodeCntr++;

	return true;
}

bool CLinkedList::Insert(CNode *pNode, void *pData, long lSize)
{
	if (pNode == NULL)
	{
		return false;
	}

	if (pData == NULL || lSize == 0)
	{
		return false;
	}

	if (bFixedDataSize == true)
	{
		return false;
	}

	if (Insert(pNode) == false)
	{
		return false;
	}

	CNode *pNew = (CNode *)
		pNode->GetPrev();

	return SetData(pNew, pData, lSize);
}

bool CLinkedList::Insert(CNode *pNode, void *pData)
{
	if (pNode == NULL)
	{
		return false;
	}

	if (pData == NULL)
	{
		return false;
	}

	if (Insert(pNode) == false)
	{
		return false;
	}

	CNode *pNew = (CNode *)
		pNode->GetPrev();

	return SetData(pNew, pData);
}


//* Delete the selected node
bool CLinkedList::Delete(CNode *pNode)
{
	if (pNode == NULL)
	{
		return false;
	}

	CNode *pPrv = (CNode *)
		pNode->GetPrev();

	CNode *pNxt = (CNode *)
		pNode->GetNext();

	if (m_pHead == pNode || pPrv == NULL)
	{
		m_pHead = pNxt;
	}
	else
	{
		pPrv->SetNext(pNxt);
	}
		
	if (m_pTail == pNode || pNxt == NULL)
	{
		m_pTail = pPrv;
	}
	else
	{
		pNxt->SetPrev(pPrv);
	}

	//pNode->Free();

	DEL_CNODE(pNode);

	m_lNodeCntr--;

	return true;
}


//* Delete the selected node
bool CLinkedList::Delete(long x)
{
	long lIdx = 0;
	
	CNode *pCur = m_pHead;
	
	while (1)
	{
		if (pCur == NULL)
		{
			return NULL;
		}

		if (lIdx == x)
		{
			Delete(pCur);
			return true;
		}

		pCur = GetNextNode(pCur);

		lIdx++;
	}

	return false;
}


//* CLinkedList  SetFixedDataSize
bool CLinkedList::SetFixedDataSize(long lSize)
{
	m_lDataSize = lSize;

	if (lSize > 0)
	{
		bFixedDataSize = true;
	}

	return true;
}

//* CLinkedList  SetData
//* Allocate a data buffer in the 
//* selected node of "lSize" bytes
//* and copy data to it.
bool CLinkedList::SetData(CNode *pNode, void *pData, long lSize)
{
	if (pNode == NULL)
	{
		return false;
	}

	if (bFixedDataSize == true)
	{
		return false;
	}

	return pNode->SetData(pData, lSize);
}

bool CLinkedList::SetData(CNode *pNode, void *pData)
{
	if (pNode == NULL || pData == NULL)
	{
		return false;
	}

	if (bFixedDataSize == false)
	{
		m_lDataSize = -1;
		bLocalAlloc = false;

		return pNode->SetData(pData);
	}

	return pNode->SetData(pData, m_lDataSize);
}



