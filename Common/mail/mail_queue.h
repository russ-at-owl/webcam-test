

#ifndef mail_queue_H_
#define mail_queue_H_

#include "mutex.h"

#include "mail/mail_send.h"

#include "ZThread/thread.h"

#include <deque>


using namespace ZThread;


class mailQueue 
{
private:
	
	class mailQueueData
	{

	protected:

		XP_MUTEX				m_lock;

		int						m_milliSpeed;

		int						m_errorFrequency; // how many errors before a mail logs an error (modulo)
	
		bool					m_quit;

		void					*m_pParent;

	public:
		
		std::deque<mailSend>	m_mailQueue;
	
		mailQueueData(void * pPrnt) :
			m_milliSpeed(2000),
			m_errorFrequency(300),
			m_quit(false)
		{
			XP_MUTEX_INIT(&m_lock);

			//* default values
			//* m_milli_speed = 2000, 
			//* m_errorFrequency = 300

			m_pParent = pPrnt;
		};

		void init(int nSpeed, int nFreq)
		{
			m_milliSpeed = nSpeed;
			m_errorFrequency = nFreq;
		};

		int	milliSpeed()
		{
			return m_milliSpeed;
		};

		int errorFreq()
		{
			return m_errorFrequency;
		};

		bool quit()
		{
			return m_quit;
		};

		void quit(bool bQuit)
		{
			m_quit = bQuit;
		};

		void mutexLock()
		{
			XP_MUTEX_LOCK(&m_lock);
		};

		void mutexUnlock()
		{
			XP_MUTEX_UNLOCK(&m_lock);
		};

		void queueEmpty()
		{
			m_mailQueue.empty();
		};
	};

protected:

	mailQueueData			*m_pQueueData;

	bool					m_bInitialized;
	
	Task					*m_pQueueTask;
	Thread					*m_pQueueHandler;


public:

    class mailQueueHandler :  public Runnable
    {

    protected:

		mailQueueData	*m_pQueueData;

    public:

		mailQueueHandler(mailQueueData	*pQueueData) :
			m_pQueueData(pQueueData)
		{

		};

		~mailQueueHandler()
		{

		};

		void run();
	};

public:		

	mailQueue() :
		m_bInitialized(false),
		m_pQueueData(NULL)
	{
		m_pQueueTask = NULL;
		m_pQueueHandler = NULL;

		m_pQueueData = new 
			mailQueueData((void *) this);
	};

	~mailQueue()
	{
		if (m_pQueueData != NULL)
		{
			delete m_pQueueData;
		}
	}

	void config(int nSpeed, int nFreq)
	{
		m_pQueueData->init(nSpeed, nFreq);
	};

	void init()
	{
		if (m_pQueueData == NULL || m_bInitialized == true)
		{
			return;
		}

		//* create a "Task" class for the "mailQueueHandler"

		Task tskQue(new mailQueueHandler(m_pQueueData));

		m_pQueueTask = (Task *) &tskQue;

		m_bInitialized = true;
	};

	int start()
	{
		if (m_bInitialized == false)
		{
			return 1;
		}

		while (1)
		{
			try
			{
				//* create new "Thread" class for previously created "Task" class

				Thread thrdQue(*m_pQueueTask, false);

				m_pQueueHandler = (Thread *) &thrdQue;

				thrdQue.wait();
			}
			//catch(Synchronization_Exception& e) 
			catch(...) 
			{ 
				return -1;
			}

			//* we should never get here
			return -2;
		}

		return 0;
	};

	void queue(const mailSend &mail);
	
	void quit();

	void cancel();
};

#endif
