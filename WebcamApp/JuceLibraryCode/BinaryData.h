/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace Resources
{
    extern const char*   icon_audio_output_oj_svg;
    const int            icon_audio_output_oj_svgSize = 447;

    extern const char*   icon_branched_oj_svg;
    const int            icon_branched_oj_svgSize = 546;

    extern const char*   icon_Check_oj_svg;
    const int            icon_Check_oj_svgSize = 274;

    extern const char*   icon_Clip_oj_svg;
    const int            icon_Clip_oj_svgSize = 406;

    extern const char*   icon_configure_oj_svg;
    const int            icon_configure_oj_svgSize = 1074;

    extern const char*   icon_Delete_oj_svg;
    const int            icon_Delete_oj_svgSize = 396;

    extern const char*   icon_Edit_oj_svg;
    const int            icon_Edit_oj_svgSize = 372;

    extern const char*   icon_info_oj_svg;
    const int            icon_info_oj_svgSize = 275;

    extern const char*   icon_inline_oj_svg;
    const int            icon_inline_oj_svgSize = 400;

    extern const char*   icon_Load_oj_svg;
    const int            icon_Load_oj_svgSize = 356;

    extern const char*   icon_metronome_oj_svg;
    const int            icon_metronome_oj_svgSize = 583;

    extern const char*   icon_Minus_oj_svg;
    const int            icon_Minus_oj_svgSize = 271;

    extern const char*   icon_mixer_oj_svg;
    const int            icon_mixer_oj_svgSize = 857;

    extern const char*   icon_mono_oj_svg;
    const int            icon_mono_oj_svgSize = 258;

    extern const char*   icon_Mute_oj_svg;
    const int            icon_Mute_oj_svgSize = 449;

    extern const char*   icon_muteM_oj_svg;
    const int            icon_muteM_oj_svgSize = 528;

    extern const char*   icon_On_Off_oj_svg;
    const int            icon_On_Off_oj_svgSize = 320;

    extern const char*   icon_Pause_oj_svg;
    const int            icon_Pause_oj_svgSize = 283;

    extern const char*   icon_Play_oj_svg;
    const int            icon_Play_oj_svgSize = 233;

    extern const char*   icon_Plus_oj_svg;
    const int            icon_Plus_oj_svgSize = 298;

    extern const char*   icon_refresh_oj_svg;
    const int            icon_refresh_oj_svgSize = 441;

    extern const char*   icon_Save_oj_svg;
    const int            icon_Save_oj_svgSize = 432;

    extern const char*   icon_Settings_oj_svg;
    const int            icon_Settings_oj_svgSize = 607;

    extern const char*   icon_solo_oj_svg;
    const int            icon_solo_oj_svgSize = 693;

    extern const char*   icon_stereo_oj_svg;
    const int            icon_stereo_oj_svgSize = 445;

    extern const char*   icon_stop_oj_svg;
    const int            icon_stop_oj_svgSize = 413;

    extern const char*   icon_Touch_oj_svg;
    const int            icon_Touch_oj_svgSize = 595;

    extern const char*   icon_X_oj_svg;
    const int            icon_X_oj_svgSize = 334;

    extern const char*   icon__stop_red_svg;
    const int            icon__stop_red_svgSize = 413;

    extern const char*   icon_audio_output_red_svg;
    const int            icon_audio_output_red_svgSize = 447;

    extern const char*   icon_branched_red_svg;
    const int            icon_branched_red_svgSize = 546;

    extern const char*   icon_Check_red_svg;
    const int            icon_Check_red_svgSize = 274;

    extern const char*   icon_Clip_red_svg;
    const int            icon_Clip_red_svgSize = 406;

    extern const char*   icon_configure_red_svg;
    const int            icon_configure_red_svgSize = 1074;

    extern const char*   icon_Delete_red_svg;
    const int            icon_Delete_red_svgSize = 396;

    extern const char*   icon_Edit_red_svg;
    const int            icon_Edit_red_svgSize = 372;

    extern const char*   icon_info_red_svg;
    const int            icon_info_red_svgSize = 275;

    extern const char*   icon_inline_red_svg;
    const int            icon_inline_red_svgSize = 400;

    extern const char*   icon_Load_red_svg;
    const int            icon_Load_red_svgSize = 356;

    extern const char*   icon_metronome_red_svg;
    const int            icon_metronome_red_svgSize = 562;

    extern const char*   icon_Minus_red_svg;
    const int            icon_Minus_red_svgSize = 271;

    extern const char*   icon_mixer_red_svg;
    const int            icon_mixer_red_svgSize = 857;

    extern const char*   icon_mono_red_svg;
    const int            icon_mono_red_svgSize = 258;

    extern const char*   icon_Mute_red_svg;
    const int            icon_Mute_red_svgSize = 449;

    extern const char*   icon_muteM_red_svg;
    const int            icon_muteM_red_svgSize = 527;

    extern const char*   icon_On_Off_red_svg;
    const int            icon_On_Off_red_svgSize = 320;

    extern const char*   icon_Pause_red_svg;
    const int            icon_Pause_red_svgSize = 283;

    extern const char*   icon_Play_red_svg;
    const int            icon_Play_red_svgSize = 233;

    extern const char*   icon_Plus_red_svg;
    const int            icon_Plus_red_svgSize = 298;

    extern const char*   icon_refresh_red_svg;
    const int            icon_refresh_red_svgSize = 439;

    extern const char*   icon_Save_red_svg;
    const int            icon_Save_red_svgSize = 432;

    extern const char*   icon_Settings_red_svg;
    const int            icon_Settings_red_svgSize = 607;

    extern const char*   icon_solo_red_svg;
    const int            icon_solo_red_svgSize = 691;

    extern const char*   icon_stereo_red_svg;
    const int            icon_stereo_red_svgSize = 445;

    extern const char*   icon_Touch_red_svg;
    const int            icon_Touch_red_svgSize = 595;

    extern const char*   icon_X_red_svg;
    const int            icon_X_red_svgSize = 334;

    extern const char*   app_App_Icon_Black_circle_svg;
    const int            app_App_Icon_Black_circle_svgSize = 1520;

    extern const char*   app_App_Icon_Black_rounded_svg;
    const int            app_App_Icon_Black_rounded_svgSize = 1512;

    extern const char*   app_App_Icon_Black_square_svg;
    const int            app_App_Icon_Black_square_svgSize = 1509;

    extern const char*   icon_audio_output_svg;
    const int            icon_audio_output_svgSize = 447;

    extern const char*   icon_audio_output_svg2;
    const int            icon_audio_output_svg2Size = 447;

    extern const char*   icon_branched_svg;
    const int            icon_branched_svgSize = 538;

    extern const char*   icon_browser_svg;
    const int            icon_browser_svgSize = 455;

    extern const char*   icon_Check_svg;
    const int            icon_Check_svgSize = 274;

    extern const char*   icon_Clip_svg;
    const int            icon_Clip_svgSize = 406;

    extern const char*   icon_configure_svg;
    const int            icon_configure_svgSize = 1018;

    extern const char*   icon_configure2_svg;
    const int            icon_configure2_svgSize = 1018;

    extern const char*   icon_Delete_svg;
    const int            icon_Delete_svgSize = 396;

    extern const char*   icon_Edit_svg;
    const int            icon_Edit_svgSize = 372;

    extern const char*   icon_info_svg;
    const int            icon_info_svgSize = 275;

    extern const char*   icon_inline_svg;
    const int            icon_inline_svgSize = 398;

    extern const char*   icon_Load_svg;
    const int            icon_Load_svgSize = 399;

    extern const char*   icon_metronome_svg;
    const int            icon_metronome_svgSize = 512;

    extern const char*   icon_Minus_svg;
    const int            icon_Minus_svgSize = 271;

    extern const char*   icon_mixer_svg;
    const int            icon_mixer_svgSize = 857;

    extern const char*   icon_mixer_color_svg;
    const int            icon_mixer_color_svgSize = 1569;

    extern const char*   icon_mono_svg;
    const int            icon_mono_svgSize = 258;

    extern const char*   icon_Mute_svg;
    const int            icon_Mute_svgSize = 449;

    extern const char*   icon_muteM_svg;
    const int            icon_muteM_svgSize = 464;

    extern const char*   icon_On_Off_svg;
    const int            icon_On_Off_svgSize = 320;

    extern const char*   icon_Pause_svg;
    const int            icon_Pause_svgSize = 283;

    extern const char*   icon_Play_svg;
    const int            icon_Play_svgSize = 233;

    extern const char*   icon_Plus_svg;
    const int            icon_Plus_svgSize = 298;

    extern const char*   icon_refresh_svg;
    const int            icon_refresh_svgSize = 385;

    extern const char*   icon_Save_svg;
    const int            icon_Save_svgSize = 432;

    extern const char*   icon_Settings_svg;
    const int            icon_Settings_svgSize = 607;

    extern const char*   icon_solo_svg;
    const int            icon_solo_svgSize = 620;

    extern const char*   icon_stereo_svg;
    const int            icon_stereo_svgSize = 454;

    extern const char*   icon_stop_svg;
    const int            icon_stop_svgSize = 234;

    extern const char*   icon_Touch_svg;
    const int            icon_Touch_svgSize = 595;

    extern const char*   icon_X_svg;
    const int            icon_X_svgSize = 334;

    extern const char*   Volume_Logo_Final_Version_1_0_App_Icon_Black_circle_png;
    const int            Volume_Logo_Final_Version_1_0_App_Icon_Black_circle_pngSize = 11161;

    extern const char*   Volume_Logo_Final_Version_1_0_App_Icon_Black_rounded_png;
    const int            Volume_Logo_Final_Version_1_0_App_Icon_Black_rounded_pngSize = 8882;

    extern const char*   Volume_Logo_Final_Version_1_0_App_Icon_Black_square_png;
    const int            Volume_Logo_Final_Version_1_0_App_Icon_Black_square_pngSize = 7961;

    extern const char*   Volume_com_SVG_Conversions_1_02_audio_output_png;
    const int            Volume_com_SVG_Conversions_1_02_audio_output_pngSize = 641;

    extern const char*   Volume_com_SVG_Conversions_1_02_Check_png;
    const int            Volume_com_SVG_Conversions_1_02_Check_pngSize = 555;

    extern const char*   Volume_com_SVG_Conversions_1_02_Clip_png;
    const int            Volume_com_SVG_Conversions_1_02_Clip_pngSize = 339;

    extern const char*   Volume_com_SVG_Conversions_1_02_Delete_png;
    const int            Volume_com_SVG_Conversions_1_02_Delete_pngSize = 396;

    extern const char*   Volume_com_SVG_Conversions_1_02_Edit_png;
    const int            Volume_com_SVG_Conversions_1_02_Edit_pngSize = 562;

    extern const char*   Volume_com_SVG_Conversions_1_02_info_png;
    const int            Volume_com_SVG_Conversions_1_02_info_pngSize = 620;

    extern const char*   Volume_com_SVG_Conversions_1_02_Load_png;
    const int            Volume_com_SVG_Conversions_1_02_Load_pngSize = 441;

    extern const char*   Volume_com_SVG_Conversions_1_02_Minus_png;
    const int            Volume_com_SVG_Conversions_1_02_Minus_pngSize = 191;

    extern const char*   Volume_com_SVG_Conversions_1_02_mixer_color_png;
    const int            Volume_com_SVG_Conversions_1_02_mixer_color_pngSize = 1113;

    extern const char*   Volume_com_SVG_Conversions_1_02_mixer_png;
    const int            Volume_com_SVG_Conversions_1_02_mixer_pngSize = 749;

    extern const char*   Volume_com_SVG_Conversions_1_02_Mute_png;
    const int            Volume_com_SVG_Conversions_1_02_Mute_pngSize = 555;

    extern const char*   Volume_com_SVG_Conversions_1_02_On_Off_png;
    const int            Volume_com_SVG_Conversions_1_02_On_Off_pngSize = 718;

    extern const char*   Volume_com_SVG_Conversions_1_02_Pause_png;
    const int            Volume_com_SVG_Conversions_1_02_Pause_pngSize = 235;

    extern const char*   Volume_com_SVG_Conversions_1_02_Play12_png;
    const int            Volume_com_SVG_Conversions_1_02_Play12_pngSize = 522;

    extern const char*   Volume_com_SVG_Conversions_1_02_Play14_png;
    const int            Volume_com_SVG_Conversions_1_02_Play14_pngSize = 423;

    extern const char*   Volume_com_SVG_Conversions_1_02_Plus_png;
    const int            Volume_com_SVG_Conversions_1_02_Plus_pngSize = 264;

    extern const char*   Volume_com_SVG_Conversions_1_02_Save_png;
    const int            Volume_com_SVG_Conversions_1_02_Save_pngSize = 469;

    extern const char*   Volume_com_SVG_Conversions_1_02_Settings_png;
    const int            Volume_com_SVG_Conversions_1_02_Settings_pngSize = 673;

    extern const char*   Volume_com_SVG_Conversions_1_02_X_png;
    const int            Volume_com_SVG_Conversions_1_02_X_pngSize = 491;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 113;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}
