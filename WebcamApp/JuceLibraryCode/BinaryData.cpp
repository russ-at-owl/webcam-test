/* ==================================== JUCER_BINARY_RESOURCE ====================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

namespace Resources
{

//================== icon_audio_output_oj.svg ==================
static const unsigned char temp_binary_data_0[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><g id=\"Isolation_Mode\" data-name=\"Isolation Mode\"><polygon class=\"cls-1\" points=\"3.75 7.4 2.16 7.4 2.16 11.1 3.75 11.1 10.13 17.61"
" 10.13 1.64 3.75 7.4\"/><path class=\"cls-1\" d=\"M12,7.65a3.5,3.5,0,0,1,0,4.23l1.6,1.21a5.51,5.51,0,0,0,0-6.65Z\"/><path class=\"cls-1\" d=\"M14.74,6.12a6,6,0,0,1,0,7.29l1.6,1.21a8.06,8.06,0,0,0,0-9.71Z\"/></g></svg>";

const char* icon_audio_output_oj_svg = (const char*) temp_binary_data_0;

//================== icon_branched_oj.svg ==================
static const unsigned char temp_binary_data_1[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"13.95 12.67 15.01 13.73 11.05 13.73 11.05 10.84 "
"9.06 10.84 9.06 13.73 6.21 13.73 6.21 7.07 15.04 7.07 13.99 8.13 15.05 9.19 17.91 6.32 15.04 3.45 13.98 4.51 15.04 5.57 2.06 5.57 2.06 7.07 4.71 7.07 4.71 14.48 4.72 14.48 4.72 15.23 9.06 15.23 9.06 18.2 11.05 18.2 11.05 15.23 15.01 15.23 13.95 16.29"
" 15.01 17.35 17.88 14.48 15.01 11.61 13.95 12.67\"/></svg>";

const char* icon_branched_oj_svg = (const char*) temp_binary_data_1;

//================== icon_Check_oj.svg ==================
static const unsigned char temp_binary_data_2[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"7.79 17.39 2.59 11.84 4.78 9.79 7.45 12.64 13.93"
" 3.38 16.39 5.1 7.79 17.39\"/></svg>";

const char* icon_Check_oj_svg = (const char*) temp_binary_data_2;

//================== icon_Clip_oj.svg ==================
static const unsigned char temp_binary_data_3[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><rect class=\"cls-1\" x=\"11.27\" y=\"2.43\" width=\"1.5\" height=\"16\"/><polygon"
" class=\"cls-1\" points=\"2.13 13.91 2.13 17.37 9.56 17.39 9.56 9.66 2.13 13.91\"/><polygon class=\"cls-1\" points=\"18.04 4.83 14.56 6.81 14.56 17.4 18.04 17.41 18.04 4.83\"/></svg>";

const char* icon_Clip_oj_svg = (const char*) temp_binary_data_3;

//================== icon_configure_oj.svg ==================
static const unsigned char temp_binary_data_4[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><rect class=\"cls-1\" x=\"3.79\" y=\"2.4\" width=\"5.13\" height=\"10.01\" rx=\"1."
"99\"/><path class=\"cls-1\" d=\"M6.74,14.69H6a3.07,3.07,0,0,1-2.78-1.52l1.36-.63A1.64,1.64,0,0,0,6,13.19h.77a1.62,1.62,0,0,0,1.41-.65l1.37.62A3.06,3.06,0,0,1,6.74,14.69Z\"/><rect class=\"cls-1\" x=\"5.6\" y=\"14.11\" width=\"1.5\" height=\"2.67\"/><r"
"ect class=\"cls-1\" x=\"3.51\" y=\"16.03\" width=\"5.69\" height=\"1.5\"/><polygon class=\"cls-1\" points=\"12.89 5.38 11.98 5.38 11.98 7.51 12.89 7.51 16.58 11.27 16.58 2.05 12.89 5.38\"/><path class=\"cls-1\" d=\"M17.65,15.31V14.09h-.87a2.39,2.39,0"
",0,0-.26-.64l.62-.62L16.28,12l-.61.61a2.41,2.41,0,0,0-.66-.27v-.87H13.8v.88a2.21,2.21,0,0,0-.65.28L12.53,12l-.86.86.63.64a2.22,2.22,0,0,0-.24.62h-.92v1.22h.93a2.55,2.55,0,0,0,.26.61l-.65.65.86.86.66-.66a2.63,2.63,0,0,0,.6.25v.92H15V17a2.52,2.52,0,0,0"
",.61-.25l.65.65.86-.86-.64-.63a2.5,2.5,0,0,0,.27-.63Zm-3.26.8a1.42,1.42,0,1,1,1.42-1.42A1.42,1.42,0,0,1,14.39,16.11Z\"/></svg>";

const char* icon_configure_oj_svg = (const char*) temp_binary_data_4;

//================== icon_Delete_oj.svg ==================
static const unsigned char temp_binary_data_5[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"11.78 4.01 11.78 2.01 8.78 2.01 8.78 4.01 4.5 4."
"01 4.5 6.01 15.5 6.01 15.5 4.01 11.78 4.01\"/><path class=\"cls-1\" d=\"M4.91,7.61,5.91,18h8.34l1-10.38Zm3.24,9h-1v-8h1Zm2.43,0h-1v-8h1Zm2.44,0H12v-8h1Z\"/></svg>";

const char* icon_Delete_oj_svg = (const char*) temp_binary_data_5;

//================== icon_Edit_oj.svg ==================
static const unsigned char temp_binary_data_6[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><rect class=\"cls-1\" x=\"3.42\" y=\"3.27\" width=\"4\" height=\"2.45\" transform="
"\"translate(-1.62 4.79) rotate(-42.01)\"/><polygon class=\"cls-1\" points=\"16 13.24 8.7 5.15 5.73 7.83 13.02 15.92 16.88 17.21 16 13.24\"/></svg>";

const char* icon_Edit_oj_svg = (const char*) temp_binary_data_6;

//================== icon_info_oj.svg ==================
static const unsigned char temp_binary_data_7[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M10,2.43a8,8,0,1,0,8,8A8,8,0,0,0,10,2.43Zm.86,13.11H9V8."
"2h1.87Zm0-8.36H9V5.32h1.87Z\"/></svg>";

const char* icon_info_oj_svg = (const char*) temp_binary_data_7;

//================== icon_inline_oj.svg ==================
static const unsigned char temp_binary_data_8[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"15.04 7.51 13.98 8.57 15.04 9.63 11.05 9.63 11.0"
"5 6.71 9.06 6.71 9.06 9.63 2.06 9.63 2.06 11.13 9.06 11.13 9.06 14.07 11.05 14.07 11.05 11.13 15.04 11.13 13.99 12.19 15.05 13.25 17.91 10.38 15.04 7.51\"/></svg>";

const char* icon_inline_oj_svg = (const char*) temp_binary_data_8;

//================== icon_Load_oj.svg ==================
static const unsigned char temp_binary_data_9[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><rect class=\"cls-1\" x=\"2\" y=\"1.95\" width=\"16\" height=\"2\"/><polygon class"
"=\"cls-1\" points=\"14.3 11.54 9.99 6.35 5.62 11.54 7.15 12.82 9 10.63 9 18.05 11 18.05 11 10.7 12.76 12.82 14.3 11.54\"/></svg>";

const char* icon_Load_oj_svg = (const char*) temp_binary_data_9;

//================== icon_metronome_oj.svg ==================
static const unsigned char temp_binary_data_10[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"12.96 10.85 13.66 11.16 15.46 8.73 14.19 8.17 16"
".54 3.08 16.45 3.04 16.46 3.03 14.72 2.22 14.71 2.24 14.56 2.17 12.2 7.29 10.84 6.69 10.2 9.63 10.97 9.97 8.16 16.04 5.78 16.04 8.02 3.45 10.72 3.45 10.88 4.38 10.88 4.38 12.58 4.34 12.15 1.75 12.06 1.75 12.06 1.75 6.66 1.75 6.66 1.75 6.6 1.75 3.75 1"
"7.75 15 17.75 14.24 13.48 12.54 13.46 12.54 13.46 12.97 16.04 10.56 16.04 12.96 10.85\"/></svg>";

const char* icon_metronome_oj_svg = (const char*) temp_binary_data_10;

//================== icon_Minus_oj.svg ==================
static const unsigned char temp_binary_data_11[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><rect class=\"cls-1\" x=\"8.5\" y=\"4.78\" width=\"3\" height=\"12\" transform=\"t"
"ranslate(20.78 0.78) rotate(90)\"/></svg>";

const char* icon_Minus_oj_svg = (const char*) temp_binary_data_11;

//================== icon_mixer_oj.svg ==================
static const unsigned char temp_binary_data_12[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"15.43 10.15 11.27 10.15 11.27 10.15 11.27 11.89 "
"12.16 11.89 14.54 11.89 15.43 11.89 15.43 10.15 15.43 10.15 15.43 10.15\"/><circle class=\"cls-1\" cx=\"6.73\" cy=\"13.24\" r=\"1.48\"/><path class=\"cls-1\" d=\"M16,2.34H4.05A2.05,2.05,0,0,0,2,4.39v12a2.05,2.05,0,0,0,2.05,2.06H16A2.05,2.05,0,0,0,18,"
"16.37v-12A2.05,2.05,0,0,0,16,2.34ZM6.73,15.77a2.57,2.57,0,1,1,2.55-2.56A2.55,2.55,0,0,1,6.73,15.77Zm0-6.46A2.56,2.56,0,1,1,9.29,6.75,2.56,2.56,0,0,1,6.74,9.31Zm9.64,3.37H14.54v3.09H12.16V12.68H10.31V9.2h1.85V5h2.38V9.2h1.84Z\"/><path class=\"cls-1\" "
"d=\"M6.74,5.2A1.56,1.56,0,1,0,8.29,6.75,1.56,1.56,0,0,0,6.74,5.2Zm0,2.62A1.07,1.07,0,1,1,7.79,6.75,1.06,1.06,0,0,1,6.73,7.82Z\"/></svg>";

const char* icon_mixer_oj_svg = (const char*) temp_binary_data_12;

//================== icon_mono_oj.svg ==================
static const unsigned char temp_binary_data_13[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M10,6a4,4,0,1,1-4,4,4,4,0,0,1,4-4m0-2a6,6,0,1,0,6,6,6,6,"
"0,0,0-6-6Z\"/></svg>";

const char* icon_mono_oj_svg = (const char*) temp_binary_data_13;

//================== icon_Mute_oj.svg ==================
static const unsigned char temp_binary_data_14[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"3.89 7.77 2.3 7.77 2.3 11.48 3.89 11.48 10.27 17"
".99 10.27 2.01 3.89 7.77\"/><polygon class=\"cls-1\" points=\"17.99 8.48 16.58 7.06 14.79 8.85 13 7.06 11.59 8.48 13.38 10.27 11.59 12.05 13 13.47 14.79 11.68 16.58 13.47 17.99 12.05 16.21 10.27 17.99 8.48\"/></svg>";

const char* icon_Mute_oj_svg = (const char*) temp_binary_data_14;

//================== icon_muteM_oj.svg ==================
static const unsigned char temp_binary_data_15[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M2.42,15.34V5.4H5.18V6.92A3.23,3.23,0,0,1,6.37,5.67a3.46"
",3.46,0,0,1,1.85-.5A2.71,2.71,0,0,1,11,6.9a3.79,3.79,0,0,1,3.35-1.73,3.12,3.12,0,0,1,2.36,1A4,4,0,0,1,17.6,9v6.37H14.87V9.28c0-1.23-.52-1.84-1.56-1.84A1.86,1.86,0,0,0,11.93,8a2,2,0,0,0-.54,1.5v5.87H8.66V9.28c0-1.23-.52-1.84-1.56-1.84A1.86,1.86,0,0,0,"
"5.72,8a2,2,0,0,0-.54,1.5v5.87Z\"/></svg>";

const char* icon_muteM_oj_svg = (const char*) temp_binary_data_15;

//================== icon_On_Off_oj.svg ==================
static const unsigned char temp_binary_data_16[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M14.34,3.26,13.29,5a6,6,0,1,1-6.44-.09l-1-1.69a8,8,0,1,0"
",8.54.08Z\"/><rect class=\"cls-1\" x=\"9.15\" y=\"2.02\" width=\"2\" height=\"8.01\"/></svg>";

const char* icon_On_Off_oj_svg = (const char*) temp_binary_data_16;

//================== icon_Pause_oj.svg ==================
static const unsigned char temp_binary_data_17[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><rect class=\"cls-1\" x=\"3.66\" y=\"2\" width=\"4\" height=\"16\"/><rect class=\""
"cls-1\" x=\"11.66\" y=\"2\" width=\"4\" height=\"16\"/></svg>";

const char* icon_Pause_oj_svg = (const char*) temp_binary_data_17;

//================== icon_Play_oj.svg ==================
static const unsigned char temp_binary_data_18[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"4.23 2 4.23 18 16.31 10.08 4.23 2\"/></svg>";

const char* icon_Play_oj_svg = (const char*) temp_binary_data_18;

//================== icon_Plus_oj.svg ==================
static const unsigned char temp_binary_data_19[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"16 8.5 11.5 8.5 11.5 4 8.5 4 8.5 8.5 4 8.5 4 11."
"5 8.5 11.5 8.5 16 11.5 16 11.5 11.5 16 11.5 16 8.5\"/></svg>";

const char* icon_Plus_oj_svg = (const char*) temp_binary_data_19;

//================== icon_refresh_oj.svg ==================
static const unsigned char temp_binary_data_20[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M9.58,5.94V7.66l3.47-2.88L9.58,1.9V3.66A6.3,6.3,0,0,0,3."
"75,9.9a6.18,6.18,0,0,0,1.87,4.46l1.59-1.61A4,4,0,0,1,9.58,5.94Z\"/><path class=\"cls-1\" d=\"M14.22,5.28,12.57,6.83l.12.13a4,4,0,0,1-2,6.85V12.15L7.23,15l3.48,2.87V16.12A6.25,6.25,0,0,0,14.22,5.28Z\"/></svg>";

const char* icon_refresh_oj_svg = (const char*) temp_binary_data_20;

//================== icon_Save_oj.svg ==================
static const unsigned char temp_binary_data_21[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"16 9.73 16 15.73 4 15.73 4 9.73 2 9.73 2 15.73 2"
" 17.73 4 17.73 16 17.73 18 17.73 18 15.73 18 9.73 16 9.73\"/><polygon class=\"cls-1\" points=\"12.85 6.86 11 9.05 11 1.64 9 1.64 9 8.99 7.24 6.86 5.7 8.14 10.01 13.33 14.38 8.15 12.85 6.86\"/></svg>";

const char* icon_Save_oj_svg = (const char*) temp_binary_data_21;

//================== icon_Settings_oj.svg ==================
static const unsigned char temp_binary_data_22[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M18,12V9H15.88a5.87,5.87,0,0,0-.63-1.58l1.52-1.53L14.65,"
"3.72,13.14,5.23a5.65,5.65,0,0,0-1.62-.67V2.41h-3V4.58a6.06,6.06,0,0,0-1.59.68L5.4,3.72,3.28,5.84,4.84,7.41A5.87,5.87,0,0,0,4.24,9H2v3h2.3a5.77,5.77,0,0,0,.64,1.51l-1.61,1.6,2.12,2.13,1.63-1.63a5.68,5.68,0,0,0,1.46.6v2.28h3V16.18a5.93,5.93,0,0,0,1.5-."
"59l1.6,1.59,2.12-2.12L15.17,13.5A6.11,6.11,0,0,0,15.84,12Zm-8,2a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,10,13.93Z\"/></svg>";

const char* icon_Settings_oj_svg = (const char*) temp_binary_data_22;

//================== icon_solo_oj.svg ==================
static const unsigned char temp_binary_data_23[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M10.22,15.67a5.1,5.1,0,0,1-3.14-.86,3.13,3.13,0,0,1-1.19"
"-2.47H8.45A1.59,1.59,0,0,0,9,13.39a1.81,1.81,0,0,0,1.25.36c1,0,1.56-.36,1.56-1.09a.83.83,0,0,0-.46-.76,4.69,4.69,0,0,0-1.56-.41,6.12,6.12,0,0,1-2.84-1,2.59,2.59,0,0,1-.82-2.11A2.63,2.63,0,0,1,7.23,6.1,4.68,4.68,0,0,1,10,5.29q3.76,0,4.1,3.08H11.59A1.4"
",1.4,0,0,0,10,7.17a1.69,1.69,0,0,0-1,.29.9.9,0,0,0-.37.74A.76.76,0,0,0,9,8.9a4.84,4.84,0,0,0,1.51.38,6.87,6.87,0,0,1,2.94,1,2.52,2.52,0,0,1,.92,2.17,2.9,2.9,0,0,1-1.09,2.43A5,5,0,0,1,10.22,15.67Z\"/></svg>";

const char* icon_solo_oj_svg = (const char*) temp_binary_data_23;

//================== icon_stereo_oj.svg ==================
static const unsigned char temp_binary_data_24[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M7.62,6.38A3.62,3.62,0,1,1,4,10,3.62,3.62,0,0,1,7.62,6.3"
"8m0-2A5.62,5.62,0,1,0,13.24,10,5.62,5.62,0,0,0,7.62,4.38Z\"/><path class=\"cls-1\" d=\"M12.38,6.38A3.62,3.62,0,1,1,8.76,10a3.63,3.63,0,0,1,3.62-3.62m0-2A5.62,5.62,0,1,0,18,10a5.62,5.62,0,0,0-5.62-5.62Z\"/></svg>";

const char* icon_stereo_oj_svg = (const char*) temp_binary_data_24;

//================== icon_stop_oj.svg ==================
static const unsigned char temp_binary_data_25[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><path class=\"cls-1\" d=\"M10,2.57a7.82,7.82,0,1,0,7.82,7.82A7.82,7.82,0,0,0,10,2."
"57Zm5.82,7.82a5.85,5.85,0,0,1-.88,3.07l-8-8A5.76,5.76,0,0,1,10,4.57,5.83,5.83,0,0,1,15.82,10.39Zm-11.64,0a5.81,5.81,0,0,1,1.26-3.6L13.6,15a5.82,5.82,0,0,1-9.42-4.56Z\"/></svg>";

const char* icon_stop_oj_svg = (const char*) temp_binary_data_25;

//================== icon_Touch_oj.svg ==================
static const unsigned char temp_binary_data_26[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"16.95 9.14 15.45 8.73 15.45 11.01 14.68 11.01 14"
".67 8.52 13.17 8.11 13.17 11.01 12.38 11.01 12.39 7.9 10.84 7.48 10.84 11.01 10.07 11.01 10.07 9.9 10.07 7.27 10.07 5.09 7.82 5.09 7.82 9.9 7.82 9.9 7.82 13.31 4.24 10.22 3.05 11.01 5.26 14.24 7.83 17.99 7.83 17.99 7.83 17.99 15.3 17.94 15.3 17.94 16"
".95 14.73 16.95 9.14 16.95 9.14\"/><rect class=\"cls-1\" x=\"7.82\" y=\"2.01\" width=\"2.25\" height=\"2.06\"/></svg>";

const char* icon_Touch_oj_svg = (const char*) temp_binary_data_26;

//================== icon_X_oj.svg ==================
static const unsigned char temp_binary_data_27[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#ff9c33;}</style></defs><polygon class=\"cls-1\" points=\"16.64 5.15 14.51 3.02 9.66 7.88 4.8 3.02 2.68 5."
"15 7.54 10 2.68 14.85 4.8 16.98 9.66 12.12 14.51 16.98 16.64 14.85 11.78 10 16.64 5.15\"/></svg>";

const char* icon_X_oj_svg = (const char*) temp_binary_data_27;

//================== icon_ stop_red.svg ==================
static const unsigned char temp_binary_data_28[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M10,2.57a7.82,7.82,0,1,0,7.82,7.82A7.82,7.82,0,0,0,10,2."
"57Zm5.82,7.82a5.85,5.85,0,0,1-.88,3.07l-8-8A5.76,5.76,0,0,1,10,4.57,5.83,5.83,0,0,1,15.82,10.39Zm-11.64,0a5.81,5.81,0,0,1,1.26-3.6L13.6,15a5.82,5.82,0,0,1-9.42-4.56Z\"/></svg>";

const char* icon__stop_red_svg = (const char*) temp_binary_data_28;

//================== icon_audio_output_red.svg ==================
static const unsigned char temp_binary_data_29[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><g id=\"Isolation_Mode\" data-name=\"Isolation Mode\"><polygon class=\"cls-1\" points=\"3.75 7.4 2.16 7.4 2.16 11.1 3.75 11.1 10.13 17.61"
" 10.13 1.64 3.75 7.4\"/><path class=\"cls-1\" d=\"M12,7.65a3.5,3.5,0,0,1,0,4.23l1.6,1.21a5.51,5.51,0,0,0,0-6.65Z\"/><path class=\"cls-1\" d=\"M14.74,6.12a6,6,0,0,1,0,7.29l1.6,1.21a8.06,8.06,0,0,0,0-9.71Z\"/></g></svg>";

const char* icon_audio_output_red_svg = (const char*) temp_binary_data_29;

//================== icon_branched_red.svg ==================
static const unsigned char temp_binary_data_30[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"13.95 12.67 15.01 13.73 11.05 13.73 11.05 10.84 "
"9.06 10.84 9.06 13.73 6.21 13.73 6.21 7.07 15.04 7.07 13.99 8.13 15.05 9.19 17.91 6.32 15.04 3.45 13.98 4.51 15.04 5.57 2.06 5.57 2.06 7.07 4.71 7.07 4.71 14.48 4.72 14.48 4.72 15.23 9.06 15.23 9.06 18.2 11.05 18.2 11.05 15.23 15.01 15.23 13.95 16.29"
" 15.01 17.35 17.88 14.48 15.01 11.61 13.95 12.67\"/></svg>";

const char* icon_branched_red_svg = (const char*) temp_binary_data_30;

//================== icon_Check_red.svg ==================
static const unsigned char temp_binary_data_31[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"7.79 17.39 2.59 11.84 4.78 9.79 7.45 12.64 13.93"
" 3.38 16.39 5.1 7.79 17.39\"/></svg>";

const char* icon_Check_red_svg = (const char*) temp_binary_data_31;

//================== icon_Clip_red.svg ==================
static const unsigned char temp_binary_data_32[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><rect class=\"cls-1\" x=\"11.27\" y=\"2.43\" width=\"1.5\" height=\"16\"/><polygon"
" class=\"cls-1\" points=\"2.13 13.91 2.13 17.37 9.56 17.39 9.56 9.66 2.13 13.91\"/><polygon class=\"cls-1\" points=\"18.04 4.83 14.56 6.81 14.56 17.4 18.04 17.41 18.04 4.83\"/></svg>";

const char* icon_Clip_red_svg = (const char*) temp_binary_data_32;

//================== icon_configure_red.svg ==================
static const unsigned char temp_binary_data_33[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><rect class=\"cls-1\" x=\"3.79\" y=\"2.4\" width=\"5.13\" height=\"10.01\" rx=\"1."
"99\"/><path class=\"cls-1\" d=\"M6.74,14.69H6a3.07,3.07,0,0,1-2.78-1.52l1.36-.63A1.64,1.64,0,0,0,6,13.19h.77a1.62,1.62,0,0,0,1.41-.65l1.37.62A3.06,3.06,0,0,1,6.74,14.69Z\"/><rect class=\"cls-1\" x=\"5.6\" y=\"14.11\" width=\"1.5\" height=\"2.67\"/><r"
"ect class=\"cls-1\" x=\"3.51\" y=\"16.03\" width=\"5.69\" height=\"1.5\"/><polygon class=\"cls-1\" points=\"12.89 5.38 11.98 5.38 11.98 7.51 12.89 7.51 16.58 11.27 16.58 2.05 12.89 5.38\"/><path class=\"cls-1\" d=\"M17.65,15.31V14.09h-.87a2.39,2.39,0"
",0,0-.26-.64l.62-.62L16.28,12l-.61.61a2.41,2.41,0,0,0-.66-.27v-.87H13.8v.88a2.21,2.21,0,0,0-.65.28L12.53,12l-.86.86.63.64a2.22,2.22,0,0,0-.24.62h-.92v1.22h.93a2.55,2.55,0,0,0,.26.61l-.65.65.86.86.66-.66a2.63,2.63,0,0,0,.6.25v.92H15V17a2.52,2.52,0,0,0"
",.61-.25l.65.65.86-.86-.64-.63a2.5,2.5,0,0,0,.27-.63Zm-3.26.8a1.42,1.42,0,1,1,1.42-1.42A1.42,1.42,0,0,1,14.39,16.11Z\"/></svg>";

const char* icon_configure_red_svg = (const char*) temp_binary_data_33;

//================== icon_Delete_red.svg ==================
static const unsigned char temp_binary_data_34[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"11.78 4.01 11.78 2.01 8.78 2.01 8.78 4.01 4.5 4."
"01 4.5 6.01 15.5 6.01 15.5 4.01 11.78 4.01\"/><path class=\"cls-1\" d=\"M4.91,7.61,5.91,18h8.34l1-10.38Zm3.24,9h-1v-8h1Zm2.43,0h-1v-8h1Zm2.44,0H12v-8h1Z\"/></svg>";

const char* icon_Delete_red_svg = (const char*) temp_binary_data_34;

//================== icon_Edit_red.svg ==================
static const unsigned char temp_binary_data_35[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><rect class=\"cls-1\" x=\"3.42\" y=\"3.27\" width=\"4\" height=\"2.45\" transform="
"\"translate(-1.62 4.79) rotate(-42.01)\"/><polygon class=\"cls-1\" points=\"16 13.24 8.7 5.15 5.73 7.83 13.02 15.92 16.88 17.21 16 13.24\"/></svg>";

const char* icon_Edit_red_svg = (const char*) temp_binary_data_35;

//================== icon_info_red.svg ==================
static const unsigned char temp_binary_data_36[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M10,2.43a8,8,0,1,0,8,8A8,8,0,0,0,10,2.43Zm.86,13.11H9V8."
"2h1.87Zm0-8.36H9V5.32h1.87Z\"/></svg>";

const char* icon_info_red_svg = (const char*) temp_binary_data_36;

//================== icon_inline_red.svg ==================
static const unsigned char temp_binary_data_37[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"15.04 7.51 13.98 8.57 15.04 9.63 11.05 9.63 11.0"
"5 6.71 9.06 6.71 9.06 9.63 2.06 9.63 2.06 11.13 9.06 11.13 9.06 14.07 11.05 14.07 11.05 11.13 15.04 11.13 13.99 12.19 15.05 13.25 17.91 10.38 15.04 7.51\"/></svg>";

const char* icon_inline_red_svg = (const char*) temp_binary_data_37;

//================== icon_Load_red.svg ==================
static const unsigned char temp_binary_data_38[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><rect class=\"cls-1\" x=\"2\" y=\"1.95\" width=\"16\" height=\"2\"/><polygon class"
"=\"cls-1\" points=\"14.3 11.54 9.99 6.35 5.62 11.54 7.15 12.82 9 10.63 9 18.05 11 18.05 11 10.7 12.76 12.82 14.3 11.54\"/></svg>";

const char* icon_Load_red_svg = (const char*) temp_binary_data_38;

//================== icon_metronome_red.svg ==================
static const unsigned char temp_binary_data_39[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"12.96 11 13.66 11.31 15.46 8.88 14.19 8.33 16.54"
" 3.24 16.45 3.2 16.46 3.18 14.72 2.38 14.71 2.39 14.56 2.32 12.2 7.45 10.84 6.84 10.2 9.79 10.97 10.12 8.16 16.2 5.78 16.2 8.02 3.6 10.72 3.6 10.88 4.53 10.88 4.53 12.58 4.5 12.15 1.9 12.06 1.9 12.06 1.9 6.66 1.9 6.66 1.9 6.6 1.9 3.75 17.9 15 17.9 14"
".24 13.64 12.54 13.62 12.54 13.62 12.97 16.2 10.56 16.2 12.96 11\"/></svg>";

const char* icon_metronome_red_svg = (const char*) temp_binary_data_39;

//================== icon_Minus_red.svg ==================
static const unsigned char temp_binary_data_40[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><rect class=\"cls-1\" x=\"8.5\" y=\"4.78\" width=\"3\" height=\"12\" transform=\"t"
"ranslate(20.78 0.78) rotate(90)\"/></svg>";

const char* icon_Minus_red_svg = (const char*) temp_binary_data_40;

//================== icon_mixer_red.svg ==================
static const unsigned char temp_binary_data_41[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"15.43 10.15 11.27 10.15 11.27 10.15 11.27 11.89 "
"12.16 11.89 14.54 11.89 15.43 11.89 15.43 10.15 15.43 10.15 15.43 10.15\"/><circle class=\"cls-1\" cx=\"6.73\" cy=\"13.24\" r=\"1.48\"/><path class=\"cls-1\" d=\"M16,2.34H4.05A2.05,2.05,0,0,0,2,4.39v12a2.05,2.05,0,0,0,2.05,2.06H16A2.05,2.05,0,0,0,18,"
"16.37v-12A2.05,2.05,0,0,0,16,2.34ZM6.73,15.77a2.57,2.57,0,1,1,2.55-2.56A2.55,2.55,0,0,1,6.73,15.77Zm0-6.46A2.56,2.56,0,1,1,9.29,6.75,2.56,2.56,0,0,1,6.74,9.31Zm9.64,3.37H14.54v3.09H12.16V12.68H10.31V9.2h1.85V5h2.38V9.2h1.84Z\"/><path class=\"cls-1\" "
"d=\"M6.74,5.2A1.56,1.56,0,1,0,8.29,6.75,1.56,1.56,0,0,0,6.74,5.2Zm0,2.62A1.07,1.07,0,1,1,7.79,6.75,1.06,1.06,0,0,1,6.73,7.82Z\"/></svg>";

const char* icon_mixer_red_svg = (const char*) temp_binary_data_41;

//================== icon_mono_red.svg ==================
static const unsigned char temp_binary_data_42[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M10,6a4,4,0,1,1-4,4,4,4,0,0,1,4-4m0-2a6,6,0,1,0,6,6,6,6,"
"0,0,0-6-6Z\"/></svg>";

const char* icon_mono_red_svg = (const char*) temp_binary_data_42;

//================== icon_Mute_red.svg ==================
static const unsigned char temp_binary_data_43[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"3.89 7.77 2.3 7.77 2.3 11.48 3.89 11.48 10.27 17"
".99 10.27 2.01 3.89 7.77\"/><polygon class=\"cls-1\" points=\"17.99 8.48 16.58 7.06 14.79 8.85 13 7.06 11.59 8.48 13.38 10.27 11.59 12.05 13 13.47 14.79 11.68 16.58 13.47 17.99 12.05 16.21 10.27 17.99 8.48\"/></svg>";

const char* icon_Mute_red_svg = (const char*) temp_binary_data_43;

//================== icon_muteM_red.svg ==================
static const unsigned char temp_binary_data_44[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M2.42,15.34V5.4H5.18V6.92A3.23,3.23,0,0,1,6.37,5.67a3.46"
",3.46,0,0,1,1.85-.49A2.7,2.7,0,0,1,11,6.9a3.78,3.78,0,0,1,3.34-1.72,3.09,3.09,0,0,1,2.37,1A4,4,0,0,1,17.6,9v6.37H14.87V9.28c0-1.23-.52-1.84-1.56-1.84A1.86,1.86,0,0,0,11.93,8a2,2,0,0,0-.54,1.5v5.87H8.65V9.28c0-1.23-.52-1.84-1.55-1.84A1.86,1.86,0,0,0,5"
".72,8a2,2,0,0,0-.54,1.5v5.87Z\"/></svg>";

const char* icon_muteM_red_svg = (const char*) temp_binary_data_44;

//================== icon_On_Off_red.svg ==================
static const unsigned char temp_binary_data_45[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M14.34,3.26,13.29,5a6,6,0,1,1-6.44-.09l-1-1.69a8,8,0,1,0"
",8.54.08Z\"/><rect class=\"cls-1\" x=\"9.15\" y=\"2.02\" width=\"2\" height=\"8.01\"/></svg>";

const char* icon_On_Off_red_svg = (const char*) temp_binary_data_45;

//================== icon_Pause_red.svg ==================
static const unsigned char temp_binary_data_46[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><rect class=\"cls-1\" x=\"3.66\" y=\"2\" width=\"4\" height=\"16\"/><rect class=\""
"cls-1\" x=\"11.66\" y=\"2\" width=\"4\" height=\"16\"/></svg>";

const char* icon_Pause_red_svg = (const char*) temp_binary_data_46;

//================== icon_Play_red.svg ==================
static const unsigned char temp_binary_data_47[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"4.23 2 4.23 18 16.31 10.08 4.23 2\"/></svg>";

const char* icon_Play_red_svg = (const char*) temp_binary_data_47;

//================== icon_Plus_red.svg ==================
static const unsigned char temp_binary_data_48[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"16 8.5 11.5 8.5 11.5 4 8.5 4 8.5 8.5 4 8.5 4 11."
"5 8.5 11.5 8.5 16 11.5 16 11.5 11.5 16 11.5 16 8.5\"/></svg>";

const char* icon_Plus_red_svg = (const char*) temp_binary_data_48;

//================== icon_refresh red.svg ==================
static const unsigned char temp_binary_data_49[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M9.58,6.1V7.81l3.47-2.87L9.58,2.06V3.82a6.3,6.3,0,0,0-5."
"83,6.24,6.2,6.2,0,0,0,1.87,4.46L7.21,12.9A4,4,0,0,1,9.58,6.1Z\"/><path class=\"cls-1\" d=\"M14.22,5.43,12.57,7l.12.12a4,4,0,0,1-2,6.86V12.31L7.23,15.18l3.48,2.88V16.27A6.25,6.25,0,0,0,14.22,5.43Z\"/></svg>";

const char* icon_refresh_red_svg = (const char*) temp_binary_data_49;

//================== icon_Save_red.svg ==================
static const unsigned char temp_binary_data_50[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"16 9.73 16 15.73 4 15.73 4 9.73 2 9.73 2 15.73 2"
" 17.73 4 17.73 16 17.73 18 17.73 18 15.73 18 9.73 16 9.73\"/><polygon class=\"cls-1\" points=\"12.85 6.86 11 9.05 11 1.64 9 1.64 9 8.99 7.24 6.86 5.7 8.14 10.01 13.33 14.38 8.15 12.85 6.86\"/></svg>";

const char* icon_Save_red_svg = (const char*) temp_binary_data_50;

//================== icon_Settings_red.svg ==================
static const unsigned char temp_binary_data_51[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M18,12V9H15.88a5.87,5.87,0,0,0-.63-1.58l1.52-1.53L14.65,"
"3.72,13.14,5.23a5.65,5.65,0,0,0-1.62-.67V2.41h-3V4.58a6.06,6.06,0,0,0-1.59.68L5.4,3.72,3.28,5.84,4.84,7.41A5.87,5.87,0,0,0,4.24,9H2v3h2.3a5.77,5.77,0,0,0,.64,1.51l-1.61,1.6,2.12,2.13,1.63-1.63a5.68,5.68,0,0,0,1.46.6v2.28h3V16.18a5.93,5.93,0,0,0,1.5-."
"59l1.6,1.59,2.12-2.12L15.17,13.5A6.11,6.11,0,0,0,15.84,12Zm-8,2a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,10,13.93Z\"/></svg>";

const char* icon_Settings_red_svg = (const char*) temp_binary_data_51;

//================== icon_solo_red.svg ==================
static const unsigned char temp_binary_data_52[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M10.22,15.66a5.1,5.1,0,0,1-3.14-.85,3.13,3.13,0,0,1-1.19"
"-2.47H8.45A1.59,1.59,0,0,0,9,13.39a1.81,1.81,0,0,0,1.25.36c1,0,1.56-.37,1.56-1.09a.83.83,0,0,0-.46-.76,4.69,4.69,0,0,0-1.56-.41,6.12,6.12,0,0,1-2.84-1,2.59,2.59,0,0,1-.82-2.11A2.63,2.63,0,0,1,7.23,6.1,4.68,4.68,0,0,1,10,5.29q3.76,0,4.1,3.08H11.59A1.4"
",1.4,0,0,0,10,7.17a1.69,1.69,0,0,0-1,.29.9.9,0,0,0-.37.74A.76.76,0,0,0,9,8.9a4.84,4.84,0,0,0,1.51.38,6.87,6.87,0,0,1,2.94,1,2.5,2.5,0,0,1,.92,2.17,2.9,2.9,0,0,1-1.09,2.43A5,5,0,0,1,10.22,15.66Z\"/></svg>";

const char* icon_solo_red_svg = (const char*) temp_binary_data_52;

//================== icon_stereo_red.svg ==================
static const unsigned char temp_binary_data_53[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><path class=\"cls-1\" d=\"M7.62,6.38A3.62,3.62,0,1,1,4,10,3.62,3.62,0,0,1,7.62,6.3"
"8m0-2A5.62,5.62,0,1,0,13.24,10,5.62,5.62,0,0,0,7.62,4.38Z\"/><path class=\"cls-1\" d=\"M12.38,6.38A3.62,3.62,0,1,1,8.76,10a3.63,3.63,0,0,1,3.62-3.62m0-2A5.62,5.62,0,1,0,18,10a5.62,5.62,0,0,0-5.62-5.62Z\"/></svg>";

const char* icon_stereo_red_svg = (const char*) temp_binary_data_53;

//================== icon_Touch_red.svg ==================
static const unsigned char temp_binary_data_54[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"16.95 9.14 15.45 8.73 15.45 11.01 14.68 11.01 14"
".67 8.52 13.17 8.11 13.17 11.01 12.38 11.01 12.39 7.9 10.84 7.48 10.84 11.01 10.07 11.01 10.07 9.9 10.07 7.27 10.07 5.09 7.82 5.09 7.82 9.9 7.82 9.9 7.82 13.31 4.24 10.22 3.05 11.01 5.26 14.24 7.83 17.99 7.83 17.99 7.83 17.99 15.3 17.94 15.3 17.94 16"
".95 14.73 16.95 9.14 16.95 9.14\"/><rect class=\"cls-1\" x=\"7.82\" y=\"2.01\" width=\"2.25\" height=\"2.06\"/></svg>";

const char* icon_Touch_red_svg = (const char*) temp_binary_data_54;

//================== icon_X_red.svg ==================
static const unsigned char temp_binary_data_55[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#932525;}</style></defs><polygon class=\"cls-1\" points=\"16.64 5.15 14.51 3.02 9.66 7.88 4.8 3.02 2.68 5."
"15 7.54 10 2.68 14.85 4.8 16.98 9.66 12.12 14.51 16.98 16.64 14.85 11.78 10 16.64 5.15\"/></svg>";

const char* icon_X_red_svg = (const char*) temp_binary_data_55;

//================== app_App Icon Black circle.svg ==================
static const unsigned char temp_binary_data_56[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 191.91 191.91\"><defs><style>.cls-1{fill:#141414;}.cls-2,.cls-3{fill-rule:evenodd;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:url(#linear-gradien"
"t-2);}</style><linearGradient id=\"linear-gradient\" x1=\"119.02\" y1=\"142.62\" x2=\"119.02\" y2=\"58.77\" gradientUnits=\"userSpaceOnUse\"><stop offset=\"0\" stop-color=\"#ac2020\"/><stop offset=\"1\" stop-color=\"#ff9c33\"/></linearGradient><linea"
"rGradient id=\"linear-gradient-2\" x1=\"80.59\" y1=\"142.62\" x2=\"80.59\" y2=\"58.77\" xlink:href=\"#linear-gradient\"/></defs><g id=\"volume_logos_w_E\" data-name=\"volume logos w/ E\"><rect class=\"cls-1\" x=\"19.93\" y=\"20.67\" width=\"152.06\" "
"height=\"152.06\" rx=\"76.03\"/><path class=\"cls-2\" d=\"M119,105.07,101.09,123l-2.56-2.56,15.34-15.34Zm20.52-51.14v47.52h-3.63V47.38h3.63v6.55ZM117.77,83.68v17.77h-3.62V81.23h3.62v2.45ZM125,73.77v27.68H121.4V70H125v3.82Zm7.25-9.92v37.6h-3.63V58.67h"
"3.63v5.18ZM134.55,110,131,113.63l-19.61,19.61-2.56-2.57,25.6-25.6h5.13l-5,5Zm-5.29-5-3.43,3.43-19.61,19.61-2.56-2.57,20.47-20.47Z\"/><path class=\"cls-3\" d=\"M73.53,105.68l30.12,30.12,2.57,2.57,2.56-2.56L78.05,105.07H72.92l.61.61ZM52.4,53.93v47.52H5"
"6V47.38H52.4v6.55ZM74.14,83.68v17.77h3.63V81.23H74.14v2.45ZM66.9,73.77v27.68h3.62V70H66.9v3.82Zm-7.25-9.92v37.6h3.62V58.67H59.65v5.18Zm3.62,52.09,30.12,30.12L96,148.63l2.56-2.56-41-41H52.4L55,107.64,57.36,110l2.57,2.58,1,1,2.31,2.31Zm5.13-5.12,30.12,"
"30.11,2.57,2.57,2.56-2.56L67.78,105.07H62.66l2.4,2.41,1,1,2.31,2.32Z\"/></g></svg>";

const char* app_App_Icon_Black_circle_svg = (const char*) temp_binary_data_56;

//================== app_App Icon Black rounded.svg ==================
static const unsigned char temp_binary_data_57[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 191.91 191.91\"><defs><style>.cls-1{fill:#141414;}.cls-2,.cls-3{fill-rule:evenodd;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:url(#linear-gradien"
"t-2);}</style><linearGradient id=\"linear-gradient\" x1=\"119.02\" y1=\"140.35\" x2=\"119.02\" y2=\"56.5\" gradientUnits=\"userSpaceOnUse\"><stop offset=\"0\" stop-color=\"#ac2020\"/><stop offset=\"1\" stop-color=\"#ff9c33\"/></linearGradient><linear"
"Gradient id=\"linear-gradient-2\" x1=\"80.59\" y1=\"140.35\" x2=\"80.59\" y2=\"56.5\" xlink:href=\"#linear-gradient\"/></defs><g id=\"volume_logos_w_E\" data-name=\"volume logos w/ E\"><rect class=\"cls-1\" x=\"19.49\" y=\"19.27\" width=\"152.93\" he"
"ight=\"152.93\" rx=\"20.87\"/><path class=\"cls-2\" d=\"M119,102.8l-17.91,17.9-2.56-2.56,15.34-15.34Zm20.52-51.14V99.17h-3.63V45.11h3.63v6.55ZM117.77,81.41V99.17h-3.62V79h3.62v2.45ZM125,71.49V99.17H121.4V67.68H125v3.81Zm7.25-9.91V99.17h-3.63V56.39h3."
"63v5.19Zm2.28,46.18-3.59,3.6L111.35,131l-2.56-2.56,25.6-25.6h5.13l-5,5Zm-5.29-5-3.43,3.43-19.61,19.6-2.56-2.56,20.47-20.47Z\"/><path class=\"cls-3\" d=\"M73.53,103.41l30.12,30.12,2.57,2.57,2.56-2.57L78.05,102.8H72.92l.61.61ZM52.4,51.66V99.17H56V45.11"
"H52.4v6.55ZM74.14,81.41V99.17h3.63V79H74.14v2.45ZM66.9,71.49V99.17h3.62V67.68H66.9v3.81Zm-7.25-9.91V99.17h3.62V56.39H59.65v5.19Zm3.62,52.09,30.12,30.12L96,146.36l2.56-2.57-41-41H52.4L55,105.37l2.39,2.39,2.57,2.57,1,1,2.31,2.31Zm5.13-5.13,30.12,30.12,"
"2.57,2.57,2.56-2.56L67.78,102.8H62.66l2.4,2.4,1,1,2.31,2.31Z\"/></g></svg>";

const char* app_App_Icon_Black_rounded_svg = (const char*) temp_binary_data_57;

//================== app_App Icon Black square.svg ==================
static const unsigned char temp_binary_data_58[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 191.91 191.91\"><defs><style>.cls-1{fill:#141414;}.cls-2,.cls-3{fill-rule:evenodd;}.cls-2{fill:url(#linear-gradient);}.cls-3{fill:url(#linear-gradien"
"t-2);}</style><linearGradient id=\"linear-gradient\" x1=\"119.02\" y1=\"142.62\" x2=\"119.02\" y2=\"58.77\" gradientUnits=\"userSpaceOnUse\"><stop offset=\"0\" stop-color=\"#ac2020\"/><stop offset=\"1\" stop-color=\"#ff9c33\"/></linearGradient><linea"
"rGradient id=\"linear-gradient-2\" x1=\"80.59\" y1=\"142.62\" x2=\"80.59\" y2=\"58.77\" xlink:href=\"#linear-gradient\"/></defs><g id=\"volume_logos_w_E\" data-name=\"volume logos w/ E\"><rect class=\"cls-1\" x=\"19.93\" y=\"20.67\" width=\"152.06\" "
"height=\"152.06\"/><path class=\"cls-2\" d=\"M119,105.07,101.09,123l-2.56-2.56,15.34-15.34Zm20.52-51.14v47.52h-3.63V47.38h3.63v6.55ZM117.77,83.68v17.77h-3.62V81.23h3.62v2.45ZM125,73.77v27.68H121.4V70H125v3.82Zm7.25-9.92v37.6h-3.63V58.67h3.63v5.18ZM13"
"4.55,110,131,113.63l-19.61,19.61-2.56-2.57,25.6-25.6h5.13l-5,5Zm-5.29-5-3.43,3.43-19.61,19.61-2.56-2.57,20.47-20.47Z\"/><path class=\"cls-3\" d=\"M73.53,105.68l30.12,30.12,2.57,2.57,2.56-2.56L78.05,105.07H72.92l.61.61ZM52.4,53.93v47.52H56V47.38H52.4v"
"6.55ZM74.14,83.68v17.77h3.63V81.23H74.14v2.45ZM66.9,73.77v27.68h3.62V70H66.9v3.82Zm-7.25-9.92v37.6h3.62V58.67H59.65v5.18Zm3.62,52.09,30.12,30.12L96,148.63l2.56-2.56-41-41H52.4L55,107.64,57.36,110l2.57,2.58,1,1,2.31,2.31Zm5.13-5.12,30.12,30.11,2.57,2."
"57,2.56-2.56L67.78,105.07H62.66l2.4,2.41,1,1,2.31,2.32Z\"/></g></svg>";

const char* app_App_Icon_Black_square_svg = (const char*) temp_binary_data_58;

//================== icon_audio output.svg ==================
static const unsigned char temp_binary_data_59[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><g id=\"Isolation_Mode\" data-name=\"Isolation Mode\"><polygon class=\"cls-1\" points=\"3.75 7.4 2.16 7.4 2.16 11.1 3.75 11.1 10.13 17.61"
" 10.13 1.64 3.75 7.4\"/><path class=\"cls-1\" d=\"M12,7.65a3.5,3.5,0,0,1,0,4.23l1.6,1.21a5.51,5.51,0,0,0,0-6.65Z\"/><path class=\"cls-1\" d=\"M14.74,6.12a6,6,0,0,1,0,7.29l1.6,1.21a8.06,8.06,0,0,0,0-9.71Z\"/></g></svg>";

const char* icon_audio_output_svg = (const char*) temp_binary_data_59;

//================== icon_audio_output.svg ==================
static const unsigned char temp_binary_data_60[] =
"<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><g id=\"Isolation_Mode\" data-name=\"Isolation Mode\"><polygon class=\"cls-1\" points=\"3.75 7.4 2.16 7.4 2.16 11.1 3.75 11.1 10.13 17.61"
" 10.13 1.64 3.75 7.4\"/><path class=\"cls-1\" d=\"M12,7.65a3.5,3.5,0,0,1,0,4.23l1.6,1.21a5.51,5.51,0,0,0,0-6.65Z\"/><path class=\"cls-1\" d=\"M14.74,6.12a6,6,0,0,1,0,7.29l1.6,1.21a8.06,8.06,0,0,0,0-9.71Z\"/></g></svg>";

const char* icon_audio_output_svg2 = (const char*) temp_binary_data_60;

//================== icon_branched.svg ==================
static const unsigned char temp_binary_data_61[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"14.44 12.28 15.5 13.35 11.54 13.35 11.54 10.45 9"
".55 10.45 9.55 13.35 6.7 13.35 6.7 6.68 15.53 6.68 14.48 7.74 15.54 8.8 18.4 5.93 15.53 3.06 14.47 4.13 15.53 5.18 2.55 5.18 2.55 6.68 5.2 6.68 5.2 14.09 5.21 14.09 5.21 14.85 9.55 14.85 9.55 17.81 11.54 17.81 11.54 14.85 15.49 14.85 14.44 15.9 15.5 "
"16.96 18.37 14.09 15.5 11.22 14.44 12.28\"/></svg>";

const char* icon_branched_svg = (const char*) temp_binary_data_61;

//================== icon_browser.svg ==================
static const unsigned char temp_binary_data_62[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"17.6 15.37 2.4 15.37 2.4 4.63 11.37 4.63 11.37 6"
".13 3.9 6.13 3.9 13.87 16.1 13.87 16.1 10.79 17.6 10.79 17.6 15.37\"/><polygon class=\"cls-1\" points=\"13.55 4.63 13.55 6.13 14.99 6.13 10.88 10.08 11.92 11.16 16.1 7.14 16.1 8.68 17.6 8.68 17.6 4.63 13.55 4.63\"/></svg>";

const char* icon_browser_svg = (const char*) temp_binary_data_62;

//================== icon_Check.svg ==================
static const unsigned char temp_binary_data_63[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"7.79 17.39 2.59 11.84 4.78 9.79 7.45 12.64 13.93"
" 3.38 16.39 5.1 7.79 17.39\"/></svg>";

const char* icon_Check_svg = (const char*) temp_binary_data_63;

//================== icon_Clip.svg ==================
static const unsigned char temp_binary_data_64[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect class=\"cls-1\" x=\"11.27\" y=\"2.43\" width=\"1.5\" height=\"16\"/><polygon"
" class=\"cls-1\" points=\"2.13 13.91 2.13 17.37 9.56 17.39 9.56 9.66 2.13 13.91\"/><polygon class=\"cls-1\" points=\"18.04 4.83 14.56 6.81 14.56 17.4 18.04 17.41 18.04 4.83\"/></svg>";

const char* icon_Clip_svg = (const char*) temp_binary_data_64;

//================== icon_configure.svg ==================
static const unsigned char temp_binary_data_65[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect x=\"3.79\" y=\"2.4\" width=\"5.13\" height=\"10.01\" rx=\"1.99\"/><path d=\""
"M6.74,14.69H6a3.07,3.07,0,0,1-2.78-1.52l1.36-.63A1.64,1.64,0,0,0,6,13.19h.77a1.62,1.62,0,0,0,1.41-.65l1.37.62A3.06,3.06,0,0,1,6.74,14.69Z\"/><rect x=\"5.6\" y=\"14.11\" width=\"1.5\" height=\"2.67\"/><rect x=\"3.51\" y=\"16.03\" width=\"5.69\" height"
"=\"1.5\"/><polygon class=\"cls-1\" points=\"12.89 5.38 11.98 5.38 11.98 7.51 12.89 7.51 16.58 11.27 16.58 2.05 12.89 5.38\"/><path class=\"cls-1\" d=\"M17.65,15.31V14.09h-.87a2.39,2.39,0,0,0-.26-.64l.62-.62L16.28,12l-.61.61a2.41,2.41,0,0,0-.66-.27v-."
"87H13.8v.88a2.21,2.21,0,0,0-.65.28L12.53,12l-.86.86.63.64a2.22,2.22,0,0,0-.24.62h-.92v1.22h.93a2.55,2.55,0,0,0,.26.61l-.65.65.86.86.66-.66a2.63,2.63,0,0,0,.6.25v.92H15V17a2.52,2.52,0,0,0,.61-.25l.65.65.86-.86-.64-.63a2.5,2.5,0,0,0,.27-.63Zm-3.26.8a1."
"42,1.42,0,1,1,1.42-1.42A1.42,1.42,0,0,1,14.39,16.11Z\"/></svg>";

const char* icon_configure_svg = (const char*) temp_binary_data_65;

//================== icon_configure2.svg ==================
static const unsigned char temp_binary_data_66[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect x=\"3.79\" y=\"2.4\" width=\"5.13\" height=\"10.01\" rx=\"1.99\"/><path d=\""
"M6.74,14.69H6a3.07,3.07,0,0,1-2.78-1.52l1.36-.63A1.64,1.64,0,0,0,6,13.19h.77a1.62,1.62,0,0,0,1.41-.65l1.37.62A3.06,3.06,0,0,1,6.74,14.69Z\"/><rect x=\"5.6\" y=\"14.11\" width=\"1.5\" height=\"2.67\"/><rect x=\"3.51\" y=\"16.03\" width=\"5.69\" height"
"=\"1.5\"/><polygon class=\"cls-1\" points=\"12.89 5.38 11.98 5.38 11.98 7.51 12.89 7.51 16.58 11.27 16.58 2.05 12.89 5.38\"/><path class=\"cls-1\" d=\"M17.65,15.31V14.09h-.87a2.39,2.39,0,0,0-.26-.64l.62-.62L16.28,12l-.61.61a2.41,2.41,0,0,0-.66-.27v-."
"87H13.8v.88a2.21,2.21,0,0,0-.65.28L12.53,12l-.86.86.63.64a2.22,2.22,0,0,0-.24.62h-.92v1.22h.93a2.55,2.55,0,0,0,.26.61l-.65.65.86.86.66-.66a2.63,2.63,0,0,0,.6.25v.92H15V17a2.52,2.52,0,0,0,.61-.25l.65.65.86-.86-.64-.63a2.5,2.5,0,0,0,.27-.63Zm-3.26.8a1."
"42,1.42,0,1,1,1.42-1.42A1.42,1.42,0,0,1,14.39,16.11Z\"/></svg>";

const char* icon_configure2_svg = (const char*) temp_binary_data_66;

//================== icon_Delete.svg ==================
static const unsigned char temp_binary_data_67[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"11.78 4.01 11.78 2.01 8.78 2.01 8.78 4.01 4.5 4."
"01 4.5 6.01 15.5 6.01 15.5 4.01 11.78 4.01\"/><path class=\"cls-1\" d=\"M4.91,7.61,5.91,18h8.34l1-10.38Zm3.24,9h-1v-8h1Zm2.43,0h-1v-8h1Zm2.44,0H12v-8h1Z\"/></svg>";

const char* icon_Delete_svg = (const char*) temp_binary_data_67;

//================== icon_Edit.svg ==================
static const unsigned char temp_binary_data_68[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect class=\"cls-1\" x=\"3.42\" y=\"3.27\" width=\"4\" height=\"2.45\" transform="
"\"translate(-1.62 4.79) rotate(-42.01)\"/><polygon class=\"cls-1\" points=\"16 13.24 8.7 5.15 5.73 7.83 13.02 15.92 16.88 17.21 16 13.24\"/></svg>";

const char* icon_Edit_svg = (const char*) temp_binary_data_68;

//================== icon_info.svg ==================
static const unsigned char temp_binary_data_69[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><path class=\"cls-1\" d=\"M10,2.43a8,8,0,1,0,8,8A8,8,0,0,0,10,2.43Zm.86,13.11H9V8."
"2h1.87Zm0-8.36H9V5.32h1.87Z\"/></svg>";

const char* icon_info_svg = (const char*) temp_binary_data_69;

//================== icon_inline.svg ==================
static const unsigned char temp_binary_data_70[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"14.51 7.12 13.45 8.19 14.51 9.24 10.52 9.24 10.5"
"2 6.32 8.53 6.32 8.53 9.24 1.52 9.24 1.52 10.74 8.53 10.74 8.53 13.68 10.52 13.68 10.52 10.74 14.51 10.74 13.45 11.8 14.51 12.86 17.38 9.99 14.51 7.12\"/></svg>";

const char* icon_inline_svg = (const char*) temp_binary_data_70;

//================== icon_Load.svg ==================
static const unsigned char temp_binary_data_71[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect class=\"cls-1\" x=\"2\" y=\"1.95\" width=\"16\" height=\"2\" transform=\"tra"
"nslate(20 5.91) rotate(180)\"/><polygon class=\"cls-1\" points=\"14.3 11.54 9.99 6.35 5.62 11.54 7.15 12.82 9 10.63 9 18.05 11 18.05 11 10.7 12.76 12.82 14.3 11.54\"/></svg>";

const char* icon_Load_svg = (const char*) temp_binary_data_71;

//================== icon_metronome.svg ==================
static const unsigned char temp_binary_data_72[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><polygon points=\"13.5 11.1 14.21 11.4 16 8.97 14.74 8.42 17.09 3.33 17 3.29 17.01 3.27 15.27 2.47 15.26 2.48 15.11 2.41 12.75 7.54"
" 11.39 6.93 10.75 9.88 11.51 10.21 8.71 16.29 6.32 16.29 8.57 3.69 11.27 3.69 11.42 4.62 11.42 4.62 13.12 4.59 12.7 1.99 12.61 1.99 12.61 1.99 7.2 1.99 7.2 1.99 7.14 1.99 4.29 17.99 15.55 17.99 14.79 13.73 13.09 13.71 13.09 13.71 13.52 16.29 11.1 16."
"29 13.5 11.1\"/></svg>";

const char* icon_metronome_svg = (const char*) temp_binary_data_72;

//================== icon_Minus.svg ==================
static const unsigned char temp_binary_data_73[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect class=\"cls-1\" x=\"8.5\" y=\"4.78\" width=\"3\" height=\"12\" transform=\"t"
"ranslate(20.78 0.78) rotate(90)\"/></svg>";

const char* icon_Minus_svg = (const char*) temp_binary_data_73;

//================== icon_mixer.svg ==================
static const unsigned char temp_binary_data_74[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"15.43 10.15 11.27 10.15 11.27 10.15 11.27 11.89 "
"12.16 11.89 14.54 11.89 15.43 11.89 15.43 10.15 15.43 10.15 15.43 10.15\"/><circle class=\"cls-1\" cx=\"6.73\" cy=\"13.24\" r=\"1.48\"/><path class=\"cls-1\" d=\"M16,2.34H4.05A2.05,2.05,0,0,0,2,4.39v12a2.05,2.05,0,0,0,2.05,2.06H16A2.05,2.05,0,0,0,18,"
"16.37v-12A2.05,2.05,0,0,0,16,2.34ZM6.73,15.77a2.57,2.57,0,1,1,2.55-2.56A2.55,2.55,0,0,1,6.73,15.77Zm0-6.46A2.56,2.56,0,1,1,9.29,6.75,2.56,2.56,0,0,1,6.74,9.31Zm9.64,3.37H14.54v3.09H12.16V12.68H10.31V9.2h1.85V5h2.38V9.2h1.84Z\"/><path class=\"cls-1\" "
"d=\"M6.74,5.2A1.56,1.56,0,1,0,8.29,6.75,1.56,1.56,0,0,0,6.74,5.2Zm0,2.62A1.07,1.07,0,1,1,7.79,6.75,1.06,1.06,0,0,1,6.73,7.82Z\"/></svg>";

const char* icon_mixer_svg = (const char*) temp_binary_data_74;

//================== icon_mixer_color.svg ==================
static const unsigned char temp_binary_data_75[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}."
"cls-3{fill:url(#linear-gradient-3);}.cls-4{fill:url(#linear-gradient-4);}</style><linearGradient id=\"linear-gradient\" x1=\"13.35\" y1=\"-1.73\" x2=\"13.35\" y2=\"35.81\" gradientUnits=\"userSpaceOnUse\"><stop offset=\"0\" stop-color=\"#fcc14a\"/><s"
"top offset=\"0.43\" stop-color=\"#932525\"/><stop offset=\"1\"/></linearGradient><linearGradient id=\"linear-gradient-2\" x1=\"6.73\" y1=\"-1.73\" x2=\"6.73\" y2=\"35.81\" xlink:href=\"#linear-gradient\"/><linearGradient id=\"linear-gradient-3\" x1=\""
"10\" y1=\"-1.73\" x2=\"10\" y2=\"35.81\" xlink:href=\"#linear-gradient\"/><linearGradient id=\"linear-gradient-4\" x1=\"6.74\" y1=\"-1.73\" x2=\"6.74\" y2=\"35.81\" xlink:href=\"#linear-gradient\"/></defs><polygon class=\"cls-1\" points=\"15.43 9.77 "
"11.27 9.77 11.27 9.77 11.27 11.51 12.16 11.51 14.54 11.51 15.43 11.51 15.43 9.77 15.43 9.77 15.43 9.77\"/><circle class=\"cls-2\" cx=\"6.73\" cy=\"12.85\" r=\"1.48\"/><path class=\"cls-3\" d=\"M16,2H4.05A2.06,2.06,0,0,0,2,4V16a2.06,2.06,0,0,0,2.05,2."
"06H16A2.06,2.06,0,0,0,18,16V4A2.06,2.06,0,0,0,16,2ZM6.73,15.39a2.57,2.57,0,1,1,2.55-2.57A2.56,2.56,0,0,1,6.73,15.39Zm0-6.47A2.55,2.55,0,1,1,9.29,6.37,2.56,2.56,0,0,1,6.74,8.92Zm9.64,3.37H14.54v3.1H12.16v-3.1H10.31V8.81h1.85V4.61h2.38v4.2h1.84Z\"/><pa"
"th class=\"cls-4\" d=\"M6.74,4.82A1.55,1.55,0,1,0,8.29,6.37,1.55,1.55,0,0,0,6.74,4.82Zm0,2.61A1.06,1.06,0,1,1,7.79,6.37,1.05,1.05,0,0,1,6.73,7.43Z\"/></svg>";

const char* icon_mixer_color_svg = (const char*) temp_binary_data_75;

//================== icon_mono.svg ==================
static const unsigned char temp_binary_data_76[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><path class=\"cls-1\" d=\"M10,6a4,4,0,1,1-4,4,4,4,0,0,1,4-4m0-2a6,6,0,1,0,6,6,6,6,"
"0,0,0-6-6Z\"/></svg>";

const char* icon_mono_svg = (const char*) temp_binary_data_76;

//================== icon_Mute.svg ==================
static const unsigned char temp_binary_data_77[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"3.89 7.77 2.3 7.77 2.3 11.48 3.89 11.48 10.27 17"
".99 10.27 2.01 3.89 7.77\"/><polygon class=\"cls-1\" points=\"17.99 8.48 16.58 7.06 14.79 8.85 13 7.06 11.59 8.48 13.38 10.27 11.59 12.05 13 13.47 14.79 11.68 16.58 13.47 17.99 12.05 16.21 10.27 17.99 8.48\"/></svg>";

const char* icon_Mute_svg = (const char*) temp_binary_data_77;

//================== icon_muteM.svg ==================
static const unsigned char temp_binary_data_78[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><path d=\"M2.42,15.34V5.4H5.18V6.92A3.23,3.23,0,0,1,6.37,5.67a3.46,3.46,0,0,1,1.85-.49A2.7,2.7,0,0,1,11,6.9a3.78,3.78,0,0,1,3.35-1."
"72,3.08,3.08,0,0,1,2.36,1A4,4,0,0,1,17.6,9v6.37H14.87V9.28c0-1.23-.52-1.84-1.56-1.84A1.86,1.86,0,0,0,11.93,8a2,2,0,0,0-.54,1.5v5.87H8.65V9.28c0-1.23-.51-1.84-1.55-1.84A1.86,1.86,0,0,0,5.72,8a2,2,0,0,0-.54,1.5v5.87Z\"/></svg>";

const char* icon_muteM_svg = (const char*) temp_binary_data_78;

//================== icon_On_Off.svg ==================
static const unsigned char temp_binary_data_79[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><path class=\"cls-1\" d=\"M14.34,3.26,13.29,5a6,6,0,1,1-6.44-.09l-1-1.69a8,8,0,1,0"
",8.54.08Z\"/><rect class=\"cls-1\" x=\"9.15\" y=\"2.02\" width=\"2\" height=\"8.01\"/></svg>";

const char* icon_On_Off_svg = (const char*) temp_binary_data_79;

//================== icon_Pause.svg ==================
static const unsigned char temp_binary_data_80[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect class=\"cls-1\" x=\"3.66\" y=\"2\" width=\"4\" height=\"16\"/><rect class=\""
"cls-1\" x=\"11.66\" y=\"2\" width=\"4\" height=\"16\"/></svg>";

const char* icon_Pause_svg = (const char*) temp_binary_data_80;

//================== icon_Play.svg ==================
static const unsigned char temp_binary_data_81[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"4.23 2 4.23 18 16.31 10.08 4.23 2\"/></svg>";

const char* icon_Play_svg = (const char*) temp_binary_data_81;

//================== icon_Plus.svg ==================
static const unsigned char temp_binary_data_82[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"16 8.5 11.5 8.5 11.5 4 8.5 4 8.5 8.5 4 8.5 4 11."
"5 8.5 11.5 8.5 16 11.5 16 11.5 11.5 16 11.5 16 8.5\"/></svg>";

const char* icon_Plus_svg = (const char*) temp_binary_data_82;

//================== icon_refresh.svg ==================
static const unsigned char temp_binary_data_83[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><path d=\"M10.12,6.19V7.9L13.6,5,10.12,2.15V3.91A6.29,6.29,0,0,0,4.3,10.15a6.18,6.18,0,0,0,1.87,4.46L7.76,13a4,4,0,0,1,2.36-6.81Z\""
"/><path d=\"M14.77,5.52,13.12,7.08l.12.12a4,4,0,0,1,1.3,3,4,4,0,0,1-3.29,3.91V12.4L7.77,15.27l3.48,2.88V16.36A6.26,6.26,0,0,0,14.77,5.52Z\"/></svg>";

const char* icon_refresh_svg = (const char*) temp_binary_data_83;

//================== icon_Save.svg ==================
static const unsigned char temp_binary_data_84[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"16 9.73 16 15.73 4 15.73 4 9.73 2 9.73 2 15.73 2"
" 17.73 4 17.73 16 17.73 18 17.73 18 15.73 18 9.73 16 9.73\"/><polygon class=\"cls-1\" points=\"12.85 6.86 11 9.05 11 1.64 9 1.64 9 8.99 7.24 6.86 5.7 8.14 10.01 13.33 14.38 8.15 12.85 6.86\"/></svg>";

const char* icon_Save_svg = (const char*) temp_binary_data_84;

//================== icon_Settings.svg ==================
static const unsigned char temp_binary_data_85[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><path class=\"cls-1\" d=\"M18,12V9H15.88a5.87,5.87,0,0,0-.63-1.58l1.52-1.53L14.65,"
"3.72,13.14,5.23a5.65,5.65,0,0,0-1.62-.67V2.41h-3V4.58a6.06,6.06,0,0,0-1.59.68L5.4,3.72,3.28,5.84,4.84,7.41A5.87,5.87,0,0,0,4.24,9H2v3h2.3a5.77,5.77,0,0,0,.64,1.51l-1.61,1.6,2.12,2.13,1.63-1.63a5.68,5.68,0,0,0,1.46.6v2.28h3V16.18a5.93,5.93,0,0,0,1.5-."
"59l1.6,1.59,2.12-2.12L15.17,13.5A6.11,6.11,0,0,0,15.84,12Zm-8,2a3.5,3.5,0,1,1,3.5-3.5A3.5,3.5,0,0,1,10,13.93Z\"/></svg>";

const char* icon_Settings_svg = (const char*) temp_binary_data_85;

//================== icon_solo.svg ==================
static const unsigned char temp_binary_data_86[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><path d=\"M10.22,15.66a5.1,5.1,0,0,1-3.14-.85,3.13,3.13,0,0,1-1.19-2.47H8.45A1.59,1.59,0,0,0,9,13.39a1.81,1.81,0,0,0,1.25.36c1,0,1."
"56-.37,1.56-1.09a.83.83,0,0,0-.46-.76,4.9,4.9,0,0,0-1.56-.42,6,6,0,0,1-2.84-1,2.59,2.59,0,0,1-.82-2.11A2.63,2.63,0,0,1,7.23,6.1,4.68,4.68,0,0,1,10,5.29q3.76,0,4.1,3.08H11.59A1.4,1.4,0,0,0,10,7.17a1.69,1.69,0,0,0-1,.29.9.9,0,0,0-.37.74A.76.76,0,0,0,9,"
"8.9a4.84,4.84,0,0,0,1.51.38,6.87,6.87,0,0,1,2.94,1,2.5,2.5,0,0,1,.92,2.17,2.9,2.9,0,0,1-1.09,2.43A5,5,0,0,1,10.22,15.66Z\"/></svg>";

const char* icon_solo_svg = (const char*) temp_binary_data_86;

//================== icon_stereo.svg ==================
static const unsigned char temp_binary_data_87[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><path class=\"cls-1\" d=\"M7.62,6.59A3.62,3.62,0,1,1,4,10.21,3.62,3.62,0,0,1,7.62,"
"6.59m0-2a5.62,5.62,0,1,0,5.62,5.62A5.62,5.62,0,0,0,7.62,4.59Z\"/><path class=\"cls-1\" d=\"M12.38,6.59a3.62,3.62,0,1,1-3.62,3.62,3.63,3.63,0,0,1,3.62-3.62m0-2A5.62,5.62,0,1,0,18,10.21a5.62,5.62,0,0,0-5.62-5.62Z\"/></svg>";

const char* icon_stereo_svg = (const char*) temp_binary_data_87;

//================== icon_stop.svg ==================
static const unsigned char temp_binary_data_88[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><rect class=\"cls-1\" x=\"3.96\" y=\"3.72\" width=\"12.08\" height=\"12.08\"/></sv"
"g>";

const char* icon_stop_svg = (const char*) temp_binary_data_88;

//================== icon_Touch.svg ==================
static const unsigned char temp_binary_data_89[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"16.95 9.14 15.45 8.73 15.45 11.01 14.68 11.01 14"
".67 8.52 13.17 8.11 13.17 11.01 12.38 11.01 12.39 7.9 10.84 7.48 10.84 11.01 10.07 11.01 10.07 9.9 10.07 7.27 10.07 5.09 7.82 5.09 7.82 9.9 7.82 9.9 7.82 13.31 4.24 10.22 3.05 11.01 5.26 14.24 7.83 17.99 7.83 17.99 7.83 17.99 15.3 17.94 15.3 17.94 16"
".95 14.73 16.95 9.14 16.95 9.14\"/><rect class=\"cls-1\" x=\"7.82\" y=\"2.01\" width=\"2.25\" height=\"2.06\"/></svg>";

const char* icon_Touch_svg = (const char*) temp_binary_data_89;

//================== icon_X.svg ==================
static const unsigned char temp_binary_data_90[] =
"<svg id=\"Custom_Icon_SVGs\" data-name=\"Custom Icon SVGs\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 20 20\"><defs><style>.cls-1{fill:#141414;}</style></defs><polygon class=\"cls-1\" points=\"16.64 5.15 14.51 3.02 9.66 7.88 4.8 3.02 2.68 5."
"15 7.54 10 2.68 14.85 4.8 16.98 9.66 12.12 14.51 16.98 16.64 14.85 11.78 10 16.64 5.15\"/></svg>";

const char* icon_X_svg = (const char*) temp_binary_data_90;

//================== Volume Logo Final Version 1.0_App Icon Black circle.png ==================
static const unsigned char temp_binary_data_91[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,1,145,0,0,1,145,8,6,0,0,0,164,33,142,87,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,32,0,73,68,65,84,120,156,237,221,105,148,29,229,125,231,241,71,106,237,173,150,196,82,32,3,66,11,187,
227,88,114,60,47,50,241,162,6,99,242,18,229,197,204,57,243,10,17,59,241,130,109,196,34,192,54,32,137,197,216,108,150,177,99,207,56,246,88,188,158,156,19,52,121,101,27,27,41,137,51,51,39,39,65,74,98,99,54,209,146,48,8,138,69,13,234,110,181,182,158,243,
175,251,220,118,131,186,239,243,175,186,183,238,125,150,239,39,71,7,7,116,171,235,86,85,63,191,250,63,75,213,172,137,137,9,3,0,64,21,179,57,106,0,128,170,8,17,0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,
34,0,128,202,8,17,0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,
8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,178,57,28,58,196,42,
203,178,117,198,152,101,198,152,85,246,143,121,223,255,54,246,191,175,109,227,16,236,55,198,12,77,249,255,15,27,99,246,216,255,61,212,252,111,121,158,239,226,66,67,140,102,77,76,76,112,98,17,164,44,203,154,129,208,12,139,65,251,61,214,123,252,125,246,
78,9,26,249,167,132,203,80,158,231,67,138,207,2,222,33,68,16,132,44,203,6,109,88,52,67,195,231,160,168,106,175,13,151,33,27,46,123,242,60,63,28,230,87,65,42,8,17,120,199,118,67,53,67,99,93,155,221,77,161,219,111,131,69,254,236,162,91,12,190,33,68,208,
115,182,202,24,156,18,28,75,57,43,45,237,110,134,138,13,22,170,21,244,12,33,130,174,155,82,105,200,159,107,57,3,109,219,109,3,229,137,60,207,247,212,252,179,128,247,32,68,208,21,89,150,109,176,161,33,255,92,201,81,175,205,176,132,201,148,80,161,74,65,
173,8,17,212,34,203,178,101,54,48,154,225,65,23,85,111,236,156,18,40,204,0,67,199,17,34,232,168,44,203,54,218,224,160,155,202,63,50,251,107,59,21,10,58,137,16,65,219,236,192,120,51,60,168,56,194,176,211,134,201,142,212,15,4,218,67,136,160,18,187,208,
111,163,253,195,24,71,184,154,99,40,219,25,148,71,21,132,8,74,177,3,228,27,233,174,138,18,221,93,40,141,16,129,147,29,36,223,68,213,145,12,169,78,118,216,234,132,193,120,180,68,136,96,70,182,203,106,171,49,230,58,142,82,178,118,218,48,97,165,60,166,69,
136,224,52,118,160,124,107,164,207,167,66,53,123,109,152,48,16,143,247,32,68,48,137,240,128,130,60,203,107,43,97,130,38,66,4,132,7,170,32,76,80,32,68,18,70,120,160,3,8,147,196,17,34,9,34,60,80,3,194,36,81,132,72,66,236,108,171,237,172,241,64,141,118,
219,48,97,54,87,34,8,145,4,216,117,30,82,121,220,152,250,177,64,215,200,212,224,77,172,51,137,31,33,18,185,44,203,54,217,0,225,153,86,232,133,109,118,106,48,43,224,35,69,136,68,202,142,123,108,79,252,213,178,240,195,176,173,74,24,47,137,16,33,18,25,219,
117,181,157,85,230,240,144,140,151,108,164,139,43,46,179,83,63,0,49,177,15,71,28,34,64,224,41,153,13,248,82,150,101,91,57,65,241,160,18,137,128,157,117,181,131,41,187,8,200,94,219,197,197,44,174,192,17,34,129,99,224,28,129,251,142,157,18,204,192,123,
160,8,145,64,217,177,143,39,168,62,16,129,189,118,172,132,151,98,5,136,49,145,0,77,25,251,32,64,16,3,153,65,248,52,99,37,97,162,18,9,8,51,175,144,0,169,74,54,48,131,43,28,132,72,32,178,44,91,103,187,175,120,179,32,98,55,108,187,183,158,224,76,251,143,
16,9,128,29,60,255,118,234,199,1,201,121,220,206,224,98,208,221,99,132,136,199,108,247,213,14,30,152,136,132,209,189,229,57,6,214,61,101,187,175,118,17,32,72,156,12,186,239,177,147,73,224,33,66,196,67,89,150,109,180,1,194,115,175,128,198,26,168,191,101,
246,150,159,232,206,242,140,253,69,217,146,250,113,0,102,176,211,14,186,51,78,226,9,66,196,19,140,127,0,106,140,147,120,132,16,241,128,125,246,213,19,116,95,1,106,50,13,120,144,85,238,189,199,152,72,143,217,1,244,61,4,8,80,202,82,187,202,125,35,135,173,
183,8,145,30,178,51,78,118,241,240,68,160,178,159,216,117,84,232,17,186,179,122,196,222,65,253,36,201,47,15,116,222,227,121,158,83,149,244,0,149,72,15,216,59,39,2,4,232,156,235,178,44,227,245,187,61,64,37,210,101,246,66,231,1,138,64,61,118,219,153,91,
76,1,238,18,66,164,139,8,16,160,43,246,218,153,91,4,73,23,208,157,213,37,4,8,208,53,50,211,113,151,93,123,133,154,17,34,93,64,128,0,93,71,144,116,9,33,82,51,2,4,232,25,130,164,11,8,145,26,17,32,64,207,17,36,53,35,68,106,66,128,0,222,32,72,106,68,136,
212,128,0,1,188,67,144,212,132,16,233,48,2,4,240,214,90,251,152,33,116,16,33,210,65,246,81,38,4,8,224,175,181,172,108,239,44,66,164,67,120,22,22,16,12,30,145,210,65,172,88,239,128,44,203,6,141,49,79,5,255,69,128,180,220,148,231,249,118,206,121,123,8,
145,54,217,247,129,240,56,119,32,76,215,231,121,78,85,210,6,66,164,13,118,166,199,80,183,3,100,197,25,115,204,127,251,232,226,25,255,251,175,246,29,45,254,0,62,241,248,186,253,8,111,72,172,110,78,168,59,222,107,54,64,122,82,129,92,120,198,28,179,249,
234,22,63,246,73,67,136,192,59,30,95,183,50,245,151,87,237,86,196,192,122,117,219,121,165,45,16,5,73,182,29,172,33,169,134,16,169,32,203,178,173,76,229,5,162,34,55,132,140,141,84,64,136,148,100,223,139,190,37,168,157,6,160,113,173,189,65,68,9,132,72,
9,118,38,22,119,43,64,188,182,216,27,69,40,17,34,74,182,191,116,7,83,121,129,232,237,176,55,140,80,32,68,244,24,72,7,210,192,64,123,9,132,136,2,207,196,2,146,179,214,222,56,194,129,16,113,176,101,45,23,19,144,158,235,236,13,36,90,32,68,90,96,28,4,72,
222,246,44,203,86,165,126,16,90,33,68,90,219,202,56,8,144,52,185,129,124,34,245,131,208,10,33,50,3,59,205,239,70,47,119,14,64,55,173,101,253,200,204,8,145,105,76,233,198,2,0,99,215,143,12,114,36,78,71,136,76,143,113,16,0,239,199,180,223,105,16,34,239,
99,187,177,174,245,106,167,0,248,96,165,29,39,197,20,132,200,20,116,99,1,112,184,145,110,173,247,34,68,222,139,110,44,0,46,220,104,78,65,136,88,116,99,1,80,90,201,108,173,223,35,68,126,223,141,197,170,116,0,90,91,120,72,99,3,33,210,176,201,14,154,1,128,
86,242,55,158,134,16,153,124,54,22,47,153,2,80,214,122,158,173,69,136,24,238,38,0,180,97,123,234,107,71,146,14,17,123,23,177,222,131,93,1,16,166,165,169,175,29,153,227,193,62,244,132,189,123,72,238,228,127,108,205,2,243,196,95,158,59,227,127,255,167,
125,71,205,181,63,124,173,171,251,132,112,220,118,245,50,179,249,234,153,103,193,63,244,228,176,121,240,201,195,169,157,81,89,59,178,61,207,243,33,15,246,165,235,82,174,68,24,76,7,208,41,201,174,29,73,50,68,236,251,1,54,121,176,43,149,77,76,180,248,227,
216,104,59,159,69,218,38,184,246,102,178,62,213,149,236,169,86,34,91,131,95,153,62,225,248,83,215,103,145,54,215,181,147,246,181,151,100,53,146,92,136,216,42,36,232,247,165,215,249,59,76,134,192,133,252,152,209,202,20,167,252,166,88,137,132,63,165,215,
213,167,208,234,87,178,221,254,8,36,174,213,181,211,230,181,23,199,197,151,220,100,157,164,66,196,246,89,70,241,124,172,150,57,224,248,93,140,255,247,24,117,113,93,119,237,92,123,174,207,6,34,185,106,36,181,74,36,158,187,132,118,250,5,232,203,66,59,184,
246,92,146,90,128,152,76,136,216,42,36,154,133,133,117,253,30,147,35,112,33,67,156,150,134,62,251,179,140,148,42,145,184,250,42,153,33,131,94,104,119,102,70,58,215,222,166,84,170,145,36,66,36,182,42,196,53,182,217,242,119,177,205,62,109,164,173,173,121,
25,237,92,183,225,145,106,100,67,10,23,75,42,149,72,50,165,37,0,111,36,49,83,43,250,16,177,235,66,162,122,99,97,173,51,124,169,68,208,10,51,124,203,72,98,166,86,10,149,72,156,119,3,140,110,162,87,184,246,202,136,190,26,137,58,68,98,88,157,62,19,214,137,
160,23,88,39,82,154,84,35,81,143,141,196,94,137,68,90,74,214,57,61,134,20,129,11,83,179,74,138,122,76,54,246,16,137,243,228,145,33,232,37,50,164,172,245,246,53,220,81,138,54,68,236,128,86,216,79,234,109,161,157,46,41,6,214,81,89,27,3,235,38,237,174,212,
104,171,145,152,43,145,184,167,245,50,184,137,94,225,218,171,98,67,172,139,15,163,12,17,91,58,174,245,96,87,106,83,103,143,22,80,245,218,225,218,155,209,210,88,199,104,99,173,68,226,95,92,200,111,50,122,129,20,105,71,148,237,82,116,33,98,75,198,168,167,
212,213,57,197,146,49,17,184,48,189,183,178,149,49,190,66,55,198,74,100,67,204,3,234,0,130,22,93,151,214,28,15,246,161,211,146,120,78,86,171,59,55,205,29,225,204,255,177,245,103,55,125,124,230,124,126,121,248,132,249,155,127,31,105,189,1,244,220,31,95,
56,223,252,241,133,11,102,220,13,57,135,114,46,167,189,60,28,85,67,59,215,94,34,213,72,49,192,158,231,249,97,15,246,165,35,162,10,17,187,66,61,234,1,245,73,237,36,65,27,159,221,244,177,129,25,255,219,255,59,56,78,136,4,64,66,164,213,121,252,191,7,142,
206,24,34,147,115,124,103,84,227,29,76,28,154,79,247,221,17,203,23,138,173,59,43,153,167,245,246,106,157,8,253,218,17,104,231,28,178,78,164,19,162,234,210,138,45,68,146,120,126,63,128,160,173,183,189,38,81,136,38,68,236,218,144,149,30,236,74,253,92,51,
172,90,237,0,15,208,75,222,68,27,21,129,235,179,109,93,123,105,157,152,104,110,120,99,170,68,162,127,110,63,128,104,68,211,94,197,20,34,201,116,101,213,122,39,73,37,18,191,26,43,217,118,174,189,196,74,145,181,177,116,105,69,17,34,73,117,101,53,181,179,
234,183,87,159,133,63,184,126,124,16,197,141,111,44,149,72,114,93,89,117,85,19,204,174,137,95,157,149,40,149,108,41,81,180,91,177,132,8,179,178,0,132,38,138,46,173,224,23,27,166,218,149,53,81,245,182,205,241,89,215,118,219,249,44,252,209,242,92,57,171,
137,122,174,189,68,13,134,190,240,48,134,74,36,186,7,154,169,244,234,73,170,105,63,133,53,14,117,158,127,174,159,178,130,239,69,137,33,68,232,202,2,16,170,107,67,63,115,65,135,136,125,236,251,122,15,118,165,235,218,89,180,213,171,207,194,147,107,199,
53,65,162,141,207,214,121,237,197,42,203,178,160,111,132,67,175,68,210,236,202,50,116,103,161,13,116,103,249,38,232,118,44,244,129,245,36,67,164,157,233,144,174,207,106,166,104,86,253,44,252,81,215,53,224,250,28,215,200,180,130,110,199,66,175,68,24,15,
1,16,186,181,182,107,62,72,193,134,136,157,95,157,214,212,222,41,88,108,136,170,88,108,232,165,96,171,145,144,43,145,116,199,67,76,155,253,202,189,250,44,252,193,245,227,155,96,219,179,144,199,68,146,14,145,214,139,197,90,255,70,182,94,48,88,253,231,
210,223,29,136,137,137,54,22,12,58,62,219,198,181,151,248,5,68,37,210,3,235,2,222,119,0,152,42,216,113,145,32,43,17,123,176,211,120,151,250,116,92,51,172,218,249,44,179,179,162,55,225,58,87,142,107,171,182,107,47,153,51,48,35,185,49,222,229,233,190,205,
40,212,74,132,42,132,117,34,168,138,117,34,190,10,178,75,43,212,49,145,180,199,67,218,153,171,223,206,157,100,155,159,133,63,234,186,6,90,126,142,106,213,133,16,233,162,180,103,102,5,234,215,159,59,111,198,29,255,231,87,143,153,141,255,251,141,212,15,
145,218,13,255,105,192,124,241,163,3,51,254,245,141,127,247,166,249,231,87,198,189,219,111,180,20,100,15,11,221,89,161,10,177,59,139,174,140,206,241,245,28,114,13,180,99,105,136,239,23,9,174,18,177,131,234,75,61,216,149,222,169,113,96,221,245,139,220,
171,207,226,244,195,85,215,224,120,59,159,109,231,218,227,18,40,200,13,242,144,7,251,161,22,98,37,66,21,2,32,86,193,181,111,33,142,137,48,30,98,92,11,190,170,127,182,206,55,27,242,86,196,14,114,190,37,176,245,245,81,219,103,219,184,246,40,69,10,193,133,
72,136,149,72,240,239,36,6,128,25,80,137,116,1,33,210,195,105,186,76,15,246,67,157,211,188,219,249,44,215,64,219,130,123,168,108,136,149,72,146,111,50,60,13,179,179,210,198,236,172,104,101,89,22,84,151,125,80,149,72,136,211,223,234,16,234,75,169,120,
100,74,103,249,120,30,93,159,227,60,171,4,245,12,173,208,42,17,66,4,64,236,130,26,23,9,109,76,132,233,189,86,109,119,146,172,19,9,66,157,213,104,59,159,165,26,237,136,160,110,150,67,11,145,96,95,33,217,113,237,252,194,133,248,89,196,113,30,185,6,52,8,
145,26,177,70,196,170,171,34,96,76,36,16,237,140,47,212,249,89,170,209,78,8,170,199,37,228,151,82,1,64,140,130,122,172,19,99,34,33,114,172,26,158,104,99,197,49,43,214,195,48,209,198,234,239,58,63,219,214,181,71,41,50,73,102,162,230,121,30,196,51,180,
66,11,145,180,31,188,56,85,59,221,2,33,126,22,167,31,175,54,186,179,106,251,108,157,215,94,90,86,133,242,32,198,96,66,132,53,34,191,23,106,27,64,251,209,89,62,158,71,215,231,56,207,106,193,76,34,10,105,76,132,16,153,170,157,85,191,33,126,22,92,3,105,
9,166,235,62,212,55,27,38,143,117,34,105,99,157,8,124,17,82,136,48,168,222,68,127,22,122,121,46,232,207,234,134,96,186,179,66,10,17,22,26,78,209,206,239,34,25,18,135,186,174,1,95,63,155,24,186,179,80,51,102,103,165,141,217,89,240,4,149,72,160,120,159,
72,218,38,218,24,67,168,243,179,92,3,233,97,76,36,84,181,253,38,215,56,50,202,168,106,231,144,34,177,163,59,11,245,97,92,29,189,60,23,140,171,119,69,48,11,171,9,145,16,145,34,232,229,185,32,69,48,5,33,18,40,214,137,164,141,117,34,240,5,79,241,5,0,84,
70,37,18,42,22,138,128,133,34,240,0,179,179,2,69,134,128,12,129,15,66,10,17,30,3,63,85,47,6,70,123,249,89,156,126,30,88,108,8,15,208,157,21,34,215,224,102,171,175,212,195,129,81,6,85,59,199,185,212,195,113,125,212,245,217,182,174,189,86,159,77,80,150,
101,203,242,60,63,236,251,55,39,68,66,85,91,107,238,56,30,44,52,243,3,211,179,82,32,93,248,187,124,255,158,132,72,128,88,38,130,94,158,11,150,137,116,13,111,54,68,141,24,89,7,35,235,81,227,29,235,157,183,223,24,179,50,160,253,173,85,109,139,13,29,122,
245,89,156,126,14,233,205,130,15,66,10,145,33,66,196,162,63,11,189,60,23,244,103,97,10,186,179,2,69,111,22,232,205,130,15,8,145,80,133,152,4,164,72,231,176,78,4,158,32,68,2,197,235,68,210,230,92,235,209,234,60,212,248,89,102,121,167,135,7,48,2,0,42,163,
18,9,208,132,252,95,197,91,58,215,103,39,28,247,131,189,250,44,78,59,96,213,143,103,141,159,109,231,218,227,18,120,143,253,30,237,75,75,33,133,136,172,220,92,239,193,126,244,220,161,209,83,102,199,243,163,51,238,198,211,111,30,155,241,191,185,62,123,
104,244,100,203,175,215,171,207,226,189,90,157,99,99,207,243,76,234,252,108,59,215,158,107,219,137,9,98,141,136,152,213,242,206,192,35,89,150,109,53,198,108,9,98,103,1,160,61,187,243,60,31,12,225,24,50,38,2,0,168,44,164,16,9,166,188,3,128,54,237,9,229,
0,18,34,0,224,31,239,31,1,223,68,119,22,0,160,178,144,66,36,152,242,14,0,218,228,253,123,68,154,130,153,226,43,111,248,202,178,204,131,61,233,189,197,115,102,153,139,6,116,167,110,228,196,132,121,225,221,19,170,191,187,246,140,185,234,239,246,226,187,
39,204,145,19,238,153,125,203,23,246,153,115,23,232,238,85,94,59,122,202,28,26,99,170,175,139,15,231,127,239,219,199,85,127,143,243,31,191,208,22,27,14,243,174,245,134,27,46,237,55,23,45,214,157,190,7,127,115,196,252,244,213,163,206,191,183,124,65,159,
185,237,131,139,85,219,124,241,200,9,115,243,191,12,59,131,68,26,188,123,63,188,196,244,207,153,229,220,166,52,120,55,255,235,176,186,209,75,145,28,207,71,63,186,84,117,238,155,199,83,227,246,15,14,152,107,62,48,95,245,119,127,246,234,184,42,68,46,30,
152,99,30,253,163,165,170,115,223,188,158,48,41,152,49,224,208,198,68,232,210,50,166,104,184,229,23,78,126,241,52,36,24,254,244,3,11,156,127,83,130,70,2,71,67,26,49,105,204,22,59,26,8,9,4,105,200,70,20,85,139,52,54,210,232,92,172,188,203,78,77,149,0,
209,4,114,217,0,249,214,111,222,117,254,189,42,1,162,169,108,83,17,202,11,169,76,128,33,18,204,140,133,186,53,131,68,26,137,230,139,130,90,253,217,124,69,185,32,209,108,115,77,255,28,243,200,31,233,130,228,38,105,36,142,79,56,183,185,168,111,150,121,
228,35,186,134,50,37,114,140,229,88,203,49,119,29,67,57,206,55,253,139,46,64,110,187,98,192,124,122,249,124,213,249,254,233,43,186,0,145,115,39,231,80,206,165,107,155,197,77,6,1,242,126,193,60,242,196,80,137,132,77,126,241,110,249,215,225,98,124,98,242,
17,221,45,254,108,190,124,177,249,211,229,186,32,121,72,42,18,197,54,47,234,111,52,24,174,32,41,238,54,165,34,57,62,225,220,102,127,223,44,243,40,65,50,169,8,16,57,30,253,115,156,199,78,142,175,28,103,77,149,42,1,114,205,242,249,170,243,44,21,200,131,
207,232,2,68,206,157,156,67,215,54,229,186,149,235,151,0,57,77,80,203,25,168,68,2,87,4,201,211,141,70,67,209,22,152,91,175,88,108,174,209,4,201,161,163,230,193,103,142,168,182,185,102,113,137,32,121,186,209,104,184,182,185,104,14,21,137,153,18,32,114,
140,93,199,172,168,78,159,214,5,200,230,102,5,162,56,191,63,61,164,15,144,162,2,153,51,203,185,77,217,199,91,158,38,64,102,64,136,212,136,74,100,26,205,32,217,87,162,34,209,4,201,207,14,29,53,15,61,163,171,72,138,174,173,117,186,32,145,125,213,86,36,
178,205,84,131,164,8,144,117,141,46,44,77,5,114,139,54,64,46,31,48,215,156,171,175,64,30,210,6,200,58,93,5,34,215,41,1,210,18,33,82,35,86,173,207,160,8,146,61,141,174,45,77,255,246,173,151,149,8,146,223,234,198,72,86,247,207,49,15,175,85,6,201,30,253,
24,137,108,51,181,32,145,99,40,223,123,181,114,12,164,56,247,202,0,249,244,185,186,49,144,159,29,26,55,15,253,86,23,32,178,175,154,49,144,162,11,107,15,1,226,16,212,205,114,80,33,18,210,140,133,94,144,95,204,91,247,14,155,125,202,89,91,101,130,228,225,
103,117,179,182,214,44,214,7,137,236,171,118,214,86,74,65,210,12,144,53,202,89,88,114,28,203,4,136,198,207,95,43,23,32,154,89,88,251,236,57,39,64,156,130,106,231,130,121,20,124,83,150,101,188,87,196,65,26,161,135,214,218,110,16,133,71,158,61,98,126,246,
154,123,29,201,53,231,46,48,183,92,166,91,71,178,111,228,132,217,172,104,48,164,17,122,232,195,186,70,72,26,204,205,255,166,159,218,28,162,50,231,174,204,241,184,245,178,114,1,242,240,179,186,0,209,158,59,237,245,128,226,102,217,125,64,61,18,226,179,
179,168,70,28,228,23,117,115,179,34,81,244,123,223,114,233,226,34,32,92,36,104,36,112,84,99,36,139,26,13,140,166,34,145,125,213,142,145,60,244,135,118,150,82,132,138,0,249,240,210,226,216,105,198,64,54,43,43,144,91,47,29,48,159,62,71,55,6,242,243,67,
202,0,233,159,83,156,11,213,24,200,17,2,164,132,189,193,236,169,69,136,68,234,200,228,93,234,241,226,149,164,174,63,55,95,210,175,186,83,149,32,121,248,185,119,85,219,92,189,168,207,60,248,135,75,220,65,34,119,169,255,38,99,36,167,156,219,92,212,103,
138,109,198,22,36,114,140,228,123,201,49,115,29,3,57,78,197,185,29,113,7,136,220,32,92,125,206,60,213,249,106,158,91,23,57,246,178,175,114,46,92,219,148,235,175,56,183,4,136,86,112,237,91,136,33,18,204,131,201,122,77,126,113,111,251,247,119,138,174,4,
141,91,46,89,172,10,18,233,238,120,228,121,229,24,137,109,112,52,65,34,251,170,29,35,137,41,72,154,1,162,237,194,146,227,164,13,144,162,2,81,248,249,235,227,230,145,231,220,231,180,25,32,218,46,44,217,87,2,164,148,224,102,160,134,24,34,76,243,45,97,50,
72,180,93,91,23,235,26,158,34,72,158,211,119,109,61,248,161,146,65,162,232,218,138,33,72,138,0,249,208,18,93,23,86,153,0,145,27,130,76,217,133,245,90,201,0,81,118,97,17,32,149,4,215,190,5,55,176,110,26,131,235,82,242,173,244,96,87,130,81,220,189,255,
129,238,110,87,60,250,194,145,226,238,212,69,2,231,230,139,245,131,237,183,253,218,93,109,20,213,203,135,108,99,229,48,114,114,194,220,246,31,250,106,203,39,101,206,73,153,239,41,55,2,87,43,43,144,39,165,2,121,193,29,32,101,206,137,246,60,99,90,171,67,
155,133,26,234,75,169,168,70,74,42,238,98,127,173,111,108,111,214,86,36,175,143,23,129,163,81,52,68,127,224,238,10,41,26,161,255,120,167,104,56,93,138,138,228,67,250,112,244,5,1,130,105,12,135,184,140,129,16,73,72,51,72,138,71,164,40,22,155,221,116,145,
237,18,113,40,130,228,121,229,130,196,69,115,204,183,62,168,15,146,226,17,41,174,5,137,179,103,153,111,149,168,178,122,77,190,187,28,3,57,22,206,133,132,39,244,1,34,193,255,169,76,183,144,240,231,37,2,68,142,173,28,99,231,66,194,35,4,72,155,130,28,239,
13,53,68,24,92,175,72,126,193,111,255,141,109,148,20,253,229,234,32,201,199,205,183,95,208,143,145,104,131,228,246,95,43,199,72,102,55,26,102,223,131,164,25,32,218,49,144,219,149,213,227,205,23,45,54,87,159,173,27,3,121,82,89,61,22,1,34,231,105,182,98,
12,68,206,213,111,8,144,54,5,121,115,28,228,152,136,105,140,139,112,181,182,65,186,38,154,119,195,26,223,222,119,196,60,153,187,199,72,174,206,230,155,155,214,232,198,72,94,26,181,13,143,163,219,74,26,220,111,126,80,63,70,114,135,132,228,168,127,99,36,
101,142,121,153,239,33,1,242,169,179,117,93,88,191,120,99,220,60,250,162,34,64,74,28,115,237,121,132,211,149,121,158,7,119,131,28,106,37,98,66,92,148,227,19,249,133,151,95,252,151,148,141,173,4,195,213,138,138,68,130,70,2,71,99,178,107,203,209,80,73,
67,122,135,178,145,146,109,125,179,121,167,239,17,2,4,46,33,6,136,9,60,68,232,210,106,83,51,72,164,43,66,179,24,109,211,234,254,70,151,137,67,17,36,47,30,81,109,115,213,194,62,243,205,43,180,65,34,139,214,20,11,18,103,27,243,192,21,3,102,141,172,134,
243,64,17,108,87,44,41,190,171,115,33,225,137,83,197,247,212,4,200,77,107,250,205,85,103,233,22,18,62,153,31,85,6,72,95,113,236,228,24,186,182,57,217,133,69,128,116,66,176,55,197,132,72,226,138,187,222,103,222,49,47,141,156,84,245,167,171,131,228,141,
113,179,125,223,136,106,155,171,37,72,46,215,4,201,73,243,213,103,244,99,36,15,92,190,164,231,65,82,4,200,229,75,138,239,168,25,3,145,239,39,223,211,229,166,213,253,230,83,103,233,198,64,126,81,84,135,35,206,109,22,1,114,185,110,12,68,174,23,185,110,
8,144,142,9,182,61,35,68,208,8,146,223,74,69,114,82,53,179,231,198,85,253,170,46,20,9,146,111,191,52,162,218,166,220,165,63,112,153,50,72,126,171,159,181,245,141,203,122,23,36,242,93,228,59,53,42,16,247,44,44,249,94,154,0,145,32,191,234,44,221,44,172,
230,57,112,145,99,36,199,74,51,11,75,174,19,185,94,8,144,142,34,68,186,45,207,243,195,140,139,116,142,52,8,95,125,86,198,72,220,141,152,216,164,12,18,233,135,223,62,228,110,196,76,49,70,162,15,146,175,61,171,31,35,233,69,144,52,3,100,181,226,231,202,
247,144,239,163,13,144,162,2,81,248,197,155,227,102,123,137,0,209,141,129,156,44,174,19,2,164,227,8,145,30,161,26,233,160,102,144,72,99,166,170,72,86,234,26,180,34,72,74,84,36,154,6,109,50,72,180,21,201,165,221,11,146,102,112,105,42,16,217,127,117,128,
172,234,55,87,157,169,171,64,154,199,220,165,8,144,75,149,21,8,1,82,151,189,246,166,56,72,132,8,222,163,121,87,92,84,36,138,254,118,117,144,188,57,110,190,51,164,28,35,89,208,104,216,180,65,50,170,24,35,41,130,228,146,37,102,205,194,122,131,164,8,144,
75,151,20,223,193,181,79,163,101,2,100,101,35,64,52,199,239,151,202,234,79,142,133,28,147,69,154,49,144,18,213,31,74,123,34,228,67,22,236,58,145,38,214,139,212,67,26,195,251,47,181,3,194,10,143,237,31,41,130,194,69,2,231,43,43,251,85,219,124,105,236,
164,249,250,115,238,134,75,246,177,184,155,86,116,199,140,74,72,62,247,78,177,237,78,43,115,204,202,236,135,4,245,85,202,46,172,95,74,88,239,119,7,72,153,99,166,61,15,168,44,200,245,33,77,161,87,34,98,167,7,251,16,29,105,48,164,225,208,86,36,95,185,176,
223,124,234,76,93,69,242,88,137,138,228,254,75,220,21,137,52,114,210,32,151,169,72,180,225,168,85,4,200,37,37,42,144,50,1,162,173,64,202,4,72,137,10,132,0,169,213,112,200,1,98,34,9,17,186,180,106,82,4,201,243,141,32,209,244,195,127,249,66,219,224,57,
252,226,173,70,99,167,26,35,145,32,185,88,25,36,207,235,198,72,22,206,158,85,108,179,83,65,82,4,200,197,75,138,125,85,141,129,60,175,11,16,9,230,43,207,80,142,129,148,8,16,217,215,133,138,49,144,34,64,158,39,64,106,22,124,251,21,67,136,4,221,159,232,
187,34,72,94,120,199,12,201,226,55,69,107,246,149,21,139,204,85,103,206,115,126,171,95,190,53,110,30,59,112,68,181,205,85,11,102,155,251,47,26,80,5,137,236,235,232,137,83,206,109,202,98,58,217,102,187,65,82,4,200,69,3,197,62,186,126,166,236,151,236,159,
42,64,86,244,155,171,206,152,167,58,62,82,129,60,118,64,25,32,23,53,22,18,186,182,41,231,91,246,149,0,169,93,240,237,87,240,99,34,166,49,46,34,15,46,91,235,193,174,68,75,26,203,251,164,177,84,54,186,223,61,56,98,126,249,214,49,231,223,147,192,249,242,
10,221,24,201,208,216,73,115,231,139,239,170,198,72,100,95,181,99,36,178,205,42,99,36,101,142,73,153,159,35,1,114,165,34,136,197,83,111,29,51,143,29,212,5,136,246,152,104,143,51,58,226,140,144,103,102,153,72,42,17,67,151,86,253,70,108,35,40,13,140,166,
127,254,203,23,216,59,105,7,9,154,239,30,208,141,145,72,119,209,125,202,138,68,246,85,59,70,114,223,154,242,21,201,100,128,40,199,64,74,5,72,81,129,184,143,71,169,0,89,51,160,26,3,33,64,186,106,119,232,1,98,34,10,145,29,30,236,67,244,154,65,242,210,209,
147,154,54,206,124,105,133,50,72,222,62,86,84,46,154,109,174,92,208,103,238,213,6,201,190,70,99,232,218,230,194,190,89,230,222,18,65,34,63,91,246,65,246,197,181,237,226,152,237,211,5,136,84,100,131,103,204,83,29,7,57,102,218,0,145,239,38,223,209,181,
77,57,175,4,72,87,69,209,21,31,69,136,228,121,46,221,89,251,61,216,149,232,73,3,115,87,137,138,228,75,218,138,228,237,99,230,123,7,149,21,201,252,70,195,168,9,146,187,74,84,36,247,174,30,104,204,174,114,144,99,160,153,181,38,63,247,46,101,5,34,149,219,
149,203,244,21,200,119,53,1,34,129,187,90,95,129,220,69,128,116,27,33,226,25,6,216,187,164,8,18,123,119,173,153,57,116,195,249,182,139,198,161,168,72,94,214,205,218,90,57,191,207,220,179,90,17,36,71,79,22,251,170,157,181,117,143,50,72,100,63,159,122,
251,88,203,89,88,197,49,58,170,11,144,193,101,243,84,223,251,41,123,140,92,228,59,200,119,81,205,194,26,179,199,136,0,233,166,189,33,190,10,119,58,49,133,8,93,90,93,36,13,206,221,47,149,168,72,206,179,119,218,14,210,72,126,239,101,125,69,114,207,42,93,
144,200,190,106,43,18,217,166,54,72,118,189,125,108,218,10,68,126,158,42,64,206,239,55,131,75,117,21,200,174,50,1,178,74,95,129,200,190,18,32,93,23,77,123,21,197,236,172,166,44,203,36,217,87,250,177,55,105,144,6,92,26,172,85,138,70,87,124,239,119,35,
230,169,195,238,89,91,18,56,95,58,95,57,107,75,66,98,72,49,107,171,217,184,42,103,109,201,54,213,65,96,3,178,234,231,92,118,29,62,102,190,251,187,18,1,162,153,133,165,60,110,168,69,240,179,178,154,98,170,68,196,118,15,246,33,41,35,205,70,83,219,181,165,
173,72,14,31,43,2,71,219,181,181,109,165,178,107,203,54,154,154,174,173,109,202,112,148,198,93,42,168,162,155,79,25,32,82,153,173,95,90,162,11,75,17,32,178,175,178,207,218,46,44,2,164,103,118,198,18,32,38,194,74,100,149,180,21,30,236,74,114,164,1,223,
118,225,98,117,69,242,87,175,140,154,167,134,21,21,201,210,121,230,134,243,22,169,182,41,119,214,91,14,28,113,54,140,178,143,247,172,92,220,232,238,113,24,61,53,97,238,222,127,164,216,182,203,57,115,103,155,215,143,159,114,254,189,47,157,183,168,209,
133,165,176,107,248,152,249,222,43,163,206,191,88,230,59,105,143,19,106,115,125,158,231,116,103,249,42,203,50,89,51,178,62,170,47,21,8,9,146,173,37,131,100,151,34,72,6,75,6,201,86,101,144,108,43,17,36,91,148,65,226,114,67,201,0,249,43,101,128,104,191,
139,246,248,160,54,242,172,172,101,49,29,222,216,186,179,12,3,236,189,35,13,147,52,80,69,99,171,24,40,190,225,3,186,6,117,178,49,85,14,182,75,144,185,186,182,138,187,241,253,71,138,49,12,231,96,251,172,114,85,214,76,138,239,187,68,57,136,126,184,68,128,
92,184,184,216,71,231,32,58,1,226,131,232,102,145,70,87,137,152,70,53,34,253,141,75,61,216,149,36,201,59,186,165,33,151,177,10,141,239,31,210,87,36,95,92,174,171,72,246,143,219,6,243,148,163,34,177,161,163,173,72,138,144,28,47,95,145,220,176,124,81,49,
6,162,177,91,66,243,144,34,64,74,236,187,246,120,160,118,31,177,235,218,162,17,99,37,98,88,51,210,91,35,182,177,221,175,172,72,190,120,174,189,67,119,144,160,249,254,171,186,138,100,229,188,62,179,117,197,226,34,208,90,25,178,141,171,182,34,145,109,174,
82,134,99,83,17,32,202,10,164,84,128,172,208,85,32,251,143,18,32,158,216,27,91,128,152,136,67,100,171,7,251,144,180,34,72,14,54,238,218,21,109,167,249,194,114,101,144,188,115,172,168,92,52,219,188,112,126,159,217,162,13,146,131,141,70,214,181,77,153,
249,180,165,68,144,200,119,250,228,18,221,163,76,228,187,105,3,68,246,161,152,133,229,216,230,212,239,134,158,139,114,246,104,148,33,98,87,130,238,246,96,87,146,38,13,215,182,131,250,138,228,11,218,138,228,157,99,230,7,135,244,21,201,150,11,116,65,34,
251,170,173,72,100,155,154,32,145,125,149,234,66,83,129,124,95,27,32,23,232,43,144,109,4,136,47,134,99,237,33,137,181,18,49,12,176,251,161,8,146,151,27,131,237,154,53,17,159,63,199,118,253,56,20,21,201,107,163,170,109,94,56,175,207,220,125,190,50,72,
94,62,162,91,71,50,107,86,177,77,77,144,200,126,202,254,206,180,173,230,119,113,145,159,37,63,83,126,182,107,255,228,120,23,223,133,0,241,197,142,152,214,134,76,21,229,192,122,19,43,216,253,33,13,184,52,128,82,25,104,252,224,245,81,179,251,93,247,96,
251,250,129,121,230,11,231,40,7,219,143,157,52,247,252,78,55,216,126,247,121,250,193,246,123,94,209,13,182,203,126,202,254,78,37,223,81,190,171,75,153,125,210,126,79,116,213,234,88,158,149,245,126,49,87,34,134,106,196,31,69,69,242,187,35,102,232,152,
110,140,228,243,211,52,184,211,105,54,194,170,49,146,121,125,230,174,243,221,13,113,81,145,188,162,31,35,185,235,60,221,76,180,102,48,54,63,171,13,16,217,182,252,12,213,24,200,177,147,197,113,38,64,188,178,51,214,0,49,9,84,34,178,168,103,136,233,190,
254,144,6,252,238,243,250,213,21,201,127,207,199,212,21,201,231,179,133,170,109,22,119,234,175,140,20,85,68,43,178,143,178,175,250,138,100,164,216,182,203,23,236,126,254,32,31,115,254,221,50,251,160,253,94,232,186,43,243,60,143,246,197,121,81,135,136,
105,4,137,84,35,215,121,176,43,176,164,65,188,235,3,229,130,228,239,143,184,131,228,147,139,203,5,201,189,175,234,130,68,246,85,27,36,178,77,77,144,104,148,249,217,218,239,131,174,147,105,189,235,98,62,236,177,119,103,25,166,251,250,103,178,177,29,215,
205,218,250,252,217,11,139,128,112,145,160,145,192,81,205,218,154,219,103,238,90,238,110,160,39,27,103,229,172,45,217,166,54,28,91,41,2,68,246,79,51,11,107,156,0,241,88,244,15,133,141,190,18,49,84,35,222,42,42,146,229,253,197,88,133,198,255,120,67,95,
145,124,238,108,93,69,114,64,66,226,144,174,34,185,83,17,58,198,134,228,125,135,170,87,36,101,126,150,118,255,209,19,251,243,60,95,21,251,161,79,161,18,49,84,35,126,42,42,146,67,35,230,128,178,34,249,220,89,250,138,68,2,71,179,205,11,165,34,101,149,171,
191,0,0,11,63,73,68,65,84,57,87,87,145,72,48,104,43,146,59,207,173,86,145,20,1,114,174,174,2,145,227,70,128,120,45,137,118,39,137,74,196,80,141,120,77,26,240,59,207,209,87,36,63,124,115,204,252,253,136,162,34,233,159,103,254,242,44,125,69,114,223,235,
186,138,228,235,231,232,43,146,251,95,215,87,36,101,182,173,221,95,244,76,18,85,136,73,168,18,49,84,35,254,42,186,127,94,31,41,26,70,13,9,6,9,8,23,9,26,9,28,13,9,176,59,21,13,184,4,194,253,202,198,91,182,37,161,160,173,72,62,217,63,151,0,137,71,50,237,
77,50,149,136,161,26,241,94,179,209,149,46,38,141,31,190,53,102,254,65,81,145,124,66,42,146,51,149,21,201,113,93,72,200,192,252,215,74,84,36,223,144,138,228,184,59,36,101,63,63,209,34,32,181,251,135,158,74,166,10,49,137,85,34,134,106,196,111,205,238,
159,3,138,198,214,40,26,220,38,9,26,9,28,13,9,48,77,151,146,4,194,55,74,84,36,18,56,43,21,225,216,42,24,9,144,96,36,213,206,36,85,137,24,170,145,32,20,141,110,182,72,93,145,252,245,219,210,240,30,119,254,189,79,244,207,53,127,113,134,190,34,249,70,62,
234,108,176,101,31,101,95,213,21,73,62,170,10,73,9,200,143,47,154,91,122,127,208,115,73,85,33,38,193,74,196,80,141,248,111,178,177,61,166,155,181,245,23,203,22,154,79,76,105,112,103,34,65,243,215,111,41,103,109,205,233,51,95,59,219,29,14,147,141,187,
114,214,150,108,83,19,142,82,145,252,163,4,227,68,99,12,132,0,9,198,166,212,190,112,114,149,136,105,84,35,18,36,91,60,216,21,180,32,13,248,87,207,210,87,36,63,58,60,102,254,97,84,81,145,44,154,107,62,187,76,95,145,60,240,166,178,34,57,107,81,241,124,
43,151,49,9,201,55,117,21,201,159,13,204,55,63,29,57,70,128,132,97,119,158,231,131,169,125,233,84,67,132,103,106,5,130,32,65,64,162,126,70,214,76,82,236,206,50,246,185,254,116,107,5,64,26,110,105,192,165,75,71,243,238,144,207,44,93,104,62,190,80,209,
181,53,122,188,24,75,209,108,115,197,156,62,243,213,51,149,93,91,54,108,92,219,92,48,171,92,56,194,123,59,83,12,16,147,106,37,210,196,251,70,194,81,84,36,103,46,42,26,116,141,31,13,143,153,127,28,115,87,36,18,56,159,93,170,171,72,14,158,56,105,30,120,
75,87,145,200,190,202,203,163,92,198,38,38,138,109,82,145,4,47,218,247,133,184,36,89,137,76,177,209,155,61,65,75,69,69,242,214,104,209,144,107,124,86,89,145,72,208,72,224,104,148,169,72,100,95,199,20,55,104,18,52,178,77,42,146,160,109,75,53,64,76,234,
149,136,105,84,35,242,222,227,107,61,216,21,40,200,12,167,59,74,84,36,63,126,71,95,145,124,102,137,190,34,249,166,84,36,142,223,29,153,225,117,71,137,138,68,182,121,64,25,146,240,198,126,99,204,186,88,95,125,171,145,122,37,98,236,148,188,97,15,246,3,
10,163,182,177,61,120,92,55,253,247,51,3,11,205,199,23,232,42,146,31,15,235,166,255,174,232,235,51,119,156,177,168,241,144,196,22,14,216,176,145,65,116,215,54,223,56,113,202,188,113,242,20,151,64,120,54,165,28,32,134,16,41,6,217,135,82,120,230,127,76,
138,32,121,187,49,142,32,149,180,235,207,159,15,44,48,31,211,4,201,209,227,230,71,239,140,169,182,121,65,223,108,115,251,50,101,144,188,45,171,204,79,205,184,45,249,30,242,125,92,149,13,188,35,83,122,159,72,253,180,36,223,157,213,196,32,123,120,164,1,
151,134,124,197,28,221,189,208,143,223,61,106,126,117,212,221,181,37,129,243,153,129,5,170,109,30,60,113,202,124,235,176,166,107,171,17,58,239,239,218,210,126,30,222,25,182,221,88,201,142,133,52,37,95,137,76,193,32,123,96,164,225,149,6,248,192,241,83,
170,169,186,127,190,88,87,145,72,208,72,224,104,182,41,21,201,109,75,53,21,137,13,139,41,211,127,101,191,9,144,96,109,39,64,26,168,68,166,200,178,76,186,181,110,244,102,135,160,34,13,248,109,75,244,21,201,255,60,114,212,252,106,92,81,145,204,159,91,4,
143,134,84,20,15,190,163,171,72,100,95,223,56,57,161,250,251,240,82,244,239,77,47,131,16,153,194,174,100,223,67,183,86,120,36,72,54,75,144,244,233,130,228,39,35,250,32,185,190,95,25,36,39,79,153,135,20,193,32,251,248,230,169,9,2,36,92,31,201,243,124,
79,234,7,161,137,238,172,41,236,44,11,186,181,2,36,13,178,52,224,7,149,51,156,36,24,36,32,92,36,104,36,112,52,36,28,36,200,92,93,91,178,143,4,72,176,182,17,32,239,69,37,50,13,186,181,194,37,13,248,173,3,229,42,146,127,58,230,174,72,254,100,94,185,138,
228,225,119,233,170,138,16,221,88,211,160,18,153,222,86,187,136,8,129,145,134,91,26,112,25,163,208,172,249,184,126,209,130,34,32,92,36,104,138,138,68,179,142,100,246,108,115,235,98,119,69,130,224,208,75,49,13,66,100,26,116,107,133,173,8,146,35,141,32,
209,204,176,218,184,176,68,144,140,42,103,109,73,144,244,19,36,17,185,137,110,172,233,17,34,51,176,79,228,220,230,229,206,193,169,8,146,145,81,243,242,201,147,238,210,193,76,152,141,11,231,171,131,100,199,216,81,213,54,47,232,155,85,108,23,193,147,69,
133,44,72,158,1,33,210,66,158,231,210,173,181,215,219,29,68,75,141,32,25,51,47,43,187,182,54,46,152,111,254,100,174,50,72,70,199,157,219,147,199,157,252,221,248,244,239,75,71,48,100,81,225,6,78,215,204,8,17,183,13,60,91,43,92,69,144,140,142,21,131,221,
154,110,168,235,180,65,114,92,42,146,241,25,183,35,139,10,37,192,180,179,197,224,173,141,169,63,27,203,133,16,113,176,171,82,25,31,9,152,4,201,35,163,99,230,229,83,186,6,189,76,144,60,126,116,252,180,127,63,102,127,222,65,229,207,131,183,190,195,179,
177,220,152,226,171,196,180,223,240,201,32,247,205,11,23,22,131,222,26,143,143,143,155,255,115,220,61,253,247,63,207,157,107,174,155,223,24,251,144,0,121,116,140,0,137,0,211,121,149,8,145,18,178,44,147,217,25,107,131,217,97,156,166,206,32,249,175,243,
230,17,32,113,144,238,235,85,116,99,233,16,34,37,216,199,162,72,247,214,210,96,118,26,167,169,43,72,16,141,43,83,125,95,122,21,140,137,148,96,239,76,6,131,217,97,76,107,212,118,57,169,199,72,230,207,47,42,13,36,225,38,2,164,28,66,164,36,187,224,232,250,
160,118,26,167,33,72,48,141,199,89,15,82,30,221,89,21,101,89,182,67,218,150,32,119,30,147,232,218,130,197,64,122,69,84,34,21,229,121,46,211,126,119,7,185,243,152,68,69,2,59,144,78,55,117,69,132,72,123,54,176,162,61,124,101,131,68,102,97,33,26,69,128,
48,19,171,58,66,164,13,246,194,99,69,123,4,180,65,210,92,7,130,104,108,228,193,138,237,33,68,218,100,87,180,15,18,36,225,115,5,137,252,251,175,143,142,178,14,36,30,215,179,34,189,125,132,72,7,216,59,25,30,141,18,129,153,130,68,254,127,249,247,188,104,
42,26,242,134,194,29,169,31,132,78,96,118,86,7,101,89,38,65,242,147,104,190,80,194,166,206,218,34,64,162,243,184,157,24,131,14,32,68,58,140,32,137,135,4,201,127,153,63,223,252,175,241,113,2,36,30,4,72,135,17,34,53,200,178,76,222,67,178,37,186,47,6,132,
141,0,169,1,99,34,53,176,47,179,122,60,186,47,6,132,75,166,226,111,226,252,117,30,33,82,19,123,199,67,144,0,189,183,151,181,32,245,33,68,106,68,144,0,61,71,128,212,140,16,169,25,65,2,244,12,1,210,5,132,72,23,16,36,64,215,17,32,93,66,136,116,137,13,146,
239,36,241,101,129,222,122,156,0,233,30,166,248,118,25,235,72,128,90,49,141,183,203,168,68,186,204,62,106,129,151,90,1,157,71,128,244,0,33,210,3,54,72,254,140,135,54,2,29,115,19,1,210,27,116,103,245,80,150,101,242,38,53,121,159,243,210,100,15,2,208,190,
235,121,152,98,239,80,137,244,144,125,250,239,32,47,182,2,42,145,74,254,74,2,164,183,168,68,60,144,101,217,50,99,140,188,215,96,125,234,199,2,80,218,47,47,132,227,133,82,189,71,136,120,36,203,50,185,163,186,46,245,227,0,56,176,6,196,35,132,136,103,152,
2,12,180,196,12,44,207,16,34,30,202,178,108,208,118,111,49,224,14,252,30,3,232,30,34,68,60,101,199,73,100,230,214,218,212,143,5,146,55,108,187,175,24,255,240,16,179,179,60,37,253,189,121,158,175,227,81,41,72,220,110,99,204,42,2,196,95,84,34,1,200,178,
108,131,49,102,7,221,91,72,204,54,251,130,55,120,140,16,9,68,150,101,171,236,56,9,221,91,136,221,176,157,190,187,139,51,237,63,186,179,2,145,231,249,144,237,222,218,150,250,177,64,212,118,218,238,43,2,36,16,84,34,1,178,179,183,164,123,107,101,234,199,
2,209,144,234,99,107,158,231,219,57,165,97,33,68,2,101,103,111,109,103,113,34,34,32,131,231,27,165,218,230,100,134,135,16,9,28,131,238,8,24,213,71,4,24,19,9,92,158,231,50,216,190,138,169,192,8,140,84,31,235,8,144,240,81,137,68,132,177,18,4,128,234,35,
50,132,72,132,178,44,147,185,245,91,82,63,14,240,206,78,59,246,193,131,19,35,66,136,68,202,174,43,217,193,227,229,225,129,253,54,60,152,182,27,33,66,36,114,118,224,125,59,93,92,232,1,186,174,18,64,136,36,194,118,113,109,98,22,23,186,228,113,185,222,232,
186,138,31,33,146,16,214,150,160,11,88,243,145,24,66,36,65,118,188,100,43,97,130,14,218,109,187,174,24,247,72,12,33,146,176,44,203,214,217,202,132,193,119,84,181,215,118,91,17,30,137,34,68,208,92,95,178,149,48,65,9,251,109,229,193,155,6,19,71,136,96,
146,173,76,54,209,205,133,22,232,182,194,123,16,34,56,13,99,38,152,6,225,129,105,17,34,152,145,157,205,181,137,169,193,73,123,220,134,7,179,173,48,45,66,4,42,89,150,109,180,97,194,155,21,227,183,223,62,237,96,59,235,60,224,66,136,160,148,41,227,38,27,
168,78,162,35,207,182,218,97,159,12,13,168,16,34,168,196,118,117,109,160,58,9,94,179,234,216,65,151,21,170,32,68,208,54,59,16,223,172,78,120,70,151,255,228,153,86,79,216,224,96,160,28,109,33,68,208,81,182,187,107,35,129,226,157,102,112,60,65,119,21,58,
137,16,65,109,8,148,158,35,56,80,59,66,4,93,97,3,101,208,134,10,99,40,245,217,63,37,56,232,170,66,237,8,17,116,157,29,148,31,180,21,202,32,85,74,91,164,218,216,101,131,99,23,131,227,232,54,66,4,61,103,7,230,7,167,252,33,84,102,214,12,141,93,54,52,246,
248,186,163,72,3,33,2,239,76,169,84,214,77,249,103,170,107,82,228,113,35,123,236,31,42,13,120,135,16,65,16,108,181,178,110,202,159,85,145,141,173,200,88,198,144,173,48,228,159,123,168,50,16,2,66,4,65,179,3,246,205,202,101,153,13,153,101,158,6,76,51,40,
154,127,36,36,14,51,0,142,144,17,34,136,154,125,87,138,153,18,48,77,131,211,124,239,50,239,83,217,61,205,191,43,66,193,254,239,102,80,24,91,85,240,12,42,68,137,16,1,0,84,54,155,67,7,0,168,138,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,
0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,50,66,4,
0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,50,66,4,0,80,25,33,2,0,168,140,16,1,0,84,70,136,0,0,42,35,68,0,0,149,17,34,0,128,202,8,17,0,64,101,132,8,0,160,26,99,204,255,7,103,12,121,239,57,227,
221,157,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_Logo_Final_Version_1_0_App_Icon_Black_circle_png = (const char*) temp_binary_data_91;

//================== Volume Logo Final Version 1.0_App Icon Black rounded.png ==================
static const unsigned char temp_binary_data_92[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,1,145,0,0,1,144,8,6,0,0,0,111,125,93,242,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,32,0,73,68,65,84,120,156,237,221,89,144,28,197,157,199,241,28,205,32,230,150,4,20,2,140,78,48,135,
177,65,198,187,222,136,5,27,25,3,126,52,177,47,187,251,176,97,225,131,181,141,13,2,33,78,11,4,152,67,167,101,48,62,240,37,197,70,236,110,248,97,87,251,104,78,97,27,188,71,216,192,174,205,33,132,24,137,75,80,8,29,115,107,52,234,141,127,118,181,24,196,
76,231,191,178,187,213,149,85,223,79,196,60,16,162,107,178,179,106,242,215,255,204,202,234,150,82,169,100,0,0,240,49,141,94,3,0,248,34,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,
222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,
1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,183,54,186,46,157,40,138,230,27,99,228,103,177,49,102,166,49,102,209,132,3,92,20,194,123,0,96,158,51,198,236,
77,186,161,111,194,207,179,113,28,63,75,247,232,181,148,74,165,80,218,218,20,81,20,45,74,2,163,242,51,163,128,221,0,20,205,147,198,152,45,242,19,199,241,22,206,254,212,8,145,73,36,193,177,196,24,115,185,49,102,94,230,26,8,224,104,218,103,140,217,44,63,
113,28,111,166,231,63,136,16,153,32,138,34,9,142,165,198,152,243,50,211,40,0,89,178,195,24,179,209,24,179,33,142,227,202,116,88,161,21,62,68,162,40,154,153,4,199,18,170,14,0,41,108,50,198,172,140,227,184,175,200,157,86,232,16,73,42,143,13,172,115,0,168,
193,29,69,174,76,10,25,34,81,20,45,78,194,131,105,43,0,245,32,235,38,75,227,56,222,88,180,222,44,84,136,36,83,87,43,141,49,215,100,160,57,0,242,71,238,234,90,82,164,41,174,194,132,72,114,199,213,70,170,15,0,13,182,47,9,146,66,220,201,85,136,29,235,201,
218,199,22,2,4,192,81,32,107,172,255,30,69,209,134,34,116,118,238,43,145,228,68,50,125,5,160,25,100,122,235,242,60,47,186,231,58,68,162,40,146,233,171,47,101,160,41,0,138,75,30,177,178,56,175,65,146,219,233,44,2,4,64,70,200,52,250,150,228,198,158,220,
201,101,136,16,32,0,50,38,183,65,146,187,16,33,64,0,100,84,46,131,36,87,33,18,69,209,74,2,4,64,134,157,151,220,41,154,27,185,9,145,40,138,228,137,187,183,103,160,41,0,80,205,121,201,140,73,46,228,226,238,172,100,35,225,22,158,129,5,32,32,87,228,225,49,
41,121,169,68,54,18,32,0,2,179,33,249,166,212,160,5,31,34,201,58,8,59,209,1,132,102,70,242,1,56,104,65,79,103,37,211,88,207,100,160,41,0,224,235,218,56,142,131,125,68,74,232,33,34,235,32,23,101,160,41,0,224,75,30,216,56,63,212,29,237,193,78,103,37,15,
85,36,64,0,132,110,70,242,253,70,65,10,182,18,137,162,168,143,175,179,5,144,35,11,66,252,30,146,32,43,145,164,10,33,64,0,228,201,202,16,223,75,144,149,8,85,8,128,156,10,174,26,9,174,18,73,118,166,19,32,0,242,40,184,106,36,196,233,172,37,25,104,3,0,52,
194,229,161,61,160,49,168,16,73,118,119,126,49,3,77,1,128,70,144,59,181,46,15,169,103,67,171,68,130,234,92,0,240,64,136,52,16,83,89,0,242,238,139,33,77,105,5,19,34,73,167,242,140,44,0,69,16,76,53,18,82,37,194,84,22,128,162,88,28,202,251,12,41,68,130,
233,84,0,168,17,33,210,0,139,2,106,43,0,212,98,94,40,223,53,18,82,136,176,30,2,160,72,130,248,224,28,68,136,68,81,196,84,22,128,162,33,68,234,40,248,175,144,4,128,148,152,206,170,35,66,4,64,209,16,34,117,20,212,179,100,0,160,14,8,145,58,226,206,44,0,
69,19,196,211,202,131,253,122,92,0,64,243,17,34,0,0,111,132,8,0,192,27,33,2,0,240,70,136,0,0,188,17,34,0,0,111,132,8,0,192,27,33,2,0,240,70,136,0,0,188,17,34,0,0,111,132,8,0,192,27,33,2,0,240,70,136,0,0,188,181,209,117,225,185,96,97,187,217,124,229,236,
41,219,189,230,209,125,102,245,163,123,139,222,77,200,24,174,219,124,162,18,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,
0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,218,232,186,98,153,51,171,205,252,253,167,186,167,124,207,59,247,28,52,255,250,135,129,162,119,19,166,112,193,194,118,251,51,149,167,182,
143,216,31,20,7,33,82,48,115,103,181,153,229,151,204,152,242,77,63,189,125,132,16,193,148,36,64,170,93,63,230,81,67,136,20,12,33,18,168,82,105,234,118,87,249,167,242,191,215,240,90,20,91,137,107,15,71,96,77,4,0,224,141,74,36,64,174,79,131,213,62,210,
57,63,73,242,113,16,213,148,26,119,237,81,138,132,137,16,9,81,169,134,63,56,215,107,249,67,134,75,163,174,61,4,137,16,9,84,169,202,71,186,106,255,230,252,119,254,200,81,133,92,59,141,186,246,92,175,69,54,177,38,2,0,240,70,37,18,168,90,62,180,81,136,160,
22,141,186,246,16,38,66,36,84,181,172,107,176,38,2,95,181,174,169,113,237,229,14,211,89,0,0,111,84,34,33,114,220,102,89,245,3,157,235,181,124,26,68,21,53,109,54,172,229,186,69,102,17,34,161,98,74,1,205,192,116,22,142,64,136,4,200,185,105,171,138,90,31,
91,1,52,234,218,67,152,88,19,1,0,120,163,18,9,146,107,195,87,181,247,84,253,181,124,84,68,53,165,146,107,195,96,213,87,215,240,90,100,21,33,18,34,30,123,130,102,226,177,39,152,128,16,9,84,45,15,178,163,16,129,183,26,30,192,104,120,0,99,46,177,38,2,0,
240,70,37,18,40,190,24,8,205,192,151,82,225,72,132,72,168,184,87,31,205,192,62,17,28,129,16,9,80,201,53,47,93,133,235,181,172,137,192,165,81,215,30,194,196,154,8,0,192,27,149,72,168,152,82,64,51,48,157,133,35,16,34,129,170,186,105,203,241,215,88,253,
27,228,170,191,182,239,198,83,167,252,183,255,122,109,212,252,237,63,199,65,247,107,17,44,189,176,215,44,189,160,119,202,119,250,119,255,18,155,255,220,57,58,233,191,149,92,27,6,107,184,246,92,175,69,54,49,157,5,0,240,70,37,18,168,102,109,54,100,81,62,
7,106,89,224,102,179,33,142,64,37,2,0,240,70,37,18,162,38,126,41,21,149,72,248,156,143,100,119,92,91,124,41,21,38,34,68,66,213,172,59,100,152,142,8,95,51,31,224,201,245,147,59,132,72,128,154,249,165,84,124,146,204,135,70,93,3,85,95,71,181,154,75,172,
137,0,0,188,81,137,4,170,81,235,26,220,93,147,127,205,124,108,14,107,106,249,67,136,132,168,161,243,89,140,2,133,192,124,22,234,132,16,9,84,45,127,138,172,139,162,81,215,79,35,95,139,108,34,68,66,197,221,89,240,197,221,89,168,35,66,36,80,205,186,195,
138,187,179,194,199,221,125,168,39,66,36,84,84,34,240,69,37,130,58,34,68,2,196,24,128,90,53,234,26,112,189,142,107,36,127,8,145,16,145,34,168,21,41,130,58,33,68,2,197,62,17,248,98,159,8,234,137,16,9,21,247,104,162,22,92,63,168,19,66,36,80,236,19,65,45,
200,16,212,11,33,18,170,134,205,41,56,250,131,123,52,195,199,124,22,234,136,16,9,17,223,39,130,26,56,247,137,56,174,45,190,79,4,19,17,34,161,98,159,8,124,113,119,31,234,136,71,193,3,0,188,81,137,4,40,212,199,86,92,245,23,61,83,254,219,27,253,227,102,
243,75,67,142,35,160,226,47,79,57,214,124,250,148,233,83,246,135,244,165,244,233,84,120,136,47,234,133,16,9,85,128,211,89,223,60,127,234,16,249,159,183,14,16,34,41,124,250,228,233,85,251,243,191,223,60,48,117,136,48,157,133,58,34,68,2,213,140,13,199,
205,124,45,62,124,30,154,177,113,156,12,193,145,8,145,16,133,58,10,48,130,212,87,22,207,163,235,117,156,231,220,33,68,2,21,226,102,67,50,164,190,66,172,40,57,207,249,67,136,4,41,208,141,34,108,50,169,159,70,110,246,96,163,8,82,32,68,2,21,226,151,82,49,
126,212,79,35,239,208,171,229,181,92,3,197,67,136,132,42,196,57,41,230,179,234,135,149,117,100,4,33,18,32,214,213,209,204,115,193,186,58,38,34,68,66,68,138,160,153,231,130,20,193,4,132,72,160,66,252,82,42,190,208,170,126,26,249,32,222,90,94,203,189,21,
197,67,136,132,138,251,59,193,53,128,12,32,68,2,197,62,17,144,33,200,2,66,36,84,220,157,85,108,220,157,133,140,32,68,66,100,231,165,167,254,139,43,57,54,139,85,125,173,99,98,186,89,175,197,145,167,177,84,189,207,170,254,83,227,94,91,211,181,71,138,4,
137,239,19,1,0,120,163,18,9,144,115,199,177,227,3,42,79,61,201,129,70,62,249,166,134,215,54,234,105,43,200,46,66,36,84,172,170,130,107,0,25,64,136,4,138,125,34,197,198,62,17,100,5,107,34,0,0,111,84,34,33,170,225,147,164,115,62,156,53,145,96,100,178,162,
172,250,66,206,115,30,17,34,161,98,159,72,177,177,79,4,25,65,136,4,138,239,19,41,54,231,157,78,213,206,67,3,95,203,53,80,60,132,72,176,120,240,9,184,61,11,205,71,136,4,136,125,34,96,159,8,178,130,16,9,81,168,115,218,12,32,245,149,197,243,232,122,29,231,
57,119,184,197,23,0,224,141,74,36,80,108,54,44,54,54,27,34,43,8,145,80,177,174,14,214,213,145,1,132,72,160,200,16,144,33,200,2,66,36,84,205,88,24,109,230,107,241,225,243,192,102,67,100,0,33,18,162,70,222,222,201,45,190,65,104,228,109,222,53,221,34,94,
173,243,106,185,110,145,89,220,157,5,0,240,70,37,18,160,242,167,65,191,143,245,174,215,186,171,137,230,188,22,31,238,176,234,95,53,91,245,36,54,236,181,181,92,123,92,4,97,34,68,66,197,202,58,88,89,71,6,16,34,129,106,216,62,17,135,102,189,22,31,62,135,
236,19,65,22,176,38,2,0,240,70,37,18,34,158,157,133,102,158,11,158,157,133,9,8,145,64,53,234,241,35,220,226,27,136,6,126,187,101,77,175,173,101,42,149,107,32,72,76,103,1,0,188,81,137,4,170,97,223,46,71,37,18,4,231,134,193,42,26,249,218,154,174,61,159,
55,131,166,35,68,66,197,99,79,138,141,199,158,32,35,8,145,0,245,143,29,50,207,236,62,48,101,195,119,13,141,79,249,111,174,215,110,219,127,176,106,135,52,235,181,248,32,57,199,213,250,83,206,243,84,26,249,218,90,174,189,106,175,69,118,181,84,221,65,154,
17,81,20,109,49,198,92,196,117,4,160,72,226,56,110,201,250,219,101,97,29,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,99,159,72,128,78,234,104,53,15,253,213,44,211,213,230,190,251,111,240,96,201,92,247,135,189,102,91,191,
123,31,198,85,103,116,155,191,153,219,161,234,144,213,207,247,155,95,191,57,226,252,255,46,136,142,53,119,158,215,171,58,230,43,253,7,109,91,7,14,178,235,172,154,27,207,233,49,151,157,220,174,250,127,255,109,231,176,121,112,235,128,243,255,59,189,167,
205,172,255,212,76,213,53,165,61,79,221,109,45,246,152,167,245,232,134,153,219,158,219,111,158,138,71,85,255,47,178,131,74,36,64,187,134,199,237,31,241,160,108,10,171,124,193,195,20,63,93,173,198,172,63,127,134,29,36,92,100,176,121,88,130,193,113,76,
249,185,225,236,110,243,5,197,64,38,131,194,234,63,247,171,142,121,90,119,171,89,255,169,25,118,240,193,228,110,252,88,143,185,236,164,99,85,253,41,231,82,29,32,231,207,176,215,138,235,152,175,244,143,165,8,144,25,246,156,106,218,42,215,8,1,18,38,66,
36,80,82,89,92,251,135,125,102,96,172,228,252,27,237,108,109,49,235,62,41,127,208,238,32,89,101,43,140,81,205,223,189,89,174,12,146,95,191,53,98,86,63,63,160,58,230,194,174,54,179,238,124,130,100,50,55,156,221,99,46,61,233,88,85,63,202,57,148,115,233,
34,215,132,92,27,114,141,184,142,185,205,86,32,251,84,1,34,231,80,206,165,166,173,114,109,200,53,130,48,17,34,1,123,101,224,160,185,238,143,251,204,224,88,233,253,103,26,77,241,211,213,218,98,214,43,131,100,245,11,253,230,225,183,70,157,199,148,159,229,
103,117,155,47,156,164,11,146,53,207,15,168,142,121,90,87,121,96,35,72,222,39,1,82,174,64,220,253,39,231,78,206,161,139,92,11,114,77,200,181,225,58,166,76,97,45,251,163,50,64,228,58,235,106,83,181,117,13,1,18,60,66,36,112,54,72,158,41,255,113,187,254,
102,59,219,244,21,137,12,66,191,222,53,170,25,7,204,245,103,119,155,203,52,65,178,107,196,172,126,97,64,117,204,133,221,4,73,197,242,74,5,162,232,55,57,103,218,0,177,21,72,91,139,243,152,114,141,45,123,70,31,32,114,238,52,109,149,107,65,174,9,132,141,
16,201,129,202,31,185,182,34,89,183,72,23,36,107,82,86,36,154,32,121,120,215,136,89,243,130,174,34,177,83,91,139,138,29,36,203,207,234,49,151,205,214,87,32,107,180,1,178,72,87,129,108,239,79,17,32,139,202,83,88,170,10,228,133,1,123,45,32,124,132,72,78,
216,32,121,118,159,189,27,203,165,171,45,69,144,188,216,111,30,222,165,91,240,76,21,36,47,186,23,124,77,165,34,41,104,144,216,0,145,41,44,5,57,71,114,174,92,14,7,136,162,63,183,39,215,148,58,64,20,215,147,177,215,20,1,146,39,132,72,142,84,130,68,187,
216,190,246,188,116,65,162,89,36,189,254,204,116,65,162,57,230,130,174,54,219,214,34,5,137,4,200,165,179,117,139,232,105,2,68,250,81,179,136,110,215,64,148,1,34,199,92,160,92,68,39,64,242,135,16,201,25,9,146,235,159,211,79,109,173,61,87,31,36,143,236,
210,77,109,93,127,70,183,185,108,182,46,72,214,190,168,159,218,42,74,144,44,63,179,199,92,122,162,110,10,235,145,52,1,114,174,114,10,43,185,134,180,1,162,157,194,90,75,128,228,18,33,146,67,149,32,209,86,36,107,62,145,220,77,227,176,230,37,125,69,178,
76,27,36,111,143,152,181,47,41,43,146,206,54,179,230,220,124,7,201,245,103,244,152,75,78,76,81,129,188,164,8,144,174,54,123,142,85,21,72,138,0,145,115,33,231,68,211,86,57,199,114,174,145,63,132,72,78,189,50,120,208,44,255,95,89,35,57,228,252,136,40,249,
177,230,220,94,85,144,172,221,218,111,30,177,131,129,251,163,231,178,51,186,202,139,194,14,50,184,172,219,218,175,58,230,194,174,86,219,214,60,6,137,84,112,151,206,158,174,234,7,57,7,107,183,42,3,228,220,94,83,62,181,213,143,185,125,112,204,94,51,186,
0,233,181,231,66,211,86,57,183,4,72,126,17,34,57,86,14,146,253,234,138,100,245,39,180,65,34,159,42,117,21,201,117,31,237,182,115,251,46,114,60,57,174,182,34,145,182,230,41,72,164,114,83,87,32,73,95,185,200,185,148,126,210,86,32,246,90,81,4,136,28,83,
93,129,36,215,10,242,139,16,201,57,9,146,27,254,111,127,249,174,45,197,26,137,54,72,214,109,29,48,143,188,173,91,35,89,118,122,119,121,142,223,65,142,39,199,85,173,145,72,144,124,60,31,65,178,76,130,54,82,174,129,84,250,200,161,18,32,218,53,16,185,70,
84,1,242,241,94,219,247,154,182,30,190,70,144,107,132,72,1,216,138,68,6,137,113,197,134,196,214,22,179,234,19,189,229,197,82,135,117,47,15,152,71,222,209,109,72,180,21,137,38,72,222,25,181,199,213,28,83,238,8,90,245,241,94,213,237,170,89,37,1,98,43,16,
197,251,173,244,141,139,156,187,85,149,10,196,113,76,123,109,252,201,29,32,210,199,210,215,246,46,44,69,91,43,215,6,242,143,16,41,136,237,105,42,146,105,201,39,78,101,144,60,170,172,72,174,211,86,36,239,140,154,245,47,167,168,72,206,9,51,72,164,66,187,
68,89,129,72,31,107,3,68,206,157,156,67,85,5,242,167,253,206,189,69,210,183,210,199,218,10,100,61,1,82,40,132,72,129,216,32,73,62,117,58,215,72,166,181,152,85,231,40,131,100,91,82,145,40,230,200,175,61,45,153,186,113,168,4,137,118,141,100,213,199,194,
10,18,9,212,207,71,186,53,16,91,129,108,83,86,32,231,244,218,115,167,89,3,185,225,207,186,0,145,190,213,174,129,16,32,197,67,136,20,140,4,201,141,127,214,87,36,50,128,104,130,100,253,182,1,243,232,59,186,138,68,29,36,241,168,249,222,54,125,69,18,74,144,
92,119,90,183,185,228,4,101,5,34,97,170,13,144,143,41,43,16,185,6,158,215,7,136,182,2,145,115,245,8,143,115,47,28,66,164,128,42,131,136,118,141,228,62,109,144,188,50,96,30,141,117,107,36,75,79,75,166,114,28,100,80,146,227,106,142,25,66,69,34,1,98,43,
16,197,251,121,52,121,239,46,114,110,228,28,105,214,64,182,15,165,11,16,91,129,40,218,42,237,36,64,138,137,16,41,40,9,146,155,82,84,36,247,157,157,124,34,117,144,193,228,177,88,89,145,44,212,5,137,12,166,223,123,69,87,145,44,232,104,51,171,206,78,238,
74,202,24,27,32,202,10,228,49,109,128,116,182,217,115,163,169,64,94,157,88,133,86,33,125,39,125,40,125,169,170,64,146,15,15,40,38,66,164,192,228,83,233,77,207,203,230,178,67,166,84,42,85,253,233,156,102,204,189,103,247,152,133,157,173,206,14,43,87,36,
35,206,99,202,207,210,5,93,229,169,29,135,74,144,104,142,57,191,163,181,60,176,102,40,72,174,93,216,101,46,62,126,186,170,253,210,119,186,0,105,181,231,68,206,141,235,152,135,167,176,198,221,1,34,125,39,125,168,105,43,1,2,66,164,224,182,15,141,155,155,
95,208,87,36,247,158,213,171,10,146,239,109,31,84,87,36,234,32,121,119,212,108,216,62,168,172,72,90,205,125,103,101,35,72,174,93,208,101,62,127,188,190,2,145,190,115,177,1,114,150,182,2,25,55,55,189,160,12,144,179,122,109,223,105,218,42,231,66,206,9,
138,141,16,65,57,72,94,212,175,145,220,163,13,146,87,203,131,140,102,78,253,26,25,104,149,65,34,199,213,28,115,126,101,160,109,98,144,72,64,94,124,130,114,13,36,121,111,46,210,247,114,14,116,107,32,227,230,166,23,117,1,34,125,37,125,166,105,107,229,220,
2,132,8,44,25,108,110,121,81,95,145,220,115,166,46,72,54,188,58,104,30,123,87,89,145,204,215,5,137,28,79,142,171,173,72,238,61,179,57,65,178,52,77,5,82,121,79,14,54,64,206,212,87,32,55,107,3,228,204,20,21,72,229,156,162,240,12,33,130,137,108,144,188,
84,14,18,205,62,146,123,206,72,23,36,154,125,6,215,204,75,6,94,135,202,160,171,57,166,204,239,223,115,148,131,68,2,241,226,227,116,251,64,82,5,200,25,186,125,32,182,186,124,73,23,32,210,55,229,53,16,119,91,9,16,28,137,16,193,7,216,32,217,186,223,12,57,
6,31,83,153,218,210,6,73,223,160,121,124,183,110,240,185,102,190,50,72,118,143,154,239,247,185,7,95,177,224,40,6,137,13,16,69,251,133,244,201,6,197,123,56,28,32,138,246,191,58,156,124,24,80,6,136,173,64,20,164,175,31,83,158,67,20,7,33,130,15,57,252,41,
86,81,145,116,76,107,49,119,127,84,55,16,201,96,169,173,72,174,214,86,36,73,144,168,42,146,246,86,115,247,25,141,13,18,169,164,62,151,166,2,81,4,136,244,173,244,113,135,178,2,209,6,136,244,133,244,137,166,173,4,8,166,66,136,96,82,246,211,172,84,36,138,
53,18,59,181,165,12,146,239,239,72,42,18,197,220,251,213,115,187,204,231,143,211,5,201,253,125,202,53,146,246,242,128,220,136,32,145,0,145,41,44,77,59,164,15,164,47,92,108,5,245,209,242,20,150,115,13,100,104,220,220,186,85,25,32,114,190,218,117,107,32,
247,19,32,168,130,16,193,148,108,144,188,156,162,34,57,93,31,36,50,40,105,62,1,127,123,110,50,48,59,60,246,94,121,80,86,87,36,167,215,55,72,36,240,62,55,75,89,129,164,8,16,105,167,166,2,177,1,242,178,50,64,78,79,81,129,200,185,122,143,0,193,212,8,17,
84,37,65,114,235,54,253,26,137,54,72,238,223,57,104,30,87,14,78,87,43,131,68,142,39,199,213,152,223,81,191,32,209,182,47,77,27,43,1,162,89,3,233,75,206,145,58,64,148,107,32,105,206,17,138,139,16,129,83,37,72,6,21,59,219,59,166,25,243,221,211,122,244,
65,98,43,18,247,206,232,111,207,233,52,23,31,55,221,121,204,242,32,173,219,217,62,175,125,154,109,107,45,65,114,245,28,169,64,116,59,209,229,189,106,3,68,218,213,161,216,137,254,234,208,65,117,128,200,49,229,61,107,218,42,125,72,128,64,131,16,129,138,
4,201,119,94,233,87,87,36,234,32,121,109,208,60,241,222,1,85,27,190,61,167,75,25,36,7,204,3,175,233,43,18,223,32,177,1,162,104,143,144,247,120,191,162,77,149,0,209,86,32,114,78,180,1,162,173,64,164,239,30,87,158,19,128,16,129,90,37,72,180,107,36,119,
45,212,7,137,12,90,154,57,250,111,157,218,101,46,158,165,12,146,157,186,53,146,121,237,173,230,174,148,65,34,129,182,216,86,32,238,227,63,158,34,64,164,207,84,107,32,41,2,228,46,91,129,232,214,64,30,216,73,128,32,29,66,4,169,200,224,181,66,42,18,229,
93,91,119,45,232,41,223,5,228,240,64,165,34,81,220,45,164,14,146,61,7,204,15,94,211,221,181,53,255,216,242,0,174,9,18,249,127,180,119,54,61,161,172,138,228,120,210,87,154,187,176,250,146,115,160,10,144,133,61,246,189,105,218,42,125,37,125,6,164,65,136,
32,181,87,71,198,205,138,237,250,138,228,78,109,144,188,62,104,158,216,163,171,72,174,250,72,121,45,194,69,6,69,57,174,170,34,57,182,213,182,213,21,36,50,120,203,251,151,64,173,118,188,39,146,223,237,34,125,35,191,87,91,129,216,190,87,4,136,28,83,222,
147,170,2,121,157,0,129,31,66,4,94,36,72,110,123,85,191,70,146,38,72,182,236,213,13,102,223,82,6,137,12,230,63,120,67,185,70,210,174,15,18,121,255,125,35,227,147,254,187,188,135,52,1,162,90,3,73,250,92,27,32,243,21,253,45,164,111,158,32,64,224,137,16,
129,183,195,65,162,156,218,186,115,126,138,32,217,163,156,218,58,165,203,124,110,166,50,72,94,215,79,109,73,91,213,65,50,60,254,129,215,111,73,83,129,204,215,79,97,169,3,100,126,138,41,172,215,9,16,212,134,16,65,77,236,212,86,95,121,112,211,76,109,221,
49,95,247,9,249,129,55,82,76,109,105,131,100,111,185,34,209,78,109,221,49,79,25,36,125,239,79,109,217,41,44,69,213,35,125,32,125,161,157,194,186,173,79,23,32,210,102,237,20,150,173,64,148,85,31,48,21,66,4,53,179,211,44,125,253,102,232,144,98,106,43,169,
72,52,65,242,131,55,245,83,91,105,130,228,193,55,245,83,91,218,32,185,125,71,191,249,85,60,98,219,172,57,238,225,10,196,65,250,86,142,173,13,16,237,20,150,244,1,1,130,122,104,145,141,69,89,23,69,209,22,99,204,69,156,241,108,43,15,186,221,170,193,81,2,
231,246,29,3,83,174,41,76,116,213,41,157,102,241,12,221,126,140,7,223,28,50,91,246,185,7,71,57,158,28,87,67,218,184,114,231,128,115,32,215,72,211,71,218,223,43,1,178,114,110,119,138,0,209,245,17,154,47,142,227,236,124,199,243,20,168,68,80,55,229,79,205,
3,229,197,118,215,26,73,75,139,185,67,57,240,217,65,111,175,110,141,228,170,147,117,129,35,131,168,28,87,187,70,34,131,116,173,143,72,177,1,50,183,219,190,119,231,26,72,218,0,81,174,129,16,32,168,55,66,4,117,85,9,18,213,26,73,75,139,89,57,39,25,0,29,
30,124,171,28,36,154,185,254,111,158,148,34,72,222,26,210,173,145,76,111,181,109,237,82,84,16,147,177,65,52,167,219,190,103,215,239,178,1,178,67,17,32,211,202,253,39,109,211,188,7,219,135,4,8,234,140,16,65,221,245,141,150,63,69,107,215,72,14,127,146,
118,120,112,215,144,121,82,57,8,166,9,146,31,238,26,82,29,115,94,165,34,73,25,36,149,74,70,51,133,181,35,233,187,65,71,223,117,37,253,54,79,209,111,66,222,35,1,130,70,32,68,208,16,149,32,209,86,36,183,107,43,146,100,48,212,124,242,254,198,236,78,179,
184,87,25,36,202,138,100,238,244,86,219,86,109,144,200,123,186,61,77,5,162,12,16,57,230,92,101,5,242,67,42,16,52,16,33,130,134,145,32,185,227,53,253,26,201,237,167,234,130,228,135,149,138,68,177,6,160,14,146,253,7,204,143,118,233,214,72,100,250,72,218,
170,9,146,120,236,144,137,15,28,114,30,115,199,72,185,175,84,1,114,106,121,10,75,211,86,121,79,242,222,128,70,33,68,208,80,54,72,94,215,87,36,183,125,68,25,36,111,151,7,71,205,39,241,175,159,216,105,46,82,6,137,28,87,91,145,72,91,93,65,34,161,32,239,
95,170,140,106,21,136,237,35,69,128,200,239,84,87,32,111,19,32,104,60,66,4,13,39,65,114,231,27,250,53,18,109,144,252,232,237,33,243,100,191,110,144,252,134,50,72,158,148,138,228,29,253,26,137,54,72,228,253,203,122,199,145,118,36,125,163,13,16,237,26,
136,188,135,39,9,16,28,5,132,8,142,10,91,145,188,161,175,72,86,156,162,27,48,109,144,104,43,146,168,211,92,212,163,12,146,20,21,201,138,143,184,23,205,109,69,242,198,128,237,135,195,21,72,165,79,28,1,34,199,94,145,162,2,169,244,9,112,52,16,34,56,106,
42,159,186,135,198,15,57,71,194,206,22,99,110,59,185,171,60,247,239,32,159,186,127,179,127,212,61,186,150,74,230,235,81,135,46,72,250,15,152,31,75,69,162,56,230,188,99,166,153,219,78,233,114,6,201,208,225,138,228,160,253,209,84,103,182,50,59,165,203,
254,14,77,91,164,205,218,234,12,168,7,118,172,227,168,147,96,88,113,178,123,208,53,201,192,123,215,91,131,102,199,1,247,206,118,9,136,207,118,235,118,182,255,56,30,54,191,25,112,15,182,114,60,57,174,134,180,81,218,170,9,134,202,123,115,253,127,43,148,
65,154,230,61,33,28,236,88,7,38,161,29,108,77,202,129,52,205,32,170,13,28,57,158,28,87,67,27,142,242,190,9,16,228,5,33,130,166,56,28,36,202,219,127,87,156,148,34,72,250,117,183,255,126,253,132,148,65,162,185,253,247,152,86,219,86,77,149,53,21,27,32,242,
126,143,209,221,198,75,128,160,153,8,17,52,141,13,146,93,131,234,197,246,239,204,86,6,201,187,229,32,209,44,66,255,227,241,41,130,228,221,97,221,98,187,4,201,108,191,32,177,1,50,187,203,30,67,243,187,236,123,37,64,208,68,132,8,154,74,130,228,187,111,
235,167,182,212,65,178,123,216,252,86,57,184,166,9,146,159,236,214,77,109,217,187,182,82,6,201,225,0,81,78,97,73,91,8,16,52,27,33,130,166,171,4,137,182,34,185,245,68,125,144,200,32,171,249,68,127,229,113,29,230,179,93,202,32,81,86,36,115,142,105,53,223,
57,81,31,36,255,48,171,221,190,70,115,236,159,80,129,32,35,8,17,100,130,4,201,221,239,232,43,18,109,144,200,167,245,223,14,234,6,219,43,143,87,6,201,224,1,243,80,138,138,68,27,36,255,180,103,196,236,84,220,133,38,191,251,55,202,247,4,52,26,33,130,204,
176,65,146,162,34,185,37,74,22,159,29,126,146,162,34,249,218,113,29,230,51,202,32,145,227,106,43,146,91,21,65,34,1,250,221,119,202,183,51,79,89,129,16,32,200,24,66,4,153,178,99,108,220,220,35,21,137,114,67,226,45,81,167,42,72,30,122,47,169,72,20,163,
254,149,179,218,205,103,186,142,113,30,83,142,247,208,123,186,13,137,115,219,166,153,91,163,78,85,144,72,69,182,243,192,193,15,29,67,126,151,182,170,2,142,22,66,4,153,35,65,114,119,60,164,174,72,110,62,161,211,222,205,228,82,14,146,49,93,69,50,179,195,
124,166,83,19,36,99,246,184,170,138,164,173,213,220,114,130,50,72,226,161,15,84,36,149,182,3,89,67,136,32,147,118,74,69,242,238,144,122,141,228,22,109,144,236,25,54,191,27,210,13,198,95,155,165,12,146,161,49,243,211,61,202,53,146,99,244,65,34,239,95,
250,65,142,253,91,101,155,129,163,141,199,158,32,211,236,160,123,124,167,233,80,44,76,15,203,192,187,187,60,240,186,72,165,113,161,34,32,196,207,246,234,6,113,9,156,175,206,212,61,34,69,218,120,239,110,93,72,162,184,120,236,9,80,35,91,145,236,30,178,
1,225,218,185,109,23,219,143,215,85,36,63,221,155,84,36,138,29,225,95,157,209,97,62,211,161,171,72,126,182,71,183,179,125,110,91,171,185,249,56,119,69,2,100,29,33,130,204,171,4,137,124,106,119,173,59,180,155,22,59,56,107,131,68,6,126,205,122,198,87,102,
116,152,11,53,65,50,60,102,143,171,93,35,33,72,16,58,66,4,65,176,211,63,239,13,153,97,197,244,171,93,108,87,6,201,207,246,13,155,223,13,235,214,27,190,170,12,18,57,158,28,87,131,32,65,232,8,17,4,99,226,58,130,166,34,185,105,86,167,157,54,114,177,65,162,
173,72,122,211,5,137,230,152,167,182,182,218,182,202,131,38,129,208,16,34,8,202,206,131,227,230,190,247,148,107,36,105,130,100,255,176,121,106,88,183,70,242,149,158,14,115,97,187,46,72,126,190,79,183,70,50,167,181,213,156,165,124,102,22,144,37,132,8,
130,99,131,100,207,160,25,46,29,114,142,206,29,45,198,220,52,171,195,110,246,115,177,65,50,114,192,61,226,155,146,249,74,111,187,46,72,70,198,204,207,247,15,59,143,39,255,207,31,71,15,114,49,34,56,132,8,130,180,243,224,33,115,223,30,253,212,214,141,51,
59,149,65,50,98,43,8,205,52,212,151,123,218,205,5,202,32,145,227,78,117,28,251,59,71,216,7,130,48,17,34,8,150,4,201,170,189,250,169,173,27,103,232,130,228,231,253,35,230,169,17,229,212,86,183,46,72,228,120,114,220,35,95,127,248,119,1,129,34,68,16,52,
27,36,251,244,21,201,13,189,233,130,68,85,145,116,181,155,11,142,213,7,73,229,117,4,8,242,128,16,65,240,36,72,86,239,215,223,254,171,14,146,129,17,243,244,168,110,144,255,114,183,50,72,70,199,204,47,6,70,236,207,83,202,99,3,89,70,136,32,23,210,86,36,
203,123,58,205,156,86,93,144,104,43,146,43,180,21,201,232,24,1,130,220,32,68,144,27,175,141,39,21,137,114,141,228,6,101,144,252,98,48,169,72,20,107,36,87,116,182,155,11,166,235,158,201,5,228,1,33,130,92,177,65,210,159,162,34,233,214,7,137,84,15,154,138,
100,73,103,187,249,107,130,4,5,65,136,32,119,36,72,214,12,232,215,72,180,65,242,203,161,17,243,244,1,221,52,212,21,4,9,10,130,16,65,46,85,130,68,91,145,92,223,149,34,72,180,21,73,7,65,130,252,35,68,144,91,18,36,107,7,101,141,196,253,85,187,242,45,32,
215,119,118,232,130,100,120,196,252,94,42,18,77,146,240,117,33,200,57,66,4,185,102,131,100,104,216,12,149,74,206,117,241,246,150,22,179,44,69,144,60,61,118,176,234,241,54,14,143,154,167,199,184,11,11,249,70,136,32,247,36,72,214,13,14,171,239,218,90,214,
209,97,230,76,115,255,105,108,180,21,201,193,73,143,179,137,0,65,65,16,34,40,132,215,14,29,50,235,164,34,81,174,145,92,167,13,146,145,164,34,153,240,250,141,35,4,8,138,131,16,65,97,72,144,172,31,30,86,223,181,165,13,146,77,35,35,230,247,7,203,79,224,
221,52,58,106,126,79,128,160,64,8,17,20,74,35,131,68,142,75,128,160,104,8,17,20,78,163,130,100,235,248,56,23,19,10,135,16,65,33,53,42,72,128,162,225,175,2,133,69,144,0,181,227,47,2,133,70,144,0,181,225,175,1,133,231,19,36,0,202,8,17,32,101,144,252,234,
192,1,186,12,72,16,34,64,66,19,36,236,3,1,62,136,16,1,38,168,22,36,4,8,240,97,132,8,112,132,201,130,132,0,1,38,71,136,0,147,152,24,36,4,8,48,181,54,250,6,152,156,4,201,181,131,131,244,14,80,5,149,8,0,192,27,33,2,0,240,70,136,0,0,188,17,34,0,0,111,132,
8,0,192,27,33,2,0,240,70,136,0,0,188,17,34,0,0,111,132,8,0,192,27,33,2,0,240,70,136,0,0,188,17,34,0,0,111,132,8,0,192,91,40,33,242,108,6,218,0,0,71,211,142,16,122,59,148,16,217,155,129,54,0,192,209,212,23,66,111,135,18,34,65,116,38,0,212,17,33,82,71,
132,8,128,162,33,68,234,37,142,227,45,33,180,19,0,234,40,136,181,224,144,238,206,122,46,3,109,0,128,163,133,16,169,51,238,208,2,80,20,59,226,56,102,58,171,206,152,210,2,80,20,193,140,119,132,8,0,100,15,33,82,111,73,105,199,186,8,128,34,216,28,202,123,
12,237,177,39,27,51,208,6,0,104,164,255,136,227,56,152,13,214,161,133,72,48,233,12,0,158,130,26,231,130,10,145,100,74,235,201,12,52,5,0,26,97,31,33,210,120,76,105,1,200,171,205,33,77,101,137,150,82,169,148,129,102,164,19,69,145,84,36,243,66,106,51,0,
40,44,8,101,127,72,69,168,223,39,178,50,3,109,0,128,122,218,20,90,128,152,80,43,17,67,53,2,32,127,130,171,66,76,224,223,108,72,53,2,32,47,130,172,66,76,200,149,136,41,87,35,178,171,243,162,12,52,5,0,124,201,29,89,139,66,13,145,208,191,99,125,105,6,218,
0,0,181,216,16,106,128,152,208,43,17,83,174,70,54,24,99,174,201,64,83,0,32,173,231,226,56,94,20,114,175,133,94,137,200,6,196,165,60,83,11,64,128,100,26,107,73,232,39,46,248,16,73,44,73,78,8,165,255,137,252,0,0,2,88,73,68,65,84,0,132,98,105,28,199,193,
127,79,82,46,66,36,57,17,172,143,0,8,133,220,141,149,139,167,111,4,191,38,50,17,235,35,0,2,16,252,58,200,68,121,153,206,178,146,245,145,77,25,104,10,0,76,70,214,111,23,231,169,103,114,85,137,84,68,81,36,79,193,252,98,54,90,3,0,86,208,251,65,166,146,171,
74,100,130,37,60,50,30,64,134,72,128,44,206,91,128,152,188,86,34,21,81,20,201,194,213,151,178,209,26,0,5,245,92,18,32,65,61,226,93,43,175,149,136,21,199,241,18,214,72,0,52,81,174,3,196,228,189,18,169,136,162,72,194,228,151,217,104,13,128,130,216,148,
124,144,205,181,92,87,34,21,201,253,216,159,52,198,236,200,70,139,0,228,152,172,127,92,81,132,0,49,69,9,17,243,254,134,68,185,55,251,251,25,104,14,128,124,122,50,185,3,171,48,95,227,93,136,233,172,35,69,81,180,56,249,174,118,190,212,10,64,61,72,245,177,
50,142,227,13,69,235,205,66,134,72,69,20,69,75,147,47,183,154,145,141,22,1,8,208,166,228,57,88,185,93,60,175,166,208,33,98,202,65,50,51,121,238,214,82,194,4,64,10,18,30,43,243,184,247,35,141,194,135,72,69,18,38,151,39,149,9,211,92,0,38,35,211,86,50,101,
181,177,232,225,81,65,136,76,34,138,162,69,201,174,247,203,9,20,160,240,36,56,228,81,74,155,227,56,222,92,244,206,56,18,33,226,144,4,202,229,201,67,211,248,62,119,160,24,100,147,224,22,249,33,56,170,35,68,82,74,66,101,126,114,187,240,252,228,199,36,255,
205,154,10,16,142,202,243,245,100,65,92,182,0,200,244,84,95,28,199,91,56,135,122,132,8,0,192,91,97,54,27,2,0,234,143,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,
8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,
0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,55,66,4,0,224,141,16,1,0,120,35,68,0,0,222,8,17,0,128,31,99,204,255,3,105,169,107,59,11,125,194,119,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_Logo_Final_Version_1_0_App_Icon_Black_rounded_png = (const char*) temp_binary_data_92;

//================== Volume Logo Final Version 1.0_App Icon Black square.png ==================
static const unsigned char temp_binary_data_93[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,1,145,0,0,1,145,8,6,0,0,0,164,33,142,87,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,30,203,73,68,65,84,120,156,237,221,89,144,92,213,125,199,241,51,210,236,43,35,49,72,8,107,23,72,216,
96,99,187,82,9,6,108,132,49,121,73,42,169,114,242,144,188,4,199,24,131,49,152,77,32,201,134,24,204,38,180,96,118,140,137,151,167,196,15,174,74,37,79,6,12,194,6,252,224,194,198,11,32,64,18,90,140,22,6,161,101,102,122,102,52,91,234,127,166,91,12,35,77,
159,255,253,247,180,250,158,190,223,15,197,147,230,158,190,247,220,219,231,215,255,115,238,237,174,25,27,27,115,0,0,88,204,160,215,0,0,86,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,
140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,
0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,172,54,150,174,235,234,234,186,56,5,187,1,0,39,203,171,221,221,221,135,210,222,219,209,132,
136,115,238,249,20,236,3,0,156,44,43,157,115,155,211,222,219,76,103,1,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,
0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,
33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,
0,152,213,210,117,241,57,103,94,189,187,251,239,58,167,220,239,255,122,165,207,253,247,43,189,89,239,38,164,12,215,109,117,34,68,34,212,209,56,195,125,110,73,227,148,59,254,210,246,193,172,119,17,82,136,235,182,58,49,157,5,0,48,35,68,0,0,102,132,8,0,
192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,179,90,186,46,91,
58,154,102,184,115,78,175,159,242,152,15,15,140,186,63,239,57,154,245,110,194,20,230,119,214,186,5,157,83,15,27,187,14,14,187,221,7,135,233,190,12,33,68,50,70,2,228,127,174,156,51,229,65,191,188,125,192,253,195,147,251,179,222,77,152,194,191,124,182,
213,173,186,180,99,202,127,95,255,236,97,119,255,179,135,232,190,12,97,58,11,0,96,70,37,18,169,177,177,169,247,187,200,63,141,255,123,9,219,34,219,198,184,246,48,9,33,18,171,98,239,184,224,59,185,132,109,145,109,99,92,123,248,40,66,36,66,242,105,174,
216,39,186,98,66,219,90,219,69,118,148,235,218,67,156,8,145,104,149,235,227,32,16,186,118,40,69,240,33,66,36,82,165,84,19,69,255,157,247,49,138,40,181,146,165,10,174,62,132,72,172,74,121,195,241,102,69,41,184,246,48,1,33,18,169,114,189,143,121,143,35,
132,12,193,68,132,72,172,152,150,70,37,176,36,130,73,8,145,24,133,230,165,139,29,18,119,103,161,4,37,61,39,82,202,117,139,212,34,68,98,197,167,65,84,2,149,8,38,33,68,34,20,122,31,23,83,234,24,0,148,235,218,67,156,8,145,88,177,186,137,74,225,218,195,4,
132,72,164,120,78,4,149,192,115,34,152,140,16,137,17,223,123,130,74,226,123,79,48,1,95,5,15,0,48,163,18,137,84,41,83,82,20,34,48,11,21,19,76,165,102,14,33,18,43,22,55,81,41,92,123,152,128,16,137,20,183,234,163,18,120,76,4,147,17,34,177,226,157,140,74,
32,69,48,9,33,18,33,110,206,66,37,113,115,22,38,34,68,98,197,167,65,84,2,149,8,38,33,68,34,85,169,223,53,252,217,191,158,54,229,191,189,246,222,81,119,231,179,135,74,104,29,39,195,63,157,219,226,254,249,220,150,41,95,233,142,95,30,116,175,239,31,58,225,
191,145,33,152,140,16,137,85,133,238,179,252,235,249,245,246,215,69,42,124,172,99,102,209,243,216,222,80,236,241,49,238,241,197,71,17,34,145,170,212,115,34,172,167,84,129,82,214,38,200,16,76,194,19,235,0,0,51,42,145,24,85,240,71,169,168,68,226,23,250,
97,169,98,23,16,63,74,133,201,8,145,88,85,106,117,147,233,136,248,85,242,7,105,184,126,170,14,33,18,33,198,0,148,170,18,119,247,241,163,84,213,137,16,137,85,165,190,191,136,81,160,58,112,253,96,154,16,34,145,170,212,143,82,113,119,77,252,42,249,141,7,
172,169,85,31,238,206,2,0,152,81,137,196,200,127,26,180,126,148,44,190,109,168,221,82,182,69,122,20,61,87,193,106,162,60,215,30,226,68,136,196,138,187,179,96,197,157,25,152,70,132,72,164,74,185,223,190,82,219,34,29,130,207,137,20,187,118,184,126,48,9,
107,34,0,0,51,42,145,8,149,245,137,99,238,174,169,126,101,252,198,131,82,174,61,74,145,56,17,34,49,98,78,27,165,226,105,67,76,19,66,36,82,60,39,2,43,158,19,193,116,98,77,4,0,96,70,37,18,169,82,62,181,113,119,13,202,117,253,148,115,91,164,19,33,18,171,
178,205,41,4,250,131,20,137,31,243,89,152,70,132,72,140,248,61,17,148,160,172,119,247,21,219,45,126,79,164,42,177,38,2,0,48,163,18,137,21,95,123,2,43,110,17,199,52,34,68,34,20,235,215,86,252,213,188,134,41,255,173,103,112,212,109,57,48,20,104,1,5,103,
180,205,116,243,218,166,126,251,110,121,127,200,245,28,29,157,242,223,203,117,13,20,221,142,41,207,170,68,136,224,164,249,201,223,207,158,242,165,126,187,247,168,187,252,127,223,231,100,40,253,227,242,102,247,141,207,182,77,249,199,151,255,223,1,247,
219,61,131,41,219,107,84,35,66,36,82,49,62,108,200,131,138,211,167,156,55,88,149,178,45,55,102,100,15,33,18,163,88,231,180,9,145,233,149,198,243,24,218,142,243,92,117,8,145,72,149,242,94,36,67,170,67,185,174,129,180,110,139,116,34,68,162,20,233,131,34,
204,101,76,159,114,62,236,193,131,34,72,128,16,137,84,140,63,74,197,248,49,125,202,121,135,94,41,219,114,13,100,15,15,27,2,0,204,168,68,34,84,214,217,8,102,179,226,80,206,25,205,18,182,229,71,169,178,135,16,137,17,119,103,161,146,231,130,187,179,48,1,
211,89,0,0,51,42,145,72,241,176,97,182,241,176,33,210,130,16,137,21,15,9,128,107,0,41,64,136,68,170,92,21,1,11,235,145,40,161,154,40,235,182,84,163,153,195,154,8,0,192,140,74,36,70,254,211,224,212,31,219,198,2,247,89,22,221,54,240,241,182,82,219,98,242,
105,28,43,222,103,69,255,169,124,219,150,116,237,81,138,68,137,16,137,85,140,183,233,50,149,49,125,202,121,155,119,41,219,114,13,100,14,33,18,161,88,191,182,130,175,188,152,94,105,60,143,69,183,99,237,171,42,177,38,2,0,48,163,18,137,20,207,137,100,27,
207,137,32,45,8,145,24,241,181,39,168,228,185,224,107,79,48,1,33,18,41,158,19,201,56,158,19,65,74,176,38,2,0,48,163,18,137,20,63,74,149,109,229,188,67,175,148,109,185,6,178,135,16,137,85,217,222,201,204,103,69,129,20,65,74,16,34,17,98,93,29,149,60,23,
172,171,99,34,66,36,70,164,8,42,121,46,72,17,76,64,136,68,138,231,68,178,141,231,68,144,22,132,72,172,74,121,195,81,137,84,135,114,93,3,105,221,22,169,196,45,190,0,0,51,42,145,72,149,237,97,195,64,119,112,99,78,74,240,176,33,82,130,74,4,0,96,70,37,18,
163,208,226,102,177,67,170,224,194,40,139,170,211,39,248,168,71,224,250,40,215,182,37,93,123,197,182,69,106,17,34,177,42,219,104,30,232,15,230,179,210,129,219,179,144,18,132,72,132,202,250,192,49,149,72,52,248,81,42,164,1,107,34,0,0,51,42,145,72,149,
237,97,195,128,74,109,139,227,207,33,179,89,72,3,66,36,70,124,237,9,42,121,46,248,218,19,76,64,136,68,138,7,214,193,3,235,72,3,66,36,86,49,38,1,41,50,125,202,89,141,150,178,45,215,64,230,16,34,145,226,231,68,178,173,156,119,232,149,178,45,119,121,103,
15,33,18,171,74,204,105,87,114,91,28,127,30,168,68,144,2,132,72,132,198,242,255,89,132,182,13,181,91,169,109,145,158,115,81,174,107,15,113,170,25,139,100,30,161,171,171,139,171,47,175,181,174,198,45,107,155,58,255,247,245,143,186,125,253,35,166,109,123,
135,199,220,214,35,195,83,254,251,121,179,234,42,178,45,62,106,110,211,76,55,183,105,234,199,188,182,246,12,187,222,161,19,191,101,202,185,109,41,215,94,177,109,51,106,101,119,119,247,230,180,31,58,33,2,0,233,20,69,136,240,196,58,0,192,140,16,1,0,152,
17,34,0,0,51,66,4,0,96,70,136,0,0,204,120,78,36,66,114,155,229,223,158,222,168,218,241,222,225,81,247,243,93,253,170,191,253,242,130,38,215,90,171,251,92,241,82,247,160,191,21,52,228,83,157,117,238,188,206,122,85,155,210,158,180,139,226,228,54,217,11,
186,26,166,181,79,91,107,107,220,151,23,52,171,123,254,167,219,251,84,127,39,251,89,236,182,222,137,180,215,20,210,133,16,137,144,220,75,63,183,113,134,187,236,116,221,64,178,172,181,214,173,123,189,39,248,119,127,56,56,228,54,125,166,195,181,212,214,
4,255,246,203,243,27,221,141,191,59,28,124,211,111,235,25,118,215,156,213,226,150,182,234,46,181,251,95,175,113,191,216,59,160,250,219,44,146,1,89,123,142,182,245,14,187,159,239,202,5,255,78,2,100,211,103,59,18,156,163,94,213,223,201,7,157,91,62,222,
170,250,219,167,247,18,32,177,98,58,43,82,18,10,191,216,51,120,236,247,29,138,253,255,165,185,13,238,150,179,219,130,7,42,111,226,27,94,57,236,31,52,11,181,217,60,179,198,109,252,116,120,224,145,135,8,111,124,101,60,108,52,251,186,234,236,86,117,149,
149,53,210,215,210,231,210,247,161,126,148,254,150,126,151,254,47,70,2,100,227,103,58,220,146,150,90,213,249,145,0,209,132,188,156,67,57,151,154,54,229,58,214,124,200,65,58,17,34,17,187,255,141,30,255,9,78,67,170,22,77,144,200,167,87,169,48,250,2,131,
143,144,79,195,242,169,88,19,36,55,253,238,176,111,91,131,32,57,158,244,113,146,10,68,250,91,27,32,218,10,100,253,27,201,2,68,67,174,95,185,142,17,47,66,36,114,242,6,252,197,94,125,69,178,42,65,144,76,119,69,226,131,68,89,145,220,188,162,213,93,54,151,
32,113,9,43,16,233,95,117,128,124,122,250,43,16,57,103,114,238,84,21,8,1,82,21,8,145,42,176,94,42,146,125,202,138,36,65,144,220,244,123,125,69,162,14,146,223,31,118,219,19,84,36,89,15,146,66,128,104,42,144,237,249,115,166,14,144,4,21,200,211,251,116,
1,162,174,64,246,13,250,235,22,241,35,68,170,196,250,194,212,214,152,11,254,127,217,156,6,183,106,69,130,32,145,47,227,11,180,219,34,21,201,121,9,130,68,22,81,21,251,186,42,195,21,137,15,144,243,58,124,223,134,250,73,250,83,29,32,231,141,87,32,154,254,
79,20,32,43,90,85,109,202,117,74,128,84,15,66,164,138,172,223,50,94,145,168,166,182,146,4,201,171,250,169,173,13,159,82,6,201,171,9,166,182,150,103,47,72,164,15,165,47,213,83,88,175,234,2,68,218,92,172,156,194,90,191,69,31,32,114,142,52,109,250,10,100,
11,1,82,77,8,145,42,35,111,208,103,246,233,42,146,47,157,214,224,86,45,215,5,201,205,127,208,87,36,27,62,169,11,18,105,211,79,109,41,246,245,230,179,90,221,101,115,178,17,36,62,64,62,169,172,64,242,231,70,27,32,218,10,100,131,54,64,230,52,250,115,163,
105,243,25,2,164,42,17,34,85,104,253,155,61,238,233,253,131,154,247,181,187,116,78,131,187,89,27,36,127,28,31,172,66,109,54,215,214,184,245,9,130,100,91,223,176,106,95,111,90,94,253,65,34,125,38,125,39,125,24,234,15,233,55,109,128,172,47,84,32,138,126,
222,240,102,175,123,122,191,46,64,228,156,104,218,148,235,81,174,75,84,31,66,164,74,109,120,51,89,69,114,243,89,186,32,89,149,160,34,89,127,110,135,91,218,18,14,146,85,9,42,146,155,170,184,34,145,190,146,62,211,86,32,171,180,1,242,201,14,183,164,89,87,
129,108,76,18,32,9,42,144,13,4,72,213,34,68,170,216,134,183,122,252,128,32,63,60,22,250,255,210,211,234,199,7,133,0,249,244,187,74,42,146,161,209,96,155,205,51,157,187,255,220,118,93,144,252,81,158,35,25,82,237,235,141,103,182,248,53,157,106,34,125,36,
125,37,125,22,58,126,233,167,85,127,212,5,136,180,185,184,121,166,170,95,11,215,75,136,244,189,156,3,77,155,210,158,180,139,234,69,136,84,185,141,111,245,186,103,222,211,221,254,43,21,137,54,72,110,249,211,17,245,237,191,218,32,145,54,183,247,233,110,
255,189,233,204,214,170,9,146,66,128,168,110,227,205,247,189,54,64,150,4,250,189,96,227,219,189,238,153,253,225,235,68,250,92,250,94,67,174,59,185,254,80,221,8,145,12,240,65,178,95,119,215,214,165,93,186,65,194,87,36,249,193,76,115,215,214,58,197,128,
230,43,146,63,29,241,211,102,154,125,189,113,89,171,15,190,152,73,159,172,243,21,136,226,46,172,222,15,251,188,24,9,163,117,231,72,5,162,187,11,171,112,125,132,72,95,75,159,107,218,148,246,8,144,108,32,68,50,66,62,105,62,187,95,183,70,226,131,100,89,
56,72,182,79,172,72,66,107,36,51,106,220,253,231,132,131,68,218,186,229,207,71,212,107,36,49,7,137,244,133,244,137,244,141,102,13,68,250,37,84,253,249,202,239,19,237,234,53,144,77,111,235,42,213,66,128,104,218,148,235,76,174,55,100,3,33,146,33,27,183,
142,15,24,154,79,146,95,236,202,15,26,1,62,72,254,172,172,72,102,212,184,117,159,80,6,201,107,250,138,228,134,165,173,238,75,202,175,70,79,11,95,129,124,162,221,247,137,166,2,145,254,208,4,200,186,143,235,43,16,117,128,116,53,248,62,86,85,32,50,133,181,
149,0,201,18,66,36,99,54,109,237,117,207,42,127,179,227,210,4,65,114,171,12,114,35,138,53,146,153,250,32,185,245,117,253,26,201,13,203,226,9,146,66,128,248,187,176,2,124,223,190,174,15,16,237,26,200,3,242,129,66,113,29,248,0,81,92,3,66,174,171,77,4,72,
230,16,34,25,228,131,68,91,145,156,218,224,110,92,170,15,18,109,69,114,159,98,192,155,24,36,154,125,189,126,105,171,15,190,52,147,99,150,99,215,84,32,73,3,68,93,129,40,3,68,250,242,122,101,5,34,215,19,1,146,77,132,72,70,109,218,214,235,126,217,173,91,
35,73,18,36,171,95,211,175,145,220,119,118,126,238,190,8,31,36,175,29,113,239,244,233,214,72,110,88,146,222,32,145,99,149,99,214,172,129,188,83,168,238,66,1,34,149,221,217,237,110,113,147,110,13,228,129,109,186,74,84,250,80,250,82,211,166,92,71,114,61,
33,155,8,145,12,219,228,7,20,221,115,36,151,204,174,119,55,44,105,9,118,214,246,220,176,91,253,250,97,255,179,188,193,231,72,102,56,119,239,217,109,110,137,60,28,81,132,76,147,125,88,145,132,247,245,250,197,45,238,210,83,211,21,36,114,140,114,172,114,
204,161,253,63,86,129,4,166,7,37,64,36,148,22,53,233,158,3,81,7,200,169,13,190,15,53,109,202,245,67,128,100,27,33,146,113,15,108,239,115,191,124,95,183,70,34,21,137,46,72,70,220,154,55,244,107,36,247,250,138,36,28,36,171,223,56,226,222,201,141,168,246,
245,250,37,233,9,146,241,0,209,173,129,200,241,173,86,244,93,33,64,22,7,250,173,224,251,219,251,220,179,138,243,236,3,68,113,142,133,92,55,15,40,127,107,29,213,139,16,129,31,8,228,19,170,102,238,251,146,217,227,159,82,67,10,65,162,93,35,185,103,133,62,
72,182,247,141,168,246,245,91,139,91,124,240,85,146,28,147,28,155,110,13,68,31,32,247,174,40,84,32,225,126,120,64,25,32,210,87,223,242,21,136,98,13,164,155,0,193,56,66,4,222,247,223,201,87,36,154,53,146,4,65,178,118,139,126,141,228,158,229,186,32,89,
179,69,214,72,70,84,251,122,253,162,202,5,137,15,144,229,218,53,144,17,127,92,170,0,89,46,107,32,51,85,199,127,236,188,6,72,31,73,95,169,214,64,222,31,244,237,2,142,16,193,68,126,192,57,160,156,218,74,18,36,111,234,167,182,212,65,242,102,130,169,173,
10,4,201,177,0,81,78,97,173,81,244,209,177,0,209,78,97,237,72,24,32,10,114,125,16,32,152,136,16,193,71,20,62,185,170,166,182,102,233,6,159,99,65,162,157,218,58,75,31,36,210,182,106,106,107,97,139,15,190,147,193,7,200,89,202,41,172,4,1,34,161,164,157,
194,82,87,32,179,27,124,223,104,218,164,2,193,137,16,34,56,142,124,130,125,78,57,181,229,131,100,161,62,72,114,138,169,45,31,36,103,182,187,37,77,225,32,89,91,168,72,20,251,122,50,130,68,246,89,246,189,89,51,133,165,172,210,124,128,156,213,238,22,55,
234,166,176,30,220,161,171,40,11,1,162,105,83,174,7,185,46,128,201,8,17,156,208,247,119,142,15,68,154,79,168,43,103,229,7,163,128,237,253,35,110,205,91,186,138,164,105,70,141,187,251,204,252,220,127,17,62,72,222,210,87,36,215,45,104,113,95,156,85,158,
32,145,125,149,125,110,82,86,32,178,223,154,0,145,54,23,53,234,42,16,117,128,204,106,240,125,161,170,64,100,10,107,39,1,130,19,35,68,48,165,7,119,246,185,231,62,208,173,145,92,162,12,146,119,250,71,220,218,183,143,184,156,98,141,68,190,217,246,30,101,
144,124,251,237,35,190,109,141,235,22,78,127,144,44,46,84,32,154,53,144,254,17,191,191,218,0,9,29,127,193,67,18,252,138,243,229,3,68,113,174,132,156,255,7,9,16,20,65,136,160,168,7,147,84,36,157,227,159,110,67,10,65,162,174,72,150,37,8,18,101,69,114,237,
130,22,31,124,211,193,87,32,203,116,21,136,236,159,58,64,150,37,168,64,148,1,34,199,124,109,130,10,132,0,65,8,33,130,160,135,118,245,185,231,100,138,68,49,242,92,210,89,239,174,155,175,11,146,111,111,149,53,146,209,96,155,242,148,247,221,75,219,116,65,
178,245,136,219,145,27,86,237,235,117,243,155,221,37,179,234,75,186,0,124,128,44,29,127,18,61,244,122,178,95,178,127,170,0,89,218,230,22,53,206,80,29,199,67,187,122,85,21,163,28,171,28,179,166,77,57,223,114,222,129,16,66,4,42,15,237,150,169,173,163,154,
53,88,183,114,86,130,32,217,214,227,7,213,80,155,77,51,107,220,93,218,32,217,214,227,219,214,236,235,181,243,91,204,65,34,251,34,251,36,251,22,122,157,137,199,90,76,75,254,56,23,202,93,88,138,253,47,156,151,16,57,70,57,86,77,155,210,158,180,11,104,16,
34,80,147,129,229,121,25,176,20,35,209,202,4,21,201,119,182,245,168,239,218,186,107,137,46,72,164,205,29,253,186,187,182,174,253,88,139,175,160,146,240,1,178,164,77,117,23,214,142,252,49,106,3,100,145,242,46,172,135,119,41,3,164,179,222,31,163,166,205,
231,9,16,36,68,136,32,145,99,21,137,98,78,253,226,83,242,131,87,64,33,72,180,107,36,223,91,220,54,126,187,107,17,133,32,241,21,137,98,95,191,153,32,72,100,176,151,125,80,173,129,36,8,144,239,45,105,115,11,27,116,107,32,15,203,121,56,168,11,16,57,54,77,
155,84,32,176,32,68,144,216,195,127,233,115,207,43,6,48,177,178,83,25,36,3,35,238,182,237,61,234,187,182,100,192,213,4,137,180,185,99,64,119,215,150,54,72,164,221,31,237,205,5,255,110,71,254,152,180,1,178,40,112,60,5,143,252,37,89,128,104,200,249,148,
243,10,36,69,136,192,164,16,36,211,90,145,20,6,93,101,69,114,167,178,34,145,54,181,21,201,53,103,180,248,224,11,121,46,63,232,22,171,64,180,1,34,199,161,174,64,148,1,34,199,32,199,162,105,147,0,65,41,8,17,152,201,192,179,249,160,110,141,228,226,142,122,
119,237,25,186,32,185,253,29,253,26,201,157,139,116,65,34,109,106,215,72,190,57,175,197,173,60,37,28,36,50,248,74,85,112,162,53,16,121,61,85,128,44,106,115,139,26,116,107,32,143,40,43,64,217,119,57,6,77,155,155,9,16,148,136,16,65,73,30,126,183,207,109,
62,164,155,218,242,21,137,54,72,118,232,167,182,212,65,178,35,193,212,214,25,202,32,57,116,212,61,242,238,135,131,240,142,252,190,171,3,68,59,133,245,110,159,127,173,16,31,32,138,62,22,114,222,30,126,151,0,65,105,8,17,148,76,6,34,237,212,214,23,58,242,
159,146,3,252,212,86,126,48,214,76,109,221,161,24,144,11,65,162,158,218,210,86,36,249,32,145,118,181,1,114,199,66,253,20,86,146,0,145,125,86,79,97,17,32,152,6,53,242,19,151,49,232,234,234,138,99,71,51,236,155,243,154,253,180,149,198,230,195,71,221,35,
123,194,139,211,18,12,119,46,108,29,191,149,54,32,55,58,230,110,223,217,27,172,54,252,32,190,160,85,93,5,60,186,39,231,158,63,172,171,182,66,202,245,218,43,59,36,64,154,85,109,106,251,30,21,183,178,187,187,123,115,218,79,3,149,8,166,141,12,76,242,137,
89,85,145,180,215,187,107,78,15,15,122,126,122,104,103,175,174,34,169,209,13,208,210,214,127,236,234,245,213,142,102,95,191,113,186,62,28,139,145,0,249,238,130,86,125,5,162,12,16,217,55,217,71,85,5,114,136,0,193,244,34,68,48,173,30,221,155,243,159,116,
53,100,240,211,6,137,12,250,82,105,132,72,197,162,13,146,239,238,234,117,59,6,117,107,36,215,148,24,36,133,0,241,139,232,10,218,126,212,246,161,203,87,32,143,42,110,77,6,146,32,68,48,237,252,0,152,164,34,153,171,12,146,4,21,201,119,231,135,7,108,31,36,
249,233,47,85,69,50,215,22,36,242,243,184,178,63,11,235,117,21,72,146,0,145,125,210,180,41,231,131,0,65,57,16,34,40,139,71,247,229,220,11,135,117,183,255,170,131,100,112,196,87,15,254,174,173,208,237,191,218,32,25,29,175,72,118,14,232,110,255,253,198,
156,102,119,113,187,62,72,38,6,136,166,253,199,180,1,210,94,239,247,69,211,166,156,7,57,31,64,57,16,34,40,27,25,184,100,64,212,124,82,254,124,91,126,80,12,240,65,178,91,95,145,252,199,199,148,65,178,91,95,145,92,157,32,72,62,222,92,235,22,40,43,144,199,
164,191,142,232,2,68,246,65,85,129,16,32,40,51,66,4,101,245,216,254,156,123,65,49,48,138,47,180,235,131,228,142,191,232,215,72,180,65,34,109,238,84,174,145,104,131,228,183,189,67,238,241,253,225,65,92,254,38,73,128,104,72,191,63,166,120,109,160,20,132,
8,202,238,177,252,0,169,173,72,52,131,100,33,72,180,21,201,237,103,232,131,68,91,145,92,117,90,179,15,190,144,205,249,193,124,202,10,68,25,32,242,90,242,154,170,10,132,0,193,73,66,136,224,164,120,188,80,145,104,214,72,90,235,221,213,167,233,130,228,206,
119,245,107,36,183,207,211,5,137,180,233,43,18,197,190,94,221,213,236,190,208,22,14,18,57,118,95,145,76,218,254,113,101,165,38,175,33,175,165,90,3,41,188,22,112,18,16,34,56,105,30,127,47,231,94,232,209,253,176,149,175,72,148,65,114,199,158,94,63,248,
135,218,148,39,219,111,155,55,254,156,70,49,190,34,121,183,215,237,56,170,251,97,43,95,145,104,130,164,231,168,239,131,194,118,133,254,8,145,182,125,5,162,216,151,194,107,0,39,11,79,172,227,164,187,186,171,201,135,132,198,175,100,80,236,238,15,254,165,
220,253,116,251,188,22,245,147,237,119,238,233,115,59,143,22,95,255,144,182,164,77,127,103,149,194,19,221,253,234,80,112,249,1,95,243,183,87,117,53,169,94,95,219,87,136,6,79,172,3,39,242,120,126,176,213,204,237,95,212,170,27,68,37,16,36,24,180,107,36,
183,157,30,14,135,66,216,72,181,163,217,215,175,159,218,228,62,223,170,171,72,52,1,34,109,73,155,154,215,126,129,0,65,133,16,34,168,8,249,212,46,159,156,53,115,52,159,111,169,119,87,157,170,11,146,239,237,237,83,175,145,220,54,87,23,36,210,166,118,141,
228,42,101,144,132,72,27,254,152,21,175,41,253,248,4,1,130,10,33,68,80,49,79,188,223,239,126,213,171,91,35,185,168,53,65,144,236,235,83,175,145,124,71,27,36,251,198,167,191,52,251,170,173,72,166,114,172,2,81,188,150,244,159,244,35,80,41,132,8,42,74,6,
192,95,43,43,146,139,164,34,153,173,11,146,187,246,233,43,146,239,204,209,7,201,46,101,69,242,245,217,182,32,241,1,50,91,87,129,72,191,17,32,168,52,66,4,21,247,196,129,124,69,162,152,251,191,176,37,63,200,6,248,32,217,175,95,35,249,246,105,202,32,217,
63,62,181,165,217,215,43,103,53,249,169,56,45,249,91,217,70,211,182,175,64,14,16,32,168,60,66,4,169,240,131,3,253,238,215,125,186,39,219,47,74,16,36,119,191,215,167,126,178,93,27,36,119,189,215,231,118,5,238,236,42,184,114,118,178,32,209,144,126,250,
1,1,130,148,32,68,144,26,63,72,82,145,52,143,127,106,15,241,65,146,160,34,89,219,213,226,22,214,233,130,196,175,145,40,246,245,107,179,154,124,240,133,252,42,31,14,161,10,132,0,65,154,16,34,72,149,39,63,200,87,36,138,209,249,162,230,58,93,144,12,141,
184,123,164,34,25,25,13,182,217,92,227,220,218,174,102,85,144,220,237,43,146,97,213,190,94,217,217,232,46,106,169,11,238,171,28,251,147,31,228,78,216,198,248,191,17,32,72,23,66,4,169,227,131,36,55,164,186,59,233,194,4,65,114,119,119,78,125,215,214,154,
174,102,183,64,19,36,221,57,223,182,102,95,191,214,217,164,12,146,33,247,228,193,254,143,108,43,253,65,128,32,141,8,17,164,146,12,152,47,246,13,169,238,82,186,176,169,206,93,217,25,14,146,93,82,145,116,231,212,119,109,173,61,85,23,36,210,166,95,35,81,
236,235,215,78,105,242,21,84,136,4,201,15,37,52,198,156,239,7,2,4,105,197,215,158,32,213,100,208,189,80,49,232,138,23,115,67,238,135,135,194,131,173,4,195,218,217,205,190,226,8,233,151,144,56,144,243,1,84,140,44,204,175,153,29,14,157,130,167,14,141,87,
91,33,43,26,106,221,150,193,97,46,210,108,226,107,79,128,82,73,40,188,168,24,108,93,126,106,75,66,39,196,87,36,7,114,62,32,66,36,104,214,42,194,65,42,146,123,21,97,83,112,133,178,34,33,64,144,118,132,8,82,239,135,249,79,237,154,59,161,46,104,170,243,
3,116,72,33,72,100,240,15,181,217,88,163,171,50,142,5,137,242,174,173,175,118,52,249,169,56,32,102,132,8,162,240,84,161,34,209,172,145,52,214,185,43,58,116,65,114,111,161,34,9,180,217,228,106,220,154,89,202,32,249,32,231,118,15,233,214,72,174,32,72,16,
57,66,4,209,120,234,240,120,144,168,42,18,9,146,118,125,144,168,42,18,87,227,86,119,54,187,5,181,250,169,45,85,69,210,78,144,32,94,132,8,162,242,212,145,126,247,82,191,110,141,196,79,109,105,130,100,120,196,221,247,65,206,245,43,110,50,145,7,18,87,207,
82,4,201,216,152,111,115,247,176,110,141,68,130,100,69,125,173,234,111,129,52,33,68,16,157,241,32,209,61,144,120,65,99,173,187,162,189,49,120,136,227,65,210,231,250,71,195,15,36,74,44,173,238,108,114,11,106,139,191,125,142,5,137,159,218,42,222,166,28,
207,150,163,44,162,35,62,132,8,162,244,212,145,1,247,98,191,110,106,235,115,13,117,238,171,109,154,32,25,117,247,29,212,79,109,221,122,74,179,46,72,14,202,212,214,232,148,109,201,113,200,241,0,49,34,68,16,173,255,236,25,112,47,13,232,22,219,47,72,16,
36,235,14,233,23,219,111,237,208,5,137,180,185,123,104,244,184,54,100,255,229,56,128,88,17,34,136,154,15,146,65,221,87,164,36,169,72,214,29,206,249,193,63,212,166,220,254,123,139,54,72,14,231,124,219,133,109,101,191,9,16,196,142,16,65,244,100,32,126,
89,89,145,124,174,190,206,125,181,85,23,36,247,31,214,87,36,183,180,235,130,68,218,148,138,228,101,42,16,84,9,190,246,4,85,227,223,91,26,125,181,161,241,242,224,144,251,81,95,120,16,159,63,115,134,15,8,185,43,43,68,238,238,186,255,72,206,237,150,111,
11,46,66,190,151,43,23,201,251,14,21,197,215,158,0,39,147,132,130,132,131,182,34,145,208,9,145,64,144,96,80,87,36,109,205,62,120,138,33,64,80,77,8,17,84,21,9,146,151,142,234,214,72,206,175,175,115,95,209,6,73,143,126,141,100,149,34,72,128,106,193,149,
142,170,243,227,36,21,73,93,157,251,74,179,46,72,214,247,232,43,146,85,173,4,9,178,129,171,28,85,233,199,185,241,32,209,60,71,114,190,4,73,147,50,72,122,245,207,145,220,220,66,144,160,250,113,133,163,106,253,184,127,192,253,230,232,144,98,98,107,204,
157,95,95,171,14,146,13,125,242,21,41,163,193,54,155,106,198,43,29,160,154,17,34,168,106,18,36,47,31,29,86,85,36,127,83,87,235,46,87,7,73,127,176,34,145,215,253,217,192,32,23,24,170,26,33,130,170,247,19,95,145,12,171,214,72,206,175,173,117,151,55,234,
130,100,99,95,255,148,107,36,242,122,242,186,64,181,35,68,144,9,63,25,24,112,47,15,41,43,146,218,90,247,111,154,32,25,29,117,27,115,199,87,36,242,58,242,122,64,22,16,34,200,140,159,14,12,184,223,12,235,190,41,247,252,4,65,178,169,191,255,216,215,200,
75,251,63,37,64,144,33,132,8,50,165,156,65,242,220,208,16,1,130,204,225,87,112,144,57,126,160,111,108,244,33,17,226,255,166,177,49,24,14,18,36,187,7,89,68,71,246,80,137,32,147,202,81,145,0,89,68,136,32,179,8,18,160,116,132,8,50,141,32,1,74,67,136,32,
243,8,18,192,142,16,1,18,6,9,128,15,17,34,64,158,38,72,120,14,4,248,40,66,4,152,160,88,144,16,32,192,241,8,17,96,146,19,5,9,1,2,156,24,33,2,156,192,196,32,33,64,128,169,241,196,58,48,5,9,142,183,234,234,220,111,134,134,232,34,96,10,84,34,64,17,4,8,80,
28,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,
1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,
8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,89,109,68,93,247,66,10,246,1,0,78,150,67,49,244,116,205,216,216,88,10,118,3,0,16,35,166,179,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,
51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,
2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,25,33,2,0,48,35,68,0,0,102,132,8,0,192,140,16,1,0,152,17,34,0,0,51,66,4,0,96,70,136,0,0,204,8,17,0,128,141,115,238,255,1,192,35,249,158,125,210,47,61,0,0,
0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_Logo_Final_Version_1_0_App_Icon_Black_square_png = (const char*) temp_binary_data_93;

//================== Volume.com SVG Conversions 1.02_audio output.png ==================
static const unsigned char temp_binary_data_94[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,2,51,73,68,65,84,88,133,237,153,209,77,2,65,16,134,127,141,239,248,242,63,171,21,72,7,210,129,116,160,
84,32,118,64,7,218,129,80,129,218,129,118,128,29,240,62,47,88,1,102,201,156,158,123,179,194,177,115,27,73,248,19,18,178,7,147,239,102,102,103,102,239,142,86,171,21,246,69,199,123,67,122,128,237,80,157,192,146,124,236,194,174,59,44,201,41,128,59,111,187,
65,39,94,134,72,158,2,120,3,112,233,101,51,150,139,103,75,128,194,3,150,100,31,192,188,107,80,228,166,129,130,6,143,246,252,144,210,218,217,179,36,135,37,65,177,43,44,201,91,0,207,94,160,33,66,36,231,36,7,127,253,174,53,44,201,49,128,167,44,186,223,246,
134,181,205,57,213,205,106,170,21,172,214,208,7,71,208,113,20,161,51,0,227,108,88,5,189,241,0,212,52,130,122,52,86,18,118,227,136,168,97,121,1,112,213,6,74,68,142,12,59,225,134,175,1,124,2,232,139,200,34,225,132,145,136,76,99,155,166,103,67,111,39,249,
22,62,90,67,91,129,26,246,206,213,206,181,46,133,176,79,244,251,196,248,203,208,178,147,74,131,190,2,94,105,30,229,106,9,32,222,56,107,160,224,93,0,175,209,53,179,42,20,25,17,69,36,192,198,147,88,175,86,170,226,220,237,89,85,161,228,60,219,200,65,141,
32,52,69,82,215,190,85,12,86,195,29,171,242,222,114,27,27,255,229,164,96,221,72,67,255,5,182,17,114,75,197,96,181,173,198,170,60,154,108,177,117,165,70,196,113,205,192,185,211,44,96,117,166,170,10,52,74,149,136,52,186,155,9,43,34,191,118,39,73,228,0,
235,220,27,55,150,143,218,166,139,189,254,97,217,217,42,13,180,245,141,118,34,253,185,249,251,104,121,93,119,53,61,226,198,99,205,12,155,103,131,186,116,0,217,202,195,241,108,128,31,15,135,57,99,33,34,3,93,179,142,68,23,86,169,107,181,193,156,60,220,
175,194,78,114,98,128,190,39,106,114,251,106,224,0,188,212,246,11,245,114,156,159,214,96,179,214,78,165,43,23,184,102,103,174,149,96,166,75,51,171,10,84,202,122,228,249,87,14,91,57,187,193,214,250,120,83,243,122,67,217,207,103,83,192,109,97,183,81,118,
7,243,74,137,34,176,40,8,236,54,27,148,0,118,29,100,186,6,118,123,228,89,41,0,235,1,209,93,135,183,53,93,233,0,219,149,246,7,22,192,23,89,19,195,132,107,125,83,123,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_audio_output_png = (const char*) temp_binary_data_94;

//================== Volume.com SVG Conversions 1.02_Check.png ==================
static const unsigned char temp_binary_data_95[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,221,73,68,65,84,88,133,237,152,193,77,195,64,16,69,63,52,0,151,127,38,29,144,14,8,21,144,18,76,5,
64,7,161,3,168,32,238,128,80,1,233,0,211,65,56,207,37,169,32,104,165,137,180,138,156,217,245,218,187,40,146,255,49,94,111,94,254,172,231,79,124,177,223,239,113,46,186,60,27,210,17,54,163,70,216,92,26,97,67,34,57,37,121,221,245,190,226,176,10,89,3,216,
144,156,119,185,247,63,156,93,0,184,5,112,5,224,131,100,29,235,114,209,4,35,57,3,240,213,114,233,23,64,37,34,107,235,254,98,176,234,222,70,29,109,211,78,68,76,135,75,30,131,218,0,117,122,14,109,80,4,150,100,5,224,193,88,242,41,34,117,104,159,236,199,
128,228,4,64,99,149,31,192,68,68,182,161,189,74,56,27,42,255,60,6,20,185,97,73,186,54,117,103,44,121,15,117,0,95,217,142,129,75,41,0,223,198,18,215,174,166,177,174,34,151,179,94,74,89,138,46,127,86,88,47,165,78,233,85,68,154,174,155,14,126,12,140,148,
58,232,71,68,166,41,123,71,57,235,202,74,178,209,126,105,174,3,176,50,150,184,54,213,105,120,241,21,132,85,128,181,150,117,25,0,14,181,169,133,136,108,178,192,234,19,221,28,157,191,86,224,200,148,122,75,5,133,117,102,21,116,109,56,117,127,232,145,67,
166,148,37,203,217,69,160,164,43,253,65,136,40,127,213,23,20,1,103,253,179,122,74,59,5,125,50,214,184,148,10,78,84,189,96,61,96,87,222,155,196,253,59,167,148,37,243,1,211,47,153,171,131,41,234,156,82,201,176,10,236,156,157,37,0,39,165,148,165,232,4,139,
232,14,190,146,83,202,82,244,108,224,57,28,82,175,148,26,4,214,3,126,12,44,235,149,82,150,146,6,25,77,171,101,203,37,151,82,89,92,69,234,136,168,127,238,142,29,118,229,55,7,157,190,74,158,103,21,248,197,251,104,144,148,178,212,123,158,117,175,127,0,108,
135,74,169,172,176,37,53,190,159,205,165,17,54,151,206,7,22,192,31,22,126,206,223,222,153,230,40,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Check_png = (const char*) temp_binary_data_95;

//================== Volume.com SVG Conversions 1.02_Clip.png ==================
static const unsigned char temp_binary_data_96[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,5,73,68,65,84,88,133,237,153,203,13,131,48,16,68,135,40,5,228,50,231,80,66,58,72,74,72,7,73,41,41,
129,18,82,10,165,228,190,69,16,33,249,128,16,31,219,176,54,150,246,221,44,44,120,50,163,177,17,85,215,117,40,133,83,49,166,38,171,72,81,178,103,237,7,144,124,3,168,221,176,21,145,54,246,94,234,178,0,122,217,251,96,28,45,107,153,213,194,100,183,66,242,
66,242,118,120,89,146,31,0,63,0,207,241,181,20,109,224,5,201,94,174,1,112,157,155,159,93,214,189,238,102,84,111,147,100,149,37,249,5,240,242,157,159,59,179,222,162,176,234,82,196,100,181,216,93,214,245,165,10,187,85,215,168,47,171,67,202,246,251,184,
147,12,170,161,24,54,197,96,176,143,171,139,34,118,101,125,246,113,13,130,100,67,246,241,108,178,41,115,185,196,106,102,83,231,114,137,217,149,117,159,208,159,212,185,92,98,82,54,244,232,150,138,185,24,212,233,85,214,177,131,140,22,38,171,133,201,106,
49,217,179,34,242,72,241,112,17,9,58,247,22,181,178,246,107,73,11,147,85,1,192,31,184,22,38,175,160,211,81,4,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Clip_png = (const char*) temp_binary_data_96;

//================== Volume.com SVG Conversions 1.02_Delete.png ==================
static const unsigned char temp_binary_data_97[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,62,73,68,65,84,88,133,237,152,193,109,195,48,12,69,127,139,222,219,11,247,232,6,169,55,232,40,29,
33,221,36,43,116,130,36,27,164,27,184,231,127,169,39,112,97,32,1,12,35,65,248,45,17,178,11,189,155,105,130,248,150,73,145,224,67,223,247,88,11,143,171,81,90,197,6,242,148,51,180,153,189,221,120,213,146,108,83,227,103,21,11,96,127,195,254,9,96,155,26,
188,22,88,20,171,18,235,110,10,102,22,217,61,26,146,135,123,78,53,13,162,248,183,77,161,9,212,113,242,56,213,169,43,10,169,221,154,89,114,203,188,194,48,55,236,60,142,82,26,4,221,181,95,36,223,61,142,106,26,252,76,158,59,0,71,0,223,19,187,215,6,111,113,
205,17,59,29,243,78,36,135,177,240,99,108,244,218,84,150,80,96,238,57,87,21,235,254,101,2,97,98,127,3,196,186,41,46,214,51,109,93,88,66,26,184,41,93,96,157,226,92,250,100,165,120,146,88,146,171,42,176,220,196,157,236,153,99,70,193,210,159,42,125,178,
225,98,147,215,64,35,194,211,32,167,88,137,210,105,32,125,248,28,177,217,238,90,117,179,56,103,139,56,46,138,87,51,27,122,251,203,216,193,107,83,73,21,251,12,96,115,197,199,99,147,175,64,57,13,72,22,27,102,230,46,147,115,44,60,228,214,93,151,28,81,84,
177,33,0,248,3,211,77,95,94,76,18,187,145,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Delete_png = (const char*) temp_binary_data_97;

//================== Volume.com SVG Conversions 1.02_Edit.png ==================
static const unsigned char temp_binary_data_98[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,228,73,68,65,84,88,133,237,153,209,81,2,49,16,134,23,199,119,121,217,119,58,192,18,206,14,232,128,
163,3,173,64,233,128,14,144,10,164,3,177,3,237,64,158,255,23,169,0,39,51,123,99,60,189,203,38,110,162,56,247,207,48,48,119,201,240,177,108,242,103,247,70,199,227,145,78,69,103,39,67,58,192,102,212,255,135,101,230,218,30,37,172,40,88,102,30,51,243,61,
17,173,229,189,168,212,91,151,3,37,162,29,17,77,189,203,75,0,119,165,128,85,176,29,160,141,22,0,138,68,57,8,203,204,151,2,122,209,49,228,0,96,156,133,174,165,222,156,85,128,190,16,81,149,27,178,209,121,224,126,29,2,5,240,70,31,63,140,0,60,231,0,37,101,
26,184,124,156,183,46,63,17,209,204,3,117,209,221,202,189,73,115,221,90,154,173,235,90,162,216,104,3,192,143,168,139,254,163,252,3,238,181,147,5,105,174,216,221,224,25,64,237,93,119,159,215,223,76,249,148,34,86,74,62,34,118,164,135,175,141,255,195,44,
148,106,183,33,80,167,185,181,203,69,195,50,243,196,45,46,229,112,7,108,230,112,209,176,0,94,101,209,105,117,107,117,240,73,74,3,177,215,69,196,148,181,5,112,242,121,86,128,151,17,83,86,141,113,164,234,199,5,163,114,177,53,58,200,150,150,228,114,38,213,
109,2,112,146,203,89,149,53,109,151,235,83,178,203,153,192,74,148,170,8,224,105,10,176,89,193,232,1,239,149,83,28,240,42,230,59,76,171,91,1,118,134,113,80,78,137,114,57,243,82,92,86,122,69,113,192,42,151,203,214,235,98,102,23,225,135,136,41,87,0,118,
125,3,178,53,57,0,108,35,93,46,120,222,200,218,145,17,151,187,81,14,15,186,91,145,150,167,214,52,0,140,250,238,23,233,117,201,33,124,19,26,39,181,92,167,138,53,230,4,56,100,26,189,169,80,186,139,88,73,14,119,65,247,70,246,215,218,244,94,197,81,123,109,
169,61,128,73,215,156,63,241,76,161,5,62,147,106,228,139,134,7,32,185,52,192,230,210,0,155,69,68,244,14,35,131,204,173,55,125,241,172,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Edit_png = (const char*) temp_binary_data_98;

//================== Volume.com SVG Conversions 1.02_info.png ==================
static const unsigned char temp_binary_data_99[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,2,30,73,68,65,84,88,133,237,153,193,113,194,48,16,69,127,50,185,135,203,158,67,42,128,14,66,7,33,21,
4,58,128,14,40,129,18,160,130,56,29,64,5,129,10,2,231,189,224,10,200,104,102,157,97,176,214,146,101,9,240,12,127,198,7,4,246,62,175,164,175,149,120,56,30,143,104,139,30,91,67,122,135,77,168,86,193,62,197,122,16,17,13,0,116,0,244,79,154,15,0,54,230,98,
230,67,211,24,141,220,128,136,70,0,134,0,222,61,126,190,5,176,0,144,49,243,46,36,94,16,172,64,206,0,188,132,4,5,176,4,48,169,155,237,90,176,68,212,151,236,244,66,8,207,148,11,240,194,247,6,111,88,34,26,10,232,115,4,208,83,45,153,121,228,243,67,47,55,
144,110,255,74,0,106,244,73,68,94,217,117,102,86,186,254,199,51,112,46,179,191,80,191,198,11,58,51,92,9,75,68,198,138,118,53,2,174,153,217,88,88,113,255,10,192,155,231,189,70,227,170,49,236,26,6,41,198,104,149,230,146,32,171,84,88,49,121,31,255,140,41,
147,152,185,246,60,117,24,16,81,22,0,187,151,222,40,52,10,244,226,87,219,194,97,133,37,162,46,128,223,128,32,177,52,101,230,82,134,181,97,48,12,12,106,220,96,125,114,229,129,207,177,186,130,86,200,12,148,118,151,54,13,221,160,80,207,76,180,243,229,88,
203,108,95,105,191,164,74,12,26,108,104,129,18,83,222,176,183,160,146,223,222,183,53,169,116,203,176,155,243,6,13,118,155,158,197,169,210,10,166,193,150,222,234,210,98,102,239,204,102,87,102,253,182,53,90,97,153,57,107,176,84,198,144,53,89,85,19,76,45,
213,18,107,175,21,224,46,216,107,100,119,166,125,161,194,74,17,161,222,152,72,219,170,109,77,229,241,145,169,41,101,11,238,91,57,117,100,135,241,255,185,198,59,229,90,105,88,200,103,119,107,2,174,34,29,108,84,233,67,38,182,42,231,10,38,195,97,144,120,
161,24,187,64,81,243,68,166,35,150,18,82,76,107,50,93,63,176,45,0,54,121,215,6,38,195,178,11,152,70,114,9,99,252,93,95,80,52,56,69,52,89,158,200,85,247,92,193,64,206,153,121,85,55,110,227,127,107,196,45,134,82,217,219,38,225,94,106,141,85,147,179,217,
40,176,151,212,189,248,78,165,246,192,2,248,3,4,241,203,113,238,189,108,101,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_info_png = (const char*) temp_binary_data_99;

//================== Volume.com SVG Conversions 1.02_Load.png ==================
static const unsigned char temp_binary_data_100[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,107,73,68,65,84,88,133,237,152,203,77,195,64,16,134,127,16,119,184,252,103,74,48,29,144,18,232,32,
184,131,148,16,58,128,10,112,58,160,3,156,14,66,7,225,60,23,168,192,104,165,65,88,214,58,126,236,76,132,165,253,110,94,79,214,159,39,59,251,240,69,211,52,88,10,151,139,49,205,178,142,92,197,186,38,185,250,7,110,71,17,57,182,27,162,178,0,222,207,227,115,
146,39,0,219,118,64,46,48,47,22,37,155,23,5,47,178,172,23,46,178,36,111,60,250,53,151,37,121,23,86,31,146,143,214,125,155,206,6,154,209,26,64,1,224,27,192,74,68,14,86,253,91,103,246,77,69,3,215,65,220,114,72,152,201,146,172,0,220,119,154,77,133,77,100,
117,124,174,123,110,135,76,63,91,60,39,89,150,228,3,128,215,129,176,53,201,237,64,204,32,73,5,166,149,95,235,223,61,134,82,68,170,179,203,234,56,12,149,126,59,225,103,73,51,68,202,48,168,39,138,34,181,224,102,201,106,229,23,145,91,251,206,245,167,102,
179,205,108,225,201,178,36,55,61,149,95,106,182,219,132,51,84,236,60,87,116,143,44,99,152,147,217,42,146,193,93,95,225,232,248,44,59,205,31,218,143,175,172,136,124,137,72,200,214,78,155,246,34,114,114,31,160,47,242,242,27,63,183,200,250,78,183,131,4,
65,146,181,46,177,99,226,55,36,15,41,83,215,108,89,252,101,204,45,190,75,222,124,123,145,101,189,200,178,94,100,89,47,150,255,229,59,129,216,174,203,140,252,21,209,139,44,235,2,128,31,228,211,114,148,87,128,66,203,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Load_png = (const char*) temp_binary_data_100;

//================== Volume.com SVG Conversions 1.02_Minus.png ==================
static const unsigned char temp_binary_data_101[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,42,0,0,0,43,8,6,0,0,0,14,159,26,254,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,0,113,73,68,65,84,88,133,237,216,193,13,128,32,16,5,209,175,177,5,234,21,59,179,31,138,192,14,228,50,
152,96,230,157,9,76,178,217,11,91,239,61,43,216,151,168,52,116,2,67,105,134,210,12,165,25,74,51,148,102,40,205,80,154,161,180,99,116,95,41,165,38,57,39,119,92,173,181,250,118,192,209,211,12,165,253,103,235,147,220,31,116,12,223,240,75,135,102,40,205,
80,154,161,52,67,105,134,210,12,165,25,138,74,242,0,122,140,12,211,194,202,244,156,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Minus_png = (const char*) temp_binary_data_101;

//================== Volume.com SVG Conversions 1.02_mixer color.png ==================
static const unsigned char temp_binary_data_102[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,4,11,73,68,65,84,88,133,237,89,77,104,19,65,20,126,21,239,230,40,120,72,122,48,86,91,147,86,91,45,181,
177,107,21,5,69,140,40,5,161,216,210,131,69,42,116,139,34,246,96,140,241,208,75,193,173,40,82,15,53,74,161,32,10,17,81,168,248,51,154,182,10,254,37,177,5,77,14,166,7,193,99,123,222,157,29,153,58,145,77,118,246,103,218,166,181,224,7,67,216,153,247,102,
190,121,251,230,189,183,147,10,66,8,172,21,172,91,51,76,255,147,45,35,214,219,77,253,235,254,126,15,0,212,174,32,159,252,198,211,47,243,86,131,150,7,236,231,72,107,20,0,100,0,216,80,78,118,28,12,109,234,122,37,243,6,184,100,103,135,247,81,225,235,43,
76,210,136,33,111,247,107,19,97,46,217,31,183,164,185,85,176,104,41,42,43,123,80,145,75,152,124,54,167,180,72,46,137,190,1,0,31,0,120,203,68,150,206,109,79,22,107,182,73,98,158,250,113,213,133,183,241,66,199,183,193,189,116,82,5,0,142,45,51,89,19,204,
100,85,75,178,148,168,84,221,159,76,205,12,132,124,108,231,20,169,234,254,100,120,102,32,228,228,231,247,74,45,5,0,87,202,69,54,28,136,76,164,50,177,102,106,197,94,227,38,50,177,102,41,16,153,80,50,177,230,48,0,180,88,232,199,3,145,9,100,236,200,196,
154,133,200,154,146,2,117,3,78,155,165,11,125,137,236,145,176,70,122,177,70,250,2,145,137,10,172,145,74,172,145,60,214,72,130,233,42,22,250,92,247,178,147,229,201,155,201,170,132,215,16,27,147,176,74,210,117,177,73,106,93,168,139,77,230,177,74,162,88,
37,222,143,253,77,190,186,216,100,194,66,159,251,198,236,100,121,242,34,110,96,185,160,91,125,145,181,120,112,235,6,18,27,67,88,35,193,41,185,113,33,96,79,201,141,62,172,145,40,214,72,122,247,224,187,252,148,220,24,94,97,55,208,121,205,155,236,217,37,
53,41,239,17,86,245,33,172,234,215,147,61,187,8,86,245,31,88,213,61,88,213,59,153,174,108,161,191,208,92,174,101,41,47,18,103,19,168,187,65,146,134,63,200,168,187,65,41,132,46,105,248,195,130,63,163,238,6,217,38,18,112,225,16,211,157,201,106,214,126,
68,179,26,122,209,85,47,31,24,249,24,47,196,204,23,93,245,139,78,10,54,107,185,35,171,219,239,150,18,190,251,252,244,206,187,46,210,109,31,77,24,134,231,84,169,128,174,145,125,134,199,90,167,226,73,196,178,165,112,122,229,169,195,99,159,145,157,192,193,
251,159,254,142,63,59,181,195,113,65,161,208,181,24,60,57,89,231,56,225,209,135,95,42,220,172,43,90,200,0,123,253,113,67,158,167,41,182,211,170,82,115,123,136,220,200,137,90,182,239,196,211,180,82,210,135,30,29,9,210,62,154,114,131,130,243,9,201,137,
88,246,106,219,120,70,121,112,40,224,97,150,12,51,235,198,219,198,51,232,193,161,128,196,158,139,44,140,53,242,198,21,217,101,180,236,60,11,79,133,58,161,96,65,122,200,58,198,90,183,31,63,245,234,107,98,172,117,123,180,244,68,183,141,103,36,87,100,23,
101,89,190,18,106,79,78,207,141,134,106,194,188,87,13,0,148,36,45,98,18,6,178,202,104,168,102,206,32,35,183,39,167,139,194,215,104,168,198,24,45,60,194,100,53,78,154,43,196,72,77,213,173,62,203,23,54,208,158,156,206,199,27,183,21,245,217,145,209,84,93,
40,227,185,77,10,18,27,51,5,118,134,89,250,51,82,191,85,232,142,193,33,1,153,224,54,41,180,220,9,86,121,206,164,191,37,238,4,171,210,28,171,201,76,55,44,178,248,146,211,173,205,169,164,7,172,147,149,139,69,209,224,236,204,119,116,187,122,75,109,129,180,
91,44,185,144,177,57,149,29,55,253,126,56,151,205,118,50,226,127,227,237,77,191,159,18,69,162,119,13,162,217,210,236,179,246,242,29,55,252,126,137,37,0,196,10,25,201,101,197,85,123,195,239,23,34,87,10,179,101,157,117,188,236,235,182,215,89,180,8,75,190,
142,18,181,236,170,194,244,89,115,62,155,69,152,192,60,38,0,171,220,76,87,159,220,203,100,29,136,162,3,129,85,108,143,47,230,178,238,200,94,202,229,162,24,96,8,51,31,94,225,246,24,255,9,141,38,216,254,91,115,109,243,102,31,139,167,142,121,123,25,64,235,
8,116,57,151,179,202,146,246,100,255,53,252,255,107,169,92,88,59,100,1,224,55,203,207,98,195,36,83,120,188,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_mixer_color_png = (const char*) temp_binary_data_102;

//================== Volume.com SVG Conversions 1.02_mixer.png ==================
static const unsigned char temp_binary_data_103[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,2,159,73,68,65,84,88,133,237,153,223,113,226,48,16,198,151,76,222,143,60,236,115,124,21,192,85,128,
169,32,233,32,190,14,156,10,14,42,8,29,28,116,64,42,192,116,64,7,144,231,125,9,21,112,179,204,231,25,141,144,141,228,179,76,60,147,239,197,128,252,231,231,149,246,211,74,12,78,167,19,245,69,119,189,33,253,134,141,168,94,193,222,215,53,50,115,74,68,105,
119,56,180,19,145,117,85,163,211,13,152,121,72,68,122,209,36,54,157,67,91,34,122,22,145,79,187,169,106,24,44,110,4,74,120,238,210,213,112,17,89,102,78,136,104,223,21,89,141,126,138,200,193,108,118,69,54,241,184,145,118,213,156,136,222,137,232,24,9,246,
130,163,54,193,28,82,200,204,126,99,102,206,137,232,173,101,216,11,133,192,110,69,36,5,92,102,188,249,82,68,22,204,172,9,241,183,230,250,87,205,118,227,251,56,244,5,67,96,51,128,234,3,71,136,178,2,255,97,230,169,136,44,97,117,47,21,215,171,45,21,229,
23,102,14,225,60,203,119,82,208,168,30,208,221,10,250,75,163,44,34,9,198,109,153,189,149,30,217,134,124,97,203,136,164,0,55,187,83,109,238,209,58,239,166,176,67,28,15,198,231,82,73,197,231,214,229,11,251,140,163,118,243,136,153,207,227,19,137,166,145,
93,161,61,234,212,236,11,251,168,96,72,144,223,128,223,32,251,245,5,114,76,209,249,87,128,85,169,61,105,82,169,85,13,145,100,3,17,201,208,190,54,198,110,20,133,88,215,15,141,38,51,175,224,173,5,166,102,237,250,89,108,80,106,48,131,17,124,244,197,195,
39,167,166,175,218,66,219,160,252,25,30,189,105,27,54,72,128,168,85,221,75,117,10,123,45,90,208,192,227,156,70,203,154,57,202,55,125,192,3,220,225,163,193,125,130,21,18,89,45,5,83,115,246,66,53,175,158,187,134,223,86,213,5,157,195,170,207,238,152,121,
140,236,127,66,68,23,168,186,114,56,131,237,10,243,174,97,63,116,33,7,227,47,96,99,4,176,55,45,15,81,117,45,236,178,79,68,102,93,195,150,213,84,102,128,154,202,81,121,153,89,189,177,236,109,106,149,136,87,173,202,150,111,130,149,43,77,187,136,41,53,178,
206,139,34,95,216,49,142,85,126,248,110,157,119,83,216,39,157,90,209,141,43,171,237,104,20,48,153,227,218,214,20,226,179,231,213,0,10,151,41,178,92,61,54,193,42,34,131,67,68,83,136,117,77,152,185,128,133,21,230,144,248,138,171,91,194,110,201,158,153,
183,128,77,80,219,186,28,194,86,102,213,9,193,171,138,166,181,193,164,193,246,210,127,207,110,253,222,159,197,120,140,181,37,228,171,163,181,33,226,134,133,162,174,165,60,52,243,222,242,212,121,30,182,212,117,132,245,121,175,90,24,185,26,175,254,181,
228,83,233,183,165,107,43,134,239,255,193,98,169,63,176,68,244,15,19,146,244,85,243,139,109,110,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_mixer_png = (const char*) temp_binary_data_103;

//================== Volume.com SVG Conversions 1.02_Mute.png ==================
static const unsigned char temp_binary_data_104[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,221,73,68,65,84,88,133,237,153,193,81,2,65,16,69,63,150,119,189,252,179,102,32,25,72,8,100,160,100,
64,6,146,129,100,32,70,128,33,96,6,144,1,158,251,34,17,96,13,53,83,53,98,207,194,206,246,12,82,197,175,226,178,203,52,175,102,187,127,247,44,189,237,118,139,115,209,213,217,144,94,96,11,170,8,44,201,219,18,113,205,97,73,246,1,172,173,227,194,26,214,131,
46,0,220,88,198,13,50,131,37,57,44,9,10,43,88,146,207,0,230,37,65,97,1,75,114,12,224,205,6,167,89,215,93,22,147,156,1,120,170,192,185,83,246,206,214,6,69,206,206,122,15,117,133,244,80,6,41,173,86,59,123,74,80,180,129,245,30,186,60,21,40,142,133,141,204,
254,174,203,143,145,156,164,90,177,179,63,111,129,73,169,57,235,225,66,208,123,0,211,174,30,26,21,228,144,228,64,68,190,99,208,96,127,36,33,34,51,45,134,58,124,147,116,187,248,216,5,78,68,122,10,104,208,10,192,14,56,6,141,52,210,128,139,143,136,110,23,
21,139,115,121,191,104,104,40,83,45,86,113,88,17,113,79,105,164,220,114,192,175,202,245,141,219,117,45,86,149,225,219,63,82,13,120,95,27,159,30,75,237,102,181,147,194,17,192,141,160,248,103,199,154,245,161,161,189,26,108,162,234,99,133,162,75,30,137,
82,176,238,81,124,250,207,87,5,208,160,70,224,131,47,57,114,231,129,224,179,254,4,49,87,190,178,241,22,245,162,220,91,137,72,127,255,226,193,52,240,157,102,224,141,60,71,11,101,109,40,166,73,162,232,242,221,160,11,176,178,246,87,213,43,46,241,46,34,234,
140,208,234,93,87,155,148,136,219,109,180,246,3,192,88,179,39,159,215,131,20,104,107,216,54,192,251,176,22,106,109,93,6,57,156,173,44,159,61,21,112,118,83,56,5,112,167,14,86,27,184,115,187,173,9,108,50,27,212,2,54,27,100,34,96,103,250,69,100,58,117,149,
6,54,31,17,125,119,250,51,132,88,232,242,215,82,41,93,96,139,8,192,15,11,127,202,68,176,30,20,88,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Mute_png = (const char*) temp_binary_data_104;

//================== Volume.com SVG Conversions 1.02_On Off.png ==================
static const unsigned char temp_binary_data_105[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,2,128,73,68,65,84,88,133,237,153,205,81,27,65,16,133,27,151,239,235,75,159,33,3,201,17,160,12,80,6,
64,6,235,8,188,25,64,8,114,4,136,8,12,25,136,12,116,239,139,21,129,168,166,158,205,48,234,25,245,204,142,170,44,74,175,74,7,70,108,207,183,173,254,155,221,179,237,118,75,199,162,47,71,67,122,130,61,160,142,10,246,107,75,99,204,60,16,209,207,96,233,89,
68,102,173,236,127,222,48,96,230,111,204,220,183,216,152,153,231,165,215,184,97,153,121,74,68,43,34,186,99,230,155,98,186,143,182,20,244,129,153,151,234,0,239,117,46,88,24,127,34,162,115,44,221,151,108,18,217,210,235,238,241,231,149,218,245,218,218,11,
11,143,62,16,81,23,44,119,193,134,165,234,131,155,86,77,188,192,89,88,102,190,128,71,45,173,42,97,167,198,154,2,47,71,193,18,209,34,242,168,106,67,68,223,69,164,202,179,34,162,33,245,195,248,234,114,95,242,38,97,145,68,151,6,232,76,68,106,189,250,38,
220,168,5,60,228,194,33,231,217,193,88,235,199,130,254,21,128,31,163,229,14,49,109,202,132,69,246,159,71,203,143,34,178,104,1,26,232,6,191,86,168,50,88,24,137,213,164,25,132,18,145,63,70,85,233,82,13,35,5,27,247,115,237,241,235,166,164,239,178,18,213,
7,139,186,26,87,128,214,63,255,63,193,187,207,209,178,85,222,76,207,90,217,216,36,169,50,138,107,249,196,250,87,11,118,103,164,107,85,1,50,114,133,216,255,50,34,186,96,155,14,223,34,50,36,234,115,19,89,158,221,185,203,218,9,171,64,174,211,132,11,214,
107,108,132,46,162,75,227,70,145,132,181,146,169,120,170,47,84,108,223,76,232,29,88,212,189,151,24,246,80,161,128,110,21,215,117,115,92,76,85,131,184,9,100,7,140,145,178,236,142,130,85,245,24,198,155,9,243,107,60,134,38,91,187,9,139,80,248,21,45,119,
158,105,222,43,180,117,171,204,37,75,95,174,41,244,70,86,78,152,121,244,156,0,208,39,35,86,117,12,77,29,163,210,176,240,174,21,79,215,204,188,26,113,186,157,37,64,55,137,209,116,63,44,128,23,70,56,16,6,141,117,201,3,15,141,119,252,42,191,13,80,213,28,
14,74,202,245,48,25,155,92,39,190,222,32,33,53,158,87,225,134,72,200,25,234,232,85,102,139,91,207,41,196,253,228,123,15,112,173,54,56,215,185,242,192,61,117,137,136,198,211,109,170,21,86,232,5,39,101,119,194,22,191,83,8,30,255,212,122,89,111,118,168,
121,238,80,253,2,4,241,56,71,6,155,147,125,36,61,118,47,199,156,144,155,188,173,129,183,167,248,132,37,77,59,209,58,87,59,75,116,122,181,116,40,157,96,15,34,34,122,5,187,174,209,9,217,240,221,53,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_On_Off_png = (const char*) temp_binary_data_105;

//================== Volume.com SVG Conversions 1.02_Pause.png ==================
static const unsigned char temp_binary_data_106[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,0,157,73,68,65,84,88,133,237,217,177,17,131,48,16,68,209,181,135,2,156,168,15,74,176,58,195,149,217,
37,168,4,242,75,232,64,78,72,209,41,217,128,153,255,210,131,155,79,160,68,60,122,239,186,139,231,109,74,137,53,90,178,213,165,148,119,242,200,30,17,251,224,253,151,164,53,217,209,34,226,200,90,210,88,73,223,100,254,145,180,13,230,235,196,142,42,233,151,
133,112,192,92,136,117,33,214,133,88,23,98,93,136,117,33,214,133,88,23,98,93,136,117,33,214,133,88,23,98,93,136,117,33,214,133,88,151,153,203,228,154,204,47,111,189,79,109,98,71,155,249,64,126,45,185,16,107,33,233,15,116,211,23,7,108,112,195,186,0,0,
0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Pause_png = (const char*) temp_binary_data_106;

//================== Volume.com SVG Conversions 1.02_Play-12.png ==================
static const unsigned char temp_binary_data_107[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,42,0,0,0,43,8,6,0,0,0,14,159,26,254,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,188,73,68,65,84,88,133,237,153,193,77,195,64,16,69,127,16,119,184,204,157,14,112,7,49,29,208,65,92,
130,59,192,84,64,168,128,80,1,208,65,168,32,41,1,206,115,73,42,48,90,105,12,201,106,215,89,123,199,198,86,252,36,31,50,145,87,95,227,249,179,227,245,172,44,75,140,129,139,81,168,156,132,118,192,101,236,146,68,148,0,184,182,194,91,102,222,105,202,141,
22,10,96,9,96,110,197,238,0,172,21,214,254,101,50,147,54,231,99,38,0,185,203,76,10,235,30,49,154,157,73,163,61,25,215,39,86,56,103,102,213,172,106,60,250,196,209,158,236,82,136,102,114,189,54,103,213,158,156,200,12,144,203,86,186,102,230,175,65,10,21,
67,45,228,50,204,136,40,53,113,102,126,111,186,88,223,143,222,8,125,35,162,82,218,154,201,252,77,200,141,255,89,163,85,239,93,17,209,142,136,86,146,113,39,67,49,211,149,148,200,224,133,158,164,214,76,226,220,149,252,76,181,167,246,38,120,51,74,68,247,
0,54,0,110,229,90,139,240,16,50,185,14,215,43,172,71,59,151,88,144,153,234,50,106,134,138,189,212,15,14,196,166,1,3,199,194,17,123,8,140,57,241,102,84,26,116,97,133,141,232,13,17,101,158,219,58,163,214,76,204,108,122,221,167,227,175,23,34,202,7,35,84,
200,164,4,108,158,76,239,235,90,96,197,73,161,82,2,190,236,45,28,179,104,39,4,245,81,102,54,153,251,232,67,144,143,38,13,223,87,2,189,16,44,84,154,125,239,110,175,104,180,133,202,120,214,101,9,120,205,217,102,175,55,89,253,142,211,227,228,181,110,184,
110,44,180,195,18,176,55,151,35,90,77,79,204,108,94,47,158,163,165,253,241,120,234,85,37,102,204,43,148,74,96,47,71,151,181,180,22,170,88,2,203,144,241,49,250,236,201,115,226,220,132,160,211,233,233,243,141,54,147,80,85,0,252,0,162,35,130,63,179,207,
40,10,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Play12_png = (const char*) temp_binary_data_107;

//================== Volume.com SVG Conversions 1.02_Play-14.png ==================
static const unsigned char temp_binary_data_108[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,89,73,68,65,84,88,133,237,153,193,77,195,64,16,69,63,136,59,92,254,153,148,64,9,233,128,116,0,29,
0,29,132,14,146,14,220,1,161,2,160,3,232,32,57,207,133,84,96,100,100,75,123,72,98,239,238,204,108,44,249,159,237,209,211,211,238,204,88,190,168,235,26,99,201,229,104,72,39,88,195,12,130,37,57,27,13,44,128,138,228,134,228,141,49,207,201,196,28,131,123,
0,91,146,139,2,156,255,137,61,179,215,0,222,74,89,78,189,96,69,44,231,116,3,119,203,26,173,203,205,178,86,159,117,177,172,61,20,76,45,91,76,48,51,203,150,227,86,221,178,245,110,208,89,174,52,44,123,45,50,15,0,190,73,206,115,138,120,110,93,183,0,62,72,
174,82,45,151,88,17,159,82,45,151,218,103,147,44,151,94,190,163,44,159,195,151,66,103,185,183,197,157,3,236,30,192,139,136,108,250,30,188,242,225,57,154,47,0,143,34,178,29,242,112,41,216,198,230,82,68,86,49,47,149,128,141,178,25,198,19,182,177,249,44,
34,85,106,1,47,216,247,214,230,111,78,17,107,216,125,11,217,123,211,135,196,178,117,53,54,103,90,160,48,50,171,106,51,140,182,217,181,182,205,48,90,102,119,237,217,252,84,170,119,48,26,102,27,155,119,214,160,200,52,235,98,51,76,170,89,55,155,97,98,205,
254,180,83,200,21,178,75,12,236,171,136,44,109,113,78,103,40,236,34,119,84,106,100,250,181,100,149,9,214,36,0,254,0,103,230,114,215,86,80,239,89,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Play14_png = (const char*) temp_binary_data_108;

//================== Volume.com SVG Conversions 1.02_Plus.png ==================
static const unsigned char temp_binary_data_109[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,42,0,0,0,43,8,6,0,0,0,14,159,26,254,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,0,186,73,68,65,84,88,133,237,216,177,13,194,48,16,133,225,23,148,1,210,92,207,8,108,144,100,18,24,129,
13,16,27,176,17,25,129,17,232,111,8,167,113,235,139,37,63,10,163,247,181,103,57,191,20,203,82,50,164,148,208,131,83,23,149,10,253,1,133,178,41,148,173,155,208,145,177,137,153,93,0,188,10,227,187,187,127,90,159,65,9,5,48,1,152,131,89,51,157,81,54,133,
178,41,148,237,127,46,124,51,91,0,44,7,203,206,193,236,150,247,136,108,238,190,53,133,230,200,71,197,186,146,107,229,186,48,84,103,148,77,161,108,221,132,82,126,64,228,235,231,93,24,175,71,87,79,13,189,122,54,133,178,41,148,77,161,108,172,239,250,47,
128,103,48,107,166,95,227,108,10,101,83,40,155,66,169,0,236,56,129,29,210,168,37,200,93,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Plus_png = (const char*) temp_binary_data_109;

//================== Volume.com SVG Conversions 1.02_Save.png ==================
static const unsigned char temp_binary_data_110[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,135,73,68,65,84,88,133,237,153,221,77,195,48,20,70,63,16,239,244,229,190,119,3,186,1,48,1,221,128,
176,65,153,128,178,1,76,208,176,65,153,128,176,65,187,1,60,127,47,48,65,144,165,11,138,130,19,28,231,26,41,224,35,85,106,45,251,250,212,190,254,73,123,80,215,53,166,194,225,100,76,179,108,66,142,44,67,139,72,1,192,189,62,217,145,92,89,197,55,149,5,48,
7,112,106,28,243,139,188,192,82,145,101,83,145,101,83,145,101,83,241,63,100,69,100,38,34,165,30,177,161,245,43,17,57,139,237,51,74,214,117,12,160,2,112,9,96,35,34,139,128,102,165,30,197,79,161,95,176,205,96,89,21,123,1,112,210,40,118,35,54,239,105,227,
68,47,26,69,27,45,75,43,171,162,109,142,1,108,1,204,60,162,133,206,64,72,156,94,162,30,107,116,116,43,149,236,227,189,163,206,3,201,193,169,16,149,179,36,119,0,66,238,169,62,209,125,96,219,111,120,71,86,68,218,133,231,36,43,79,189,53,128,155,1,253,189,
2,88,144,124,11,232,243,150,228,186,89,48,106,159,213,96,143,129,213,93,74,44,125,162,161,88,28,10,133,78,237,143,245,52,125,162,25,45,171,35,181,212,145,235,226,154,228,118,108,95,38,199,45,73,183,13,117,157,76,110,229,223,89,244,99,118,55,208,41,190,
106,21,63,199,108,81,93,152,94,100,72,186,83,233,94,63,238,53,61,204,176,126,20,119,194,43,17,113,111,203,49,43,223,135,185,44,84,56,69,220,124,249,78,197,159,252,97,174,24,115,195,183,34,84,214,119,31,253,117,114,206,166,98,82,178,249,223,154,84,100,
217,84,76,71,22,192,7,182,182,117,240,85,19,205,65,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Save_png = (const char*) temp_binary_data_110;

//================== Volume.com SVG Conversions 1.02_Settings.png ==================
static const unsigned char temp_binary_data_111[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,42,0,0,0,43,8,6,0,0,0,14,159,26,254,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,2,83,73,68,65,84,88,133,237,89,203,113,219,64,12,125,201,228,30,229,128,179,221,65,148,10,66,119,224,
14,108,87,16,165,130,72,21,88,37,136,29,200,21,68,238,192,169,192,206,25,23,169,2,101,144,129,102,86,242,2,11,46,105,205,200,246,187,112,68,16,216,199,93,124,169,15,219,237,22,167,128,143,39,193,242,157,232,11,224,100,136,126,26,194,8,17,157,3,184,54,
196,11,102,126,234,187,198,32,68,1,8,209,95,134,108,5,160,55,209,215,231,163,68,52,34,162,133,92,135,88,152,136,230,68,52,142,62,31,34,170,228,228,8,175,228,218,151,172,188,48,128,31,106,43,68,182,88,153,18,146,95,147,219,127,0,52,0,68,54,5,112,9,224,
179,99,230,14,192,156,153,87,74,242,42,145,109,196,22,51,63,84,19,53,72,238,240,23,192,153,251,150,113,157,34,89,147,104,129,228,75,64,200,142,173,84,102,250,40,51,175,1,184,199,49,48,150,94,190,117,131,137,153,37,137,183,71,32,217,234,90,38,66,109,94,
38,0,178,139,201,174,0,88,235,239,145,6,89,81,175,68,18,29,136,74,229,121,52,196,146,1,46,173,99,83,221,165,227,235,55,204,188,40,113,136,38,252,169,67,178,241,124,75,101,141,62,219,197,246,30,162,59,186,206,228,73,137,210,115,13,186,136,141,145,214,
252,92,190,253,214,57,143,18,145,164,164,239,129,181,103,204,28,218,141,196,246,212,105,94,82,220,51,115,147,222,232,211,148,20,253,106,32,157,255,168,37,186,169,233,49,85,103,83,179,96,45,209,62,133,160,74,183,150,104,196,135,7,213,173,246,209,46,189,
100,31,157,29,114,163,200,66,155,145,20,185,72,157,56,115,146,133,137,113,191,61,24,87,158,249,127,52,143,62,24,149,229,66,122,204,8,67,34,146,116,243,219,16,127,41,229,227,232,209,207,141,251,203,200,113,234,51,75,67,220,70,138,70,248,219,147,81,157,
118,152,105,7,191,62,208,25,233,113,123,73,190,88,149,194,68,131,221,147,224,62,241,239,38,24,225,187,126,193,221,213,200,204,20,37,217,7,69,178,174,143,30,137,36,52,80,221,233,246,117,124,128,40,140,34,173,6,81,180,118,183,142,173,226,209,215,6,211,
222,248,64,68,18,217,183,134,250,94,70,200,216,10,5,83,232,232,15,118,54,55,227,120,233,101,149,146,56,176,21,34,25,38,154,44,112,19,25,196,130,182,126,70,73,162,235,103,199,200,16,214,193,150,85,237,178,120,91,95,156,213,71,47,28,89,111,188,255,207,
52,52,78,131,40,128,127,242,9,32,61,29,54,203,208,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_Settings_png = (const char*) temp_binary_data_111;

//================== Volume.com SVG Conversions 1.02_X.png ==================
static const unsigned char temp_binary_data_112[] =
{ 137,80,78,71,13,10,26,10,0,0,0,13,73,72,68,82,0,0,0,43,0,0,0,43,8,6,0,0,0,225,93,113,192,0,0,0,9,112,72,89,115,0,0,23,17,0,0,23,17,1,202,38,243,63,0,0,1,157,73,68,65,84,88,133,237,217,205,109,2,49,16,5,224,151,40,5,112,216,57,147,14,72,9,116,66,74,72,
9,148,64,9,161,19,74,128,14,146,179,47,233,96,35,71,94,105,181,194,227,249,243,129,104,159,132,56,224,245,124,90,121,205,24,158,198,113,196,163,228,249,97,164,43,182,99,86,108,175,252,63,44,17,109,136,232,74,68,159,81,133,243,92,101,206,141,244,154,38,
182,76,118,1,176,3,112,136,0,151,57,14,101,206,139,20,204,98,23,208,41,46,240,12,58,69,12,174,98,43,80,23,248,14,84,5,230,238,236,71,5,58,69,5,102,160,83,118,165,102,53,108,111,32,40,144,115,78,41,189,59,161,162,121,216,53,91,46,62,55,138,176,119,56,
10,10,201,110,224,1,71,66,33,221,103,45,224,104,40,90,107,118,25,41,160,188,135,66,213,88,200,193,173,168,161,176,244,6,194,37,193,197,4,53,97,225,3,155,161,240,116,93,6,176,11,10,203,154,93,134,136,190,0,108,27,195,190,83,74,175,174,66,222,126,182,60,
108,45,104,206,54,162,91,51,99,13,187,130,187,189,52,97,29,219,151,11,172,198,6,236,179,102,176,10,171,248,6,115,53,63,181,188,68,67,167,237,137,136,208,24,159,193,208,108,103,210,3,163,186,41,137,104,47,151,145,28,24,205,221,83,52,184,117,96,116,183,
121,145,96,238,192,120,140,106,243,20,224,35,55,128,187,179,39,0,55,230,115,213,119,189,0,124,43,53,171,169,98,83,74,63,0,246,21,176,169,41,97,192,185,198,190,212,212,99,25,176,171,123,186,3,22,65,33,237,186,102,63,120,92,189,109,222,108,206,252,64,189,
73,161,127,201,88,201,107,24,134,141,116,108,175,57,215,63,64,122,101,197,246,202,138,237,18,0,191,222,219,106,176,108,246,29,195,0,0,0,0,73,69,78,68,174,66,96,130,0,0 };

const char* Volume_com_SVG_Conversions_1_02_X_png = (const char*) temp_binary_data_112;


const char* getNamedResource (const char* resourceNameUTF8, int& numBytes);
const char* getNamedResource (const char* resourceNameUTF8, int& numBytes)
{
    unsigned int hash = 0;

    if (resourceNameUTF8 != nullptr)
        while (*resourceNameUTF8 != 0)
            hash = 31 * hash + (unsigned int) *resourceNameUTF8++;

    switch (hash)
    {
        case 0xd45f598f:  numBytes = 447; return icon_audio_output_oj_svg;
        case 0x167a7678:  numBytes = 546; return icon_branched_oj_svg;
        case 0xdef5c33d:  numBytes = 274; return icon_Check_oj_svg;
        case 0x3959eae9:  numBytes = 406; return icon_Clip_oj_svg;
        case 0xb496c87f:  numBytes = 1074; return icon_configure_oj_svg;
        case 0x3269ddae:  numBytes = 396; return icon_Delete_oj_svg;
        case 0x605c7aef:  numBytes = 372; return icon_Edit_oj_svg;
        case 0x1730dbeb:  numBytes = 275; return icon_info_oj_svg;
        case 0x4823c920:  numBytes = 400; return icon_inline_oj_svg;
        case 0xd18ef493:  numBytes = 356; return icon_Load_oj_svg;
        case 0x1c6a4633:  numBytes = 583; return icon_metronome_oj_svg;
        case 0x03391ad5:  numBytes = 271; return icon_Minus_oj_svg;
        case 0xd82414fc:  numBytes = 857; return icon_mixer_oj_svg;
        case 0x01309016:  numBytes = 258; return icon_mono_oj_svg;
        case 0x8921b800:  numBytes = 449; return icon_Mute_oj_svg;
        case 0xfe2b3551:  numBytes = 528; return icon_muteM_oj_svg;
        case 0x758043ea:  numBytes = 320; return icon_On_Off_oj_svg;
        case 0x4a513a8f:  numBytes = 283; return icon_Pause_oj_svg;
        case 0xcdbe1a85:  numBytes = 233; return icon_Play_oj_svg;
        case 0xf3cfb95f:  numBytes = 298; return icon_Plus_oj_svg;
        case 0x3f30b46a:  numBytes = 441; return icon_refresh_oj_svg;
        case 0x1949f01c:  numBytes = 432; return icon_Save_oj_svg;
        case 0xb20eb1b6:  numBytes = 607; return icon_Settings_oj_svg;
        case 0x5603e29a:  numBytes = 693; return icon_solo_oj_svg;
        case 0x2423dae1:  numBytes = 445; return icon_stereo_oj_svg;
        case 0x40219217:  numBytes = 413; return icon_stop_oj_svg;
        case 0x80f62d26:  numBytes = 595; return icon_Touch_oj_svg;
        case 0x73d1eccd:  numBytes = 334; return icon_X_oj_svg;
        case 0x134f3312:  numBytes = 413; return icon__stop_red_svg;
        case 0x4df3a407:  numBytes = 447; return icon_audio_output_red_svg;
        case 0x4f3c243e:  numBytes = 546; return icon_branched_red_svg;
        case 0x962a7019:  numBytes = 274; return icon_Check_red_svg;
        case 0x884b3ded:  numBytes = 406; return icon_Clip_red_svg;
        case 0x74aa1317:  numBytes = 1074; return icon_configure_red_svg;
        case 0xb139a3c8:  numBytes = 396; return icon_Delete_red_svg;
        case 0x419aaea7:  numBytes = 372; return icon_Edit_red_svg;
        case 0x65526d2b:  numBytes = 275; return icon_info_red_svg;
        case 0x52bd2696:  numBytes = 400; return icon_inline_red_svg;
        case 0xf6b76983:  numBytes = 356; return icon_Load_red_svg;
        case 0x07464be3:  numBytes = 562; return icon_metronome_red_svg;
        case 0xfa520b81:  numBytes = 271; return icon_Minus_red_svg;
        case 0xc2c6563a:  numBytes = 857; return icon_mixer_red_svg;
        case 0xbb493e60:  numBytes = 258; return icon_mono_red_svg;
        case 0x317d13b6:  numBytes = 449; return icon_Mute_red_svg;
        case 0x5da34085:  numBytes = 527; return icon_muteM_red_svg;
        case 0xd0f0050c:  numBytes = 320; return icon_On_Off_red_svg;
        case 0x963de307:  numBytes = 283; return icon_Pause_red_svg;
        case 0x806d01d1:  numBytes = 233; return icon_Play_red_svg;
        case 0x1c8f3e37:  numBytes = 298; return icon_Plus_red_svg;
        case 0x3d4da48c:  numBytes = 439; return icon_refresh_red_svg;
        case 0xa65bdf1a:  numBytes = 432; return icon_Save_red_svg;
        case 0x262f50c0:  numBytes = 607; return icon_Settings_red_svg;
        case 0x00e03c5c:  numBytes = 691; return icon_solo_red_svg;
        case 0xf6bf4cf5:  numBytes = 445; return icon_stereo_red_svg;
        case 0x34374350:  numBytes = 595; return icon_Touch_red_svg;
        case 0x9cd37889:  numBytes = 334; return icon_X_red_svg;
        case 0xcdb6555f:  numBytes = 1520; return app_App_Icon_Black_circle_svg;
        case 0xe8b92c28:  numBytes = 1512; return app_App_Icon_Black_rounded_svg;
        case 0x8c85032c:  numBytes = 1509; return app_App_Icon_Black_square_svg;
        case 0x33d53d35:  numBytes = 447; return icon_audio_output_svg;
        case 0x46d2699d:  numBytes = 447; return icon_audio_output_svg2;
        case 0xb7dc70ec:  numBytes = 538; return icon_branched_svg;
        case 0x5ce3f207:  numBytes = 455; return icon_browser_svg;
        case 0x6768e247:  numBytes = 274; return icon_Check_svg;
        case 0x9421ba1b:  numBytes = 406; return icon_Clip_svg;
        case 0x80bdb445:  numBytes = 1018; return icon_configure_svg;
        case 0x94759237:  numBytes = 1018; return icon_configure2_svg;
        case 0x0dcb4576:  numBytes = 396; return icon_Delete_svg;
        case 0xbcea97d5:  numBytes = 372; return icon_Edit_svg;
        case 0x642e3859:  numBytes = 275; return icon_info_svg;
        case 0x5c2c5f44:  numBytes = 398; return icon_inline_svg;
        case 0xcb6f20b1:  numBytes = 399; return icon_Load_svg;
        case 0x808e3311:  numBytes = 512; return icon_metronome_svg;
        case 0x953671af:  numBytes = 271; return icon_Minus_svg;
        case 0x136700e8:  numBytes = 857; return icon_mixer_svg;
        case 0x4bba030c:  numBytes = 1569; return icon_mixer_color_svg;
        case 0x463fec0e:  numBytes = 258; return icon_mono_svg;
        case 0x912fdc64:  numBytes = 449; return icon_Mute_svg;
        case 0x1c52c8b3:  numBytes = 464; return icon_muteM_svg;
        case 0xd94ce8ba:  numBytes = 320; return icon_On_Off_svg;
        case 0x8116fc35:  numBytes = 283; return icon_Pause_svg;
        case 0xcd690fff:  numBytes = 233; return icon_Play_svg;
        case 0xef356f65:  numBytes = 298; return icon_Plus_svg;
        case 0x3866483a:  numBytes = 385; return icon_refresh_svg;
        case 0xe1e3f9c8:  numBytes = 432; return icon_Save_svg;
        case 0x00ce2e6e:  numBytes = 607; return icon_Settings_svg;
        case 0xb21d480a:  numBytes = 620; return icon_solo_svg;
        case 0x56c68d23:  numBytes = 454; return icon_stereo_svg;
        case 0xbfc911ad:  numBytes = 234; return icon_stop_svg;
        case 0x6555e8fe:  numBytes = 595; return icon_Touch_svg;
        case 0xf547a2b7:  numBytes = 334; return icon_X_svg;
        case 0x31babbe6:  numBytes = 11161; return Volume_Logo_Final_Version_1_0_App_Icon_Black_circle_png;
        case 0x0543056b:  numBytes = 8882; return Volume_Logo_Final_Version_1_0_App_Icon_Black_rounded_png;
        case 0xf08969b3:  numBytes = 7961; return Volume_Logo_Final_Version_1_0_App_Icon_Black_square_png;
        case 0x41e9e283:  numBytes = 641; return Volume_com_SVG_Conversions_1_02_audio_output_png;
        case 0x36f2dca3:  numBytes = 555; return Volume_com_SVG_Conversions_1_02_Check_png;
        case 0x5080f869:  numBytes = 339; return Volume_com_SVG_Conversions_1_02_Clip_png;
        case 0x2f820584:  numBytes = 396; return Volume_com_SVG_Conversions_1_02_Delete_png;
        case 0x7949d623:  numBytes = 562; return Volume_com_SVG_Conversions_1_02_Edit_png;
        case 0x208d76a7:  numBytes = 620; return Volume_com_SVG_Conversions_1_02_info_png;
        case 0x87ce5eff:  numBytes = 441; return Volume_com_SVG_Conversions_1_02_Load_png;
        case 0x64c06c0b:  numBytes = 191; return Volume_com_SVG_Conversions_1_02_Minus_png;
        case 0x64f47028:  numBytes = 1113; return Volume_com_SVG_Conversions_1_02_mixer_color_png;
        case 0xe2f0fb44:  numBytes = 749; return Volume_com_SVG_Conversions_1_02_mixer_png;
        case 0x4d8f1ab2:  numBytes = 555; return Volume_com_SVG_Conversions_1_02_Mute_png;
        case 0xfb03a8c8:  numBytes = 718; return Volume_com_SVG_Conversions_1_02_On_Off_png;
        case 0x50a0f691:  numBytes = 235; return Volume_com_SVG_Conversions_1_02_Pause_png;
        case 0xe6feaf2e:  numBytes = 522; return Volume_com_SVG_Conversions_1_02_Play12_png;
        case 0xe71ade30:  numBytes = 423; return Volume_com_SVG_Conversions_1_02_Play14_png;
        case 0xab94adb3:  numBytes = 264; return Volume_com_SVG_Conversions_1_02_Plus_png;
        case 0x9e433816:  numBytes = 469; return Volume_com_SVG_Conversions_1_02_Save_png;
        case 0x9003003c:  numBytes = 673; return Volume_com_SVG_Conversions_1_02_Settings_png;
        case 0xd7128093:  numBytes = 491; return Volume_com_SVG_Conversions_1_02_X_png;
        default: break;
    }

    numBytes = 0;
    return nullptr;
}

const char* namedResourceList[] =
{
    "icon_audio_output_oj_svg",
    "icon_branched_oj_svg",
    "icon_Check_oj_svg",
    "icon_Clip_oj_svg",
    "icon_configure_oj_svg",
    "icon_Delete_oj_svg",
    "icon_Edit_oj_svg",
    "icon_info_oj_svg",
    "icon_inline_oj_svg",
    "icon_Load_oj_svg",
    "icon_metronome_oj_svg",
    "icon_Minus_oj_svg",
    "icon_mixer_oj_svg",
    "icon_mono_oj_svg",
    "icon_Mute_oj_svg",
    "icon_muteM_oj_svg",
    "icon_On_Off_oj_svg",
    "icon_Pause_oj_svg",
    "icon_Play_oj_svg",
    "icon_Plus_oj_svg",
    "icon_refresh_oj_svg",
    "icon_Save_oj_svg",
    "icon_Settings_oj_svg",
    "icon_solo_oj_svg",
    "icon_stereo_oj_svg",
    "icon_stop_oj_svg",
    "icon_Touch_oj_svg",
    "icon_X_oj_svg",
    "icon__stop_red_svg",
    "icon_audio_output_red_svg",
    "icon_branched_red_svg",
    "icon_Check_red_svg",
    "icon_Clip_red_svg",
    "icon_configure_red_svg",
    "icon_Delete_red_svg",
    "icon_Edit_red_svg",
    "icon_info_red_svg",
    "icon_inline_red_svg",
    "icon_Load_red_svg",
    "icon_metronome_red_svg",
    "icon_Minus_red_svg",
    "icon_mixer_red_svg",
    "icon_mono_red_svg",
    "icon_Mute_red_svg",
    "icon_muteM_red_svg",
    "icon_On_Off_red_svg",
    "icon_Pause_red_svg",
    "icon_Play_red_svg",
    "icon_Plus_red_svg",
    "icon_refresh_red_svg",
    "icon_Save_red_svg",
    "icon_Settings_red_svg",
    "icon_solo_red_svg",
    "icon_stereo_red_svg",
    "icon_Touch_red_svg",
    "icon_X_red_svg",
    "app_App_Icon_Black_circle_svg",
    "app_App_Icon_Black_rounded_svg",
    "app_App_Icon_Black_square_svg",
    "icon_audio_output_svg",
    "icon_audio_output_svg2",
    "icon_branched_svg",
    "icon_browser_svg",
    "icon_Check_svg",
    "icon_Clip_svg",
    "icon_configure_svg",
    "icon_configure2_svg",
    "icon_Delete_svg",
    "icon_Edit_svg",
    "icon_info_svg",
    "icon_inline_svg",
    "icon_Load_svg",
    "icon_metronome_svg",
    "icon_Minus_svg",
    "icon_mixer_svg",
    "icon_mixer_color_svg",
    "icon_mono_svg",
    "icon_Mute_svg",
    "icon_muteM_svg",
    "icon_On_Off_svg",
    "icon_Pause_svg",
    "icon_Play_svg",
    "icon_Plus_svg",
    "icon_refresh_svg",
    "icon_Save_svg",
    "icon_Settings_svg",
    "icon_solo_svg",
    "icon_stereo_svg",
    "icon_stop_svg",
    "icon_Touch_svg",
    "icon_X_svg",
    "Volume_Logo_Final_Version_1_0_App_Icon_Black_circle_png",
    "Volume_Logo_Final_Version_1_0_App_Icon_Black_rounded_png",
    "Volume_Logo_Final_Version_1_0_App_Icon_Black_square_png",
    "Volume_com_SVG_Conversions_1_02_audio_output_png",
    "Volume_com_SVG_Conversions_1_02_Check_png",
    "Volume_com_SVG_Conversions_1_02_Clip_png",
    "Volume_com_SVG_Conversions_1_02_Delete_png",
    "Volume_com_SVG_Conversions_1_02_Edit_png",
    "Volume_com_SVG_Conversions_1_02_info_png",
    "Volume_com_SVG_Conversions_1_02_Load_png",
    "Volume_com_SVG_Conversions_1_02_Minus_png",
    "Volume_com_SVG_Conversions_1_02_mixer_color_png",
    "Volume_com_SVG_Conversions_1_02_mixer_png",
    "Volume_com_SVG_Conversions_1_02_Mute_png",
    "Volume_com_SVG_Conversions_1_02_On_Off_png",
    "Volume_com_SVG_Conversions_1_02_Pause_png",
    "Volume_com_SVG_Conversions_1_02_Play12_png",
    "Volume_com_SVG_Conversions_1_02_Play14_png",
    "Volume_com_SVG_Conversions_1_02_Plus_png",
    "Volume_com_SVG_Conversions_1_02_Save_png",
    "Volume_com_SVG_Conversions_1_02_Settings_png",
    "Volume_com_SVG_Conversions_1_02_X_png"
};

const char* originalFilenames[] =
{
    "icon_audio_output_oj.svg",
    "icon_branched_oj.svg",
    "icon_Check_oj.svg",
    "icon_Clip_oj.svg",
    "icon_configure_oj.svg",
    "icon_Delete_oj.svg",
    "icon_Edit_oj.svg",
    "icon_info_oj.svg",
    "icon_inline_oj.svg",
    "icon_Load_oj.svg",
    "icon_metronome_oj.svg",
    "icon_Minus_oj.svg",
    "icon_mixer_oj.svg",
    "icon_mono_oj.svg",
    "icon_Mute_oj.svg",
    "icon_muteM_oj.svg",
    "icon_On_Off_oj.svg",
    "icon_Pause_oj.svg",
    "icon_Play_oj.svg",
    "icon_Plus_oj.svg",
    "icon_refresh_oj.svg",
    "icon_Save_oj.svg",
    "icon_Settings_oj.svg",
    "icon_solo_oj.svg",
    "icon_stereo_oj.svg",
    "icon_stop_oj.svg",
    "icon_Touch_oj.svg",
    "icon_X_oj.svg",
    "icon_ stop_red.svg",
    "icon_audio_output_red.svg",
    "icon_branched_red.svg",
    "icon_Check_red.svg",
    "icon_Clip_red.svg",
    "icon_configure_red.svg",
    "icon_Delete_red.svg",
    "icon_Edit_red.svg",
    "icon_info_red.svg",
    "icon_inline_red.svg",
    "icon_Load_red.svg",
    "icon_metronome_red.svg",
    "icon_Minus_red.svg",
    "icon_mixer_red.svg",
    "icon_mono_red.svg",
    "icon_Mute_red.svg",
    "icon_muteM_red.svg",
    "icon_On_Off_red.svg",
    "icon_Pause_red.svg",
    "icon_Play_red.svg",
    "icon_Plus_red.svg",
    "icon_refresh red.svg",
    "icon_Save_red.svg",
    "icon_Settings_red.svg",
    "icon_solo_red.svg",
    "icon_stereo_red.svg",
    "icon_Touch_red.svg",
    "icon_X_red.svg",
    "app_App Icon Black circle.svg",
    "app_App Icon Black rounded.svg",
    "app_App Icon Black square.svg",
    "icon_audio output.svg",
    "icon_audio_output.svg",
    "icon_branched.svg",
    "icon_browser.svg",
    "icon_Check.svg",
    "icon_Clip.svg",
    "icon_configure.svg",
    "icon_configure2.svg",
    "icon_Delete.svg",
    "icon_Edit.svg",
    "icon_info.svg",
    "icon_inline.svg",
    "icon_Load.svg",
    "icon_metronome.svg",
    "icon_Minus.svg",
    "icon_mixer.svg",
    "icon_mixer_color.svg",
    "icon_mono.svg",
    "icon_Mute.svg",
    "icon_muteM.svg",
    "icon_On_Off.svg",
    "icon_Pause.svg",
    "icon_Play.svg",
    "icon_Plus.svg",
    "icon_refresh.svg",
    "icon_Save.svg",
    "icon_Settings.svg",
    "icon_solo.svg",
    "icon_stereo.svg",
    "icon_stop.svg",
    "icon_Touch.svg",
    "icon_X.svg",
    "Volume Logo Final Version 1.0_App Icon Black circle.png",
    "Volume Logo Final Version 1.0_App Icon Black rounded.png",
    "Volume Logo Final Version 1.0_App Icon Black square.png",
    "Volume.com SVG Conversions 1.02_audio output.png",
    "Volume.com SVG Conversions 1.02_Check.png",
    "Volume.com SVG Conversions 1.02_Clip.png",
    "Volume.com SVG Conversions 1.02_Delete.png",
    "Volume.com SVG Conversions 1.02_Edit.png",
    "Volume.com SVG Conversions 1.02_info.png",
    "Volume.com SVG Conversions 1.02_Load.png",
    "Volume.com SVG Conversions 1.02_Minus.png",
    "Volume.com SVG Conversions 1.02_mixer color.png",
    "Volume.com SVG Conversions 1.02_mixer.png",
    "Volume.com SVG Conversions 1.02_Mute.png",
    "Volume.com SVG Conversions 1.02_On Off.png",
    "Volume.com SVG Conversions 1.02_Pause.png",
    "Volume.com SVG Conversions 1.02_Play-12.png",
    "Volume.com SVG Conversions 1.02_Play-14.png",
    "Volume.com SVG Conversions 1.02_Plus.png",
    "Volume.com SVG Conversions 1.02_Save.png",
    "Volume.com SVG Conversions 1.02_Settings.png",
    "Volume.com SVG Conversions 1.02_X.png"
};

const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8)
{
    for (unsigned int i = 0; i < (sizeof (namedResourceList) / sizeof (namedResourceList[0])); ++i)
    {
        if (namedResourceList[i] == resourceNameUTF8)
            return originalFilenames[i];
    }

    return nullptr;
}

}
