/*
  ==============================================================================

    AppButtonCls.cpp
    Created: 10 Jul 2020 2:20:29pm
    Author:  RussB

  ==============================================================================
*/

#include "AppInc.h"



//==============================================================================

#if 0
void AppButtonCls::paint (juce::Graphics& g)
{
	juce::Rectangle<int> controlArea = getLocalBounds();

	if (juce::Button::getClickingTogglesState())
	{
        // set the background to the selected color
        g.fillAll(UI_Color2);
	}
    else
	{
		if (m_bFillBackground == true)
		{
			// clear the background
			//g.fillAll(CtrlBackgroundColor);
			g.fillAll(UI_Color3);
		}
	}

	if (m_bDrawBorder == true)
	{
		g.setColour (UI_Color3);
		g.drawRect (getLocalBounds(), 1);   // draw an outline around the component
	}

	if (m_icon == "")
		return;

	unsigned long w = controlArea.getWidth();
	unsigned long h = controlArea.getHeight();

	unsigned iconSize = 0;

	float tmpX = ((float) w * 0.9f);
	float tmpY = ((float) h * 0.8f);

	if (tmpY < tmpX)
		iconSize = (unsigned int)tmpY;
	else
		iconSize = (unsigned int)tmpX;

	int x = (int) (((float) w / 2) - ((float) iconSize / 2)) + 1;
	int y = (int) (((float) h / 2) - ((float) iconSize / 2)) + 1;

	// draw  graphic
	Utils::drawGraphicFromSvg(m_icon, g, x, y, iconSize, iconSize);

}
#endif


void AppButtonCls::paintButton
	(
		juce::Graphics& g,
		bool shouldDrawButtonAsHighlighted,
		bool shouldDrawButtonAsDown
	) 
{
#if 1
	juce::Colour bgColor = findColour(juce::TextButton::ColourIds::buttonColourId);

	drawButtonBackground(g, *this, bgColor, shouldDrawButtonAsHighlighted, shouldDrawButtonAsDown);

	if (m_icon == "")
		return;

	juce::Rectangle<int> controlArea = getLocalBounds();

	unsigned long w = controlArea.getWidth();
	unsigned long h = controlArea.getHeight();

	unsigned iconSize = 0;

	float tmpX = ((float) w * m_iconScalingFactor);
	float tmpY = ((float) h * m_iconScalingFactor);

	if (tmpY < tmpX)
		iconSize = (unsigned int) tmpY;
	else
		iconSize = (unsigned int) tmpX;

	bool selected = getToggleState();

	// draw  graphic
	//if (m_selected == true && m_selectedIcon != "")
	if (selected == true && m_selectedIcon != "")
	{
		Utils::drawGraphicFromSvg
			(
				m_selectedIcon,
				g,
				(((w / 2) - (iconSize / 2)) + m_offset_X),
				(((h / 2) - (iconSize / 2)) + m_offset_Y),
				iconSize,
				iconSize
			);
	}
	else
	{
		Utils::drawGraphicFromSvg
			(
				m_icon,
				g, 
				(((w / 2) - (iconSize / 2)) + m_offset_X),
				(((h / 2) - (iconSize / 2)) + m_offset_Y), 
				iconSize,
				iconSize
			);
	}
#endif
}



