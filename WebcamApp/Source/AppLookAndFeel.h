/*
  ==============================================================================

    AppLookAndFeel.h
    Created: 14 Jul 2020 1:45:59pm
    Author:  RussB

  ==============================================================================
*/

#pragma once


class AppLookAndFeelCls :
    //public LookAndFeel_V2
    public juce::LookAndFeel_V3
{

public:
    AppLookAndFeelCls()
    {
        //setColour(DocumentWindow::ColourIds::textColourId, UI_Color2);

        setColour(juce::ResizableWindow::ColourIds::backgroundColourId, CtrlBackgroundColor);

        setColour(juce::GroupComponent::ColourIds::textColourId, juce::Colours::white);
        setColour(juce::GroupComponent::ColourIds::outlineColourId, UI_Color3);

        setColour(juce::Toolbar::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::Toolbar::ColourIds::separatorColourId, UI_Color3);
        setColour(juce::Toolbar::ColourIds::labelTextColourId, juce::Colours::white);

        setColour(juce::PropertyComponent::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::PropertyComponent::ColourIds::labelTextColourId, juce::Colours::white);

        setColour(juce::BubbleComponent::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::BubbleComponent::ColourIds::outlineColourId, UI_Color3);

        setColour(juce::TreeView::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::TreeView::ColourIds::linesColourId, UI_Color3);
        setColour(juce::TreeView::ColourIds::selectedItemBackgroundColourId, UI_Color1);

        //setColour(juce::Button::ColourIds::tickColourId, Button_Background_Color);

        //setColour(juce::ToggleButton::ColourIds::tickColourId, Button_Background_Color);
        setColour(juce::ToggleButton::ColourIds::tickColourId, UI_Color2);
        setColour(juce::ToggleButton::ColourIds::textColourId, juce::Colours::white);
        setColour(juce::ToggleButton::ColourIds::tickDisabledColourId, UI_Color3);

        setColour(juce::TextButton::ColourIds::buttonColourId, Button_Background_Color);
        setColour(juce::TextButton::ColourIds::buttonOnColourId, Button_Background_Color);
        setColour(ButtonOutlineColorID, UI_Color3);
        setColour(juce::TextButton::ColourIds::textColourOffId, juce::Colours::black);
        setColour(juce::TextButton::ColourIds::textColourOnId, juce::Colours::white);

        //setColour(juce::Label::ColourIds::backgroundColourId, CtrlBackgroundColor);
        //setColour(juce::Label::ColourIds::backgroundColourId, juce::Colours::darkgrey);
        setColour(juce::Label::ColourIds::outlineColourId, Color_extremedarkgrey);
        setColour(juce::Label::ColourIds::outlineWhenEditingColourId, UI_Color2);
        setColour(juce::Label::ColourIds::textColourId, juce::Colours::white);

        setColour(juce::ProgressBar::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::ProgressBar::ColourIds::foregroundColourId, UI_Color1);

        //setColour(juce::ScrollBar::ColourIds::backgroundColourId, CtrlBackgroundColor);
        setColour(juce::ScrollBar::ColourIds::backgroundColourId, juce::Colours::darkgrey);
        setColour(juce::ScrollBar::ColourIds::trackColourId, UI_Color3);
        setColour(juce::ScrollBar::ColourIds::thumbColourId, UI_Color1);

        setColour(juce::TextEditor::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::TextEditor::ColourIds::outlineColourId, UI_Color3);
        setColour(juce::TextEditor::ColourIds::highlightColourId, UI_Color2);
        setColour(juce::TextEditor::ColourIds::highlightedTextColourId, UI_Color1);
        setColour(juce::TextEditor::ColourIds::focusedOutlineColourId, UI_Color1);
        setColour(juce::TextEditor::ColourIds::shadowColourId, juce::Colours::black);
        setColour(juce::TextEditor::ColourIds::textColourId, juce::Colours::white);

        setColour(juce::ListBox::ColourIds::backgroundColourId, Color_extremedarkgrey);
        //setColour(juce::ListBox::ColourIds::backgroundColourId, juce::Colours::black);
        setColour(juce::ListBox::ColourIds::outlineColourId, UI_Color3);
        setColour(juce::ListBox::ColourIds::textColourId, juce::Colours::white);

        setColour(juce::ComboBox::ColourIds::backgroundColourId, Color_extremedarkgrey);
        setColour(juce::ComboBox::ColourIds::outlineColourId, UI_Color3);
        setColour(juce::ComboBox::ColourIds::buttonColourId, UI_Color2);
        setColour(juce::ComboBox::ColourIds::focusedOutlineColourId, UI_Color1);
        setColour(juce::ComboBox::ColourIds::textColourId, juce::Colours::white);

        setColour(juce::PopupMenu::ColourIds::backgroundColourId, AppBackgroundColor);
        //setColour(juce::PopupMenu::ColourIds::backgroundColourId, Color_verydarkorange);
        setColour(juce::PopupMenu::ColourIds::textColourId, juce::Colours::white);
        setColour(juce::PopupMenu::ColourIds::highlightedBackgroundColourId, UI_Color3);
        setColour(juce::PopupMenu::ColourIds::highlightedTextColourId, UI_Color1);
        setColour(juce::PopupMenu::ColourIds::headerTextColourId, UI_Color2);
        setColour(juce::PopupMenu::ColourIds::textColourId, juce::Colours::white);

        setColour(juce::Slider::ColourIds::backgroundColourId, CtrlBackgroundColor);
        setColour(juce::Slider::ColourIds::thumbColourId, UI_Color1);
        setColour(juce::Slider::ColourIds::trackColourId, UI_Color2);
        setColour(juce::Slider::ColourIds::rotarySliderFillColourId, UI_Color2);
        setColour(juce::Slider::ColourIds::rotarySliderOutlineColourId, juce::Colours::black);
        setColour(juce::Slider::ColourIds::textBoxBackgroundColourId, UI_Color3);
        setColour(juce::Slider::ColourIds::textBoxTextColourId, juce::Colours::white);
        setColour(juce::Slider::ColourIds::textBoxHighlightColourId, UI_Color1);
        setColour(juce::Slider::ColourIds::textBoxOutlineColourId, UI_Color3);

        setColour(juce::TooltipWindow::ColourIds::backgroundColourId, juce::Colours::black);
        setColour(juce::TooltipWindow::ColourIds::textColourId, juce::Colours::white);
        setColour(juce::TooltipWindow::ColourIds::outlineColourId, UI_Color3);

        setColour(juce::TabbedButtonBar::ColourIds::tabOutlineColourId, UI_Color3);
        setColour(juce::TabbedButtonBar::ColourIds::frontOutlineColourId, UI_Color1);
        setColour(juce::TabbedButtonBar::ColourIds::tabTextColourId, juce::Colours::white);
        setColour(juce::TabbedButtonBar::ColourIds::frontTextColourId, juce::Colours::white);

        setColour(juce::AlertWindow::ColourIds::textColourId, juce::Colours::white);
        setColour(juce::AlertWindow::ColourIds::outlineColourId, UI_Color3);
        setColour(juce::AlertWindow::ColourIds::backgroundColourId, CtrlBackgroundColor);

#if 1
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::windowBackground, AppBackgroundColor);
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::widgetBackground, CtrlBackgroundColor);
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::defaultFill, CtrlBackgroundColor);
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::highlightedFill, UI_Color2);
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::menuBackground, Color_verydarkorange);
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::outline, UI_Color3);
        setColour(juce::LookAndFeel_V4::ColourScheme::UIColour::defaultText, juce::Colours::white);
#endif
    }

    ~AppLookAndFeelCls()
    {

    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AppLookAndFeelCls)
};


