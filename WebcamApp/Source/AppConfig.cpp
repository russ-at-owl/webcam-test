/*
  ==============================================================================

    AppConfig.cpp
    Created: 2 Aug 2020 8:06:36pm
    Author:  RussB

  ==============================================================================
*/

#include "AppInc.h"





//*********************************************************************************

AppConfigMgr::AppConfigMgr() :
    m_pPropFile(nullptr)
{
    try
    {
        m_appOptions.applicationName = AppName;
        m_appOptions.filenameSuffix = (".settings");
        m_appOptions.storageFormat = juce::PropertiesFile::StorageFormat::storeAsXML;

#if JUCE_MAC || JUCE_IOS
        m_appOptions.osxLibrarySubFolder = ("Application Support/Volume.com");
#endif

        m_appProperties.setStorageParameters(m_appOptions);

        m_currentPropSet.addAllPropertiesFrom(*m_appProperties.getUserSettings());

        m_appOptions.getDefaultFile();

        //*****************************************************

        m_nSelectedWebcamDev = 0;

        m_videoFormatSelect = eVideoFormatSelect::VideoFormat_any;

    }
    catch (...)
    {

    }
}


AppConfigMgr::~AppConfigMgr()
{
    try
    {
        m_appProperties.closeFiles();

        if (m_pPropFile != nullptr)
        {
            m_pPropFile->removeAllChangeListeners();

            m_pPropFile->clear();
        }

        m_pPropFile.release();

        m_currentPropSet.setFallbackPropertySet(nullptr);

        m_currentPropSet.clear();
    }
    catch (...)
    {

    }
}



