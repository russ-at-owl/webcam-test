/*
  ==============================================================================

    This file contains the basic startup code for a JUCE application.

  ==============================================================================
*/

//#include <JuceHeader.h>
//#include "MainComponent.h"

#include "AppInc.h"


//==============================================================================
class WebcamApplication  : 
    public juce::JUCEApplication
{
    JuceWindowMgr_def           m_windowMgr;

public:
    //==============================================================================
    WebcamApplication() {}

    bool moreThanOneInstanceAllowed() override
    {
        return false;
    }

    JuceWindowMgr_def* getWindowMgr()
    {
        return &m_windowMgr;
    }

    const juce::String getApplicationName() override       
    { 
        return ProjectInfo::projectName; 
    }
    
    const juce::String getApplicationVersion() override    
    { 
        return ProjectInfo::versionString; 
    }

    //==============================================================================
    void initialise(const juce::String& /* commandLine */) override
    {
        // This method is where you should put your application's initialisation code..

        auto sAppName = getApplicationName();

        m_windowMgr.addWindow(new MainWindow(sAppName), (sAppName + "_MainWindow"));
    }

    void shutdown() override
    {
        // Add your application's shutdown code here..

        mainWindow = nullptr; // (deletes our window)
    }

    //==============================================================================
    void systemRequestedQuit() override
    {
        // This is called when the app is being asked to quit: you can ignore this
        // request and let the app carry on running, or call quit() to allow the app to close.
        quit();
    }

    void anotherInstanceStarted (const juce::String& commandLine) override
    {
        // When another instance of the app is launched while this one is running,
        // this method is invoked, and the commandLine parameter tells you what
        // the other instance's command-line arguments were.

        quit();
    }

    //==============================================================================
    /*
        This class implements the desktop window that contains an instance of
        our MainComponent class.
    */
    class MainWindow    : 
        public juce::DocumentWindow
    {
        SafeUniquePtr<MainComponent>        m_pMainComponent;

        juce::TooltipWindow                 m_tooltipWindow{ this, 500 };

    public:
        MainWindow (juce::String name) :
            DocumentWindow 
            (
                name,
                juce::Desktop::getInstance().getDefaultLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId),
                DocumentWindow::allButtons
            ),
            m_pMainComponent(nullptr)
        {
            setUsingNativeTitleBar (true);

            m_pMainComponent.reset(new MainComponent);

            if (m_pMainComponent == nullptr)
            {
                JUCEApplication::getInstance()->systemRequestedQuit();
            }

            setWindowTitle();

            setContentOwned (m_pMainComponent.get(), true);

           #if JUCE_IOS || JUCE_ANDROID
            setFullScreen (true);
           #else
            setResizable (true, true);
            centreWithSize (getWidth(), getHeight());
           #endif

            setVisible (true);
        }

        MainComponent* getMainComponentPtr()
        {
            return m_pMainComponent.get();
        }

        void closeWindow()
        {
            JUCEApplication::getInstance()->systemRequestedQuit();
        }

        void closeButtonPressed() override
        {
            // This is called when the user tries to close this window. Here, we'll just
            // ask the app to quit when this happens, but you can change this to do
            // whatever you need.

            closeWindow();
        }

        void minimiseButtonPressed() override
        {
            auto pWinMgr = ::getAppWindowMgr();

            if (pWinMgr == nullptr)
            {
                delete this;

                return;
            }

            pWinMgr->minimiseAll();
        }

        void maximiseButtonPressed() override
        {
            auto pWinMgr = ::getAppWindowMgr();

            if (pWinMgr == nullptr)
            {
                delete this;

                return;
            }

            pWinMgr->normaliseAll();
        }

        void setWindowTitle()
        {
            juce::String sDev = "";

            if (m_pMainComponent != nullptr)
            {
                if (m_pMainComponent->m_pCameraDevice != nullptr)
                {
                    sDev = (m_pMainComponent->m_pCameraDevice->getName());
                }
            }

            if (sDev.length() > 0)
            {
                juce::String sTmp = (" [" + sDev + "]");

                setName(AppName + sTmp);
            }
            else
            {
                setName(AppName);
            }
        }

    private:

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainWindow)
    };

    MainWindow* getMainWindowPtr()
    {
        juce::DocumentWindow* pMainWin =
            m_windowMgr.getWindow(getApplicationName() + "_MainWindow");

        if (pMainWin == nullptr)
        {
            return nullptr;
        }

        MainWindow* pOut = (dynamic_cast<MainWindow*> (pMainWin));

        return pOut;
    }

private:
    std::unique_ptr<MainWindow> mainWindow;
};


static WebcamApplication& getApp()
{
    return *dynamic_cast<WebcamApplication*> (juce::JUCEApplication::getInstance());
}



juce::ApplicationProperties* getAppProperties()
{
    auto pMainWin = getApp().getMainWindowPtr();

    if (pMainWin != nullptr)
    {
        auto pMainComp = pMainWin->getMainComponentPtr();

        if (pMainComp != nullptr)
            return (pMainComp->getAppProperties());
    }

    return nullptr;
}


JuceWindowMgr_def* getAppWindowMgr()
{
    return (getApp().getWindowMgr());
}

//ApplicationCommandManager& getCmdManager() { return getApp().m_cmdManager; }


void setMainWindowTitle()
{
    auto pMainWin = (getApp().getMainWindowPtr());

    if (pMainWin != nullptr)
    {
        ((WebcamApplication::MainWindow*) pMainWin)->setWindowTitle();
    }
}

void quitApp()
{
    getApp().quit();
}



//==============================================================================
// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION (WebcamApplication)
