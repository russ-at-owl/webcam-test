/*
  ==============================================================================

    AppCfgEditor.h
    Created: 21 Jul 2020 1:57:52pm
    Author:  RussB

  ==============================================================================
*/

#pragma once

#include "AppInc.h"


#define AppConfigEditor_Width           680
#define AppConfigEditor_Height          350

#define AppCfgEditorButton_Width        80
#define AppCfgEditorButton_Height       45

#define DevButton_Width                 80
#define DevButton_Height                45

#define AppCfgEditorCheckbox_Width      200
#define AppCfgEditorCheckbox_Height     45




//*******************************************************************************

class StringListModel :
    public juce::Component,
    public juce::ListBoxModel
{
    juce::String                                m_headerText;

    unsigned int                                m_headerSize;

    juce::StringArray                           m_stringList;

    int                                         m_selectedRow;

public:

    StringListModel(const juce::String& text = "") :
        m_headerText(text),
        m_headerSize(0),
        m_selectedRow(-1)
    {
        if (m_headerText != "")
        {
            m_headerSize = 1;
        }
    }

    ~StringListModel()
    {
        m_stringList.clear();
    }

    void setHeader(const juce::String& text)
    {
        m_headerText = text;

        m_headerSize = 1;
    }

    bool addEntry(const juce::String& folder)
    {
        if (folder == "")
            return false;

        m_stringList.add(folder);

        return true;
    }

    bool updateEntry(const unsigned int num, const juce::String& folder)
    {
        if (num >= (m_stringList.size() + m_headerSize))
            return false;

        if (folder == "")
            return false;

        m_stringList.set((num + m_headerSize), folder);

        return true;
    }

    juce::String getEntry(const unsigned int num)
    {
        if (num > (unsigned int)m_stringList.size())
            return "";

        if (m_headerSize > 0)
        {
            if (num == 0)
            {
                return m_headerText;
            }
        }

        return m_stringList[num - m_headerSize];
    }

    bool deleteEntry(const unsigned int num)
    {
        if (num > (unsigned int)m_stringList.size())
            return false;

        if (m_headerSize > 0)
        {
            if (num == 0)
            {
                m_headerText = "";

                m_headerSize = 0;

                return true;
            }
        }

        m_stringList.remove(num + m_headerSize);

        return true;
    }

    int getNumRows()
    {
        return (m_stringList.size() + m_headerSize);
    }

    void paintListBoxItem(int rowNumber, juce::Graphics& g, int width, int height, bool rowIsSelected = false)
    {
        if (rowNumber < 0 || rowNumber > m_stringList.size())
            return;

        if (m_headerSize > 0)
        {
            if (rowNumber < (int)m_headerSize)
            {
                // clear the background
                //g.fillAll(juce::Colours::grey);
                g.fillAll(UI_Color2);

                juce::Rectangle<int> area = { 0, 0, width, height };

                // draw an outline around the component
                g.setColour(UI_Color3);
                g.drawRect(area, 1);

                // set text color/font
                //g.setColour(UI_TextColor);
                g.setColour(juce::Colours::black);
                g.setFont(14.0f);

                // draw  text
                g.drawText(m_headerText, 5, 0, (width - 5), height, juce::Justification::centredLeft, true);

                return;
            }
        }

        if (rowIsSelected == true)
        {

        }

        // set the background
        if (rowNumber == m_selectedRow)
        {
            g.fillAll(UI_Color3);
        }
        else
        {
            g.fillAll(juce::Colours::black);
        }

        juce::Rectangle<int> area = { 0, 0, width, height };

        // draw an outline around the component
        g.setColour(UI_Color3);
        g.drawRect(area, 1);

        // set text color/font
        g.setColour(UI_TextColor);
        g.setFont(14.0f);

        // draw  text
        g.drawText(m_stringList[rowNumber - m_headerSize], 5, 0, (width - 5), height, juce::Justification::centredLeft, true);
    }

    void listBoxItemClicked(int row, const juce::MouseEvent&)
    {
        if (m_headerSize > 0)
        {
            if (row < (int) m_headerSize)
            {
                m_selectedRow = -1;

                repaint();

                return;
            }
        }

        m_selectedRow = row;

        repaint();
    }

    void listBoxItemDoubleClicked(int row, const juce::MouseEvent&)
    {
        if (m_headerSize > 0)
        {
            if (row < (int) m_headerSize)
            {
                repaint();

                return;
            }
        }

        m_selectedRow = row;

        repaint();
    }

    int getSelectedRow()
    {
        return m_selectedRow;
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(StringListModel)
};




//*******************************************************************************

class FolderListModel :
    public juce::Component,
    public juce::ListBoxModel
{
    juce::String                                m_headerText;

    unsigned int                                m_headerSize;

    juce::StringArray                           m_folderList;

    int                                         m_selectedRow;

public:

    FolderListModel(const juce::String& text = "") :
        m_headerText(text),
        m_headerSize(0),
        m_selectedRow(-1)
    {
        if (m_headerText != "")
        {
            m_headerSize = 1;
        }
    }

    ~FolderListModel()
    {
        m_folderList.clear();
    }

    void setHeader(const juce::String& text)
    {
        m_headerText = text;

        m_headerSize = 1;
    }

    bool addEntry(const juce::String& folder)
    {
        if (folder == "")
            return false;

        m_folderList.add(folder);

        return true;
    }

    bool updateEntry(const unsigned int num, const juce::String& folder)
    {
        if (num >= (m_folderList.size() + m_headerSize))
            return false;

        if (folder == "")
            return false;

        m_folderList.set((num + m_headerSize), folder);

        return true;
    }

    juce::String getEntry(const unsigned int num)
    {
        if (num > (unsigned int) m_folderList.size())
            return "";

        if (m_headerSize > 0)
        {
            if (num == 0)
            {
                return m_headerText;
            }
        }

        return m_folderList[num - m_headerSize];
    }

    bool deleteEntry(const unsigned int num)
    {
        if (num > (unsigned int) m_folderList.size())
            return false;

        if (m_headerSize > 0)
        {
            if (num == 0)
            {
                m_headerText = "";

                m_headerSize = 0;

                return true;
            }
        }

        m_folderList.remove(num + m_headerSize);

        return true;
    }

    int getNumRows()
    {
        return (m_folderList.size() + m_headerSize);
    }

    void paintListBoxItem(int rowNumber, juce::Graphics& g, int width, int height, bool rowIsSelected = false)
    {
        if (rowNumber < 0 || rowNumber > m_folderList.size())
            return;

        if (m_headerSize > 0)
        {
            if (rowNumber < (int) m_headerSize)
            {
                // clear the background
                //g.fillAll(juce::Colours::grey);
                g.fillAll(UI_Color2);

                juce::Rectangle<int> area = { 0, 0, width, height };

                // draw an outline around the component
                g.setColour(UI_Color3);
                g.drawRect(area, 1);

                // set text color/font
                //g.setColour(UI_TextColor);
                g.setColour(juce::Colours::black);
                g.setFont(14.0f);

                // draw  text
                g.drawText(m_headerText, 5, 0, (width - 5), height, juce::Justification::centredLeft, true);

                return;
            }
        }

        if (rowIsSelected == true)
        {

        }

        // set the background
        if (rowNumber == m_selectedRow)
        {
            g.fillAll(UI_Color3);
        }
        else
        {
            g.fillAll(juce::Colours::black);
        }

        juce::Rectangle<int> area = { 0, 0, width, height };

        // draw an outline around the component
        g.setColour(UI_Color3);
        g.drawRect(area, 1);

        // set text color/font
        g.setColour(UI_TextColor);
        g.setFont(14.0f);

        // draw  text
        g.drawText(m_folderList[rowNumber - m_headerSize], 5, 0, (width - 5), height, juce::Justification::centredLeft, true);
    }

    void listBoxItemClicked(int row, const juce::MouseEvent&)
    {
        if (m_headerSize > 0)
        {
            if (row < (int) m_headerSize)
            {
                m_selectedRow = -1;

                repaint();

                return;
            }
        }

        m_selectedRow = row;

        repaint();
    }

    void listBoxItemDoubleClicked(int row, const juce::MouseEvent&)
    {
        if (m_headerSize > 0)
        {
            if (row < (int) m_headerSize)
            {
                return;
            }
        }

        juce::String path = ((m_folderList[row - m_headerSize]) + "\\");

        juce::File dir = path;

        //dir.setAsCurrentWorkingDirectory();

        juce::FileChooser fileChooser
        (
            "Edit search path folder",
            dir,
            "",
            false,
            false
        );


        bool status = fileChooser.browseForDirectory();

        if (status == true)
        {
            m_folderList.set(row, (dir.getFullPathName()));

            repaint();
        }
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FolderListModel)
};



//*******************************************************************************

class AppCfgEditorComponent :
    public  juce::Component,
    public  juce::Button::Listener //,
    //public  TextEditor::Listener
{
    ComponentNotifierCls                            m_notify;

    SimplePtr<MainComponent>                        m_pParent;

    SimplePtr<AppConfigMgr>                         m_pCfgMgr;

    SafeUniquePtr<AppButtonCls>                     m_pUpdate;
    SafeUniquePtr<AppButtonCls>                     m_pCancel;

    SafeSharedPtr<StringListModel>                  m_devList;

    SafeSharedPtr<juce::ListBox>                    m_pWebcamDevList;

    //SafeUniquePtr<AppButtonCls>                     m_pButton;

    juce::StringArray                               m_webcamDevList;

    int                                             m_nVideoDevNum;

    SafeUniquePtr<juce::ToggleButton>               m_pPanoCheckbox;

public:

    AppCfgEditorComponent
        (
            SimplePtr<MainComponent> p, 
            SimplePtr<AppConfigMgr> pCfg, 
            SimplePtr<ComponentNotificationCls> n
        ) :
        m_pCfgMgr(pCfg),
        m_pParent(p),
        m_notify(n.get())
    {
        if (m_pCfgMgr != nullptr)
        {
            m_nVideoDevNum = m_pCfgMgr->m_nSelectedWebcamDev;
        }

        m_webcamDevList = juce::CameraDevice::getAvailableDevices();

        int w = AppConfigEditor_Width;
        int h = AppConfigEditor_Height;

        int x = 0;
        int y = 0;

        m_pUpdate.reset(new AppButtonCls("Select"));

        if (m_pUpdate != 0)
        {
            m_pUpdate->setButtonIcon(Resources::icon_Check_svg);

            x = (w - ((AppCfgEditorButton_Width + 10) * 2));
            y = (h - (AppCfgEditorButton_Height + 10));

            m_pUpdate->setPosAndSize(x, y, AppCfgEditorButton_Width, AppCfgEditorButton_Height);

            m_pUpdate->setTooltip("Use currently selected application settings");

            addAndMakeVisible(m_pUpdate.get());

            m_pUpdate->addListener(this);
        }

        m_pCancel.reset(new AppButtonCls("Cancel"));

        if (m_pCancel != 0)
        {
            m_pCancel->setButtonIcon(Resources::icon_X_svg);

            x = (w - (AppCfgEditorButton_Width + 10));
            y = (h - (AppCfgEditorButton_Height + 10));

            m_pCancel->setPosAndSize(x, y, AppCfgEditorButton_Width, AppCfgEditorButton_Height);

            m_pCancel->setTooltip("Do not use currently selected application settings");

            addAndMakeVisible(m_pCancel.get());

            m_pCancel->addListener(this);
        }

        //*****************************************

        //* create a webcam device listBox component 

        m_devList.reset(new StringListModel("Webcam devices"));

        if (m_devList != nullptr)
        {
            for (auto i = m_webcamDevList.begin(); i != m_webcamDevList.end(); i++)
            {
                m_devList->addEntry((*i));
            }

            //* create Listbox

            m_pWebcamDevList.reset(new juce::ListBox(DevSelectHeaderText, m_devList.get()));

            if (m_pWebcamDevList != nullptr)
            {
                m_pWebcamDevList->setOutlineThickness(2);

                //m_pWebcamDevList->setClickingTogglesRowSelection(true);
                m_pWebcamDevList->setMultipleSelectionEnabled(false);

                m_pWebcamDevList->setPosAndSize(10, 10, (w - 20), 100);

                m_pWebcamDevList->updateContent();

                m_pWebcamDevList->setTooltip("Default plugin search folder list");

                if (m_nVideoDevNum >= 0)
                {
                    m_pWebcamDevList->selectRangeOfRows(m_nVideoDevNum, m_nVideoDevNum, false);
                }

                addAndMakeVisible(m_pWebcamDevList.get());
            }
        }

        //************************************

        m_pPanoCheckbox.reset(new juce::ToggleButton("Pano Webcam Mode"));

        if (m_pPanoCheckbox != 0)
        {
            x = 10;
            y = (h - (AppCfgEditorButton_Height + 10));

            m_pPanoCheckbox->setPosAndSize(x, y, AppCfgEditorCheckbox_Width, AppCfgEditorCheckbox_Height);

            m_pPanoCheckbox->setTooltip("Allow only webcam with 'Pano' (4320x720) resolution");

            addAndMakeVisible(m_pPanoCheckbox.get());

            m_pPanoCheckbox->addListener(this);

            if (m_pCfgMgr != nullptr)
            {
                switch (m_pCfgMgr->m_videoFormatSelect)
                {
                case eVideoFormatSelect::VideoFormat_4320x720:
                    m_pPanoCheckbox->setToggleState(true, false);
                    break;
                
                default:
                    m_pPanoCheckbox->setToggleState(false, false);
                    break;
                }
            }
        }

    }

    ~AppCfgEditorComponent()
    {

    }

    void paint(juce::Graphics& g) override
    {
        g.fillAll(Color_darkdarkgrey);
    }

    void buttonClicked(juce::Button* b)  override;

    //void textEditorTextChanged(TextEditor& e)  override;

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AppCfgEditorComponent)
};


//*******************************************************************************

class AppCfgEditorWin :
    public  juce::DocumentWindow,
    public  ComponentNotificationCls
{
    ComponentNotifierCls                            m_notify;

    SafeUniquePtr<AppCfgEditorComponent>            m_cfgEditor;

    juce::TooltipWindow                             m_tooltipWindow { this, 500 };

public:
    AppCfgEditorWin
        (
            SimplePtr<MainComponent> p, 
            SimplePtr<AppConfigMgr> pCfg, 
            SimplePtr<ComponentNotificationCls> n
        ) :
        DocumentWindow(ConfigPanelHeaderText, Color_darkdarkgrey, juce::DocumentWindow::closeButton),
        m_notify(n.get())
    {
        unsigned long x = AppConfigEditor_Width;
        unsigned long y = AppConfigEditor_Height;

        setSize(x, y);

        m_cfgEditor.reset
        (
            new AppCfgEditorComponent
                (
                    p, 
                    pCfg, 
                    this
                )
        );

        if (m_cfgEditor != nullptr)
        {
            m_cfgEditor->setBounds(0, 0, x, y);

            setContentOwned(m_cfgEditor.get(), true);
        }

        centreWithSize(getWidth(), getHeight());

        setVisible(true);
    }

    ~AppCfgEditorWin()
    {

    }

    void closeWindow()
    {
        auto pWinMgr = ::getAppWindowMgr();

        if (pWinMgr == nullptr)
        {
            delete this;

            return;
        }

        pWinMgr->closeWindow(this);
    }

    void closeButtonPressed()
    {
        m_notify.notifyParent(ComponentNotificationType_Close, this);

        closeWindow();
    }

    void componentNotifyCallback(int val, juce::Component* comp)
    {
        if (comp == nullptr)
            return;

        if (comp == this)
            return;

        if (comp == m_cfgEditor.get())
        {
            switch (val)
            {
            case ComponentNotificationType_UpdateControl:
                m_notify.notifyParent(ComponentNotificationType_UpdateControl, this);
                return;

            case ComponentNotificationType_UpdateAndClose:
                m_notify.notifyParent(ComponentNotificationType_UpdateAndClose, this);
                closeWindow();
                return;

            case ComponentNotificationType_Close:
            case ComponentNotificationType_Abort:
                m_notify.notifyParent(val, this);
                closeWindow();
                return;
            }

            return;
        }
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AppCfgEditorWin)
};

