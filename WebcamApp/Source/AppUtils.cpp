/*
  ==============================================================================

    Utils.cpp
    Created: 26 Jun 2020 1:41:02pm
    Author:  HTArtisan

  ==============================================================================
*/


#include "AppInc.h"


//TextEditor Utils::diagnosticsBox;


//==============================================================================

void Utils::logMessage(juce::String s)
{
    //diagnosticsBox.moveCaretToEnd();
    //diagnosticsBox.insertTextAtCaret(s + newLine);
}






juce::String Utils::getListOfDeviceNames(juce::BigInteger channels)
{
    juce::StringArray names;

    for (int i = 0; i <= channels.getHighestBit(); ++i)
    {
        if (channels[i])
            names.add(juce::String(i));
    }

    return names.joinIntoString(", ");
}


juce::StringArray Utils::stringToArray(juce::String in, juce::juce_wchar deliminator)
{
    juce::StringArray out;

    int start = 0;
    int pos = 0;

    juce::String tmp = "";

    while (true)
    {
        if (in == "")
            break;

        pos = in.indexOfChar(deliminator);

        if (pos >= 0)
        {
            tmp = in.substring(start, pos);

            start = pos + 1;

            in = in.substring(start);
        }
        else
        {
            tmp = in;

            in = "";
        }

        if (tmp == "")
            break;

        out.add(tmp);
    }

    return out;
}



bool Utils::drawGraphicFromSvg(const juce::String& source, juce::Graphics& g, int x, int y, int h, int w, float opacity)
{
    if (source == "")
        return false;

    SafeUniquePtr<juce::XmlElement> svg_xml(juce::XmlDocument::parse(source));
    if (svg_xml != nullptr)
    {
        SafeUniquePtr<juce::Drawable> svg_drawable(juce::Drawable::createFromSVG(*svg_xml));
        if (svg_drawable != nullptr)
        {
            svg_drawable.get()->drawWithin
                (
                    g, 
                    juce::Rectangle<float>((float) x, (float) y, (float) h, (float) w), 
                    juce::RectanglePlacement::stretchToFit,
                    opacity
                );

            return true;
        }
    }

    return false;
}


