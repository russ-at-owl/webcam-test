/*
  ==============================================================================

    AppInc.h
    Created: 19 Jun 2020 1:03:33pm
    Author:  HTArtisan

  ==============================================================================
*/


#pragma once


#if (JUCE_MAC || JUCE_IOS)
#define JUCE_PLUGINHOST_AU          1
#endif


//#define DumpConfig


#include <JuceHeader.h>

//#include <thread>  
//#include <chrono> 



#define AppName                     ("Webcam-app")
#define AppVersion                  ("1.0.0")
#define AppCompanyName              (" ")

#define AboutAppTitleHeader         ("About... ")

#define AboutAppMsgHeader           ("Application: ")
#define AboutAppVerHeader           ("Version: ")
#define AboutCompanyHeader          ("Developed by: ")
#define AboutCopyrightText          ("Copyright: (C) 2021 \n All rights reserved ")
#define AboutCompanyURL             ("Company URL:   ")

#define ConfigPanelHeaderText       ("Application config settings")

#define DevSelectHeaderText         ("Webcam devices")



#if (JUCE_WINDOWS)
#define DefaultPluginSearchPath     ("C:\\Program Files\\Common Files\\VST3")
#else
#if (JUCE_MAC)
#define DefaultPluginSearchPath     ("/")
#endif
#endif


const juce::Colour Color_darkdarkgrey{ 0xff202020 };
const juce::Colour Color_extremedarkgrey{ 0xff101010 };
const juce::Colour Color_verydarkorange{ 0xff553300 };

#define ButtonOutlineColorID        juce::ComboBox::outlineColourId



#define UI_Color1                   juce::Colours::orange
#define UI_Color2                   juce::Colours::darkorange
#define UI_Color2A                  juce::Colours::orangered
#define UI_Color3                   Color_verydarkorange
#define UI_Color4                   Color_verydarkorange

#define Button_Border_Color         juce::Colours::orange
#define Button_Background_Color     UI_Color2
#define Button_Selected_Color       UI_Color1

#define UI_TextColor                juce::Colours::white

#define AppBackgroundColor          juce::Colours::black

//#define CtrlBackgroundColor       Color_darkdarkgrey
#define CtrlBackgroundColor         Color_extremedarkgrey


template <typename t>
class SimplePtr
{
protected:

    t *m_ptr;

public:

    SimplePtr(t* ptr = nullptr) :
        m_ptr(ptr)
    {

    }

    void reset(t* ptr)
    {
        m_ptr = (t*) ptr;
    }

    t* get()
    {
        return m_ptr;
    }

    void release()
    {
        if (m_ptr != nullptr)
            delete m_ptr;
    }

    t* operator = (t &ptr)
    {
        m_ptr = ptr;

        return m_ptr;
    }

    inline bool compare(void *ptr)
    {
        return (m_ptr == ptr);
    }

    inline bool compare(SimplePtr<t> &ptr)
    {
        return (m_ptr == ptr.get());
    }

    inline bool operator == (void *ptr)
    {
        return (m_ptr == ptr);
    }

    inline bool operator == (SimplePtr<t> &ptr)
    {
        return (m_ptr == ptr.get());
    }

    inline bool operator != (void *ptr)
    {
        return !(m_ptr == ptr);
    }

    inline bool operator != (SimplePtr<t> &ptr)
    {
        return !(m_ptr == ptr.get());
    }

    inline t* operator -> ()
    {
        return m_ptr;
    }

};


#define SafeSharedPtr               std::shared_ptr
#define SafeUniquePtr               std::unique_ptr
#define StdPtr                      std::weak_ptr


//*********************************************************

enum eVideoFormatSelect
{
    VideoFormat_any,
    VideoFormat_640x480,
    VideoFormat_1280x720,
    VideoFormat_1920x1080,
    VideoFormat_4320x720,       //* MeetingOwl panoramic mode

};


//*********************************************************

#include "AppWindowMgr.h"


//*********************************************************
//* Global function defs


class WebcamApplication;

class MainComponent;

static WebcamApplication& getApp();

juce::ApplicationProperties * getAppProperties();

JuceWindowMgr_def * getAppWindowMgr();

void setMainWindowTitle();

void quitApp();


//*********************************************************


#include "AppUtils.h"
#include "MenuUtils.h"
#include "AppButtonCls.h"




//*********************************************************

#define Mixer_Width                 910
#define Mixer_Height                470


class AppConfigMgr;
class VolumeAudioApplication;


//*********************************************************


#include "AppConfig.h"
#include "AppCfgEditor.h"
#include "AppLookAndFeel.h"
#include "AppMenu.h"

#include "MainComponent.h"

