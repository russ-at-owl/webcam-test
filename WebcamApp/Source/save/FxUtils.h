/*
  ==============================================================================

    FxEditorWin.h
    Created: 21 Aug 2020 11:23:11am
    Author:  RussB

  ==============================================================================
*/

#pragma once



class FxEditorWindow;




//==============================================================================

class AudioFxInfo
{
public:

    typedef enum
    {
        fxType_Unknown,
        fxType_Internal,
        fxType_VXT,
        fxType_VXT3,
        fxType_AAX,

    } FXModuleType;

    //********************************************************

    ComponentNotifierCls                    m_notify;

    juce::String                                  m_fxName;

    String                                  m_fxDesc;

    FXModuleType                            m_fxType;

    bool                                    m_fxInline;

    SafeUniquePtr<AudioPluginInstance>      m_pFxPlugin;
    //SafeUniquePtr<AudioProcessor>         m_pFxPlugin;

    String                                  m_fxEditorWinID;

    int                                     m_numInputChls;
    int                                     m_numOutputChls;

    float                                   m_gainLevel;

    void clear()
    {
        m_fxName = "";
        m_fxDesc = "";
        m_fxEditorWinID = "";
    }

    AudioFxInfo() :
        m_notify(nullptr),
        m_fxInline(false),
        m_numInputChls(0),
        m_numOutputChls(0),
        m_gainLevel(1.0f)
    {
        clear();
    }

    ~AudioFxInfo();

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioFxInfo)
};


//typedef std::vector<AudioFxInfo*>                 FxConfigList_def;
typedef std::vector<SimplePtr<AudioFxInfo>>         FxConfigList_def;




//==============================================================================

class FxEditorWindow :
    public  DocumentWindow
{
    SimplePtr<AudioFxInfo>                  m_pFXI;

    int                                     m_fxNum;

    AudioProcessorEditor                    *m_pFxUI;

    int                                     m_trackNum;

public:
    FxEditorWindow
        (
            SimplePtr<AudioFxInfo> pFXI,
            int fxNum,
            AudioProcessorEditor * pFxUI,
            int trackNum
        ) :
        DocumentWindow("", Color_darkdarkgrey, DocumentWindow::closeButton),
        m_pFXI(pFXI),
        m_fxNum(fxNum),
        m_pFxUI(pFxUI),
        m_trackNum(trackNum)
    {
        setTitleBarText();

        if (m_pFxUI != nullptr)
        {
            //setContentOwned(m_pFxUI.get(), true);
            setContentOwned(m_pFxUI, true);
        }

        centreWithSize(getWidth(), getHeight());

        setVisible(true);
    }

    ~FxEditorWindow() override
    {

    }

    void updateFxNum(unsigned int num)
    {
        m_fxNum = (int) num;

        repaint();
    }

    void setTitleBarText()
    {
        String name = "";

        if (m_trackNum >= 0)
        {
            name = "Tr";

            name += ((char)('0' + (m_trackNum + 1)));

            name += " > Fx";

            if (m_pFXI != nullptr)
                name += ((char)('0' + (m_fxNum + 1)));

            name += " > ";
        }

        if (m_pFXI != nullptr)
            name += (m_pFXI->m_fxName);

        //name += (m_pFXI->m_fxDesc);

        DocumentWindow::setName(name);
    }

    void closeWindow()
    {
        auto pWinMgr = ::getAppWindowMgr();

        if (pWinMgr == nullptr)
        {
            delete this;

            return;
        }

        pWinMgr->closeWindow(this);
    }

    void closeButtonPressed() override
    {
        if (m_pFXI != nullptr)
            m_pFXI->m_notify.notifyParent(ComponentNotificationType_Close, this);

        closeWindow();
    }


private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(FxEditorWindow)
};



//typedef std::vector<FxEditorWindow*>           FxEditorList_def;
