/*
  ==============================================================================

    AppMenu.h
    Created: 14 Jul 2020 1:45:13pm
    Author:  RussB

  ==============================================================================
*/

#pragma once





//==============================================================================
/** A list of the commands that this app responds to. */
enum CommandIDs
{
    menuFileLoad = 1,
    menuFileSave,
    menuFileConfig,
    menuFileExit,
    menuHelpAbout,
    menuHelpOpen
};


//==============================================================================
/** A list of the top level menu IDs. */
enum MenuIDs
{
    menuFile = 0,
    menuHelp
};



class AppMenuMgr :
    public MenuInfoManagerCls
{
public:

    AppMenuMgr()
    {
        addMenu("File", "Load config file", menuFileLoad, "Load a configuration file", 'l');
        addMenu("File", "Save config file", menuFileSave, "Save a configuration file", 's');
        addMenu("File", "Configure app settings", menuFileConfig, "Display the app configuration dialog", 'c');

        addMenu("File", "Exit", menuFileExit, "Exit/close the application", 'x');

        addMenu("Help", "Open About", menuHelpAbout, "Open the About box", 'i');
        addMenu("Help", "Get help", menuHelpOpen, "Open the help web page", 'h');
    }
};





//==============================================================================
/**
    Command messages that aren't handled in the TrackMenuCommandTarget will be passed
    to this class to respond to.
*/

class HelpMenuCommandTarget :
    public BaseMenuCommandTarget
{
    MenuCommandProcessor    & m_parent;

public:
    HelpMenuCommandTarget(MenuCommandProcessor& parent, juce::ApplicationCommandManager* cmdMgr, MenuInfoManagerCls* memuMgr) :
        m_parent(parent)
    {
        initCmdTarget(menuHelp, cmdMgr, memuMgr);
    }

    ~HelpMenuCommandTarget()
    {

    }

    bool createMenuComponent()
    {
        return true;
    }

    void releaseMenuComponent()
    {

    }

    ApplicationCommandTarget* getNextCommandTarget() override
    {
        return nullptr;
    }


    //==============================================================================

    bool perform(const juce::ApplicationCommandTarget::InvocationInfo& info) override
    {
        return m_parent.menuCommandExec(info);
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(HelpMenuCommandTarget)
};





//==============================================================================
/**
    Primary (File) command target
*/

class MainMenuCommandTarget :
    public BaseMenuCommandTarget
{
    MenuCommandProcessor                    &m_parent;

    AppMenuMgr                              m_menuMgr;

    bool                                    m_bMenuCreated;

protected:

    //==============================================================================
    //* Represents the possible menu positions. 

    enum class MenuBarPosition
    {
        window,
        global,
        burger
    };

    //==============================================================================
    //* Menu variables

    juce::ApplicationCommandManager                 m_commandManager;

    Component::SafePointer<juce::MenuBarComponent>  m_menuBar;

    MenuBarPosition                                 m_menuBarPosition;

    juce::BurgerMenuComponent                       m_burgerMenu;

    juce::SidePanel                                 m_sidePanel{ "Menu", 300, false };

    BurgerMenuHeader                                m_menuHeader{ m_sidePanel };

    HelpMenuCommandTarget                           *m_helpCommandTarget;

    unsigned long                                   m_menuBarHeight;


public:
    MainMenuCommandTarget(MenuCommandProcessor& parent) :
        m_parent(parent),
        m_bMenuCreated(false),
#if JUCE_MAC
        m_menuBarPosition(MenuBarPosition::global)
#else
#if (JUCE_ANDROID || JUCE_IOS)
        m_menuBarPosition(MenuBarPosition::burger)
#else        
        m_menuBarPosition(MenuBarPosition::window)
#endif
#endif
    {
        setMenuType();  //* use default menu type

        initCmdTarget(menuFile, &m_commandManager, &m_menuMgr);
    }

    ~MainMenuCommandTarget()
    {

    }

    void setMenuType(int type = -1)
    {
        //* type -1 = default menu type

        if (type >= 0)
        {
            m_menuBarPosition = (MenuBarPosition) type;
        }

        //* calculate menu bar height

        if (m_menuBarPosition == MenuBarPosition::global)
        {
            m_menuBarHeight = 0;
        }
        else if (m_menuBarPosition == MenuBarPosition::window)
        {
            m_menuBarHeight = juce::LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight();
        }
        else
        {
            m_menuBarHeight = 40;
        }
    }

    bool createMenuComponent(Component& parent);

    void releaseMenuComponent(Component& parent);

    void resizeMenu(Component& parent);

    //==============================================================================

    ApplicationCommandTarget* getNextCommandTarget() override
    {
        return m_helpCommandTarget;
    }

    bool perform(const InvocationInfo& info) override
    {
        return m_parent.menuCommandExec(info);
    }

    //==============================================================================

    BurgerMenuHeader * getMenuHeader()
    {
        return &m_menuHeader;
    }

    juce::SidePanel * getSizePannel()
    {
        return &m_sidePanel;
    }

    unsigned long getMenuBarHeight()
    {
        return m_menuBarHeight;
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainMenuCommandTarget)
};








