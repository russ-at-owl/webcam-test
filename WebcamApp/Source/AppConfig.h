/*
  ==============================================================================

    AppConfig.h
    Created: 23 Jul 2020 7:49:28am
    Author:  RussB

  ==============================================================================
*/

#pragma once


#include "AppInc.h"


#define XmlDumpPath     ("D:\\temp\\")






//==============================================================================

class AppConfigMgr :
    public  juce::ChangeListener
{
    juce::File                                        m_configFile;

    juce::ApplicationProperties                       m_appProperties;

    juce::PropertiesFile::Options                     m_appOptions;

    SafeUniquePtr<juce::PropertiesFile>               m_pPropFile;

    juce::PropertySet                                 m_currentPropSet;

public:

    int                                               m_nSelectedWebcamDev;

    eVideoFormatSelect                                m_videoFormatSelect;

    //*******************************************************

    AppConfigMgr();

    ~AppConfigMgr();

    juce::ApplicationProperties * getAppProperties()
    { 
        return &m_appProperties; 
    }

    juce::PropertySet* getCurrProperties()
    {
        return &m_currentPropSet;
    }

    juce::PropertiesFile * getPropFile()
    {
        return m_pPropFile.get();
    }


    bool findSectionInProperties(const juce::String &secName)
    {
        return (m_currentPropSet.containsKey(secName));
    }

    //PropertiesFile * getUserSettingsFile()
    //{
    //    return m_appProperties.getUserSettings();
    //}

    juce::File getFileFrompAppProperties(const juce::String &propName)
    {
        return m_appProperties.getUserSettings()->getFile().getSiblingFile(propName);
    }

    juce::File getFileFromProperties(const juce::String& propName)
    {
        juce::String tmp = m_currentPropSet.getValue(propName);

        juce::File out(tmp);

        return out;
    }

    juce::String getStringFromProperties(const juce::String &propName)
    {
        return m_currentPropSet.getValue(propName);
    }

    bool getBoolFromProperties(const juce::String &propName)
    {
        return m_currentPropSet.getBoolValue(propName);
    }

    int getIntFromProperties(const juce::String &propName)
    {
        return m_currentPropSet.getIntValue(propName);
    }

    float getFloatFromProperties(const juce::String &propName)
    {
        juce::String tmp = m_currentPropSet.getValue(propName);

        if (tmp == "")
            return 0;

        return tmp.getFloatValue();
    }

    void setStringToProperties(const juce::String &propName, const juce::String &val)
    {
        m_currentPropSet.setValue(propName, val);
    }

    void setIntToProperties(const juce::String &propName, const bool val)
    {
        m_currentPropSet.setValue(propName, val);
    }

    void setIntToProperties(const juce::String &propName, const int val)
    {
        m_currentPropSet.setValue(propName, val);
    }

    void setFloatToProperties(const juce::String &propName, const float val)
    {
        m_currentPropSet.setValue(propName, val);
    }

    //bool getXmlValueFromProperties(const String &propName, std::unique_ptr<XmlElement> &xml)
    //{
    //    XmlElement* p = m_currentPropSet.getXmlValue(propName).get();

    //    if (p == nullptr)
    //        return false;

    //    xml = m_currentPropSet.getXmlValue(propName);

    //    return true;
    //}

    void setXmlToProperties(const juce::String &propName, const juce::XmlElement*  xml)
    {
        m_currentPropSet.setValue(propName, xml);
    }

    void savePropertiesIfNeeded()
    {
        //m_appProperties.saveIfNeeded();
        if (m_pPropFile != nullptr)
            m_pPropFile->saveIfNeeded();
    }

    bool setConfigFile(const juce::File& file)
    {
        m_configFile = file;

        if (m_configFile.getFileName() != "")
        {
            return true;
        }

        return false;
    }

    juce::FileSearchPath getPluginSearchPath(const juce::String &type)
    {
        auto key = "lastPluginScanPath_" + type;

        return juce::FileSearchPath(m_currentPropSet.getValue(key, DefaultPluginSearchPath));
    }

    juce::StringArray getPluginSearchFolders(const juce::String& propName)
    {
        juce::StringArray out;

        juce::String path = m_currentPropSet.getValue(propName);

        out = Utils::stringToArray(path, ';');

        return out;
    }

    void setSearchPath(const juce::String& type, const juce::FileSearchPath& newPath)
    {
        auto key = "lastPluginScanPath_" + type;

        if (newPath.getNumPaths() == 0)
            m_currentPropSet.removeValue(key);
        else
            m_currentPropSet.setValue(key, newPath.toString());
    }


    int loadInputCfgList()
    {

        try
        {



        }
        catch (...)
        {

        }

        return 0;
    }

    int saveInputCfgList()
    {

        try
        {




        }
        catch (...)
        {

        }

        return 0;
    }


    bool getDefaultAppProperties()
    {
        try
        {
            if (m_appOptions.applicationName == "" || m_appOptions.folderName == "")
            {
                auto tmp = m_appProperties.getStorageParameters();

                if (tmp.applicationName == "")
                {
                    tmp.applicationName = AppName;
                }

                if (tmp.folderName == "")
                {
                    juce::File dir = m_configFile.getParentDirectory();

                    tmp.folderName = dir.getFullPathName();
                }

                m_appProperties.setStorageParameters(tmp);

                m_appOptions = tmp;
            }

            m_pPropFile.reset(new juce::PropertiesFile(m_configFile, m_appProperties.getStorageParameters()));

            if (m_pPropFile == nullptr)
                return false;

            m_pPropFile->reload();

            m_currentPropSet.addAllPropertiesFrom(*m_pPropFile);

#ifdef DumpConfig
            String dumpFile = XmlDumpPath;
            dumpFile += "DumpFile-AfterUpdateFromLoad.xml";
            dumpUserPropertiesToFile(dumpFile);
#endif

            return true;
        }
        catch (...)
        {

        }

        return false;
    }

    bool getConfigFromFile()
    {
        try
        {
            if (m_appOptions.applicationName == "" || m_appOptions.folderName == "")
            {
                auto tmp = m_appProperties.getStorageParameters();

                if (tmp.applicationName == "")
                {
                    tmp.applicationName = AppName;
                }

                if (tmp.folderName == "")
                {
                    juce::File dir = m_configFile.getParentDirectory();

                    tmp.folderName = dir.getFullPathName();
                }

                m_appProperties.setStorageParameters(tmp);

                m_appOptions = tmp;
            }

            if (m_pPropFile == nullptr)
                m_pPropFile.reset(m_appProperties.getUserSettings());

            if (m_pPropFile == nullptr)
                return false;

            if (m_configFile.getFullPathName() != "" && m_pPropFile->getFile() != m_configFile)
            {
                //m_pPropFile.release();

                m_pPropFile.reset(new juce::PropertiesFile(m_configFile, m_appProperties.getStorageParameters()));

                if (m_pPropFile == nullptr)
                    return false;
            }

            m_pPropFile->reload();

            m_currentPropSet.addAllPropertiesFrom(*m_pPropFile);

//#ifdef    DumpConfig
//          String dumpFile = XmlDumpPath;
//          dumpFile += "DumpFile-FromLoadedSettings.xml";
//          dumpUserPropertiesToFile(dumpFile, m_pPropFile);
//#endif

            //m_appProperties.getUserSettings()->addAllPropertiesFrom(*m_pPropFile);

#ifdef DumpConfig
            String dumpFile = XmlDumpPath;
            dumpFile += "DumpFile-AfterUpdateFromLoad.xml";
            dumpUserPropertiesToFile(dumpFile);
   #endif

            return true;
        }
        catch (...)
        {

        }

        return false;
    }

    bool saveConfigToFile()
    {
        try
        {
            if (m_appOptions.applicationName == "" || m_appOptions.folderName == "")
            {
                auto tmp = m_appProperties.getStorageParameters();

                if (tmp.applicationName == "")
                {
                    tmp.applicationName = AppName;
                }

                if (tmp.folderName == "")
                {
                    juce::File dir = m_configFile.getParentDirectory();

                    tmp.folderName = dir.getFullPathName();
                }

                m_appProperties.setStorageParameters(tmp);

                m_appOptions = tmp;
            }

            if (m_pPropFile == nullptr)
                m_pPropFile.reset(m_appProperties.getUserSettings());

            if (m_pPropFile == nullptr)
                return false;

            if (m_configFile.getFullPathName() != "" && m_pPropFile->getFile() != m_configFile)
            {
                //m_pPropFile.release();

                m_pPropFile.reset(new juce::PropertiesFile(m_configFile, m_appProperties.getStorageParameters()));

                if (m_pPropFile == nullptr)
                    return false;

                m_pPropFile->addAllPropertiesFrom(m_currentPropSet);
            }

            m_pPropFile->save();

            return true;
        }
        catch (...)
        {

        }

        return false;
    }

    void dumpUserPropertiesToFile(const juce::String &name)
    {
        try
        {
            juce::File dumpFile(name);

            juce::PropertySet userProps;

            juce::PropertiesFile propFile(dumpFile, m_appProperties.getStorageParameters());

            propFile.addAllPropertiesFrom(m_currentPropSet);

            propFile.save();
        }
        catch (...)
        {

        }
    }

    //==============================================================================

    //void componentNotifyCallback(int val, juce::Component* comp) override;

    void changeListenerCallback(juce::ChangeBroadcaster*) override
    {

    }



private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AppConfigMgr)
};



