/*
  ==============================================================================

    AppButtonCls.h
    Created: 10 Jul 2020 2:20:29pm
    Author:  RussB

  ==============================================================================
*/

#pragma once

#define SUPPORT_APP_BUTTON


//==============================================================================
/*
*/
class AppButtonCls  : 
    public  juce::Button
{
    //bool                      m_selected;

    juce::String                m_icon;
    juce::String                m_selectedIcon;

    float                       m_iconScalingFactor;

    int                         m_offset_X;
    int                         m_offset_Y;

    //********************************************************************************************

    static void drawButtonShape(juce::Graphics& g, const juce::Path& outline, juce::Colour baseColour, float height)
    {
        const float mainBrightness = baseColour.getBrightness();
        const float mainAlpha = baseColour.getFloatAlpha();

        g.setGradientFill(juce::ColourGradient::vertical(baseColour.brighter(0.2f), 0.0f,
            baseColour.darker(0.25f), height));
        g.fillPath(outline);

        g.setColour(juce::Colours::white.withAlpha(0.4f * mainAlpha * mainBrightness * mainBrightness));
        g.strokePath(outline, juce::PathStrokeType(1.0f), juce::AffineTransform::translation(0.0f, 1.0f)
            .scaled(1.0f, (height - 1.6f) / height));

        g.setColour(juce::Colours::black.withAlpha(0.4f * mainAlpha));
        g.strokePath(outline, juce::PathStrokeType(1.0f));
    }

    void drawButtonBackground
        (
            juce::Graphics& g,
            Button& button,
            const juce::Colour& backgroundColour,
            bool shouldDrawButtonAsHighlighted,
            bool shouldDrawButtonAsDown
        )
    {
        juce::Colour baseColour(backgroundColour.withMultipliedSaturation(button.hasKeyboardFocus(true) ? 1.3f : 0.9f)
            .withMultipliedAlpha(button.isEnabled() ? 0.9f : 0.5f));

        if (shouldDrawButtonAsDown || shouldDrawButtonAsHighlighted)
            baseColour = baseColour.contrasting(shouldDrawButtonAsDown ? 0.2f : 0.1f);

        const bool flatOnLeft = button.isConnectedOnLeft();
        const bool flatOnRight = button.isConnectedOnRight();
        const bool flatOnTop = button.isConnectedOnTop();
        const bool flatOnBottom = button.isConnectedOnBottom();

        const float width = (float)button.getWidth() - 1.0f;
        const float height = (float)button.getHeight() - 1.0f;

        if (width > 0 && height > 0)
        {
            const float cornerSize = 4.0f;

            juce::Path outline;
            outline.addRoundedRectangle(0.5f, 0.5f, width, height, cornerSize, cornerSize,
                !(flatOnLeft || flatOnTop),
                !(flatOnRight || flatOnTop),
                !(flatOnLeft || flatOnBottom),
                !(flatOnRight || flatOnBottom));

            drawButtonShape(g, outline, baseColour, height);
        }
    }

public:
    AppButtonCls(const juce::String &name = "", const juce::String& icon = "", const juce::String& selectedIcon = "") :
        Button(name) //,
        //m_selected(false)
    {
        m_icon = icon;
        m_selectedIcon = selectedIcon;

        m_iconScalingFactor = (0.65f);

        m_offset_X = 0;
        m_offset_Y = 0;
    }

    void setButtonIcon(const juce::String& icon, const juce::String& selectedIcon = "")
    {
        m_icon = icon;
        m_selectedIcon = selectedIcon;
    }

    void setIconScalingFactor(float val)
    {
        m_iconScalingFactor = val;
    }

    void setIconOffsetValue(const int x, const int y)
    {
        m_offset_X = x;
        m_offset_Y = y;
    }

    //void paint(juce::Graphics&) override;
    
    void paintButton
    (
        juce::Graphics& g,
        bool shouldDrawButtonAsHighlighted,
        bool shouldDrawButtonAsDown
    ) override;

#if 0
    void setSelected(const bool sel)
    {
        m_selected = sel;

        repaint();
    }

    bool getSelected()
    {
        return m_selected;
    }

    void toggleSelected()
    {
        //* toggle selected

        m_selected = !m_selected;

        repaint();
    }
#endif

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AppButtonCls)
};
