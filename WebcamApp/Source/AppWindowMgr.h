/*
  ==============================================================================

    AppWindowMgr.h
    Created: 20 Aug 2020 3:03:25pm
    Author:  RussB

  ==============================================================================
*/

#pragma once




struct ScreenCoordinates_def
{
    unsigned int        m_x;
    unsigned int        m_y;
};



//********************************************************************************************************

class JuceWindowMgrEntry_def :
    //public std::unique_ptr<DocumentWindow>
    public SimplePtr<juce::DocumentWindow>
{
    ScreenCoordinates_def           m_windowsPos;
    ScreenCoordinates_def           m_windowsSize;

    juce::String                          m_windowID;

    juce::String                          m_windowTitle;

public:

    JuceWindowMgrEntry_def(juce::DocumentWindow* win = nullptr)
    {
        m_windowsPos.m_x = 0;
        m_windowsPos.m_y = 0;

        m_windowsSize.m_x = 0;
        m_windowsSize.m_y = 0;

        m_windowID = "";
        m_windowTitle = "";

        if (win != nullptr)
            this->reset(win);
    }

    ~JuceWindowMgrEntry_def()
    {
        closeWindow();
    }

    void setWindowPos(const unsigned int x, const unsigned int y)
    {
        m_windowsPos.m_x = x;
        m_windowsPos.m_y = y;

        if (this->get() != nullptr)
        {
            this->get()->setTopLeftPosition(x, y);
        }
    }

    void setWindowSize(const unsigned int x, const unsigned int y)
    {
        m_windowsSize.m_x = x;
        m_windowsSize.m_y = y;

        if (this->get() != nullptr)
        {
            this->get()->setSize(x, y);
        }
    }

    void setWindowID(const juce::String& name)
    {
        m_windowID = name;
    }

    juce::String getWindowID()
    {
        return m_windowID;
    }

    void setWindowTitle(const juce::String& name)
    {
        if (this->get() != nullptr)
        {
            this->get()->setName(name);
        }
    }

    juce::String getWindowTitle()
    {
        return this->get()->getName();
    }

    void closeWindow()
    {
        if (this->get() != nullptr)
        {
            this->release();
        }
    }
};


typedef JuceWindowMgrEntry_def *     WindowEntryPtr_def;


class JuceWindowMgr_def 
{
    juce::CriticalSection                         m_lock;

    std::vector<WindowEntryPtr_def>         m_windowList;

public:

    JuceWindowMgr_def()
    {

    }

    ~JuceWindowMgr_def()
    {
        closeAll();
    }

    bool addWindow(juce::DocumentWindow* win, const juce::String &id = "")
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        if (id != "")
        {
            auto pOld = getWindow(id);

            if (pOld != nullptr)
            {
                closeWindow(pOld);
            }
        }

        auto newEntry = new JuceWindowMgrEntry_def(win);

        if (newEntry == nullptr)
            return false;

        if (id != "")
        {
            newEntry->setWindowID(id);
        }

        m_windowList.push_back(newEntry);

        return true;
    }

    void closeAll()
    {
        const juce::ScopedLock sl(m_lock);

        auto i = m_windowList.begin();

        while (i != m_windowList.end())
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() != nullptr)
                {
                    //(*i)->get()->deleteAllChildren();
                    //(*i)->get()->removeAllChildren();

                    //(*i)->release();

                    delete (*i)->get();

                    (*i)->reset(nullptr);
                }

                delete (*i);
            }

            m_windowList.erase(i);

            i = m_windowList.begin();
        }
    }

    bool closeWindow(const juce::String &id)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    if ((*i)->get() != nullptr)
                    {
                        //(*i)->get()->deleteAllChildren();
                        //(*i)->get()->removeAllChildren();

                        //(*i)->release();

                        delete (*i)->get();

                        (*i)->reset(nullptr);
                    }

                    delete (*i);

                    m_windowList.erase(i);

                    return true;
                }
            }
        }

        return false;
    }

    bool closeWindow(juce::DocumentWindow *win)
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() != nullptr)
                {
                    if ((*i)->get() == win)
                    {
                        //(*i)->get()->deleteAllChildren();
                        //(*i)->get()->removeAllChildren();

                        //(*i)->release();

                        delete (*i)->get();

                        (*i)->reset(nullptr);

                        delete (*i);

                        m_windowList.erase(i);

                        return true;
                    }
                }
            }
        }

        return false;
    }

    void minimiseAll()
    {
        const juce::ScopedLock sl(m_lock);

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() != nullptr)
                {
                    (*i)->get()->setMinimised(true);
                }
            }
        }
    }

    void normaliseAll()
    {
        const juce::ScopedLock sl(m_lock);

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() != nullptr)
                {
                    (*i)->get()->setMinimised(false);
                }
            }
        }
    }

    bool minimiseWindow(const juce::String& id)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    (*i)->get()->setMinimised(true);

                    return true;
                }
            }
        }

        return false;
    }

    bool minimiseWindow(juce::DocumentWindow* win)
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() == win)
                {
                    (*i)->get()->setMinimised(true);

                    return true;
                }
            }
        }

        return false;
    }

    bool normaliseWindow(const juce::String& id)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    (*i)->get()->setMinimised(false);

                    return true;
                }
            }
        }

        return false;
    }

    bool normaliseWindow(juce::DocumentWindow* win)
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() == win)
                {
                    (*i)->get()->setMinimised(false);

                    return true;
                }
            }
        }

        return false;
    }

    juce::DocumentWindow* getWindow(const juce::String &id)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return nullptr;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    return (*i)->get();
                }
            }
        }

        return nullptr;
    }

    juce::DocumentWindow* findWindowTitle(const juce::String& title)
    {
        const juce::ScopedLock sl(m_lock);

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowTitle() == title)
                {
                    return (*i)->get();
                }
            }
        }

        return nullptr;
    }

    bool setWindowTitle(const juce::String &id, const juce::String &name, bool update = true)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    if (update == true)
                        (*i)->setWindowTitle(name);

                    return true;
                }
            }
        }

        return false;
    }

    bool setWindowTitle(juce::DocumentWindow *win, const juce::String &name, bool update = true)
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() == win)
                {
                    if (update == true)
                        (*i)->setWindowTitle(name);

                    return true;
                }
            }
        }

        return false;
    }

    bool setWindowPos(const juce::String &id, const unsigned int x, const unsigned int y, bool update = true)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    if (update == true)
                        (*i)->setWindowPos(x, y);

                    return true;
                }
            }
        }

        return false;
    }

    bool setWindowPos(juce::DocumentWindow *win, const unsigned int x, const unsigned int y, bool update = true)
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() == win)
                {
                    if (update == true)
                        (*i)->setWindowPos(x, y);

                    return true;
                }
            }
        }

        return false;
    }

    bool setWindowSize(const juce::String &id, const unsigned int x, const unsigned int y, bool update = true)
    {
        const juce::ScopedLock sl(m_lock);

        if (id == "")
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->getWindowID() == id)
                {
                    if (update == true)
                        (*i)->setWindowSize(x, y);

                    return true;
                }
            }
        }

        return false;
    }

    bool setWindowSize(juce::DocumentWindow *win, const unsigned int x, const unsigned int y, bool update = true)
    {
        const juce::ScopedLock sl(m_lock);

        if (win == nullptr)
            return false;

        for (auto i = m_windowList.begin(); i != m_windowList.end(); i++)
        {
            if ((*i) != nullptr)
            {
                if ((*i)->get() == win)
                {
                    if (update == true)
                        (*i)->setWindowSize(x, y);

                    return true;
                }
            }
        }

        return false;
    }

};


