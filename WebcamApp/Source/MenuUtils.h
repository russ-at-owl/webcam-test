/*
  ==============================================================================

    MenuUtils.h
    Created: 15 Jul 2020 3:16:08pm
    Author:  RussB

  ==============================================================================
*/

#pragma once



//==============================================================================
/**
    This struct contains a header component that will be used when the "burger" menu
    is enabled. It contains an icon that can be used to show the side panel containing
    the menu.
*/

struct BurgerMenuHeader :
    public  juce::Component
{
    BurgerMenuHeader(juce::SidePanel& sp)
        : sidePanel(sp)
    {
        static const unsigned char burgerMenuPathData[]
            = { 110,109,0,0,128,64,0,0,32,65,108,0,0,224,65,0,0,32,65,98,254,212,232,65,0,0,32,65,0,0,240,65,252,
                169,17,65,0,0,240,65,0,0,0,65,98,0,0,240,65,8,172,220,64,254,212,232,65,0,0,192,64,0,0,224,65,0,0,
                192,64,108,0,0,128,64,0,0,192,64,98,16,88,57,64,0,0,192,64,0,0,0,64,8,172,220,64,0,0,0,64,0,0,0,65,
                98,0,0,0,64,252,169,17,65,16,88,57,64,0,0,32,65,0,0,128,64,0,0,32,65,99,109,0,0,224,65,0,0,96,65,108,
                0,0,128,64,0,0,96,65,98,16,88,57,64,0,0,96,65,0,0,0,64,4,86,110,65,0,0,0,64,0,0,128,65,98,0,0,0,64,
                254,212,136,65,16,88,57,64,0,0,144,65,0,0,128,64,0,0,144,65,108,0,0,224,65,0,0,144,65,98,254,212,232,
                65,0,0,144,65,0,0,240,65,254,212,136,65,0,0,240,65,0,0,128,65,98,0,0,240,65,4,86,110,65,254,212,232,
                65,0,0,96,65,0,0,224,65,0,0,96,65,99,109,0,0,224,65,0,0,176,65,108,0,0,128,64,0,0,176,65,98,16,88,57,
                64,0,0,176,65,0,0,0,64,2,43,183,65,0,0,0,64,0,0,192,65,98,0,0,0,64,254,212,200,65,16,88,57,64,0,0,208,
                65,0,0,128,64,0,0,208,65,108,0,0,224,65,0,0,208,65,98,254,212,232,65,0,0,208,65,0,0,240,65,254,212,
                200,65,0,0,240,65,0,0,192,65,98,0,0,240,65,2,43,183,65,254,212,232,65,0,0,176,65,0,0,224,65,0,0,176,
                65,99,101,0,0 };

        juce::Path p;

        p.loadPathFromData(burgerMenuPathData, sizeof(burgerMenuPathData));

        burgerButton.setShape(p, true, true, false);

        burgerButton.onClick = [this] { showOrHide(); };

        addAndMakeVisible(burgerButton);
    }

    ~BurgerMenuHeader() override
    {
        sidePanel.showOrHide(false);
    }

public:
    void paint(juce::Graphics& g) override
    {
        auto titleBarBackgroundColour =
            getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId).darker();

        g.setColour(titleBarBackgroundColour);

        g.fillRect(getLocalBounds());
    }

    void resized() override
    {
        auto r = getLocalBounds();

        burgerButton.setBounds(r.removeFromRight(40).withSizeKeepingCentre(20, 20));

        titleLabel.setFont(juce::Font(getHeight() * 0.5f, juce::Font::plain));
        titleLabel.setBounds(r);
    }

    void showOrHide()
    {
        sidePanel.showOrHide(!sidePanel.isPanelShowing());
    }

    void setTitleLabel(juce::String& s)
    {
        titleLabel.setText(s, juce::NotificationType::dontSendNotification);
    }

private:
    juce::SidePanel& sidePanel;

    juce::Label titleLabel { "titleLabel" };

    juce::ShapeButton burgerButton 
        { 
            "burgerButton", 
            juce::Colours::lightgrey, 
            juce::Colours::lightgrey, 
            juce::Colours::white 
        };

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(BurgerMenuHeader)
};



//*******************************************************************************

typedef struct
{
    juce::String m_headerName;
    juce::String m_shortName;
    juce::String m_description;

    unsigned int m_flags;

    int m_shortCut;

    bool m_checked;

} MenuInfo_def;


//*******************************************************************************

class MenuInfoManagerCls
{
    typedef struct
    {
        juce::String                          m_shortName;

        unsigned int                    m_CommandID;

        juce::String                          m_description;

        unsigned int                    m_flags;

        int                             m_shortCut;

        bool                            m_checked;

    } SubMenuEntry_def;

    typedef struct
    {
        juce::String                          m_headerName;

        int                             m_menuID;

        std::vector<SubMenuEntry_def>   m_subMenuInfoTable;

    } MenuEntry_def;

    std::vector<MenuEntry_def>          m_menuInfoTable;

    juce::String                              m_menuTitle;

public:
    MenuInfoManagerCls(juce::String title = "") :
        m_menuTitle(title)
    {

    }

    ~MenuInfoManagerCls()
    {
        m_menuInfoTable.clear();
    }

    void setMenuTitle(const juce::String& title)
    {
        m_menuTitle = title;
    }

    bool addMenuHeader(const juce::String& header, int menuID)
    {
        if (header == "")
        {
            return false;
        }

        //* check for dublicate header names or menu IDs

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            if ((*x).m_headerName == header || (*x).m_menuID == menuID)
            {
                return false;
            }
        }

        MenuEntry_def entry;

        entry.m_headerName = header;

        entry.m_menuID = menuID;

        m_menuInfoTable.push_back(entry);

        return true;
    }

    bool addMenu
        (
            const juce::String& header,
            const juce::String& name,
            const unsigned int cmdID, 
            const juce::String& description,
            const int shortCut = 0, 
            const unsigned int flags = 0
        )
    {
        if (header == "" || name == "")
        {
            return false;
        }

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            //* check for dublicate commandID

            for (auto y = (*x).m_subMenuInfoTable.begin(); y != (*x).m_subMenuInfoTable.end(); y++)
            {
                if ((*y).m_CommandID == cmdID)
                    return false;
            }

            if (header == (*x).m_headerName)
            {
                //* Update entry

                for (auto y = (*x).m_subMenuInfoTable.begin(); y != (*x).m_subMenuInfoTable.end(); y++)
                {
                    if (name == (*y).m_shortName)
                    {
                        (*y).m_CommandID = cmdID;
                        (*y).m_description = description;
                        (*y).m_flags = flags;
                        (*y).m_shortCut = shortCut;
                        (*y).m_checked = false;

                        return true;
                    }
                }

                //* Add new sub menu

                SubMenuEntry_def info;

                info.m_CommandID = cmdID;
                info.m_shortName = name;
                info.m_description = description;
                info.m_flags = flags;
                info.m_shortCut = shortCut;
                info.m_checked = false;

                (*x).m_subMenuInfoTable.push_back(info);

                return true;
            }
        }

        //* Add new menu and sub menu

        SubMenuEntry_def info;

        info.m_CommandID = cmdID;
        info.m_shortName = name;
        info.m_description = description;
        info.m_flags = flags;
        info.m_shortCut = shortCut;
        info.m_checked = false;

        MenuEntry_def entry;

        entry.m_headerName = header;

        entry.m_menuID = (int) (m_menuInfoTable.size());

        entry.m_subMenuInfoTable.push_back(info);

        m_menuInfoTable.push_back(entry);

        return true;
    }

    bool setChecked(const juce::String& header, const juce::String& name, bool checked = true)
    {
        if (header == "" || name == "")
        {
            return false;
        }

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            if (header == (*x).m_headerName)
            {
                for (auto y = (*x).m_subMenuInfoTable.begin(); y != (*x).m_subMenuInfoTable.end(); y++)
                {
                    if (name == (*y).m_shortName)
                    {
                        (*y).m_checked = checked;

                        return true;
                    }
                }
            }
        }

        return false;
    }

    juce::String getMenuTitle()
    {
        return m_menuTitle;
    }

    juce::StringArray getMenuHeaders()
    {
        juce::StringArray headers;

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            headers.add((*x).m_headerName);
        }

        return headers;
    }

    int getNumSubMenus(const juce::String& menu)
    {
        int out = -1;

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            if (menu == (*x).m_headerName)
            {
                out = (int) ((*x).m_subMenuInfoTable.size());

                break;
            }
        }

        return out;
    }

    MenuInfo_def getMenuInfo(const juce::String& menu, unsigned int i)
    {
        MenuInfo_def info;

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            if (menu == (*x).m_headerName)
            {
                if ((*x).m_subMenuInfoTable.size() > i)
                {
                    info.m_headerName = (*x).m_headerName;

                    info.m_shortName = (*x).m_subMenuInfoTable[i].m_shortName;
                    info.m_description = (*x).m_subMenuInfoTable[i].m_description;
                    info.m_flags = (*x).m_subMenuInfoTable[i].m_flags;
                    info.m_shortCut = (*x).m_subMenuInfoTable[i].m_shortCut;
                    info.m_checked = (*x).m_subMenuInfoTable[i].m_checked;

                    break;
                }
            }
        }

        return info;
    }

    bool getMenuInfo(const unsigned int cmdID, MenuInfo_def& info)
    {
        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            for (auto y = (*x).m_subMenuInfoTable.begin(); y != (*x).m_subMenuInfoTable.end(); y++)
            {
                if ((*y).m_CommandID == cmdID)
                {
                    info.m_headerName = (*x).m_headerName;

                    info.m_shortName = (*y).m_shortName;
                    info.m_description = (*y).m_description;
                    info.m_flags = (*y).m_flags;
                    info.m_shortCut = (*y).m_shortCut;
                    info.m_checked = (*y).m_checked;

                    return true;
                }
            }
        }

        return false;
    }

    juce::Array<juce::CommandID> getMenuCmdIDs(int menuID)
    {
        juce::Array<juce::CommandID> cmdIDs;

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            if ((*x).m_menuID == menuID)
            {
                for (auto y = (*x).m_subMenuInfoTable.begin(); y != (*x).m_subMenuInfoTable.end(); y++)
                {
                    cmdIDs.addIfNotAlreadyThere((juce::CommandID)(*y).m_CommandID);
                }

                break;
            }
        }

        return cmdIDs;
    }

    juce::PopupMenu getSubMenu(int menuID, juce::ApplicationCommandManager &cmdMgr)
    {
        juce::PopupMenu menu;

        for (auto x = m_menuInfoTable.begin(); x != m_menuInfoTable.end(); x++)
        {
            if ((*x).m_menuID == menuID)
            {
                for (auto y = (*x).m_subMenuInfoTable.begin(); y != (*x).m_subMenuInfoTable.end(); y++)
                {
                    menu.addCommandItem(&cmdMgr, (int) (*y).m_CommandID, (*y).m_shortName);
                }

                break;
            }
        }

        return menu;
    }
};


struct CommandTargetData
{
    juce::ApplicationCommandManager   *m_pCmdManager;

    MenuInfoManagerCls          *m_pMenuInfoMgr;

    CommandTargetData() :
        m_pCmdManager(nullptr),
        m_pMenuInfoMgr(nullptr)
    {
        
    }

    void set(juce::ApplicationCommandManager* c, MenuInfoManagerCls* m)
    {
        m_pCmdManager = c;
        m_pMenuInfoMgr = m; 
    }
};


//*******************************************************************************

class MenuCommandProcessor
{
public:

    MenuCommandProcessor()
    {

    }

    virtual ~MenuCommandProcessor()
    {

    }

    //==============================================================================

    virtual bool menuCommandExec(const juce::ApplicationCommandTarget::InvocationInfo& info) = 0;

};



//*******************************************************************************

class BaseMenuCommandTarget :
    public juce::Component,
    public juce::ApplicationCommandTarget,
    public  juce::MenuBarModel
{
    int                         m_menuID;

    bool                        m_bInitialized;

protected:

public:

    CommandTargetData           m_cmdTargetData;

    BaseMenuCommandTarget() :
        m_bInitialized(false)
    {
    }

    ~BaseMenuCommandTarget() 
    {
    }

    void initCmdTarget(int id, CommandTargetData * data = nullptr)
    {
        m_menuID = id;

        if (data != nullptr)
        {
            m_cmdTargetData.m_pCmdManager = data->m_pCmdManager;
            m_cmdTargetData.m_pMenuInfoMgr = data->m_pMenuInfoMgr;

            m_bInitialized = true;
        }
    }

    void initCmdTarget(int id, juce::ApplicationCommandManager *c, MenuInfoManagerCls *m)
    {
        m_menuID = id;

        m_cmdTargetData.m_pCmdManager = c;
        m_cmdTargetData.m_pMenuInfoMgr = m;

        m_bInitialized = true;

        m_cmdTargetData.m_pCmdManager->registerAllCommandsForTarget(this);
    }

    //==============================================================================

    juce::StringArray getMenuBarNames() override
    {
        juce::StringArray out;

        if (m_bInitialized == true && m_cmdTargetData.m_pMenuInfoMgr != nullptr)
        {
            out = m_cmdTargetData.m_pMenuInfoMgr->getMenuHeaders();
        }

        return out;
    }

    juce::PopupMenu getMenuForIndex(int menuIndex, const juce::String& /*menuName*/) override
    {
        juce::PopupMenu out;

        if (m_bInitialized == true && m_cmdTargetData.m_pMenuInfoMgr != nullptr)
        {
            out = m_cmdTargetData.m_pMenuInfoMgr->getSubMenu(menuIndex, *(m_cmdTargetData.m_pCmdManager));
        }

        return out;
    }

    void menuItemSelected(int /*menuItemID*/, int /*topLevelMenuIndex*/) override
    {

    }

    //==============================================================================

    ApplicationCommandTarget* getNextCommandTarget() override
    {
        // this will return the next parent component that is an ApplicationCommandTarget
        return findFirstTargetParentComponent();
    }

    void getAllCommands(juce::Array<juce::CommandID>& c) override
    {
        juce::Array<juce::CommandID> commands;

        if (m_bInitialized == true)
        {
            commands = m_cmdTargetData.m_pMenuInfoMgr->getMenuCmdIDs(m_menuID);
        }

        c.addArray(commands);

        return;
    }

    void getCommandInfo(juce::CommandID commandID, juce::ApplicationCommandInfo& result) override
    {
        MenuInfo_def info;

        bool bFound = false;

        if (m_bInitialized == true)
        {
            bFound = m_cmdTargetData.m_pMenuInfoMgr->getMenuInfo((int) commandID, info);
        }

        if (bFound == true)
        {
            result.setInfo(info.m_shortName, info.m_description, info.m_headerName, (int) info.m_flags);
            result.setTicked(info.m_checked);
            result.addDefaultKeypress(info.m_shortCut, juce::ModifierKeys::commandModifier | juce::ModifierKeys::shiftModifier);
        }

        return;
    }

    virtual bool perform(const InvocationInfo& info) override = 0;
};



//*******************************************************************************

class MenuCommandManager :
    public  juce::ApplicationCommandTarget,
    public  juce::MenuBarModel,
    public  juce::MouseListener,
    public  MenuCommandProcessor
{
    //bool                                        m_bInitialized;

    //SafeSharedPtr<BaseMenuCommandTarget>        m_menuCmdTarget;
    SimplePtr<BaseMenuCommandTarget>              m_menuCmdTarget;

protected:

public:

    MenuCommandManager() :
        //m_bInitialized(false),
        m_menuCmdTarget(nullptr)
    {

    }

    ~MenuCommandManager()
    {
        if (m_menuCmdTarget != nullptr)
        {
            //m_menuCmdTarget.reset();
        }
    }

    //void setCmdTarget(SafeSharedPtr<BaseMenuCommandTarget> p)
    void setCmdTarget(BaseMenuCommandTarget *p)
    {
        m_menuCmdTarget = p;
    }

    //==============================================================================

    juce::StringArray getMenuBarNames() override
    {
        juce::StringArray out;

        if (m_menuCmdTarget != nullptr)
        {
            out = m_menuCmdTarget->getMenuBarNames();
        }

        return out;
    }

    juce::PopupMenu getMenuForIndex(int menuIndex, const juce::String & menuName) override
    {
        juce::PopupMenu out;

        if (m_menuCmdTarget != nullptr)
        {
            out = m_menuCmdTarget->getMenuForIndex(menuIndex, menuName);
        }

        return out;
    }

    void menuItemSelected(int menuItemID, int topLevelMenuIndex) override
    {
        if (m_menuCmdTarget != nullptr)
        {
             m_menuCmdTarget->menuItemSelected(menuItemID, topLevelMenuIndex);
        }
    }

    //==============================================================================

    ApplicationCommandTarget* getNextCommandTarget() override
    {
        ApplicationCommandTarget* out = nullptr;

        if (m_menuCmdTarget != nullptr)
        {
            out = m_menuCmdTarget->getNextCommandTarget();
        }

        return out;
    }

    void getAllCommands(juce::Array<juce::CommandID>& c) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->getAllCommands(c);
        }
    }

    void getCommandInfo(juce::CommandID commandID, juce::ApplicationCommandInfo& result) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->getCommandInfo(commandID, result);
        }
    }

    virtual bool perform(const InvocationInfo& info) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            return m_menuCmdTarget->perform(info);
        }

        return false;
    }

    //==============================================================================

    virtual void mouseMove(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseMove(event);
        }
    }

    virtual void mouseEnter(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseEnter(event);
        }
    }

    virtual void mouseExit(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseExit(event);
        }
    }

    virtual void mouseDown(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseDown(event);
        }
    }

    virtual void mouseDrag(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseDrag(event);
        }
    }

    virtual void mouseUp(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseUp(event);
        }
    }

    virtual void mouseDoubleClick(const juce::MouseEvent& event) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseDoubleClick(event);
        }
    }

    virtual void mouseWheelMove(const juce::MouseEvent& event, const juce::MouseWheelDetails& wheel) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseWheelMove(event, wheel);
        }
    }

    virtual void mouseMagnify(const juce::MouseEvent& event, float scaleFactor) override
    {
        if (m_menuCmdTarget != nullptr)
        {
            m_menuCmdTarget->mouseMagnify(event, scaleFactor);
        }
    }

};



//*******************************************************************************

class RootMenuCommandTarget :
    public BaseMenuCommandTarget
{
    MenuCommandManager                      &m_parent;

    MenuInfoManagerCls                      m_menuMgr;

    bool                                    m_bMenuCreated;

protected:

    //==============================================================================
    //* Represents the possible menu positions. 

    enum class MenuBarPosition
    {
        window,
        global,
        burger
    };

    //==============================================================================
    //* Menu variables

    juce::ApplicationCommandManager                 m_commandManager;

    //juce::Component::SafePointer<MenuBarComponent> m_menuBar;
    std::unique_ptr<juce::MenuBarComponent>         m_menuBar;

    MenuBarPosition                                 m_menuBarPosition;

    juce::BurgerMenuComponent                       m_burgerMenu;

    juce::SidePanel                                 m_sidePanel { "Menu", 300, false };

    BurgerMenuHeader                                m_menuHeader { m_sidePanel };

    unsigned long                                   m_menuBarHeight;


public:

    RootMenuCommandTarget(MenuCommandManager& parent) :
        m_parent(parent),
        m_bMenuCreated(false),
#if JUCE_MAC
        m_menuBarPosition(MenuBarPosition::global)
#else
#if (JUCE_ANDROID || JUCE_IOS)
        m_menuBarPosition(MenuBarPosition::burger)
#else        
        m_menuBarPosition(MenuBarPosition::window)
#endif
#endif
    {
        setMenuType();  //* use default menu type

        //initCmdTarget(menuFile, &m_commandManager, &m_menuMgr);
    }

    ~RootMenuCommandTarget()
    {
        if (m_bMenuCreated == true)
        {
            //m_parent(m_commandManager.getKeyMappings());
        }
    }

    void setMenuType(int type = -1)
    {
        //* type -1 = default menu type

        if (type >= 0)
        {
            m_menuBarPosition = (MenuBarPosition)type;
        }

        //* calculate menu bar height

        if (m_menuBarPosition == MenuBarPosition::global)
        {
            m_menuBarHeight = 0;
        }
        else if (m_menuBarPosition == MenuBarPosition::window)
        {
            m_menuBarHeight = (unsigned long)
                juce::LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight();
        }
        else
        {
            m_menuBarHeight = 40;
        }
    }

    bool createMenuBar(Component& parent)
    {
        //m_menuBar = new MenuBarComponent(this);
        m_menuBar.reset(new juce::MenuBarComponent(this));

        if (m_menuBar != nullptr)
        {
            parent.addAndMakeVisible(m_menuBar.get());
        }

        setApplicationCommandManagerToWatch(&m_commandManager);

        m_commandManager.registerAllCommandsForTarget(this);

        parent.addKeyListener(m_commandManager.getKeyMappings());

        addKeyListener(m_commandManager.getKeyMappings());

#if JUCE_MAC
        MenuBarModel::setMacMainMenu(m_menuBarPosition == MenuBarPosition::global ? this : nullptr);
#endif
        m_bMenuCreated = true;

        return true;
    }

    void resizeMenu(juce::Component& parent)
    {
        auto r = parent.getLocalBounds();

        if (m_menuBarPosition == MenuBarPosition::window)
        {
            m_menuBar->setBounds(r.removeFromTop((int) m_menuBarHeight));
        }
        else if (m_menuBarPosition == MenuBarPosition::burger)
        {
            m_menuHeader.setBounds(r.removeFromTop(40));
        }
    }

    //==============================================================================

    BurgerMenuHeader* getMenuHeader()
    {
        return &m_menuHeader;
    }

    juce::SidePanel* getSizePannel()
    {
        return &m_sidePanel;
    }

    unsigned long getMenuBarHeight()
    {
        return m_menuBarHeight;
    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(RootMenuCommandTarget)
};

