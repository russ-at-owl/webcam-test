/*
  ==============================================================================

    AppCfgEditor.cpp
    Created: 31 Jul 2020 11:32:08am
    Author:  RussB

  ==============================================================================
*/


#include "AppInc.h"


void AppCfgEditorComponent::buttonClicked(juce::Button* b)
{
    if (b == nullptr)
        return;

    if (b == m_pUpdate.get())
    {
        m_nVideoDevNum = (m_devList->getSelectedRow() - 1);
            
        if (m_pCfgMgr != nullptr)
        {
            m_pCfgMgr->m_nSelectedWebcamDev = m_nVideoDevNum;

            if (m_pPanoCheckbox->getToggleState() == true)
                m_pCfgMgr->m_videoFormatSelect = eVideoFormatSelect::VideoFormat_4320x720;
            else
                m_pCfgMgr->m_videoFormatSelect = eVideoFormatSelect::VideoFormat_any;
        }

        m_notify.notifyParent(1, this);

        return;
    }

    if (b == m_pCancel.get())
    {
        m_notify.notifyParent(0, this);

        return;
    }

    //if (b == m_pButon.get())
    //{


    //    return;
    //}
}


#if 0
void AppCfgEditorComponent::textEditorTextChanged(TextEditor& e)
{


}
#endif
