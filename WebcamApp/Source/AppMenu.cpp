/*
  ==============================================================================

    AppMenu.cpp
    Created: 17 Jul 2020 5:17:32pm
    Author:  RussB

  ==============================================================================
*/

#include "AppInc.h"


//==============================================================================



bool MainMenuCommandTarget::createMenuComponent(juce::Component &parent)
{
    m_helpCommandTarget = new HelpMenuCommandTarget(m_parent, &m_commandManager, &m_menuMgr);

    if (m_helpCommandTarget != nullptr)
    {
        m_helpCommandTarget->createMenuComponent();

        addAndMakeVisible(m_helpCommandTarget);

        parent.addAndMakeVisible(m_helpCommandTarget);

        //Component* pSubMenu = m_helpCommandTarget->getChildComponent();
        //ApplicationCommandTarget* pSubMenu = m_helpCommandTarget->getNextCommandTarget();

        //if (pSubMenu != nullptr)
        //{
        //    addAndMakeVisible(pSubMenu);

        //    parent.addAndMakeVisible(pSubMenu);
        //}
    }

    m_menuBar = new juce::MenuBarComponent(this);

    if (m_menuBar != nullptr)
    {
        parent.addAndMakeVisible(m_menuBar);
    }

    setApplicationCommandManagerToWatch(&m_commandManager);

    m_commandManager.registerAllCommandsForTarget(this);

    parent.addKeyListener(m_commandManager.getKeyMappings());

    addKeyListener(m_commandManager.getKeyMappings());

    //resizeMenu(parent);

#if JUCE_MAC
    MenuBarModel::setMacMainMenu(m_menuBarPosition == MenuBarPosition::global ? this : nullptr);
#endif

    m_bMenuCreated = true;

    return true;
}


void MainMenuCommandTarget::releaseMenuComponent(Component& parent)
{
    if (m_bMenuCreated == true)
    {
        removeKeyListener(m_commandManager.getKeyMappings());

        parent.removeKeyListener(m_commandManager.getKeyMappings());

        m_bMenuCreated = false;
    }
}


void MainMenuCommandTarget::resizeMenu(Component& parent)
{
//#if !JUCE_MAC
    auto r = parent.getLocalBounds();

    if (m_menuBarPosition == MenuBarPosition::window)
    {
        m_menuBar->setBounds(r.removeFromTop(m_menuBarHeight));
    }
    else if (m_menuBarPosition == MenuBarPosition::burger)
    {
        m_menuHeader.setBounds(r.removeFromTop(40));
    }
//#endif
}






