//#include "MainComponent.h"

#include "AppInc.h"


//==============================================================================
MainComponent::MainComponent() :
    m_bInitialized(false),
    m_pMainMenuTarget(nullptr),
    m_pCameraDevice(nullptr),
    m_menuBarHeight(0),
    m_nVideoDevNum(-1)
{
    setSize (640, 480);

    //************************************

    //m_appCfg.getConfigFromFile();

    //************************************

    m_pMainMenuTarget.reset(new MainMenuCommandTarget(*this));

    if (m_pMainMenuTarget != nullptr)
    {
        setCmdTarget(m_pMainMenuTarget.get());

        m_pMainMenuTarget->createMenuComponent(*this);

        addAndMakeVisible(m_pMainMenuTarget.get());

        m_menuBarHeight = m_pMainMenuTarget->getMenuBarHeight();

        setSize(640, (480 + m_menuBarHeight));

        m_pMainMenuTarget->resizeMenu(*this);
    }

    if (m_appCfg.m_nSelectedWebcamDev >= 0)
        m_nVideoDevNum = m_appCfg.m_nSelectedWebcamDev;
    //else
    //    m_nVideoDevNum = 0;

    if (m_nVideoDevNum >= 0)
    {
        auto pCamDev = createWebcamDevice(m_nVideoDevNum, m_appCfg.m_videoFormatSelect);

        m_pCameraDevice.reset(pCamDev);

        if (m_pCameraDevice != nullptr)
        {
            auto pCamViewer = m_pCameraDevice->createViewerComponent();

            if (pCamViewer != nullptr)
            {
                addAndMakeVisible(pCamViewer);

                int nWindowWidth = getWidth();
                int nWindowHeight = getHeight();

                pCamViewer->setBounds(0, m_menuBarHeight, nWindowWidth, nWindowHeight - m_menuBarHeight);
            }

            m_pCameraDevice->addListener((juce::CameraDevice::Listener *) &m_videoFrameHandler);

            m_videoFrameHandler.setActive(true);
        }
    }

    m_bInitialized = true;
}

MainComponent::~MainComponent()
{
    juce::LookAndFeel::setDefaultLookAndFeel(nullptr);

    if (m_pMainMenuTarget != nullptr)
    {
        m_pMainMenuTarget->releaseMenuComponent(*this);
    }

    if (m_pCameraDevice != nullptr)
    {
        m_videoFrameHandler.setActive(false);

        m_pCameraDevice->stopRecording();
            
        m_pCameraDevice->removeListener((juce::CameraDevice::Listener*)&m_videoFrameHandler);
    }
}


VideoImageHandler   MainComponent::m_videoFrameHandler;


//==============================================================================
void MainComponent::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    g.setFont (juce::Font (16.0f));
    g.setColour (juce::Colours::white);

}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.

    if (m_pMainMenuTarget != nullptr)
    {
        m_pMainMenuTarget->resizeMenu(*this);
    }


}


void MainComponent::releaseResources()
{
    // This will be called when the audio device stops, or when it is being
    // restarted due to a setting change.

    // For more details, see the help for AudioProcessor::releaseResources()

}


void MainComponent::componentNotifyCallback(int val, juce::Component* comp)
{
    if (comp == nullptr)
        return;

    auto pWinMgr = ::getAppWindowMgr();

    if (pWinMgr == nullptr)
        return;

    auto pCfgEditor = pWinMgr->getWindow("AppConfigEditor");

    if (comp == pCfgEditor)
    {
        switch (val)
        {
        case ComponentNotificationType_UpdateAndClose:
        case ComponentNotificationType_UpdateControl:
            {
                setMainWindowTitle();

                if (m_pCameraDevice != nullptr)
                {
                    m_videoFrameHandler.setActive(false);

                    m_pCameraDevice->removeListener((juce::CameraDevice::Listener*) &m_videoFrameHandler);

                    m_pCameraDevice->stopRecording();

                    removeChildComponent((Component *) m_pCameraDevice.get());

                    m_pCameraDevice.release();
                }

                m_nVideoDevNum = m_appCfg.m_nSelectedWebcamDev;

                if (m_nVideoDevNum >= 0)
                {
                    auto pCamDev = createWebcamDevice(m_nVideoDevNum, m_appCfg.m_videoFormatSelect);

                    m_pCameraDevice.reset(pCamDev);

                    if (m_pCameraDevice != nullptr)
                    {
                        setMainWindowTitle();

                        auto pCamViewer = m_pCameraDevice->createViewerComponent();

                        if (pCamViewer != nullptr)
                        {
                            addAndMakeVisible(pCamViewer);

                            int nWindowWidth = getWidth();
                            int nWindowHeight = getHeight();

                            pCamViewer->setBounds(0, m_menuBarHeight, nWindowWidth, nWindowHeight - m_menuBarHeight);
                        }

                        m_pCameraDevice->addListener((juce::CameraDevice::Listener*) &m_videoFrameHandler);

                        m_videoFrameHandler.setActive(true);
                    }
            }
        }
            return;

        case ComponentNotificationType_Close:
        case ComponentNotificationType_Abort:
            return;
        }

        return;
    }

    //if (comp == (Component*) m_pSysInfoTimer.get())
    //{
    //    m_cpuLoad = m_appCfg.getCpuLoad();

    //    //if (m_pCpuUsage != nullptr)
    //    //{
    //    //    m_pCpuUsage->repaint();
    //    //}

    //    return;
    //}

    return;
}

