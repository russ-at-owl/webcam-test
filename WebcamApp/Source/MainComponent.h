#pragma once

#include "AppInc.h"

class VideoImageHandler :
    juce::CameraDevice::Listener
{
    bool        m_bActive;

public:

    VideoImageHandler() :
        m_bActive(false)
    {

    }

    void setActive(bool bState = false)
    {
        m_bActive = bState;
    }

    void imageReceived(const juce::Image& image)
    {
        if (m_bActive == true)
        {
            auto format = image.getFormat();

            auto props = image.getProperties();

            auto data = image.getPixelData();

            auto w = image.getWidth();

            auto h = image.getHeight();
        }
    }

};


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent  : 
    public  juce::Component,
    public  MenuCommandManager,
    public  juce::Button::Listener,
    public  ComponentNotificationCls,
    private juce::ChangeListener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent() override;

    //==============================================================================
    // Private member variables go here...

    bool                                        m_bInitialized;

    //AppLookAndFeelCls                         m_LAF;

    SafeUniquePtr<MainMenuCommandTarget>        m_pMainMenuTarget;

    unsigned long                               m_menuBarHeight;

    SafeUniquePtr<juce::CameraDevice>           m_pCameraDevice;

    int                                         m_nVideoDevNum;

    AppConfigMgr                                m_appCfg;

    juce::File                                  m_configFile;

    static VideoImageHandler                    m_videoFrameHandler;

    
    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

    void releaseResources();     //override;

    //==============================================================================

    //bool createComponent();

    bool menuCommandExec(const InvocationInfo& info) override
    {
        switch (info.commandID)
        {
        case CommandIDs::menuFileLoad:
            //loadConfigFile();
            break;

        case CommandIDs::menuFileSave:
            //saveConfigFile();
            break;

        case CommandIDs::menuFileConfig:
            openAppConfig();
            break;

        case CommandIDs::menuFileExit:
            quitApp();
            break;

        case CommandIDs::menuHelpAbout:
            openAbout();
            break;

        case CommandIDs::menuHelpOpen:
            openHelp();
            break;

        default:
            return false;
        }

        return true;
    }

    void buttonClicked(juce::Button* b)  override
    {
        if (b == nullptr)
            return;

    }

    //==============================================================================

    juce::ApplicationProperties* getAppProperties()
    {
        return m_appCfg.getAppProperties();
    }

    juce::PropertySet* getCurrProperties()
    {
        return m_appCfg.getCurrProperties();
    }

    //==============================================================================

    void componentNotifyCallback(int val, juce::Component* comp) override;

    void changeListenerCallback(juce::ChangeBroadcaster*) override
    {

    }

    //==============================================================================

    AppConfigMgr* getCfgMgr();

    void loadConfigFile()
    {
        //* choose config file to load info from

        juce::File inputFile = m_configFile;

        juce::FileChooser fileChooser
        (
            "Select an application configuration File to load",
            inputFile,
            "*.settings",
            true,
            false
        );


        bool status = fileChooser.browseForFileToOpen();

        if (status == false)
            return;

        inputFile = fileChooser.getResult();

        if (inputFile.getFileName() == "")
            return;

        m_configFile = inputFile;

        m_appCfg.setConfigFile(inputFile);

        //* load config info from file

        m_appCfg.getConfigFromFile();

        //* update current app configuration

#ifdef DumpConfig
        String dumpFile = XmlDumpPath;
        dumpFile += "DumpFile-AtAppStartup.xml";
        m_appCfg.dumpUserPropertiesToFile(dumpFile);
#endif

    }

    void saveConfigFile()
    {
        //* choose file to save info to

        juce::File outputFile = m_configFile;

        juce::FileChooser fileChooser
        (
            "Select an output Mixer Configuration File",
            outputFile,
            "*.settings",
            true,
            false
        );

        bool status = fileChooser.browseForFileToSave(true);

        if (status == false)
            return;

        outputFile = fileChooser.getResult();

        if (outputFile.getFileName() == "")
            return;

        m_configFile = outputFile;

        m_appCfg.setConfigFile(outputFile);

        m_appCfg.saveConfigToFile();
    }

    void openAppConfig()
    {
        auto pWinMgr = ::getAppWindowMgr();

        if (pWinMgr == nullptr)
            return;

        pWinMgr->addWindow(new AppCfgEditorWin(this, &m_appCfg, this), "AppConfigEditor");

        auto pCfgEditor = pWinMgr->getWindow("AppConfigEditor");

        if (pCfgEditor == nullptr)
            return;

        int status = pCfgEditor->runModalLoop();

        pWinMgr->closeWindow(pCfgEditor);
    }

    void openAbout()
    {
        juce::String aboutTitle = "";
        juce::String aboutText = "";

        aboutTitle = ("\n");
        aboutTitle += AboutAppTitleHeader;
        aboutTitle += (" ");
        aboutTitle += AppName;

        aboutText = AboutAppMsgHeader;
        aboutText += (" ");
        aboutText += AppName;
        aboutText += ("\n");
        aboutText += AboutAppVerHeader;
        aboutText += (" ");
        aboutText += AppVersion;
        aboutText += ("\n");
        aboutText += AboutCompanyHeader;
        aboutText += (" ");
        aboutText += AppCompanyName;
        aboutText += ("\n");
        aboutText += AboutCopyrightText;
        aboutText += ("\n");
        aboutText += ("\n");
        aboutText += AboutCompanyURL;

        juce::AlertWindow::showMessageBox(juce::AlertWindow::AlertIconType::NoIcon, aboutTitle, aboutText, "OK", nullptr);

    }

    void openHelp()
    {

    }

    juce::CameraDevice* createWebcamDevice(int nDevNum, eVideoFormatSelect eFormat)
    {
        juce::CameraDevice* pOut = nullptr;

        if (nDevNum < 0)
            return pOut;

        switch (eFormat)
        {
        case eVideoFormatSelect::VideoFormat_any:
            pOut = juce::CameraDevice::openDevice(m_nVideoDevNum, 320, 240, 4320, 1080, true);
            break;

        case eVideoFormatSelect::VideoFormat_640x480:
            pOut = juce::CameraDevice::openDevice(m_nVideoDevNum, 640, 480, 640, 480, true);
            break;

        case eVideoFormatSelect::VideoFormat_1280x720:
            pOut = juce::CameraDevice::openDevice(m_nVideoDevNum, 1280, 720, 1280, 720, true);
            break;

        case eVideoFormatSelect::VideoFormat_1920x1080:
            pOut = juce::CameraDevice::openDevice(m_nVideoDevNum, 1920, 1080, 1920, 1080, true);
            break;

        case eVideoFormatSelect::VideoFormat_4320x720:
            pOut = juce::CameraDevice::openDevice(m_nVideoDevNum, 4320, 720, 4320, 720, true);
            break;
        }

        return pOut;
    }

private:
    //==============================================================================
    // Your private member variables go here... 


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
