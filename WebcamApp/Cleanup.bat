@echo off
echo Deleting un-needed compiler files.
echo .
del mt.dep  /s /f 
del *.obj /s /f 
del *.res /s /f 
del *.ilk /s /f 
del *.pdb /s /f 
del *.idb /s /f
del *.sbr /s /f
del *.bsc /s /f
del *.ncb /s /f 
del *.suo /s /f 
del *.APS /s /f 
del *.user /s /f 
del *.exe.embed.manifest /s /f 
del *.exe.intermediate.manifest /s /f 
del *.bak /s /f 
del *.tlog /s /f 
del RCa0* /s /f 
del ipch /s /f 
del *.ipch /s /f 
del *.orig /s /f 
del *.o /s /f

del ".vs\*.*" /s /f /a:RHIS
rd ".vs" /s /q 

echo .
echo Delete opertion completed.
pause

